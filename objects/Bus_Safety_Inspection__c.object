<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Bus_Safety_Inspection_Record_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <content>Bus_Safety_Inspection_Record_Page</content>
        <formFactor>Small</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Annual inspection and test of a bus belonging to an operator. Conducted by a licensed bus tester</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>true</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fields>
        <fullName>Address_at_which_Test_Conducted__c</fullName>
        <externalId>false</externalId>
        <label>Address at which Test Conducted</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Body_and_Chassis_Notes__c</fullName>
        <externalId>false</externalId>
        <label>Body and Chassis Notes</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Body_and_Chassis__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Body and Chassis 1st test as per VicRoads Certificate of Roadworthiness</description>
        <externalId>false</externalId>
        <label>Body and Chassis</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Brake_Performance_Notes__c</fullName>
        <externalId>false</externalId>
        <label>Brake Performance Notes</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Brake_Performance__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Brake Performance 1st test as per VicRoads Certificate of Roadworthiness</description>
        <externalId>false</externalId>
        <label>Brake Performance</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Brakes_Notes__c</fullName>
        <externalId>false</externalId>
        <label>Brakes Notes</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Brakes__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Brakes 1st test as per VicRoads Certificate of Roadworthiness</description>
        <externalId>false</externalId>
        <label>Brakes</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Bus_Operator__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Bus Operator</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Bus Safety Inspections</relationshipLabel>
        <relationshipName>Bus_Safety_Inspections</relationshipName>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Bus__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Lookup to custom Asset object (Asset__c)</description>
        <externalId>false</externalId>
        <label>Bus</label>
        <referenceTo>Asset__c</referenceTo>
        <relationshipLabel>Bus Safety Inspections</relationshipLabel>
        <relationshipName>Bus_Safety_Inspections</relationshipName>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Critical_Defect_Cleared__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <inlineHelpText>Tick once critical defect has been cleared</inlineHelpText>
        <label>Critical Defect Cleared</label>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Date_of_First_Test__c</fullName>
        <defaultValue>TODAY()</defaultValue>
        <description>Date that the initial bus safety inspection was conducted</description>
        <externalId>false</externalId>
        <label>Date of First Test</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Date_of_Second_Test__c</fullName>
        <externalId>false</externalId>
        <label>Date of Second Test</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Engine_and_Driveline_Notes__c</fullName>
        <externalId>false</externalId>
        <label>Engine and Driveline Notes</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Engine_and_Driveline__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Engine and Driveline 1st test as per VicRoads Certificate of Roadworthiness</description>
        <externalId>false</externalId>
        <label>Engine and Driveline</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Exhaust_and_Emission_Controls__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Exhaust and Emission Controls 1st test as per VicRoads Certificate of Roadworthiness</description>
        <externalId>false</externalId>
        <label>Exhaust and Emission Controls</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Exhaust_and_Emission_Notes__c</fullName>
        <externalId>false</externalId>
        <label>Exhaust and Emission Notes</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Inspection_Outcome__c</fullName>
        <externalId>false</externalId>
        <formula>IF(OR( Wheels_and_Tyres__c , Brakes__c, Steering_and_Suspension__c, Brake_Performance__c , Parking_Brake__c , Seats_and_Seat_Belts__c , Lamps_Signals_Reflectors_etc__c , Exhaust_and_Emission_Controls__c , Windscreen_and_Windows__c , Windscreen_Wipers_Washers_etc__c , Body_and_Chassis__c , Engine_and_Driveline__c , Other_Items__c , Modifications__c ), &quot;Fail&quot;, &quot;Pass&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Inspection Outcome</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Is_the_Bus_Compliant__c</fullName>
        <externalId>false</externalId>
        <label>Critical Defect?</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>No Critical Defect</fullName>
                    <default>false</default>
                    <label>No Critical Defect</label>
                </value>
                <value>
                    <fullName>Critical Defect Raised</fullName>
                    <default>false</default>
                    <label>Critical Defect Raised</label>
                </value>
                <value>
                    <fullName>Critical Defect Cleared</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>Critical Defect Cleared</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Lamps_Signals_Reflectors_Notes__c</fullName>
        <externalId>false</externalId>
        <label>Lamps, Signals, Reflectors Notes</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Lamps_Signals_Reflectors_etc__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Lamps, Signals, Reflectors etc 1st test as per VicRoads Certificate of Roadworthiness</description>
        <externalId>false</externalId>
        <label>Lamps, Signals, Reflectors etc</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Legacy_Inspection_ID__c</fullName>
        <externalId>true</externalId>
        <label>Legacy Inspection ID</label>
        <length>30</length>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Licenced_Bus_Tester__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>The Licenced Bus Tester that conducted the inspection</description>
        <externalId>false</externalId>
        <label>Licenced Bus Tester</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Account.RecordTypeId</field>
                <operation>equals</operation>
                <value>Bus Tester</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Bus Safety Inspections (Licenced Bus Tester)</relationshipLabel>
        <relationshipName>Bus_Safety_Inspections1</relationshipName>
        <required>true</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>List_Defects_Found__c</fullName>
        <description>List all defects found as part of the test</description>
        <externalId>false</externalId>
        <label>List Defects Found</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Modifications_Notes__c</fullName>
        <externalId>false</externalId>
        <label>Modifications Notes</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Modifications__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Modifications 1st test as per VicRoads Certificate of Roadworthiness</description>
        <externalId>false</externalId>
        <label>Modifications</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Odometer_Reading__c</fullName>
        <description>Bus odometer reading at time of first test</description>
        <externalId>false</externalId>
        <label>Odometer Reading</label>
        <precision>10</precision>
        <required>false</required>
        <scale>0</scale>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Other_Items_Notes__c</fullName>
        <externalId>false</externalId>
        <label>Other Items Notes</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Other_Items__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Other Items 1st test as per VicRoads Certificate of Roadworthiness</description>
        <externalId>false</externalId>
        <label>Other Items</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Parking_Brake_Notes__c</fullName>
        <externalId>false</externalId>
        <label>Parking Brake Notes</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Parking_Brake__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Parking Brake 1st test as per VicRoads Certificate of Roadworthiness</description>
        <externalId>false</externalId>
        <label>Parking Brake</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Presenter_Address__c</fullName>
        <description>Address of person bringing bus for inspection</description>
        <externalId>false</externalId>
        <label>Presenter Address</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Presenter_Name__c</fullName>
        <description>Name of person bringing bus for inspection</description>
        <externalId>false</externalId>
        <label>Presenter Name</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Presenter_Phone_Number__c</fullName>
        <externalId>false</externalId>
        <label>Presenter Phone Number</label>
        <length>20</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Seats_and_Seat_Belts_Notes__c</fullName>
        <externalId>false</externalId>
        <label>Seats and Seat Belts Notes</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Seats_and_Seat_Belts__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Seats and Seat Belts 1st test as per VicRoads Certificate of Roadworthiness</description>
        <externalId>false</externalId>
        <label>Seats and Seat Belts</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Service_Brake_Notes__c</fullName>
        <externalId>false</externalId>
        <label>Service Brake % Notes</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Service_Brake__c</fullName>
        <description>Service Brake % 1st test as per VicRoads Certificate of Roadworthiness</description>
        <externalId>false</externalId>
        <label>Service Brake %</label>
        <precision>6</precision>
        <required>false</required>
        <scale>2</scale>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Steering_and_Suspension_Notes__c</fullName>
        <externalId>false</externalId>
        <label>Steering and Suspension Notes</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Steering_and_Suspension__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Steering and Suspension 1st test as per VicRoads Certificate of Roadworthiness</description>
        <externalId>false</externalId>
        <label>Steering and Suspension</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Tester_Licence_Number__c</fullName>
        <externalId>false</externalId>
        <label>Tester Licence Number</label>
        <length>20</length>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Tester_Name__c</fullName>
        <externalId>false</externalId>
        <label>Tester Name</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Tester_Phone_Number__c</fullName>
        <externalId>false</externalId>
        <label>Tester Phone Number</label>
        <length>20</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>VIN__c</fullName>
        <description>Asset VIN</description>
        <externalId>false</externalId>
        <formula>Bus__r.VIN__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>VIN</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Vehicle_Colour__c</fullName>
        <externalId>false</externalId>
        <label>Vehicle Colour</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Vehicle_Engine_Number__c</fullName>
        <externalId>false</externalId>
        <label>Vehicle Engine Number</label>
        <length>10</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Vehicle_Make__c</fullName>
        <description>Vehicle Manufacturer</description>
        <externalId>false</externalId>
        <label>Vehicle Make</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Vehicle_Model__c</fullName>
        <description>Model Name</description>
        <externalId>false</externalId>
        <label>Vehicle Model</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Vehicle_Registration_Number__c</fullName>
        <externalId>false</externalId>
        <label>Vehicle Registration Number</label>
        <length>20</length>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Vehicle_Type__c</fullName>
        <description>Type on VicRoads form</description>
        <externalId>false</externalId>
        <label>Vehicle Type</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Vehicle_Year__c</fullName>
        <description>Year of Manufacture</description>
        <externalId>false</externalId>
        <label>Vehicle Year</label>
        <length>4</length>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>VicRoads_Serial_Number__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>Unique ID for VicRoads Certificate of Roadworthiness document (Bus Safety Inspection)</description>
        <externalId>true</externalId>
        <label>VicRoads Serial Number</label>
        <length>20</length>
        <required>true</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Washers_Notes__c</fullName>
        <externalId>false</externalId>
        <label>Washers Notes</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Wheels_and_Tyres_Notes__c</fullName>
        <externalId>false</externalId>
        <label>Wheels and Tyres Notes</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Wheels_and_Tyres__c</fullName>
        <defaultValue>false</defaultValue>
        <description>1st test as per VicRoads Certificate of Roadworthiness</description>
        <externalId>false</externalId>
        <label>Wheels and Tyres</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Windscreen_Wipers_Washers_etc__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Windscreen Wipers, Washers, etc 1st test as per VicRoads Certificate of Roadworthiness 1st test as per VicRoads Certificate of Roadworthiness</description>
        <externalId>false</externalId>
        <label>Windscreen Wipers, Washers, etc</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Windscreen_and_Windows_Notes__c</fullName>
        <externalId>false</externalId>
        <label>Windscreen and Windows Notes</label>
        <length>255</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Windscreen_and_Windows__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Windscreen and Windows 1st test as per VicRoads Certificate of Roadworthiness</description>
        <externalId>false</externalId>
        <label>Windscreen and Windows</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Bus Safety Inspection</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Bus__c</columns>
        <columns>VIN__c</columns>
        <columns>Presenter_Name__c</columns>
        <columns>Date_of_First_Test__c</columns>
        <columns>CREATEDBY_USER.ALIAS</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>INSP-{0000000}</displayFormat>
        <label>Bus Safety Inspection Number</label>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Bus Safety Inspections</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Licenced_Bus_Tester__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Date_of_First_Test__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Is_the_Bus_Compliant__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Critical_Defect_Cleared__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Bus__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Legacy_Inspection_ID__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>VIN__c</customTabListAdditionalFields>
        <searchResultsAdditionalFields>Licenced_Bus_Tester__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Date_of_First_Test__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Bus__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Is_the_Bus_Compliant__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Critical_Defect_Cleared__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Legacy_Inspection_ID__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>VIN__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>Private</sharingModel>
    <visibility>Public</visibility>
</CustomObject>