trigger ContentTrigger on Content__c  (
    after delete,
    after insert,
    after undelete,
    after update,
    before delete,
    before insert,
    before update) {
	/**
	 * Singleton Dispatcher for Waterway_Role__c
	 * @param Waterway Role Handler Class
	 * @return   NA
	 */

    //get trigger settings
    TriggerSettingsClass contentTrigger = TriggerSettingsClass.getTriggerSetting('ContentTrigger');
    if(contentTrigger.isTriggerEnabled()){
        TriggerDispatcher.run(new ContentHandler());
    }
}