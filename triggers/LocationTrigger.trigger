trigger LocationTrigger on Location__c (
    after delete,
    after insert,
    after undelete,
    after update,
    before delete,
    before insert,
    before update) {
    /**
     * Singleton Dispatcher for Location__c
     * @param Location Handler Class
     * @return   NA
     */

    //get trigger settings
    TriggerSettingsClass locationTrigger = TriggerSettingsClass.getTriggerSetting('LocationTrigger');
    if(locationTrigger.isTriggerEnabled()){
        TriggerDispatcher.run(new LocationHandler()); 
    }
}