trigger AccountContactRelationTrigger on AccountContactRelation (
    after delete,
    after insert,
    after undelete,
    after update,
    before delete,
    before insert,
    before update) {
    
    /**
     * Singleton Dispatcher for AccountContactRelation 
     * @param AccountContactRelation Handler Class
     * @return   NA
     */

    //get trigger settings
    TriggerSettingsClass AccountContactRelationTrigger = TriggerSettingsClass.getTriggerSetting('AccountContactRelationTrigger');
    if(AccountContactRelationTrigger.isTriggerEnabled()){
        TriggerDispatcher.run(new AccountContactRelationHandler());
    }

}