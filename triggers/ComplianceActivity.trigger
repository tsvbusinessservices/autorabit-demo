trigger ComplianceActivity on Compliance_Activity__c (
    after delete,
    after insert,
    after undelete,
    after update,
    before delete,
    before insert,
    before update) {
	/**
	 * Singleton Dispatcher for Compliance Activity 
	 * @param Compliance Activity  Handler Class
	 * @return   NA
	 */

    //get trigger settings
    TriggerSettingsClass complianceActivityTrigger = TriggerSettingsClass.getTriggerSetting('ComplianceActivityTrigger');
    if(complianceActivityTrigger.isTriggerEnabled()){
       TriggerDispatcher.run(new ComplianceActivityHandler());
       TriggerDispatcher.run(new ComplianceActivitySharingHandler());
    }

    TriggerSettingsClass WaterwayAuditTrigger = TriggerSettingsClass.getTriggerSetting('WaterwayAuditTrigger');
    if(WaterwayAuditTrigger.isTriggerEnabled()) {
        TriggerDispatcher.run(new WaterwayAuditHandler());
    }
}