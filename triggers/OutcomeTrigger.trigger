trigger OutcomeTrigger on Outcome__c (
	after delete,
    after insert,
    after undelete,
    after update,
    before delete,
    before insert,
    before update) {
    /**
     * Singleton Dispatcher for Outcome 
     * @param Outcome Handler Class
     * @return   NA
     */

     //get trigger settings
    TriggerSettingsClass triggerSettings = TriggerSettingsClass.getTriggerSetting('OutcomeTrigger');
    if(triggerSettings.isTriggerEnabled()){
        TriggerDispatcher.run(new OutcomeHandler());
        TriggerDispatcher.run(new OutcomeSharingHandler());
    }

}