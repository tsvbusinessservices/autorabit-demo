trigger ContentDocumentTrigger on ContentDocument (
    after delete,
    after insert,
    after undelete,
    after update,
    before delete,
    before insert,
    before update) {
	/**
	 * Singleton Dispatcher for ContentDocumentLink 
	 * @param ContentDocumentLink Handler Class
	 * @return   NA
	 */

    //get trigger settings
    TriggerSettingsClass ContentDocumentTrigger = TriggerSettingsClass.getTriggerSetting('ContentDocumentTrigger');
    if(ContentDocumentTrigger.isTriggerEnabled()){
        TriggerDispatcher.run(new ContentDocumentHandler());
    }
}