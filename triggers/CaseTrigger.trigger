trigger CaseTrigger on Case (
    after delete,
    after insert,
    after undelete,
    after update,
    before delete,
    before insert,
    before update) {
    /**
     * Singleton Dispatcher for Case 
     * @param Case Handler Class
     * @return   NA
     */

    //get trigger settings
    TriggerSettingsClass CaseTrigger = TriggerSettingsClass.getTriggerSetting('CaseTrigger');
    if(CaseTrigger.isTriggerEnabled()){
        TriggerDispatcher.run(new CaseHandler());
    }
}