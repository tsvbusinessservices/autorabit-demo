trigger ContentDocumentLinkTrigger on ContentDocumentLink (
    after delete,
    after insert,
    after undelete,
    after update,
    before delete,
    before insert,
    before update) {
    
    //get trigger settings
    TriggerSettingsClass ContentDocumentLinkTrigger = TriggerSettingsClass.getTriggerSetting('ContentDocumentLinkTrigger');
    
    if (ContentDocumentLinkTrigger.isTriggerEnabled()) {
        TriggerDispatcher.run(new ContentDocumentLinkHandler());
    }
}