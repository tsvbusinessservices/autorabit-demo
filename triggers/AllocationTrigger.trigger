trigger AllocationTrigger on Allocation__c (
    after delete,
    after insert,
    after undelete,
    after update,
    before delete,
    before insert,
    before update) {
	/**
	 * Singleton Dispatcher for Allocation__c
	 * @param Allocation Handler Class
	 * @return   NA
	 */

    //get trigger settings
    TriggerSettingsClass allocationTrigger = TriggerSettingsClass.getTriggerSetting('AllocationTrigger');
    if(allocationTrigger.isTriggerEnabled()){
        TriggerDispatcher.run(new AllocationHandler());
    }
}