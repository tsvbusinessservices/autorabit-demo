trigger AssetTrigger on Asset__c (
    after delete,
    after insert,
    after undelete,
    after update,
    before delete,
    before insert,
    before update) {
	/**
	 * Singleton Dispatcher for Asset__c
	 * @param Asset Handler Class
	 * @return   NA
	 */

    //get trigger settings
    TriggerSettingsClass AssetTrigger = TriggerSettingsClass.getTriggerSetting('AssetTrigger');
    if(AssetTrigger.isTriggerEnabled()){
        TriggerDispatcher.run(new AssetHandler());
    }
}