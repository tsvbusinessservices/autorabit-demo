trigger WaterwayRoleTrigger on Waterway_Role__c  (
    after delete,
    after insert,
    after undelete,
    after update,
    before delete,
    before insert,
    before update) {
    /**
     * Singleton Dispatcher for Waterway_Role__c
     * @param Waterway Role Handler Class
     * @return   NA
     */

    //get trigger settings
    TriggerSettingsClass locationTrigger = TriggerSettingsClass.getTriggerSetting('WaterwayRoleTrigger');
    if(locationTrigger.isTriggerEnabled()){
        TriggerDispatcher.run(new WaterwayRoleHandler());
    }
}