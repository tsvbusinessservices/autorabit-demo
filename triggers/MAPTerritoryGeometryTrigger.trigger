trigger MAPTerritoryGeometryTrigger on maps__ShapeLayerGeometry__c (
	before insert,
    before update) {
	/**
	 * Singleton Dispatcher for maps__ShapeLayerGeometry__c
	 * @param  Handler Class
	 * @return   NA
	 */

    //get trigger settings
    TriggerSettingsClass MATerritoryGeometryTrigger = TriggerSettingsClass.getTriggerSetting('MATerritoryGeometryTrigger');
    if(MATerritoryGeometryTrigger.isTriggerEnabled()){
        TriggerDispatcher.run(new MATerritoryGeometryHandler());
    }
}