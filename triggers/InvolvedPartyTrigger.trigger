trigger InvolvedPartyTrigger on Involved_Party__c (before insert) {

    /**
     * Singleton Dispatcher for Accreditaion 
     * @param Accreditaion Handler Class
     * @return   NA
     */

    //get trigger settings
    TriggerSettingsClass InvolvedPartyTrigger = TriggerSettingsClass.getTriggerSetting('InvolvedPartyTrigger');
    if(InvolvedPartyTrigger.isTriggerEnabled()){
        TriggerDispatcher.run(new InvolvedPartyTriggerHandler());
    }

}