trigger AccreditationTrigger on Accreditation__c (
    after delete,
    after insert,
    after undelete,
    after update,
    before delete,
    before insert,
    before update) {
	/**
	 * Singleton Dispatcher for Accreditaion 
	 * @param Accreditaion Handler Class
	 * @return   NA
	 */

    //get trigger settings
    TriggerSettingsClass AccreditationTrigger = TriggerSettingsClass.getTriggerSetting('AccreditationTrigger');
    if(AccreditationTrigger.isTriggerEnabled()){
        TriggerDispatcher.run(new AccreditationHandler());
    }
}