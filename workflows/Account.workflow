<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_BV_Public_User_Inactivation_request_notification_email</fullName>
        <ccEmails>contact@boating.vic.gov.au</ccEmails>
        <description>Send BV Public User Inactivation request notification email</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>BV_Email_Templates/Inactivate_BV_Public_User</template>
    </alerts>
    <rules>
        <fullName>Inactivate BV Public User</fullName>
        <actions>
            <name>Send_BV_Public_User_Inactivation_request_notification_email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Inactivate_User__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>