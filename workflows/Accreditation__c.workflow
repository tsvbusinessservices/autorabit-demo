<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Maritime_Local_Knowledge_Certificate_expiry_reminder</fullName>
        <description>Maritime: Local Knowledge Certificate expiry reminder</description>
        <protected>false</protected>
        <recipients>
            <field>Account_email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>tsvqualifications@transportsafety.vic.gov.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>TSV_Emails/Maritime_LKC_Renewal_notice</template>
    </alerts>
    <rules>
        <fullName>Maritime - LKC Renewal notice</fullName>
        <actions>
            <name>Maritime_Local_Knowledge_Certificate_expiry_reminder</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND (4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Accreditation__c.Type__c</field>
            <operation>equals</operation>
            <value>Local Knowledge Certificate</value>
        </criteriaItems>
        <criteriaItems>
            <field>Accreditation__c.Account_email__c</field>
            <operation>contains</operation>
            <value>@</value>
        </criteriaItems>
        <criteriaItems>
            <field>Accreditation__c.Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>Accreditation__c.Expiry_action__c</field>
            <operation>equals</operation>
            <value>Renewal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Accreditation__c.Expiry_action__c</field>
            <operation>equals</operation>
            <value>Resend</value>
        </criteriaItems>
        <description>Sends email alert for LKC reminders</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>