<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Camera_Unpublished_Notification</fullName>
        <ccEmails>contact@boating.vic.gov.au</ccEmails>
        <description>Camera Unpublished Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>lfletcher-seales@tsv.tims</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mrighetti@tsv.tims</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BV_Email_Templates/Camera_Unpublished_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_VicRoads_External_Id_For_Vessel</fullName>
        <field>VicRoads_External_Id__c</field>
        <formula>Registration_Number__c + &apos;-&apos; +  IF( ISBLANK( Registration_Start_Date__c ) ,&apos;TBD&apos;, TEXT(Day(Registration_Start_Date__c)) + &apos;/&apos; + TEXT(MONTH(Registration_Start_Date__c)) + &apos;/&apos; + TEXT(YEAR(Registration_Start_Date__c)))</formula>
        <name>Update VicRoads External Id For Vessel</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Camera Unpublished Notification</fullName>
        <actions>
            <name>Camera_Unpublished_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset__c.Status__c</field>
            <operation>equals</operation>
            <value>Hide</value>
        </criteriaItems>
        <description>To notify BV Admin group when a camera has been unpublished</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update VicRoads External Id</fullName>
        <actions>
            <name>Update_VicRoads_External_Id_For_Vessel</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Asset__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Vessel</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>