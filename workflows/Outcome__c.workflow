<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approved_Request</fullName>
        <description>Approved Request</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TSV_Emails/Approval_Granted</template>
    </alerts>
    <alerts>
        <fullName>Bus_Safety_Alert</fullName>
        <description>Bus Safety Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Bus_Operator_Responsible_Persons</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Bus_Email_Templates/Bus_Safety_Alert_Notification</template>
    </alerts>
    <alerts>
        <fullName>Send_Approval_Outcome_Notification</fullName>
        <description>Send Approval Outcome Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Submitted_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TSV_Emails/Approval_Outcome_Email_USR_NTM</template>
    </alerts>
    <alerts>
        <fullName>Send_Approval_Outcome_for_Bus_Safety_Alert</fullName>
        <description>Send Approval Outcome for Bus Safety Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Submitted_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TSV_Emails/Approval_Outcome_Email_Bus_Safety_Alert</template>
    </alerts>
    <alerts>
        <fullName>Send_Approval_Outcome_for_Infringement</fullName>
        <description>Send Approval Outcome for Infringement</description>
        <protected>false</protected>
        <recipients>
            <field>Submitted_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TSV_Emails/Approval_Outcome_Email_Infringement</template>
    </alerts>
    <alerts>
        <fullName>Send_Approval_Outcome_for_Notice</fullName>
        <description>Send Approval Outcome for Notice</description>
        <protected>false</protected>
        <recipients>
            <field>Submitted_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TSV_Emails/Approval_Outcome_Email_Notice_Notice</template>
    </alerts>
    <alerts>
        <fullName>Send_VOZR_Rule_Approval_Outcome_Notification</fullName>
        <description>Send VOZR Rule Approval Outcome Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Submitted_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TSV_Emails/Approval_Outcome_Email_VOZR_Review</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approval_Status_to_Approved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Approval Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Status_to_Pending</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Pending</literalValue>
        <name>Approval Status to Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Status_to_Rejected</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Approval Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Status_Approved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Set Approval Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Status_Draft</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Draft</literalValue>
        <name>Set Approval Status Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Status_Pending</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Pending</literalValue>
        <name>Set Approval Status Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Status_Recalled</fullName>
        <description>Used when a user recalls their approval request</description>
        <field>Approval_Status__c</field>
        <literalValue>Recalled</literalValue>
        <name>Set Approval Status Recalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Status_Rejected</fullName>
        <description>Set the Approval Status to Rejected</description>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Set Approval Status Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Status_to_Approved</fullName>
        <description>Change Approval Status from Pending to Approved</description>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Set Approval Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Date_of_Signature_to_Today</fullName>
        <field>Date_of_Signature__c</field>
        <formula>TODAY()</formula>
        <name>Set Date of Signature to Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Notice_Date_to_Today</fullName>
        <field>InfringementNoticeDate__c</field>
        <formula>Today()</formula>
        <name>Set Notice Date to Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Police_Infringement</fullName>
        <field>Police_Infringement__c</field>
        <literalValue>1</literalValue>
        <name>Set Police Infringement</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Signature_Date</fullName>
        <field>Date_of_Signature__c</field>
        <formula>TODAY()</formula>
        <name>Set Signature Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_In_Review</fullName>
        <field>Status__c</field>
        <literalValue>In Review</literalValue>
        <name>Set Status to In Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_New</fullName>
        <description>Set Outcome Status to New</description>
        <field>Status__c</field>
        <literalValue>New</literalValue>
        <name>Set Status to New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Published</fullName>
        <field>Status__c</field>
        <literalValue>Published</literalValue>
        <name>Set Status to Published</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Published2</fullName>
        <field>Status__c</field>
        <literalValue>Published</literalValue>
        <name>Set Status to Published</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Reviewed</fullName>
        <field>Status__c</field>
        <literalValue>Reviewed</literalValue>
        <name>Set Status to Reviewed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>USR_Owner_to_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Maritime_Rule_Approvals</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>USR Owner to Queue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Controls_Is_Populate_To_False</fullName>
        <field>Controls_Is_Populated__c</field>
        <literalValue>0</literalValue>
        <name>Update Controls Is Populate To False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Controls_Is_Populate_To_True</fullName>
        <field>Controls_Is_Populated__c</field>
        <literalValue>1</literalValue>
        <name>Update Controls Is Populate To True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Current_Rule_Content</fullName>
        <field>Rule_Content__c</field>
        <formula>Rule__r.Content__c</formula>
        <name>Update Current Rule Content</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Director_Name</fullName>
        <field>Director_Name__c</field>
        <formula>$User.FirstName&amp;&quot; &quot;&amp;$User.LastName</formula>
        <name>Update Director Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Director_Signature</fullName>
        <field>Director_Signature__c</field>
        <formula>$User.Signature_Link__c</formula>
        <name>Update Director Signature</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Director_Title</fullName>
        <field>Director_Title__c</field>
        <formula>$User.Title</formula>
        <name>Update Director Title</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_InfringementPenalty</fullName>
        <description>Update Outcome InfringementPenalty__c</description>
        <field>InfringementPenalty__c</field>
        <formula>Round(Infringement_Quantity__c * $Setup.TSV_Settings__c.Penalty_Unit__c, 0)</formula>
        <name>Update InfringementPenalty</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Infringement_Due_Date</fullName>
        <field>InfringementDueDate__c</field>
        <formula>InfringementNoticeDate__c +  $Setup.TSV_Settings__c.Infringement_grace_days__c</formula>
        <name>Update Infringement Due Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Signed_Date</fullName>
        <field>Director_Signed_Date__c</field>
        <formula>today()</formula>
        <name>Update Signed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Approved</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Submitted_for_Approval</fullName>
        <field>Status__c</field>
        <literalValue>Submitted for Approval</literalValue>
        <name>Update Status to Submitted for Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Submitted_Email_Address</fullName>
        <description>Populate Submitted Email with email address of user who initiated Outcome approval process, so they can receive an email notification when submission has been approved or rejected</description>
        <field>Submitted_Email__c</field>
        <formula>$User.Email</formula>
        <name>Update Submitted Email Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Compute Infringement Penalty</fullName>
        <actions>
            <name>Update_InfringementPenalty</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Compute Infringement Penalty from PenaltyUnitMultiplier</description>
        <formula>AND( OR(RecordType.DeveloperName == &apos;Infringement_Notice&apos;, RecordType.DeveloperName == &apos;Bus_Infringement&apos;, RecordType.DeveloperName == &apos;VicPol_Infringement_Notice&apos; ) , NOT( ISNULL( Infringement_Code__c ) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Controls Field Is Empty</fullName>
        <actions>
            <name>Update_Controls_Is_Populate_To_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND (	OR 	(	ISNEW(),  		ISCHANGED(Controls__c) 	), 	ISBLANK(Controls__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Controls Field Is Populated</fullName>
        <actions>
            <name>Update_Controls_Is_Populate_To_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND (	OR 	(	ISNEW(),  		ISCHANGED(Controls__c) 	), 	NOT(ISBLANK(Controls__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email Published Bus Safety Alert</fullName>
        <actions>
            <name>Bus_Safety_Alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Outcome__c.Status__c</field>
            <operation>equals</operation>
            <value>Published</value>
        </criteriaItems>
        <criteriaItems>
            <field>Outcome__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Bus Safety Alert</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Notice Date</fullName>
        <actions>
            <name>Set_Notice_Date_to_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Outcome__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Maritime - Infringement Notice,VicPol - Infringement Notice</value>
        </criteriaItems>
        <criteriaItems>
            <field>Outcome__c.Status__c</field>
            <operation>equals</operation>
            <value>Served,Issued</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set VicPol Infringement</fullName>
        <actions>
            <name>Set_Police_Infringement</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>RecordType.DeveloperName == &apos;VicPol_Infringement_Notice&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Infringement Due Date</fullName>
        <actions>
            <name>Update_Infringement_Due_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(  	OR( 		RecordType.DeveloperName == &apos;Infringement_Notice&apos;,  		RecordType.DeveloperName == &apos;Bus_Infringement&apos;  	) ,  	NOT( ISBLANK( InfringementNoticeDate__c )),  	InfringementNoticeDate__c != PRIORVALUE(InfringementNoticeDate__c)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Outcome Audit Item%2FControl Measure</fullName>
        <actions>
            <name>Update_Current_Rule_Content</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When a user adds a Rule to an Outcome, populate the Audit Item/Control field with the related Rule Content</description>
        <formula>AND(     RecordType.DeveloperName = &quot;Maritime_Audit_Item_Waterway&quot;,     NOT(ISBLANK(Rule__c))    )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>