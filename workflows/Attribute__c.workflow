<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Attribute_Details</fullName>
        <description>Set Attribute Details From Credential Detail</description>
        <field>Details__c</field>
        <formula>BLANKVALUE(Details__c, Credential__r.Details__c)</formula>
        <name>Update Attribute Details</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Default Attribute Details</fullName>
        <actions>
            <name>Update_Attribute_Details</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Attribute Details if Not blank</description>
        <formula>AND( NOT(ISBLANK(Credential__r.Details__c)),  Credential__r.RecordType.Name = &apos;Maritime Operator&apos; )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>