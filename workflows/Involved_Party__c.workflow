<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>Update Account Detail to Incidents</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Involved_Party__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Marine Incident</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>