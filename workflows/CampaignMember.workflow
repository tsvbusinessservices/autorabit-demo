<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CPVV_Email_Alert_Due_soon</fullName>
        <ccEmails>bsp.data@cpv.vic.gov.au</ccEmails>
        <description>CPVV Email Alert - Due Soon</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>bsp.data@cpv.vic.gov.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CPVV_Templates/CPVV_Data_due_soon</template>
    </alerts>
    <alerts>
        <fullName>CPVV_Email_Alert_First_request</fullName>
        <ccEmails>bsp.data@cpv.vic.gov.au</ccEmails>
        <description>CPVV Email Alert - First request</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>bsp.data@cpv.vic.gov.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CPVV_Templates/CPVV_First_Request</template>
    </alerts>
    <alerts>
        <fullName>CPVV_Email_Alert_Overdue</fullName>
        <ccEmails>bsp.data@cpv.vic.gov.au</ccEmails>
        <description>CPVV Email Alert - Overdue</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>bsp.data@cpv.vic.gov.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CPVV_Templates/CPVV_Data_overdue</template>
    </alerts>
</Workflow>