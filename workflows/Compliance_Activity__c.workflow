<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Compliance_Outcome</fullName>
        <description>Compliance Outcome</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Bus_Email_Templates/Inspection_Approval_Outcome</template>
    </alerts>
    <alerts>
        <fullName>Compliance_Outcome_PreApproval</fullName>
        <description>Compliance Outcome_PreApproval: Accreditation manager alert</description>
        <protected>false</protected>
        <recipients>
            <field>Case_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>Bus_Manager_Accreditation_Registration</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Bus_Email_Templates/PreApproval_Inspection_Outcome</template>
    </alerts>
    <fieldUpdates>
        <fullName>Change_Status_to_Completed</fullName>
        <field>Status__c</field>
        <literalValue>Completed</literalValue>
        <name>Change Status to Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Status_to_submitted</fullName>
        <field>Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Change Status to submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ComPact_approval_Status_Rejected</fullName>
        <description>Approval request has been rejected</description>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>ComPact: approval Status: Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CompAct_Approval_status_Approved</fullName>
        <description>Approval request has been approved</description>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>CompAct: Approval status: Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CompAct_Approval_status_Draft</fullName>
        <description>Approval request has been recalled and record is still in progress</description>
        <field>Approval_Status__c</field>
        <literalValue>Draft</literalValue>
        <name>CompAct: Approval status: Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CompAct_Approval_status_Pending</fullName>
        <description>Approval request has been submitted</description>
        <field>Approval_Status__c</field>
        <literalValue>Pending</literalValue>
        <name>CompAct: Approval status: Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Closed_Date</fullName>
        <description>Update the closed date on the Audit record when the Status changes to Completed</description>
        <field>Closed_Date__c</field>
        <formula>Now()</formula>
        <name>Update Closed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_In_Progress</fullName>
        <field>Status__c</field>
        <literalValue>In Progress</literalValue>
        <name>Update Status to In-Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Update Audit Closed Date</fullName>
        <actions>
            <name>Update_Closed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Compliance_Activity__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Bus Audit - Accredited Operator,Bus Audit - Registered Operator</value>
        </criteriaItems>
        <criteriaItems>
            <field>Compliance_Activity__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <description>Capture the date and time the status changes to completed and populate in the Closed date field</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>