<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>RULE_Update_Parent_Content</fullName>
        <field>Parent_Content__c</field>
        <formula>Parent__r.Content__c</formula>
        <name>RULE: Update Parent Content</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Rule - Set External Id</fullName>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Rule%3A Update Parent Rule Content</fullName>
        <actions>
            <name>RULE_Update_Parent_Content</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND
(    NOT(ISBLANK(Parent__c)),
     Content__c != Parent__r.Content__c
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>