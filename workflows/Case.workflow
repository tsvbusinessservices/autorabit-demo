<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Camera_Feedback_Email</fullName>
        <description>Camera Feedback Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>BV_Admin</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BV_Email_Templates/Camera_Feedback</template>
    </alerts>
    <alerts>
        <fullName>Case_acknowledgement</fullName>
        <description>Case acknowledgement</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Bus_Email_Templates/REGISTRATION_Ack_for_new_case</template>
    </alerts>
    <alerts>
        <fullName>Case_acknowledgement_accred</fullName>
        <description>Case acknowledgement accred</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Bus_Email_Templates/ACCREDITATION_Ack_for_new_case</template>
    </alerts>
    <alerts>
        <fullName>Case_acknowledgement_owner</fullName>
        <description>Case acknowledgement owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Bus_Email_Templates/REGISTRATION_Notification_for_case_owner</template>
    </alerts>
    <alerts>
        <fullName>Case_closed</fullName>
        <description>Case closed</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SUPPORT_Case_closed</template>
    </alerts>
    <alerts>
        <fullName>Compliance_Preaudit</fullName>
        <description>Compliance Preaudit</description>
        <protected>false</protected>
        <recipients>
            <recipient>epaciocco@tsv.tims</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Bus_Email_Templates/Bus_Audit_Preapproval</template>
    </alerts>
    <alerts>
        <fullName>Email_Bus_Incident_Stakeholders</fullName>
        <ccEmails>DEDJTR-TSV-BusAccidentNotify@ecodev.vic.gov.au</ccEmails>
        <ccEmails>Transport.Safety@transportsafety.vic.gov.au</ccEmails>
        <ccEmails>tsvbusnotifications@ptv.vic.gov.au</ccEmails>
        <description>Email Bus Incident Stakeholders</description>
        <protected>false</protected>
        <recipients>
            <recipient>DirectorBusSafety</recipient>
            <type>roleSubordinatesInternal</type>
        </recipients>
        <recipients>
            <recipient>Research_Analysis_Manager</recipient>
            <type>roleSubordinatesInternal</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Bus_Email_Templates/Bus_Incident_Notification</template>
    </alerts>
    <alerts>
        <fullName>Send_Approval_Outcome_for_Bus_Application</fullName>
        <description>Send Approval Outcome for Bus Application</description>
        <protected>false</protected>
        <recipients>
            <field>Submitted_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TSV_Emails/Approval_Case_Email_Bus_Application</template>
    </alerts>
    <alerts>
        <fullName>Send_Approval_Outcome_for_Bus_Inquiry</fullName>
        <description>Send Approval Outcome for Bus Inquiry</description>
        <protected>false</protected>
        <recipients>
            <field>Submitted_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TSV_Emails/Approval_Case_Email_Bus_Inquiry</template>
    </alerts>
    <alerts>
        <fullName>Send_Approval_Outcome_for_Bus_Investigation_Findings</fullName>
        <description>Send Approval Outcome for Bus Investigation Findings</description>
        <protected>false</protected>
        <recipients>
            <field>Submitted_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TSV_Emails/Approval_Case_Email_Bus_Investigation_Findings</template>
    </alerts>
    <alerts>
        <fullName>Send_Approval_Outcome_for_Bus_Investigation_Initiation</fullName>
        <description>Send Approval Outcome for Bus Investigation Initiation</description>
        <protected>false</protected>
        <recipients>
            <field>Submitted_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TSV_Emails/Approval_Case_Email_Bus_Investigation_Initiation</template>
    </alerts>
    <alerts>
        <fullName>Send_Approval_Outcome_for_Maritime_Accreditation</fullName>
        <description>Send Approval Outcome for Maritime Accreditation</description>
        <protected>false</protected>
        <recipients>
            <field>Submitted_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TSV_Emails/Approval_Case_Email_Maritime_Accreditation</template>
    </alerts>
    <alerts>
        <fullName>Send_Approval_Outcome_for_Maritime_Exemption</fullName>
        <description>Send Approval Outcome for Maritime Exemption</description>
        <protected>false</protected>
        <recipients>
            <field>Submitted_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TSV_Emails/Approval_Case_Email_Maritime_Exemption</template>
    </alerts>
    <alerts>
        <fullName>Send_Approval_Outcome_for_Maritime_Investigation</fullName>
        <description>Send Approval Outcome for Maritime Investigation</description>
        <protected>false</protected>
        <recipients>
            <field>Submitted_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TSV_Emails/Approval_Case_Email_Maritime_Investigation</template>
    </alerts>
    <alerts>
        <fullName>Send_Approval_Outcome_for_Rule_Request</fullName>
        <description>Send Approval Outcome for Rule Request</description>
        <protected>false</protected>
        <recipients>
            <field>Submitted_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>TSV_Emails/Approval_Case_Email_Rule_Request</template>
    </alerts>
    <alerts>
        <fullName>Send_Camera_Feedback_Email</fullName>
        <ccEmails>contact@boating.vic.gov.au</ccEmails>
        <description>Send Camera Feedback Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>BV_Email_Templates/Camera_Feedback</template>
    </alerts>
    <fieldUpdates>
        <fullName>Incident_date_time_adjust</fullName>
        <description>Overwrites the incident date_time when this is created in the future</description>
        <field>Incident_Date_Time__c</field>
        <formula>CreatedDate</formula>
        <name>Incident date/time adjust</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Final_Approval_Date</fullName>
        <field>Final_Approved_Date__c</field>
        <formula>TEXT(TODAY())</formula>
        <name>Set Final Approval Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Final_Approver</fullName>
        <field>Final_Approver__c</field>
        <formula>$User.FirstName + &quot; &quot; + $User.LastName</formula>
        <name>Set Final Approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lat_from_Lat_DDM</fullName>
        <description>Used by Marine Incidents</description>
        <field>MALatitude__c</field>
        <formula>IF(Lat_Degrees__c+Lat_Minutes__c +Lat_Decimal__c =0,Null, 
IF( 
(Lat_Degrees__c + ((VALUE(TEXT(Lat_Minutes__c) &amp; &quot;.&quot; &amp; LPAD(TEXT(Lat_Decimal__c),3,&apos;0&apos;)) / 60))) &gt;0, 
(Lat_Degrees__c + ((VALUE(TEXT(Lat_Minutes__c) &amp; &quot;.&quot; &amp; LPAD(TEXT(Lat_Decimal__c),3,&apos;0&apos;)) / 60))) * -1, 
(Lat_Degrees__c + ((VALUE(TEXT(Lat_Minutes__c) &amp; &quot;.&quot; &amp; LPAD(TEXT(Lat_Decimal__c),3,&apos;0&apos;)) / 60))) 
) 
)</formula>
        <name>Set Lat from Lat (DDM)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Legacy_Case_Id_with_MIR</fullName>
        <description>Used by Marine Incidents</description>
        <field>Legacy_Ref__c</field>
        <formula>&quot;MIR&quot; &amp; CaseNumber</formula>
        <name>Set Legacy Case Id with MIR</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Long_from_Long_DDM</fullName>
        <field>MALongitude__c</field>
        <formula>IF(Long_Degrees__c + Long_Minutes__c + Long_Decimal__c =0,Null,
   (Long_Degrees__c + ((VALUE(TEXT(Long_Minutes__c) &amp; &quot;.&quot; &amp; LPAD(TEXT(Long_Decimal__c),3,&apos;0&apos;)) / 60)))
  )</formula>
        <name>Set Long from Long (DDM)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Owner_To_BV_Admin_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>BV_Admin_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Owner To BV Admin Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Director_Review</fullName>
        <field>Status</field>
        <literalValue>Director Review</literalValue>
        <name>Set Status to Director Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tick_Checkbox</fullName>
        <field>Create_Accreditation__c</field>
        <literalValue>1</literalValue>
        <name>Tick Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateManagersName</fullName>
        <field>Manager_Name__c</field>
        <formula>$User.FirstName&amp;&quot; &quot;&amp;$User.LastName</formula>
        <name>Update Managers Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status_to_Approved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Approval Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status_to_Draft</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Draft</literalValue>
        <name>Update Approval Status to Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status_to_Pending</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Pending</literalValue>
        <name>Update Approval Status to Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status_to_Rejected</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Approval Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Create_Registration</fullName>
        <field>Create_Registration__c</field>
        <literalValue>1</literalValue>
        <name>Update Create Registration</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Director_Name</fullName>
        <field>Director_Name__c</field>
        <formula>$User.FirstName&amp;&quot; &quot;&amp;$User.LastName</formula>
        <name>Update Director Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Director_Signature</fullName>
        <field>Director_Signature__c</field>
        <formula>$User.Signature_Link__c</formula>
        <name>Update Director Signature</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Director_Title</fullName>
        <field>Director_Title__c</field>
        <formula>$User.Title</formula>
        <name>Update Director Title</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_IP</fullName>
        <description>Update Involved Person on Incidents</description>
        <field>Case_Contact__c</field>
        <formula>Account.Name</formula>
        <name>Update IP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Investign_Approved_to_Continue</fullName>
        <description>Tick the Detailed Investigation Approved checkbox</description>
        <field>Investigation_Approved_to_Continue__c</field>
        <literalValue>1</literalValue>
        <name>Update Investign Approved to Continue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Manager_Details</fullName>
        <description>Update details for Bus manager who endorsed application</description>
        <field>Subject</field>
        <formula>$User.Signature_Link__c</formula>
        <name>Update Manager Details</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Manager_Job_Title</fullName>
        <field>Manager_Title__c</field>
        <formula>$User.Title</formula>
        <name>Update Manager Job Title</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Manager_Signature</fullName>
        <field>Manager_Signature__c</field>
        <formula>$User.Signature_Link__c</formula>
        <name>Update Manager Signature</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Manager_Signed</fullName>
        <field>Manager_Signed_Date__c</field>
        <formula>today()</formula>
        <name>Update Manager Signed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Signed_Date</fullName>
        <field>Director_Signed_Date__c</field>
        <formula>today()</formula>
        <name>Update Signed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Active</fullName>
        <field>Status</field>
        <literalValue>Active</literalValue>
        <name>Update Status to Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Approved</fullName>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Update Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Complete</fullName>
        <field>Status</field>
        <literalValue>Complete</literalValue>
        <name>Update Status to Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Coordinator_Review</fullName>
        <description>Used for Marine Incident TSV review approval process.</description>
        <field>Status</field>
        <literalValue>Coordinator Review</literalValue>
        <name>Update Status to Coordinator Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Director_Review</fullName>
        <field>Status</field>
        <literalValue>Director Review</literalValue>
        <name>Update Status to Director Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_In_Progress</fullName>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Update Status to In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Manager_Review</fullName>
        <field>Status</field>
        <literalValue>Manager Review</literalValue>
        <name>Update Status to Manager Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Ready_for_Review</fullName>
        <field>Status</field>
        <literalValue>Ready for Review</literalValue>
        <name>Update Status to Ready for Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Rejected</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Update Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Reviewed</fullName>
        <description>Update Status to Reviewed</description>
        <field>Status</field>
        <literalValue>Reviewed</literalValue>
        <name>Update Status to Reviewed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Submitted</fullName>
        <field>Status</field>
        <literalValue>Submitted</literalValue>
        <name>Update Status to Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Submitted_Date</fullName>
        <description>Updates case field - submitted by date</description>
        <field>Submitted_Date__c</field>
        <formula>Today()</formula>
        <name>Update Submitted Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Submitted_Email_Address</fullName>
        <description>Populate Submitted Email with email address of user who initiated approval process, so they can receive an email notification when submission has been approved or rejected</description>
        <field>Submitted_Email__c</field>
        <formula>$User.Email</formula>
        <name>Update Submitted Email Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_to_Review</fullName>
        <description>Update status to In Review</description>
        <field>Status</field>
        <literalValue>In Review</literalValue>
        <name>Update Status to In Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updated_Submitted_Signatre</fullName>
        <description>Updates case field submitted signature</description>
        <field>Submitted_Signature__c</field>
        <formula>$User.Signature_Link__c</formula>
        <name>Updated Submitted Signatre</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updated_Submitted_Title</fullName>
        <description>Update case field - submitted by title</description>
        <field>Submitted_Title__c</field>
        <formula>$User.Title</formula>
        <name>Updated Submitted Title</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updated_Submitted_user</fullName>
        <description>Updates case field - Submitted by</description>
        <field>Submitted_By__c</field>
        <formula>$User.FirstName&amp;&quot; &quot;&amp;$User.LastName</formula>
        <name>Updated Submitted user</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Camera Feedback Assignment</fullName>
        <actions>
            <name>Send_Camera_Feedback_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Set_Owner_To_BV_Admin_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>BV Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Feedback</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Fix Incident Date%2FTime</fullName>
        <actions>
            <name>Incident_date_time_adjust</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Overwrite incident date/time when this is incorrect</description>
        <formula>IF(  AND(Today()&gt;=Date(Year(Today()),07,01),Today()&lt;Date(Year(Today()),07,01)+7) ,     OR(Incident_Date_Time__c &gt; NOW(),         DATEVALUE(Incident_Date_Time__c) &lt; (Date(Year(Today()),07,01)-14)      ) ,  OR(Incident_Date_Time__c &gt; NOW(),         DATEVALUE(Incident_Date_Time__c) &lt; (Date(IF(Today()&lt;Date(year(today()),07,01),Year(Today())-1,Year(Today())),07,01))      ) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Lat from Lat %28DDM%29</fullName>
        <actions>
            <name>Set_Lat_from_Lat_DDM</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used by Vessel Inspections</description>
        <formula>OR(    AND(       ISNEW(),       OR(         NOT(ISBLANK(Lat_Degrees__c)),         NOT(ISBLANK(Lat_Minutes__c)),         NOT(ISBLANK(Lat_Decimal__c))         )       ),    OR(       ISCHANGED(Lat_Degrees__c),       ISCHANGED(Lat_Minutes__c),       ISCHANGED(Lat_Decimal__c)       )   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Legacy Case Id with MIR</fullName>
        <actions>
            <name>Set_Legacy_Case_Id_with_MIR</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Maritime - Incident</value>
        </criteriaItems>
        <description>Used by Marine Incidents to set MIR Number post go live.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Long from Long %28DDM%29</fullName>
        <actions>
            <name>Set_Long_from_Long_DDM</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used by Marine Incidents</description>
        <formula>OR(    AND(       ISNEW(),       OR(         NOT(ISBLANK(Long_Degrees__c)),         NOT(ISBLANK(Long_Minutes__c)),         NOT(ISBLANK(Long_Decimal__c))         )       ),    OR(       ISCHANGED(Long_Degrees__c),       ISCHANGED(Long_Minutes__c),       ISCHANGED(Long_Decimal__c)       )   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>tick Checkbox Create Accreditation and Generate Invoice</fullName>
        <actions>
            <name>Tick_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Bus - Accreditation Application</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <description>tick Checkbox Create Accreditation and Generate Invoice</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>tick Checkbox Create Registration</fullName>
        <actions>
            <name>Update_Create_Registration</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Bus - Registration Application</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <description>tick Checkbox Create Registration</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>