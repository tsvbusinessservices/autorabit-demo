<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Bus - Additional Bus Fee</label>
    <protected>false</protected>
    <values>
        <field>Additional_Bus_Fee__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Additional_Fee_Desc__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Base_Rate_Description__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Base_Rate__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Fee_Unit__c</field>
        <value xsi:type="xsd:double">15.03</value>
    </values>
    <values>
        <field>Fee_Unity_Qty__c</field>
        <value xsi:type="xsd:double">5.5</value>
    </values>
    <values>
        <field>Fee__c</field>
        <value xsi:type="xsd:double">82.7</value>
    </values>
    <values>
        <field>Line_Description__c</field>
        <value xsi:type="xsd:string">Additional Bus Fee</value>
    </values>
    <values>
        <field>Process_Fee_Description__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Processing_Rate__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Sequence__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
</CustomMetadata>
