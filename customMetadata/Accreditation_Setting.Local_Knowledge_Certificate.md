<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Local Knowledge Certificate</label>
    <protected>false</protected>
    <values>
        <field>Details__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Endorsement_Header__c</field>
        <value xsi:type="xsd:string">Authorised:</value>
    </values>
    <values>
        <field>Footer__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Preamble__c</field>
        <value xsi:type="xsd:string">The person named on this local knowledge certificate has satisfied the &lt;BR&gt;requirements for the issue of this local knowledge certificate &lt;BR&gt;in accordance with the Marine Safety Act 2010 (Vic) &lt;BR&gt;to command a vessel on the following State waters:</value>
    </values>
</CustomMetadata>
