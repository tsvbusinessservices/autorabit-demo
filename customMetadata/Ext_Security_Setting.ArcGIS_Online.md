<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ArcGIS Online</label>
    <protected>false</protected>
    <values>
        <field>ClientID__c</field>
        <value xsi:type="xsd:string">cm93d80dnXRIslxN</value>
    </values>
    <values>
        <field>ClientSecret__c</field>
        <value xsi:type="xsd:string">0698f02e0c3646f7aa156a42c122d60b</value>
    </values>
    <values>
        <field>Platform__c</field>
        <value xsi:type="xsd:string">ArcOnline</value>
    </values>
    <values>
        <field>Token__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
