<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Default</label>
    <protected>false</protected>
    <values>
        <field>Email_Address__c</field>
        <value xsi:type="xsd:string">tims.project@transportsafety.vic.gov.au</value>
    </values>
    <values>
        <field>Enable_Error_Emails__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Send_Recalculation_Success_Emails__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Test_Case__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
