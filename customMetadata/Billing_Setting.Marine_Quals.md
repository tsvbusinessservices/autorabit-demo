<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Marine - Quals</label>
    <protected>false</protected>
    <values>
        <field>Billing_grace_days__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Charge_Code__c</field>
        <value xsi:type="xsd:string">6120-69500-5330-0000-9100-100436-02027-00000000-0000</value>
    </values>
    <values>
        <field>Comments__c</field>
        <value xsi:type="xsd:string">If there are any queries relating to payment of this invoice please contact TSVqualifications@transportsafety.vic.gov.au.</value>
    </values>
    <values>
        <field>Comments_old__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Oracle_Contact_Name__c</field>
        <value xsi:type="xsd:string">Transport Safety Victoria - 1800 223 022</value>
    </values>
    <values>
        <field>Tax_Code_Percentage__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Tax_Code__c</field>
        <value xsi:type="xsd:string">N01 NO GST 0%</value>
    </values>
    <values>
        <field>Tax_Code_old__c</field>
        <value xsi:type="xsd:string">No GST</value>
    </values>
    <values>
        <field>Term_Days__c</field>
        <value xsi:type="xsd:double">30.0</value>
    </values>
    <values>
        <field>Term_Name__c</field>
        <value xsi:type="xsd:string">30 NET</value>
    </values>
    <values>
        <field>Transaction_Type__c</field>
        <value xsi:type="xsd:string">Maritime</value>
    </values>
</CustomMetadata>
