<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Exam: Port Wtrs: Ptlnd, Hstngs</label>
    <protected>false</protected>
    <values>
        <field>Fee_Unit__c</field>
        <value xsi:type="xsd:double">15.03</value>
    </values>
    <values>
        <field>Fee_Unity_Qty__c</field>
        <value xsi:type="xsd:double">42.2</value>
    </values>
    <values>
        <field>Fee__c</field>
        <value xsi:type="xsd:double">634.3</value>
    </values>
    <values>
        <field>Line_Description__c</field>
        <value xsi:type="xsd:string">Exam:  Port Waters: Portland(declared) &amp; Hastings(declared)</value>
    </values>
    <values>
        <field>Sequence__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
</CustomMetadata>
