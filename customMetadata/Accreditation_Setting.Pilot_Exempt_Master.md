<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Pilot Exempt Master</label>
    <protected>false</protected>
    <values>
        <field>Details__c</field>
        <value xsi:type="xsd:string">Authorised to act as a Pilot Exempt Master for the vessel &lt;VESSEL&gt; in the &lt;WATERS&gt; &lt;DIRECTION-LOCATION&gt;;</value>
    </values>
    <values>
        <field>Endorsement_Header__c</field>
        <value xsi:type="xsd:string">Authorisation:</value>
    </values>
    <values>
        <field>Footer__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Preamble__c</field>
        <value xsi:type="xsd:string">The person named on this pilot exemption has satisfied the requirements&lt;BR&gt;for the grant of a pilot exemption under the Marine Safety Act 2010 (Vic)&lt;BR&gt;and is exempt from using the services of a pilot on the vessel endorsed&lt;BR&gt;on this exemption subject to any conditions or limitations attached&lt;BR&gt;to this exemption.</value>
    </values>
</CustomMetadata>
