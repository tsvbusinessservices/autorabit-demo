<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Pilot License</label>
    <protected>false</protected>
    <values>
        <field>Details__c</field>
        <value xsi:type="xsd:string">Authorised to act as a pilot in the &lt;WATERS&gt; for ships having a Draught not exceeding &lt;DRAUGHT&gt; metres and length not exceeding &lt;LENGTH&gt; metres</value>
    </values>
    <values>
        <field>Endorsement_Header__c</field>
        <value xsi:type="xsd:string">Authorisation:</value>
    </values>
    <values>
        <field>Footer__c</field>
        <value xsi:type="xsd:string">This License remains in effect unless suspended or cancelled</value>
    </values>
    <values>
        <field>Preamble__c</field>
        <value xsi:type="xsd:string">The person named on this pilot licence has satisfied the requirements for&lt;BR&gt;the grant of a pilot licence under the Marine Safety Act 2010 (Vic) and&lt;BR&gt;is entitled to act as a pilot subject to any conditions or limitations&lt;BR&gt;attached to this licence.</value>
    </values>
</CustomMetadata>
