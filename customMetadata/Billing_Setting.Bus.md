<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Bus</label>
    <protected>false</protected>
    <values>
        <field>Billing_grace_days__c</field>
        <value xsi:type="xsd:double">30.0</value>
    </values>
    <values>
        <field>Charge_Code__c</field>
        <value xsi:type="xsd:string">6120-69500-5330-0000-9100-200019-02037-00000000-0000</value>
    </values>
    <values>
        <field>Comments__c</field>
        <value xsi:type="xsd:string">If any of your details have changed please email information@transportsafety.vic.gov.au. For queries relating to payment of this invoice please contact us on 1800 223 022.</value>
    </values>
    <values>
        <field>Comments_old__c</field>
        <value xsi:type="xsd:string">This comment goes on the invoice to the operator</value>
    </values>
    <values>
        <field>Oracle_Contact_Name__c</field>
        <value xsi:type="xsd:string">Transport Safety Victoria - 1800 223 022</value>
    </values>
    <values>
        <field>Tax_Code_Percentage__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Tax_Code__c</field>
        <value xsi:type="xsd:string">N01 NO GST 0%</value>
    </values>
    <values>
        <field>Tax_Code_old__c</field>
        <value xsi:type="xsd:string">No GST</value>
    </values>
    <values>
        <field>Term_Days__c</field>
        <value xsi:type="xsd:double">30.0</value>
    </values>
    <values>
        <field>Term_Name__c</field>
        <value xsi:type="xsd:string">30 NET</value>
    </values>
    <values>
        <field>Transaction_Type__c</field>
        <value xsi:type="xsd:string">Bus</value>
    </values>
</CustomMetadata>
