({
	queryWaterway : function(component, event, helper) {
        console.log('Setting waterway');
        var params = event.getParam('arguments');
        if (params) {
            var lat = params.latitude;
            var lng = params.longitude;
            console.log('Latitude::' + lat);
            console.log('Longitude::' + lng);
            component.set("v.bWaterway",false);
            //component.set("v.bLoading",true);

            // add your code here
            var action = component.get("c.getWaterway");
            // hard code to get albertpark lake
            action.setParams({latitude: lat,longitude: lng});
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var objResp = response.getReturnValue();
                    var oWaterway = component.get("v.waterway");
                    console.log('objResp: '+JSON.stringify(objResp));
                    console.log('before if') ;
                    if(objResp === null) {
                        console.log('Response is null');
                        console.log("Waterway not found.");
	        			
                        //component.set("v.bLoading",false);
                        //Replaced ui:message with toast message
                        //component.set("v.bWaterway",true);
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            'type':'warning',
                            "title": "Warning!",
                            "message": 'Waterway not found for this location'
                        });
                        toastEvent.fire();
                        //component.set("v.bLoading",false);
                    } else {
                        console.log('objResp inside else::' + objResp);
                        oWaterway.Name = objResp.Name;
                        oWaterway.Id = objResp.Id;
                        component.set("v.waterway",oWaterway);
                        //component.set("v.bLoading",false);
                    } 
                    component.set("v.bLoading",false);
                    var compEvent = component.getEvent("waterwayLocationEvent");
                    compEvent.setParams({"waterway":oWaterway});
                    console.log("Firing waterwayLocationEvent");
                    compEvent.fire();

                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                'type':'error',
                                "title": "Error!",
                                "message": errors[0].message
                            });
                            toastEvent.fire();
                            component.set("v.bLoading",false);
                        }
                    } else {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            'type':'error',
                            "title": "Error!",
                            "message": 'Unknown error.'
                        });
                        toastEvent.fire();
                        component.set("v.bLoading",false);
                    }
                }
            });
            $A.enqueueAction(action);
        }
        //component.set("v.bLoading",false);
        
	}
})