({
    geoLocationGet : function(component) {
        //get the current position
        
        navigator.geolocation.getCurrentPosition(
            function(position){
                var lat = position.coords.latitude;
                var long = position.coords.longitude;
                var accuracy = position.coords.accuracy;
                
                component.set("v.bLoading",false);
                component.set("v.slat",lat);
                component.set("v.slong",long);
                component.set("v.saccuracy",accuracy);
                console.log("getCurrentPosition:"+lat+' '+long);
                var compEvent = component.getEvent("geoLocationEvent");
                compEvent.setParams({"sLat":lat,"sLong":long});
                compEvent.fire();
            },
            function(error){
                
                console.log('Error getCurrentPosition:'+error);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    'type':'error',
                    "title": "Error!",
                    "message": 'Error getting current location.'
                });
                toastEvent.fire();

            },{maximumAge:3000, timeout:5000, enableHighAccuracy: true}
            
        );
        component.set('v.bLoading', false);
        //component.set("v.watchId", watchId);
    }
})