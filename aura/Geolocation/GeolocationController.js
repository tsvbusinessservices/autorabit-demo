({
	doInit : function(component, event, helper) {
		var bHasResult = false;
		if (navigator.geolocation) {
			//check if GPs is allowed
            console.log("geolocation:"+navigator.geolocation);
            //component.set('v.bLoading', false);
			helper.geoLocationGet(component);
	    } else { 
	        console.log("Geolocation is not supported by this browser.");
            component.set("v.bLocation",true);
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                'type':'warning',
                "title": "Warning!",
                "message": 'Geolocation is not allowed or supported by this browser.'
            });
            toastEvent.fire();
            component.set('v.bLoading', false);
	    }
	},
    
    clearWatch :function(component, event, helper) {
        component.set("v.bLoading",true);
        component.set("v.slat","");
        component.set("v.slong","");
        component.set("v.saccuracy","");
        /*var watchId = cmp.get("v.watchId");
        if (navigator.geolocation && watchId) {
            navigator.geolocation.clearWatch(watchId);
        }*/
    },
    
    reload: function(component, event, helper) {
        component.set("v.bLoading",true);
        component.set("v.slat","");
        component.set("v.slong","");
        component.set("v.saccuracy","");
        helper.geoLocationGet(component);
    }
})