({
	doInit : function(component, event, helper) {
		var action = component.get("c.retrieveUserComplianceActivityDefault");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var objResp = response.getReturnValue();
                console.log('objResp: '+JSON.stringify(objResp));
                var ocompAct = component.get("v.compAct");
                //Map values
				ocompAct.Temperature__c = objResp.Temperature__c;
				ocompAct.Water_Conditions__c = objResp.Water_Conditions__c;
				ocompAct.Weather__c = objResp.Weather__c;
				ocompAct.Wind_Direction__c = objResp.Wind_Direction__c;
				ocompAct.Wind_Speed__c = objResp.Wind_Speed__c;
				component.set("v.compAct",ocompAct);
            }
        });
        $A.enqueueAction(action);
	},
	submitClick : function(component, event, helper) {
		var soComplianceActivityCS = {'sobjectType':'ComplianceActivityCS__c'};
		var ocompAct = component.get("v.compAct");
		//Map values
		soComplianceActivityCS.Temperature__c = ocompAct.Temperature__c;
		soComplianceActivityCS.Water_Conditions__c = ocompAct.Water_Conditions__c;
		soComplianceActivityCS.Weather__c = ocompAct.Weather__c;
		soComplianceActivityCS.Wind_Direction__c = ocompAct.Wind_Direction__c;
		soComplianceActivityCS.Wind_Speed__c = ocompAct.Wind_Speed__c;
		//Enqueue Action
		var action = component.get("c.setUserComplianceActivityDefault");
        action.setParams({ compAct : soComplianceActivityCS });
        action.setCallback(this, function(response) {
        	var toastEvent = $A.get("e.force:showToast");
            var state = response.getState();
            if (state === "SUCCESS") {
			    toastEvent.setParams({
			        "title": "Success!",
			        "message": "The record has been updated successfully."
			    });
			    //Navigate
		        component.set("v.bVisible",false);
            }else{
            	toastEvent.setParams({
			        "title": "Error!",
			        "message": "Detail: "+JSON.stringify(response.getError())
			    });
            	console.log(JSON.stringify(response.getError()));
            }
            toastEvent.fire();
        });
        $A.enqueueAction(action);
	}
})