({
    openClick : function(component, event, helper) {
		component.set("v.isOpen", true);
  	},
    
    closeClick : function(component, event, helper) {
		 component.set("v.isOpen", false);
	},

    submitClick : function(component, event, helper) {
        if(!helper.validateDegreesInput(component,"inputLatDegrees") 
           	|| !helper.validateDecimalInput(component, "inputLatDecimal")
          	|| !helper.validateDegreesInput(component, "inputLongDegrees") 
           || !helper.validateDecimalInput(component, "inputLongDecimal")) {
            return;
        }
        
		helper.setLangLat(component);
		component.find("caseRec").saveRecord(function(saveResult) {
			var resultsToast = $A.get("e.force:showToast");
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                
                resultsToast.setParams({
                    "title": "Saved",
                    "message": "The record was saved."
                });
                //Check SF1
                $A.get("e.force:closeQuickAction").fire();
                var recId = component.get("v.oCase.Id");
                var navEvt = $A.get("e.force:navigateToSObject");
			    navEvt.setParams({
			      "recordId": recId,
			      "slideDevName": "detail"
			    });
			    navEvt.fire();
                component.set("v.isOpen", false);
            } else {
                console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
                resultsToast.setParams({
                    "title": "Exception",
                    "message": JSON.stringify(saveResult.error)
                });
            }
            resultsToast.fire();
        });
	}
})