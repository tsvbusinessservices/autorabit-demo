({
	setLangLat : function(component) {
        var degreesVal = component.find("inputLatDegrees").get("v.value");        
        var decimalVal = component.find("inputLatDecimal").get("v.value");
        var lat = this.getDecValue(degreesVal, decimalVal);
        
        degreesVal = component.find("inputLongDegrees").get("v.value");        
        decimalVal = component.find("inputLongDecimal").get("v.value");
        var long = this.getDecValue(degreesVal, decimalVal);

        component.set("v.oCase.MALatitude__c",lat);

	},
    
    getDecValue:function(degreesVal, decimalVal) {
        if(degreesVal < 0) 
            return degreesVal - (decimalVal / 60.0);
        else 
            return degreesVal + (decimalVal / 60.0);            
    },
    
    validateDegreesInput:function(component, componentName) {
        var inputCmp = component.find(componentName);
        var value = inputCmp.get("v.value");

        // Is input numeric?
        if (isNaN(value)) {
            // Set error
            inputCmp.set("v.errors", [{message:"Input not a number: " + value}]);
            return false;
        } else if(value < -180 || value > 180){
            inputCmp.set("v.errors", [{message:"Input not a valid value: " + value}]);
            return false;
        } else {
            // Clear error
            inputCmp.set("v.errors", null);
        }
        return true;
    },
    
    validateDecimalInput:function(component, componentName) {
        var inputCmp = component.find(componentName);
        var value = inputCmp.get("v.value");

        // Is input numeric?
        if (isNaN(value)) {
            // Set error
            inputCmp.set("v.errors", [{message:"Input not a number: " + value}]);
            return false;
        } else if(value < 0 || value > 60){
            inputCmp.set("v.errors", [{message:"Input not a valid value: " + value}]);
            return false;
        } else {
            // Clear error
            inputCmp.set("v.errors", null);
        }
        return true;
    }
})