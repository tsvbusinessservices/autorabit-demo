({
    handleWaterwayLocatorEvent : function(component, event, helper) {
        console.log("Inside handleWaterwayLocatorEvent");
		//Set Params
		var waterway = event.getParam("waterway");
        console.log("Waterway ID::" + waterway.Id);
        component.set("v.rWaterwayId", waterway.Id);
        component.set("v.bshowSubmit",true);
	},
	handleGeoLocationEvent : function(component, event, helper) {
		//Set Params
		console.log('handleGeoLocationEvent: '+event.getParam("sLat")+' '+event.getParam("sLat"));
		component.set("v.rLat",event.getParam("sLat"));
        component.set("v.rLong",event.getParam("sLong"));
		var WWLocatorComp = component.find('WWLocator');
        WWLocatorComp.queryWaterway(event.getParam("sLat"), event.getParam("sLong"));
	},
	closeClick : function(component, event, helper) {
		// Close the action panel
		helper.clearComp(component);
        $A.get("e.force:closeQuickAction").fire();
	},
    updateClick: function(component, event, helper) {
		// Close the action panel
		var geoComp = component.find('geoComp');
        geoComp.updateLocation();
	},
	submitClick : function(component, event, helper) {
        console.log('WaterwayId before load::' + component.get("v.rWaterwayId"));
        component.set("v.bshowSubmit",false);
		component.set("v.oSObj.MALatitude__c",component.get("v.rLat"));
        component.set("v.oSObj.MALongitude__c",component.get("v.rLong"));
        component.set("v.oSObj.Waterway__c",component.get("v.rWaterwayId"));
        helper.clearComp(component);
		component.find("compActRec").saveRecord(function(saveResult) {
            //
			var resultsToast = $A.get("e.force:showToast");
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                
                resultsToast.setParams({
                    "title": "Saved",
                    "message": "The record was saved."
                });
                $A.get("e.force:closeQuickAction").fire();
                var recId = component.get("v.oSObj.Id");
                var navEvt = $A.get("e.force:navigateToSObject");
			    navEvt.setParams({
			      "recordId": recId,
			      "slideDevName": "detail"
			    });
			    navEvt.fire();
            } else {
                console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
                resultsToast.setParams({
                    "title": "Exception",
                    "message": JSON.stringify(saveResult.error)
                });
                component.set("v.bshowSubmit",true);
            }
            resultsToast.fire();
        });
	}
})