({
    doInit : function(component, event, helper) {
        console.log("Inside init handlers");
    },
    handleRecordUpdated : function(component, event, helper) {
        var sLat = component.get("v.oCase.MALatitude__c");
        var sLong = component.get("v.oCase.MALongitude__c");
        console.log("sLat::" + sLat);
        console.log("sLong::" + sLong);
        if(sLat && sLong) {
            var WWLocatorComp = component.find('WWLocator');
            if(WWLocatorComp) {
                WWLocatorComp.queryWaterway(sLat, sLong);
            } 
        } else {
            var wwCmp = component.find("WWLocator");
            wwCmp.set("v.bLoading", false);
            console.log("lat and long not found on the record");
            component.set("v.bRecLocation", true);
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                'type':'warning',
                "title": "Warning!",
                "message": 'Lat and Long is not available Incident record'
            });
            toastEvent.fire();
        }
	},
    handleWaterwayLocatorEvent : function(component, event, helper) {
		//Set Params
		var waterway = event.getParam("waterway");
        component.set("v.rWaterwayId", waterway.Id);
        component.set("v.bshowSubmit",true);
	},
	closeClick : function(component, event, helper) {
		// Close the action panel
		console.log('Clicked dismiss');
        $A.get("e.force:closeQuickAction").fire();
        var recId = component.get("v.oCase.Id");
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
          "recordId": recId,
          "slideDevName": "detail"
        });
        navEvt.fire();
        console.log('Closing the Open dialog');
        component.set("v.isOpen", false);
	},
	submitClick : function(component, event, helper) {
        var waterwayId = component.get("v.rWaterwayId");
        component.set("v.oCase.Location__c",component.get("v.rWaterwayId"));    
        component.set("v.oCase.Waterway_Verified__c",true);
		component.find("caseRec").saveRecord(function(saveResult) {
			var resultsToast = $A.get("e.force:showToast");
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                
                resultsToast.setParams({
                    "title": "Saved",
                    "message": "The record was saved."
                });
                //Check SF1
                $A.get("e.force:closeQuickAction").fire();
                var recId = component.get("v.oCase.Id");
                var navEvt = $A.get("e.force:navigateToSObject");
			    navEvt.setParams({
			      "recordId": recId,
			      "slideDevName": "detail"
			    });
			    navEvt.fire();
                component.set("v.isOpen", false);
            } else {
                console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
                resultsToast.setParams({
                    "title": "Exception",
                    "message": JSON.stringify(saveResult.error)
                });
            }
            resultsToast.fire();
        });
	}
})