public class TriggerSettingsClass {

	private Trigger_Setting__mdt setting;
	public static Boolean testOverride = null;

	private static TriggerSettingsClass instance = null;

	public TriggerSettingsClass (Trigger_Setting__mdt setting) {
		this.setting = setting;
	}

	public static TriggerSettingsClass getTriggerSetting(String TriggerName) {
		if(instance == null) {
			list<Trigger_Setting__mdt> configSetting = [Select DeveloperName,Trigger_On__c
														FROM Trigger_Setting__mdt
														WHERE DeveloperName = :TriggerName 
														LIMIT 1];
			if(configSetting.size() > 0){
				instance = new TriggerSettingsClass(configSetting[0]);
			}	
			else{
				instance = new TriggerSettingsClass(null);
			}								
			
		}
		return instance;
	}

	public Boolean isTriggerEnabled() {
		if(testOverride != null){
			return testOverride;
		}
		else{
			if(setting != null){
				return setting.Trigger_On__c;
			}
			else{
				return false;
			}
		}
	}
}