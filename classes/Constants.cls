public class  Constants {
    public static final String ACCREDITATION_TYPE_BUS_OPERATOR = 'Accredited';
    public static final String INVOICE_TYPE_BUS = 'Bus Annual Fee';

    public static final String INVOICE_RECORDTYPE = 'Invoice';    
    public static final String CREDITNOTE_RECORDTYPE = 'Credit Note';    

    public static final String ACCREDITATION_STATUS_ACCREDITED_BSA= 'Accredited';

    public static final String INVOICE_STATUS_DRAFT = 'Draft';
    public static final String INVOICE_STATUS_COMPLETE = 'Submitted';
    public static final String INVOICE_STATUS_SENT = 'Submitted';


    public static final String BILLING_SETTINGS_BUS = 'Bus';
    public static final String BILLING_SETTINGS_BUS_APPLICATION = 'Bus_Applications';
    public static final String BILLING_SETTINGS_MARITIME = 'Marine_Quals';    
    
    public static final String BILLING_CONFIG_BUS_APPLICATION_FEE = 'Bus_Accreditation_Application_Fee';
    public static final String BILLING_CONFIG_BUS_BASE_FEE = 'Bus_Base_Fee';
    public static final String BILLING_CONFIG_BUS_ADDITIONAL_FEE = 'Bus_Additional_Bus_Fee';    
    
    public static final String SECURITY_ARCONLINE= 'ArcGIS_Online';    

    //Sharing Constants
    public static final string USER_ROLE_TYPE_ROLE_SUB    = 'RoleAndSubordinates';
    public static final string PORTAL_TYPE_CUSTOMER       = 'CustomerPortal';
    public static final string SAR_ASSET    = 'SAR_Asset';
    public static final string SAR_ATON     = 'ATON';
    public static final string SAR_BUS      = 'Bus';
    public static final String EmailAddress = 'tims.project@transportsafety.vic.gov.au';  //replace with Custom Metadata
    public static final String CommunityProfileForTesting = 'TSV - Community - Bus Operator';
    public static final String SHARING_TYPE_ALL = 'ALL';
    public static final String SHARING_TYPE_ACR = 'ACR';
    /******************************************************
    BVProject Sharing Constants >>
    *******************************************************/
    //RecordType Id
    public static final String ACCOUNT_RT_ID_MARITIME   = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Maritime').getRecordTypeId();
    public static final String LOCATION_RT_ID_WATERWAY  = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Waterway').getRecordTypeId();
    public static final String LOCATION_RT_ID_FACILITY  = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Facility').getRecordTypeId();
    public static final String WATERWAY_ROLE_RT_ID_WATERWAY_MANAGER = Schema.SObjectType.Waterway_Role__c.getRecordTypeInfosByName().get('Waterway Manager').getRecordTypeId(); 
    public static final String ALLOCATION_RT_ID_FACILITY_GROUP = Schema.SObjectType.Allocation__c.getRecordTypeInfosByName().get('Facility Group').getRecordTypeId();
    public static final String ALLOCATION_RT_ID_WATERWAY_GROUP = Schema.SObjectType.Allocation__c.getRecordTypeInfosByName().get('Waterway Group').getRecordTypeId();
    public static final String ALLOCATION_RT_ID_GROUP = Schema.SObjectType.Allocation__c.getRecordTypeInfosByName().get('Group').getRecordTypeId();
    public static final String CONTENT_RT_ID_NOTIFICATION = Schema.SObjectType.Content__c.getRecordTypeInfosByName().get('Notification').getRecordTypeId();
    public static final String CONTENT_RT_ID_EDUCATIONAL_CONTENT = Schema.SObjectType.Content__c.getRecordTypeInfosByName().get('Educational Content').getRecordTypeId();
    public static final String CONTENT_RT_ID_FEATURED_STATISTICS = Schema.SObjectType.Content__c.getRecordTypeInfosByName().get('Featured Statistic').getRecordTypeId();

    public static final String PROFILE_BV_ADMIN = 'BV - Admin';
    public static final String PROFILE_SYSTEM_ADMIN = 'System Administrator';
    public static final String PERMISSION_SET_BV_ADMIN = 'BV Admin';
    public static final String ACCOUNT_TYPE_WATERWAY_MANAGER = 'Waterway Manager';
    public static final String ACCOUNT_TYPE_FACILITY_MANAGER = 'Facility Manager';
    public static final String ACCOUNT_TYPE_PORT_AUTHORITY = 'Port Authority';
    public static final String ALLOCATION_STATUS_FINALIZED = 'Finalized';
    public static final String ALLOCATION_STATUS_UNDER_DEVELOPMENT = 'Under Development';

    /******************************************************
    << BVProject Sharing Constants
    *******************************************************/
    public static final String STATUS_PUBLISHED = 'Published';
    public static final String STATUS_COMPLETED = 'Completed';
    public static final String STATUS_SERVED = 'Served';
    public static final String STATUS_CLOSED = 'Closed';
    public static final String STATUS_ISSUED = 'Issued';
    public static final String STATUS_UPHELD = 'Upheld';
    public static final String STATUS_OVERTURNED = 'Oveturned';
    public static final String STATUS_WITHDRAWN = 'Withdrawn';
    public static final String STATUS_RESPONSE_RECEIVED = 'Response Received';


    /******************************************************
    << RecordTypeID Constants
    *******************************************************/

    public static final String BUS_INFRINGEMENT_RECORDTYPE_ID = Schema.SObjectType.Outcome__c.getRecordTypeInfosByName().get('Bus - Infringement').getRecordTypeId();
    public static final String MARINE_INFRINGEMENT_RECORDTYPE_ID = Schema.SObjectType.Outcome__c.getRecordTypeInfosByName().get('Maritime - Infringement Notice').getRecordTypeId();
    public static final String POLICE_INFRINGEMENT_RECORDTYPE_ID = Schema.SObjectType.Outcome__c.getRecordTypeInfosByName().get('VicPol - Infringement Notice').getRecordTypeId();

    public static final String MARINE_INCIDENT_RECORDTYPE_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Maritime - Incident').getRecordTypeId();
    
    public static final String COMPLIANCE_ACTIVITY_RTYPEID_BUS_STD = Schema.SObjectType.Compliance_Activity__c.getRecordTypeInfosByDeveloperName().get('Bus_Compliance_Inspection_Standard').getRecordTypeId();
    
    public static final String ASSET_RAMP_RECORDTYPE_ID = Schema.SObjectType.Asset__c.getRecordTypeInfosByDeveloperName().get('Ramp').getRecordTypeId();    

    public static final String BUS_AUDIT_ACCREDITED_OPERATOR = 'Bus_Audit_Accredited_Operator';
    public static final String BUS_AUDIT_REGISTERED_OPERATOR = 'Bus_Audit_Registered_Operator';
    public static final String BUS_INSPECTION_QUICK_CHECK = 'Bus_Compliance_Inspection_Quick_Check';
    public static final String BUS_INSPECTION_STANDARD = 'Bus_Compliance_Inspection_Standard';
    public static final String BUS_INSPECTION_HAZARDOUS = 'Bus_Compliance_Inspection_Hazardous_Area';    
    public static final String MARITIME_BAE_AUDIT = 'Maritime_BAE_Audit';
    public static final String VESSEL_INSPECTION = 'Vessel_Inspection';
    public static final String VESSEL_INSPECTION_UNREGISTERED = 'Vessel_Inspection_Unregistered';
    public static final String MARITIME_AUDIT_WWAY = 'Maritime_Audit_Wway';

    public static final String BUS_DISCIPLINARY_ACTION = 'Bus_Disciplinary_Action';
    public static final String BUS_IMPROVEMENT_NOTICE = 'Bus_Improvement_Notice';
    public static final String BUS_INFRINGEMENT = 'Bus_Infringement';
    public static final String BUS_NCR_ACCREDITED_OPERATOR = 'Bus_NCR_Accredited_Operator';
    public static final String BUS_NCR_REGISTERED_OPERATOR = 'Bus_NCR_Registered_Operator';
    public static final String BUS_PROHIBITION_NOTICE = 'Bus_Prohibition_Notice';
    public static final String BUS_SAFETY_ALERT = 'Bus_Safety_Alert';
    public static final String CFF = 'CFF';
    public static final String INFRINGEMENT_NOTICE = 'Infringement_Notice';
    public static final String MARITIME_AUDIT_ITEM = 'Maritime_Audit_Item';
    public static final String MARITIME_BAE_ITEM = 'Maritime_BAE_Item';
    public static final String MARITIME_EMBARGO_NOTICE = 'Maritime_Embargo_Notice';
    public static final String MARITIME_IMPROVEMENT_NOTICE = 'Maritime_Improvement_Notice';
    public static final String MARITIME_PROHIBITION_NOTICE = 'Maritime_Prohibition_Notice';
    public static final String MARITIME_SAFETY_COMMUNICATION = 'Maritime_Safety_Communication';
    public static final String OBSERVATION_FINDING = 'Observation_Finding';
    public static final String VOZR_REVIEW = 'VOZR_Review';

    public static final String ORG_TSV = 'TSV';
    public static final String ORG_VIC_POLICE = 'VIC-POLICE';

    /******************************************************************     
        Constants for Boating Victoria Community Module
    ******************************************************************/

    //Profiles
    public static final String PROFILE_FACILITY_MANAGER = 'BV - Community - Facility Manager';
    public static final String PROFILE_WATERWAY_MANAGER = 'BV - Community - Waterway Manager';


    //PermissionSet
    public static final String PERMISSION_SET_BV_Community_Waterway_Manager = 'BV_Community_Waterway_Manager';
    public static final String PERMISSION_SET_BV_Community_Facility_Manager = 'BV_Community_Facility_Manager';
    public static final String PERMISSION_SET_BV_Community_Facility_Under_Waterway_Manager = 'BV_Community_Facility_Under_Waterway_Manager';
    

}