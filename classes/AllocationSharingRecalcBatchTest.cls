@isTest
private class AllocationSharingRecalcBatchTest {
	
	
	@isTest static void test_Scedhuling() {
		AllocationSharingRecalcBatch batch = new AllocationSharingRecalcBatch();
		String sch = '0 0 10 * * ?';
		system.schedule('Recalc test 12345', sch, batch);
	}

	//P - Run Recalc job - locations
	@isTest static void PositiveTest_RunRecalcJob() {
		Id portalAccountId1 = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;

		list<Location__c> locations =  TestDataFactory.createMultipleLocation(100, portalAccountId1, Constants.LOCATION_RT_ID_WATERWAY);
		insert locations;

        List<Waterway_Role__c> waterwayroles =  new List<Waterway_Role__c>();
        for(Location__c location : locations){
            Waterway_Role__c wwr1 = TestDataFactory.createWaterwayRole(portalAccountId1, location.id);
            waterwayroles.add(wwr1);
        }
        insert waterwayroles;
        
        List<Allocation__c> parentgroups = new List<Allocation__c>();
        for(Integer i = 0; i < 100; i++){            
            Allocation__c alocGroup = TestDataFactory.createGroup();
            alocGroup.Group_Type__c = 'Waterway';
            parentgroups.add(alocGroup);
        }
        insert parentgroups;
        
		map<Id, Allocation__c> allocationsMap = new map<Id, Allocation__c>(parentgroups);

        List<Allocation__c> waterwayGroups = new List<Allocation__c>();
        for(Integer i = 0; i < 100; i++){  
            Allocation__c alocWaterwayGroup = TestDataFactory.createLocationGroup(Constants.ALLOCATION_RT_ID_WATERWAY_GROUP);
            alocWaterwayGroup.Waterway_Name__c = locations.get(i).id;
            alocWaterwayGroup.Group__c = parentgroups.get(i).id;
            waterwayGroups.add(alocWaterwayGroup);
        }
        insert waterwayGroups;

		test.startTest();
		
		AllocationSharingRecalcBatch b = new AllocationSharingRecalcBatch();
		database.executeBatch(b, 200);

		test.stopTest();

		//check that there has been a sharing record created
		List<Allocation__Share> alocSharing = AllocationHelper.getAllocSharing(allocationsMap.keySet(), new set<String>{Allocation__Share.RowCause.Secondary_Waterway_Manager__c, Allocation__Share.RowCause.Parent_Waterway_Manager__c});
		system.assertEquals(100, alocSharing.size());
	}

	@testSetup static void testSetup(){
		Id maritime = Constants.ACCOUNT_RT_ID_MARITIME;
		String waterwayMan = Constants.ACCOUNT_TYPE_WATERWAY_MANAGER;
		TestDataFactory.createPartnerAccount(new List<Id>{maritime},new List<String>{waterwayMan});
	}
}