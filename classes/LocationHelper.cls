public without sharing class LocationHelper {

    /**
    * Gets Information to supplement the Location using a query for Batch
    * @return null
    */
    public static string getLocationForSharingRecalc(){
        return 'SELECT Id, Manager__c, Location__c, RecordTypeId FROM Location__c';
    }

    public static List<Location__Share> getLocSharing(set<Id> locIds, Set<String> rowCause){
        return [SELECT Id, UserOrGroupId FROM Location__Share WHERE ParentId IN :locIds AND RowCause IN :rowCause];
    }

    
    public static Map<Id,List<Location__Share>> getMapOfParentLocationToSharing(Set<Id> parentLoactionIds){
        Map<Id,List<Location__Share>> mapOfParentLocationToSharing = new Map<Id,List<Location__Share>>();        

        //Get all locationShares
        Set<String> rowCauses = new Set<String>{Schema.Location__Share.RowCause.Secondary_Waterway_Manager__c};
        List<Location__Share> locationShares = [SELECT Id, UserOrGroupId, RowCause, AccessLevel, ParentId FROM Location__Share WHERE RowCause IN :rowCauses AND ParentId IN :parentLoactionIds];
        
        //create a map of location to the sharing.   
        for(Location__Share locShare : locationShares){
            //If the location already exists in the map then add to the account list
            if(mapOfParentLocationToSharing.ContainsKey(locShare.ParentId)){
                List<Location__Share> sharings = mapOfParentLocationToSharing.get(locShare.ParentId);
                if(sharings != null){                    
                    sharings.add(locShare);
                } 
            }
            //else create new location set name list
            else{     
                mapOfParentLocationToSharing.put(locShare.ParentId, new List<Location__Share>{locShare});
            }            
        }

        return mapOfParentLocationToSharing;
	} 
}