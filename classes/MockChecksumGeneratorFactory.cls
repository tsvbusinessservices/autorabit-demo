@isTest
public class MockChecksumGeneratorFactory implements ChecksumGeneratorFactory {


	public ChecksumGenerator getChecksumGenerator(String algorithm){
		return new MockChecksumGenerator(algorithm);
	}
}