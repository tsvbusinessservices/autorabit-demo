@isTest
private class WaterwayRoleSharingRecalcBatchTest {
    
	@isTest static void test_Scedhuling() {
		WaterwayRoleSharingRecalcBatch batch = new WaterwayRoleSharingRecalcBatch();
		String sch = '0 0 10 * * ?';
		system.schedule('Recalc test 12345', sch, batch);
	}

	//P - Run Recalc job - locations
	@isTest static void PositiveTest_RunRecalcJob() {
		Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;

		list<Location__c> locations =  TestDataFactory.createMultipleLocation(200, portalAccountId, Constants.LOCATION_RT_ID_WATERWAY);
		insert locations;
		map<Id, Location__c> locationsMap = new map<Id, Location__c>(locations);

        List<Waterway_Role__c> waterwayroles =  new List<Waterway_Role__c>();
        for(Location__c location : locations){
            Waterway_Role__c wwr1 = TestDataFactory.createWaterwayRole(portalAccountId, location.id);
            waterwayroles.add(wwr1);
        }
        insert waterwayroles;

		test.startTest();
		
		WaterwayRoleSharingRecalcBatch b = new WaterwayRoleSharingRecalcBatch();
		database.executeBatch(b, 200);

		test.stopTest();

		//check that there has been a sharing record created
		List<Location__Share> locSharing = LocationHelper.getLocSharing(locationsMap.keySet(), new set<String>{Location__Share.RowCause.Secondary_Waterway_Manager__c});
		system.assertEquals(200, locSharing.size());
	}

	@testSetup static void testSetup(){
		TestDataFactory.createPartnerAccount(new List<Id>{Constants.ACCOUNT_RT_ID_MARITIME},new List<String>{Constants.ACCOUNT_TYPE_WATERWAY_MANAGER});
	}
}