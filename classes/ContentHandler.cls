/**
 * ContentHandler class. creates Apex sharing rules for the Location
 *
 * @author Arnie Ug
 * @date 29.May.2019
 */

 public with sharing class ContentHandler extends TriggerBase implements ITriggerBase {
    public ContentHandler() {

    }

    /**
     * beforeInsert Trigger Event
     * @return null
     */
    public override void beforeInsert() {
        confirmDocuments(records, 'Insert');
    }
    
    /**
     * afterInsert Trigger Event
     * @return null
     */
    public override void afterInsert() {
        contentSharing(records, null, 'Insert');
    }
    
    /**
     * beforeUpdate Trigger Event    
     * @return null
     */
    public override void beforeUpdate() {
        confirmDocuments(records, 'Update');
    }

    /**
     * afterUpdate Trigger Event    
     * @return null
     */
    public override void afterUpdate() {
        contentSharing(records, oldmap, 'Update');
    }

    /**
     * Controlls the Apex sharing against the Content Object   
     * @param mode = 'Insert', 'Update', 'Recalculation'
     * @return null
     */
    public static void contentSharing(list<SObject> records, Map<Id, SObject> oldMap, String mode){
        //Get an instance of the SharingHelper class with the Object Name
        SharingHelper SharingHelp = new SharingHelper(Content__Share.sObjectType.getDescribe().getName());

        //Get all accounts under waterway and facility
        //Initial map to remove duplicate SOQLs
        Map<Id,List<Allocation__Share>> mapOfGroupIdToShares = ContentHelper.getGroupIdstoSharingMap(records);

        //Divide the records into 2 categories
        Map<Id,List<Allocation__Share>> mapOfContentIdToFacilityGroupShares = ContentHelper.getContentIdtoFacilityGroupSharingMap(records, mapOfGroupIdToShares);
        Map<Id,List<Allocation__Share>> mapOfContentIdToWaterwayGroupShares = ContentHelper.getContentIdtoWaterwayGroupSharingMap(records, mapOfGroupIdToShares);

        //loop round and add the Id and the Portal account Id
        for(Content__c Content : (list<Content__c>) records){
            
            //get old Location - only applicable if running in Update mode
            Content__c oldContent = null;
            if(mode == 'Update' && oldMap != null){
                oldContent = (Content__c) oldMap.get(Content.Id);
            }
            
			if(Content.RecordTypeId == Constants.CONTENT_RT_ID_NOTIFICATION || Content.RecordTypeId == Constants.CONTENT_RT_ID_EDUCATIONAL_CONTENT ||  Content.RecordTypeId == Constants.CONTENT_RT_ID_FEATURED_STATISTICS ){
                //if new or changed or Recalculate then add the new Account to the sharing
                if(mode == 'Insert' || mode == 'Recalculation' || Content.Group_of_Waterway__c != oldContent.Group_of_Waterway__c || Content.Group_of_Facility__c != oldContent.Group_of_Facility__c){

                    if(mapOfContentIdToFacilityGroupShares != null && !mapOfContentIdToFacilityGroupShares.isEmpty() && mapOfContentIdToFacilityGroupShares.get(Content.Id) != null){
                        for(Allocation__Share facilityShare : mapOfContentIdToFacilityGroupShares.get(Content.Id)){
                            SharingHelp.addToClonedSharing(Content.Id, facilityShare.RowCause, facilityShare.UserOrGroupId, 'Edit');                    
                        } 
                    }

                    if(mapOfContentIdToWaterwayGroupShares != null && !mapOfContentIdToWaterwayGroupShares.isEmpty() && mapOfContentIdToWaterwayGroupShares.get(Content.Id) != null){
                        for(Allocation__Share waterwayShare : mapOfContentIdToWaterwayGroupShares.get(Content.Id)){
                            SharingHelp.addToClonedSharing(Content.Id, waterwayShare.RowCause, waterwayShare.UserOrGroupId, 'Edit');                      
                        } 
                    }                    

                    //if changed OR Recalculation mode than delete the old one
                    if(mode == 'Recalculation' || (mode == 'Update' && (Content.Group_of_Waterway__c != oldContent.Group_of_Waterway__c || Content.Group_of_Facility__c != oldContent.Group_of_Facility__c))){
                        SharingHelp.addToDeleteList(Content.Id);
                    }
                }
            }
        }


        //delete any sharing rules that need to be deleted
        SharingHelp.deleteSharingRules(new set<String> {Schema.Content__Share.RowCause.Facility_Manager__c,Schema.Content__Share.RowCause.Secondary_Waterway_Manager__c, Schema.Content__Share.RowCause.Parent_Waterway_Manager__c});

        //Insert Cloned Sharing rules
        SharingHelp.cloneApexSharing_Asyncronously();
    }

	private static void confirmDocuments(List<SObject> records, String mode){
        List<Content__c> educationalContents = new List<Content__c>();
        Set<Id> educContentIds = new Set<Id>();

        //get all educationalContents
        for(Content__c Content : (list<Content__c>) records){
            if(Content.RecordTypeId == Constants.CONTENT_RT_ID_EDUCATIONAL_CONTENT &&  (Content.Status__c == 'Active' || Content.Status__c == 'Pending Approval')){
                educationalContents.add(Content);
                educContentIds.add(Content.Id);
            }
        }

        if(mode == 'Update'){
            if(!educationalContents.isEmpty()){
                Map<Id,Integer> mapOfContentIdToCountofDocuments = ContentHelper.getContentIdtoCountofDocumentsMap(educContentIds);
                for(Content__c educContent : educationalContents){
                    if(mapOfContentIdToCountofDocuments.get(educContent.Id) != 1){
                        educContent.addError('Please add 1 and only 1 image file.');
                    }
                }
            }
        }else if(mode == 'Insert'){
            for(Content__c educContent : educationalContents){
                educContent.addError('You cannot create an active educational content. Please create a draft educational content, add an image file and activate the content afterwards.');
            }
        }
	} 
}