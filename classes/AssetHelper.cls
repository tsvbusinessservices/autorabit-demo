public with sharing class AssetHelper {

	/**
	 * Gets the record types for the Assets into a map
	 * @return null
	 */
	public static map<String,ID> AssetRTMap{
		get{
			if(AssetRTMap == null){
				AssetRTMap = Utility.getRecordTypeMap('Asset__c');
			}
			return AssetRTMap;
		}
		set;
	}

	/**
	* Gets Information to supplement the Assets using a query	
	* @return null
	*/
	public static map<Id, Asset__c> getEnrichedAssetData(set<Id> assetIds){
		return new map<Id, Asset__c>([SELECT Id, Location__r.Manager__c, Owner__c, RecordTypeId, ESRI_ID__c
										FROM Asset__c 
										WHERE Id IN : assetIds]);
	}

	/**
	* Gets Information to supplement the Assets using a query for Batch
	* @return null
	*/
	public static string getEnrichedAssetDataForSharingRecalc(){
		return 'SELECT Id FROM Asset__c WHERE RecordType.DeveloperName IN :ASSET_SHARING_SET';
	}

	public static List<Asset__Share> getAssetSharing(set<Id> assetIds, Set<String> rowCause){
		return [SELECT Id, UserOrGroupId FROM Asset__Share WHERE ParentId IN :assetIds AND RowCause IN :rowCause];
	}


}