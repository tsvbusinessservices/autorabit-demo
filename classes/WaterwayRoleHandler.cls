/**
 * WaterwayRoleHandler class. creates Apex sharing rules for the WaterwayRole
 *
 * @author Arnie Ug
 * @date 27.May.2018
 */


public with sharing class WaterwayRoleHandler extends TriggerBase implements ITriggerBase{
    public WaterwayRoleHandler() {

    }

    /**
     * afterInsert Trigger Event
     * @return null
     */
    public override void afterInsert() {
        waterwayRoleSharing(records, null, 'Insert');
    }
    /**
     * afterUpdate Trigger Event    
     * @return null
     */
    public override void afterUpdate() {
        waterwayRoleSharing(records, oldmap, 'Recalculation');
    }    
    /**
     * afterDelete Trigger Event    
     * @return null
     */
    public override void afterDelete() {
        waterwayRoleSharing(oldRecords, null, 'Recalculation');
    }

    /**
     * Controlls the Apex sharing against the Location Object   
     * @param mode = 'Insert', 'Recalculation'
     * @return null
     */
    public static void waterwayRoleSharing(list<SObject> records, Map<Id, SObject> oldMap, String mode){
        //Get an instance of the SharingHelper class with the Object Name
        SharingHelper SharingHelp = new SharingHelper(Location__Share.sObjectType.getDescribe().getName());

        //create a full set of waterwayroles so that recalculations will be accurate
        //also filter only Waterway Manager Record Types
        Set<Id> waterwayIds = new Set<Id>();
        for(Waterway_Role__c WaterwayRole : (List<Waterway_Role__c>) records){      
            System.debug('************** WaterwayRole' + WaterwayRole );
            System.debug('************** WaterwayRole.Waterway__c' + WaterwayRole.Waterway__c );
            if(WaterwayRole.RecordTypeId == Constants.WATERWAY_ROLE_RT_ID_WATERWAY_MANAGER){
                waterwayIds.add(WaterwayRole.Waterway__c);
            }
        }                

        List<Waterway_Role__c> waterwayRoles = new List<Waterway_Role__c>();
        List<Location__c> facilityList = new List<Location__c>();
        if(waterwayIds.size() > 0){
            //Get all waterwayRoles to reevaluate all sharing
            waterwayRoles = [SELECT Id, Account__c, Waterway__c, RecordTypeId FROM Waterway_Role__c WHERE Waterway__c IN :waterwayIds AND RecordTypeID = :Constants.WATERWAY_ROLE_RT_ID_WATERWAY_MANAGER];
            //For Facility Read Sharing - Get a map of facilities under a waterways
            facilityList =  [SELECT Id, Waterway_Name__c FROM Location__c WHERE Waterway_Name__c IN :waterwayIds];
        }
        
        Map<Id, List<Waterway_Role__c>> mapOfWaterwayIdsToWaterwayRoles = WaterwayRoleHelper.mapWaterwayIdsWithWaterwayRoles(waterwayIds,waterwayRoles);
        Map<Id, List<Location__c>> mapOfWaterwayIdsToFacilities = WaterwayRoleHelper.mapWaterwayIdsWithFacilities(facilityList);
        
        System.debug('************** waterwayRoles' + waterwayRoles );
        System.debug('************** facilityList' + facilityList );
        //loop round and add the Id and the Portal account Id
        for(Id waterwayId : waterwayIds){
            if(!mapOfWaterwayIdsToWaterwayRoles.get(waterwayId).isEmpty()){
                for(Waterway_Role__c WaterwayRole : mapOfWaterwayIdsToWaterwayRoles.get(waterwayId)){            
                    //For community profiles' waterway edit access and facility read access, if new or changed or Recalculate then add the new Account to the sharing
                    if(mode == 'Insert' || mode == 'Recalculation'){
                        
                        System.debug('************** WaterwayRole.Account__c' + WaterwayRole.Account__c );
                        System.debug('************** WaterwayRole.Waterway__c' + WaterwayRole.Waterway__c );
                        //Share the waterway edit access
                        SharingHelp.addToSharing(WaterwayRole.Account__c, WaterwayRole.Waterway__c, Schema.Location__Share.RowCause.Secondary_Waterway_Manager__c, 'Edit');
                    
                        //if changed OR Recalculation mode than delete the permissions
                        if(mode == 'Recalculation'){
                            SharingHelp.addToDeleteList(WaterwayRole.Waterway__c);
                        }

                        //Share the facility read access
                        if(mapOfWaterwayIdsToFacilities != null && !mapOfWaterwayIdsToFacilities.isEmpty() && mapOfWaterwayIdsToFacilities.get(WaterwayRole.Waterway__c) != null){
                          system.debug('Check here..'+WaterwayRole.Id);
                          system.debug('Check here..'+WaterwayRole.Waterway__c);
                          system.debug('Check here..'+mapOfWaterwayIdsToFacilities);
                            for(Location__c facility : mapOfWaterwayIdsToFacilities.get(WaterwayRole.Waterway__c)){
                                System.debug('************** facility.Id' + facility.Id );
                                SharingHelp.addToSharing(WaterwayRole.Account__c, facility.Id, Schema.Location__Share.RowCause.Secondary_Waterway_Manager__c, 'Read');                    
                                
                                //if changed OR Recalculation mode than delete the permissions
                                if(mode == 'Recalculation'){
                                    SharingHelp.addToDeleteList(facility.Id);
                                }                    
                            }
                        }
                    }                    
                }
            }else{
                System.debug('************** waterwayId ' + waterwayId );
                SharingHelp.addToDeleteList(waterwayId);

                //Delete the facility read access
                if(mapOfWaterwayIdsToFacilities != null && !mapOfWaterwayIdsToFacilities.isEmpty()){
                    for(Location__c facility : mapOfWaterwayIdsToFacilities.get(waterwayId)){
                System.debug('************** facility.id ' + facility.id );
                        SharingHelp.addToDeleteList(facility.Id);                                        
                    }
                }
            }
        }


        //delete any sharing rules that need to be deleted
        SharingHelp.deleteSharingRules(new set<String> {Schema.Location__Share.RowCause.Secondary_Waterway_Manager__c});

        //Insert Sharing rules
        if(Test.isRunningTest()){
            SharingHelp.insertApexSharing_Syncronously();
        }else{
            SharingHelp.insertApexSharing_Asyncronously();
        }
    }
}