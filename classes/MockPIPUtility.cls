@isTest
public class MockPIPUtility implements CheckPointInPolygonUtility
{

	public String location = null;

	public MockPIPUtility(String locationId) {
		this.location = locationId;
	}

	public String checkPointInPolygon(Decimal xlat, Decimal xlong) {
		return location;
	}
	
}