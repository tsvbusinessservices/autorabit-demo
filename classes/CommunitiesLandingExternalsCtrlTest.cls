/**
 * An apex page controller that takes the user to the right start page based on credentials or lack thereof
 * Made for external user communities page
 */
@IsTest public with sharing class CommunitiesLandingExternalsCtrlTest {
    @IsTest(SeeAllData=true) public static void testCommunitiesLandingExternalsController() {
        // Instantiate a new controller with all parameters in the page
        CommunitiesLandingExternalsController controller = new CommunitiesLandingExternalsController();        
        System.assert(controller.forwardToStartPage() != null);          
      }
}