public class OutcomeHelper {

	/**
	 * Gets the record types for the Outcome into a map
	 * @return null
	 */
	public static map<ID, String> OutcomeRTMap{
		get{
			if(OutcomeRTMap == null){
				OutcomeRTMap = Utility.getRecordTypeMapById('Outcome__c');
			}
			return OutcomeRTMap;
		}
		set;
	}

	/**
	 * Creates the Status Aware Shared records for the Outcome record Types 
	 * @return null
	 */
	public static Map<String, StatusAwareShare> SharedOutcome{
		get{
			if(SharedOutcome == null){
				SharedOutcome = new Map<String, StatusAwareShare>();
				SharedOutcome.put(Constants.BUS_DISCIPLINARY_ACTION, new StatusAwareShare(Constants.BUS_DISCIPLINARY_ACTION, new Set<String>{Constants.STATUS_PUBLISHED, Constants.STATUS_CLOSED}, Constants.SHARING_TYPE_ACR));
				SharedOutcome.put(Constants.BUS_IMPROVEMENT_NOTICE, new StatusAwareShare(Constants.BUS_IMPROVEMENT_NOTICE, new Set<String>{Constants.STATUS_PUBLISHED, Constants.STATUS_RESPONSE_RECEIVED, Constants.STATUS_CLOSED}, Constants.SHARING_TYPE_ACR));
				SharedOutcome.put(Constants.BUS_INFRINGEMENT, new StatusAwareShare(Constants.BUS_INFRINGEMENT, new Set<String>{Constants.STATUS_ISSUED, Constants.STATUS_UPHELD, Constants.STATUS_OVERTURNED, Constants.STATUS_WITHDRAWN},Constants.SHARING_TYPE_ACR));
				SharedOutcome.put(Constants.BUS_NCR_ACCREDITED_OPERATOR, new StatusAwareShare(Constants.BUS_NCR_ACCREDITED_OPERATOR, new Set<String>{Constants.STATUS_PUBLISHED}, Constants.SHARING_TYPE_ACR));
				SharedOutcome.put(Constants.BUS_NCR_REGISTERED_OPERATOR, new StatusAwareShare(Constants.BUS_NCR_REGISTERED_OPERATOR, new Set<String>{Constants.STATUS_PUBLISHED}, Constants.SHARING_TYPE_ACR));
				SharedOutcome.put(Constants.BUS_PROHIBITION_NOTICE, new StatusAwareShare(Constants.BUS_PROHIBITION_NOTICE, new Set<String>{Constants.STATUS_PUBLISHED, Constants.STATUS_RESPONSE_RECEIVED, Constants.STATUS_CLOSED}, Constants.SHARING_TYPE_ACR));
				SharedOutcome.put(Constants.MARITIME_AUDIT_ITEM ,new StatusAwareShare(Constants.MARITIME_AUDIT_ITEM, new Set<String>{Constants.STATUS_COMPLETED}));
				SharedOutcome.put(Constants.MARITIME_BAE_ITEM ,new StatusAwareShare(Constants.MARITIME_BAE_ITEM, new Set<String>{Constants.STATUS_COMPLETED}));
			}
			return SharedOutcome;
		}
		set;
	}

	public static List<Outcome__c> getEnrichedData(Set<Id> theOutcomeIds) {
		return [Select ID, Account__c, Status__c, Compliance_Activity__r.Status__c, RecordTypeId, RecordType.DeveloperName
					From Outcome__c
					Where Id IN :theOutcomeIds];
	}


	public static boolean isPoliceInfrigementChanged(Outcome__c newRecord, Outcome__c oldRecord) {
		return newRecord.Police_Infringement__c != oldRecord.Police_Infringement__c;
	}

	public static boolean isInfringementNumberChanged(Outcome__c newRecord, Outcome__c oldRecord) {
		return newRecord.Infringement_Auto_Number__c != oldRecord.Infringement_Auto_Number__c;
	}

	public static boolean isInfringementRecordType(Id recordTypeId) {
		if(recordTypeId == Constants.BUS_INFRINGEMENT_RECORDTYPE_ID || 
				recordTypeId == Constants.MARINE_INFRINGEMENT_RECORDTYPE_ID ||
          		recordTypeId == Constants.POLICE_INFRINGEMENT_RECORDTYPE_ID)
			return true;
		return false;
	}

	public static StatusAwareShare getShareRecord(Outcome__c rec) {
		
		String rtName = OutcomeRTMap.get(rec.recordTypeId);
		System.debug('rtName::' + rtName + ' recordTypeId::' + rec.recordTypeId);
		return SharedOutcome.get(rtName);
	}

	public static String getStatus(Outcome__c outcome) {
		if(outcome.RecordType.DeveloperName == Constants.MARITIME_BAE_AUDIT || outcome.RecordType.DeveloperName == Constants.MARITIME_AUDIT_WWAY)
			return outcome.Compliance_Activity__r.Status__c;
		return outcome.Status__c;
	}

}