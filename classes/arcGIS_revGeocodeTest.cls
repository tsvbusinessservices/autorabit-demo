@isTest
Private class arcGIS_revGeocodeTest {


 @isTest static void reverseGeocode_ComplianceTest() {
 
   //get test complianceActivity recordtype ID
   Id CARecordType = Constants.COMPLIANCE_ACTIVITY_RTYPEID_BUS_STD;

    Decimal testLat = -37.81508849838504;
    Decimal testLong = 144.97439002710632;
    
    //retrieve a compliance activity record
    Compliance_Activity__c initRecord = TestDataFactory.createComplianceActivityBus(CARecordType, testLat, testLong) ;
    
    System.assertEquals(testLat, initRecord.MALatitude__c);
    System.assertEquals(testLong, initRecord.MALongitude__c);
     
    Test.startTest();
     
     insert new List<Compliance_Activity__c>{initRecord};
     List<ID> testIDs = new List<ID>();
     testIDs.add(initRecord.Id);
     
    //Pass record through reverseGeocode
    arcGIS_revGeocode.reverseGeocode(testIDs);
    Test.stopTest();
    
    List<Compliance_Activity__c> results = [Select Id, Waterway__c, Location_Description__c From Compliance_Activity__c
                                Where id =: initRecord.id];
    System.assertEquals('1 Spring St, Melbourne, Victoria, 3000',results[0].Location_Description__c);    
 }
 
 @isTest static void reverseGeocode_CaseTest() {
 
   //get test recordtype ID
   Id RecordType = Constants.MARINE_INCIDENT_RECORDTYPE_ID;

    Decimal testLat = -37.81508849838504;
    Decimal testLong = 144.97439002710632;
    
    //retrieve a compliance activity record
    Case initRecord = TestDataFactory.createIncidentCase(RecordType, testLat, testLong) ;
    
    System.assertEquals(testLat, initRecord.MALatitude__c);
    System.assertEquals(testLong, initRecord.MALongitude__c);
     
    Test.startTest();
     
     insert new List<Case>{initRecord};
     List<ID> testIDs = new List<ID>();
     testIDs.add(initRecord.Id);
     
    //Pass record through reverseGeocode
    arcGIS_revGeocode.reverseGeocode(testIDs);
    Test.stopTest();
    
    List<Case> results = [Select Id, Location__c, Location_Description__c From Case
                                Where id =: initRecord.id];
    System.assertEquals('1 Spring St, Melbourne, Victoria, 3000',results[0].Location_Description__c);    
 }
 
 @isTest static void reverseGeocode_AssetTest() {
 
   //get test recordtype ID
   Id RecordType = Constants.ASSET_RAMP_RECORDTYPE_ID;

    Decimal testLat = -37.81508849838504;
    Decimal testLong = 144.97439002710632;
    
    //retrieve a compliance activity record
    Asset__c initRecord = TestDataFactory.createAssetLocation(RecordType, testLat, testLong) ;
    
    System.assertEquals(testLat, initRecord.MALatitude__c);
    System.assertEquals(testLong, initRecord.MALongitude__c);
     
    Test.startTest();
     
     insert new List<Asset__c>{initRecord};
     List<ID> testIDs = new List<ID>();
     testIDs.add(initRecord.Id);
     
    //Pass record through reverseGeocode
    arcGIS_revGeocode.reverseGeocode(testIDs);
    Test.stopTest();
    
    List<Asset__c> results = [Select Id, Location__c, Location_Description__c From Asset__c
                                Where id =: initRecord.id];
    System.assertEquals('1 Spring St, Melbourne, Victoria, 3000',results[0].Location_Description__c);    
 } 
 
  @isTest static void reverseGeocode_writeAddress() {
     Id CARecordType = Constants.COMPLIANCE_ACTIVITY_RTYPEID_BUS_STD;

      Decimal testLat = -37.81508849838504;
      Decimal testLong = 144.97439002710632;
      String testAddress = '1 Spring St, Melbourne, Victoria, 3000';
    
    //retrieve a compliance activity record
    Compliance_Activity__c initRecord = TestDataFactory.createComplianceActivityBus(CARecordType, testLat, testLong) ;
    
     Test.startTest();
     insert new List<Compliance_Activity__c>{initRecord};
     Id testIDs = initRecord.Id;
     
    //Pass record through reverseGeocode
    arcGIS_revGeocode.writeAddress(testIDs, 'Compliance_activity__c',testAddress);
    Test.stopTest();
    
        List<Compliance_Activity__c> results = [Select Id, Waterway__c, Location_Description__c From Compliance_Activity__c
                                Where id =: initRecord.id];
        System.assertEquals('1 Spring St, Melbourne, Victoria, 3000',results[0].Location_Description__c);
    
  }

}