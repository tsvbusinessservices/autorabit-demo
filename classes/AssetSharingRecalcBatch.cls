global class AssetSharingRecalcBatch implements Database.Batchable<sObject>, Schedulable {
	public static final set<String> ASSET_SHARING_SET = new set<String>{Constants.SAR_ASSET, Constants.SAR_ATON, Constants.SAR_BUS};
	global AssetSharingRecalcBatch() {
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(AssetHelper.getEnrichedAssetDataForSharingRecalc());
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		AssetHandler.assetSharing(scope, null, 'Recalculation');
	}
	
	global void finish(Database.BatchableContext BC) {
		//Send email
		String emailSubject = 'SUCCESS: Apex Sharing Recalculation For Asset Completed';
		String emailBody = 'The Apex sharing recalculation finished processing for the Asset Object';
		Utility.sendEmail(emailSubject, emailBody, 'Success');
	}

	global void execute(SchedulableContext sc) {
		AssetSharingRecalcBatch b = new AssetSharingRecalcBatch();
		database.executeBatch(b, 200);
	}
	
}