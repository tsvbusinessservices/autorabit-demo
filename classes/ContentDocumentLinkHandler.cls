public class ContentDocumentLinkHandler extends TriggerBase implements ITriggerBase
{
	public override void afterInsert()
	{
		String outcomePrefix = Outcome__c.SObjectType.getDescribe().getKeyPrefix();
		
		Set <Id> contentDocumentIds = new Set <Id>();
		Set <Id> targetedRecordIds = new Set <Id>();

		for (ContentDocumentLink contentDocumentLink : (List <ContentDocumentLink>)Trigger.new)
		{
			String linkedEntityId = contentDocumentLink.LinkedEntityId;

			if (linkedEntityId.startsWith(outcomePrefix) == true)
			{
				contentDocumentIds.add(contentDocumentLink.ContentDocumentId);
				targetedRecordIds.add(contentDocumentLink.Id);
			}
		}

		System.debug('contentDocumentIds::' +contentDocumentIds );
		System.debug('targetedRecordIds::' +targetedRecordIds );

		Map <Id, ContentDocument> contentDocumentMap = new Map <Id, ContentDocument> ([SELECT Id, LatestPublishedVersion.Field_Image_URL__c, Title FROM ContentDocument WHERE Id IN :contentDocumentIds]);
		System.debug('contentDocumentMap::' +contentDocumentMap );

		List <Outcome_File__c> outcomeFilesToInsert = new List <Outcome_File__c>();

		for (ContentDocumentLink contentDocumentLink : (List <ContentDocumentLink>)Trigger.new)
		{
			if (targetedRecordIds.contains(contentDocumentLink.Id) == true)
			{
				Outcome_File__c outcomeFile = new Outcome_File__c();
				outcomeFile.Outcome__c = contentDocumentLink.LinkedEntityId;
				outcomeFile.Content_Document_Link_Id__c = contentDocumentLink.Id;
				outcomeFile.File_Name__c = contentDocumentMap.get(contentDocumentLink.ContentDocumentId).Title;
				outcomeFile.File_URL__c = contentDocumentMap.get(contentDocumentLink.ContentDocumentId).LatestPublishedVersion.Field_Image_URL__c;

				// Map Asset Name and ESRI Ids to seperate fields using regular expression
				Pattern myPattern = Pattern.compile('^(.*?) - (.*) - (\\d*)$');
				Matcher myMatcher = myPattern.matcher(outcomeFile.File_Name__c);

				if (myMatcher.matches())
				{
					outcomeFile.Compliance_Activity_Name__c = myMatcher.group(1);
					outcomeFile.Asset_Name__c = myMatcher.group(2);
					outcomeFile.ESRI_Id__c = myMatcher.group(3);
				}

				outcomeFilesToInsert.add(outcomeFile);
			}
		}

		insert outcomeFilesToInsert;
	}

	public override void afterDelete()
	{
		delete [SELECT Id FROM Outcome_File__c WHERE Content_Document_Link_Id__c IN :Trigger.oldMap.keySet()];
	}
}