/**
 * SharingHelper class. Dynamically creates and Deletes Apex sharing rules for Portal user roles against any sobject type
 *
 * @author Ricky Lowe
 * @date 29.May.2018
 */


public without sharing class SharingHelper{
	private set<Id> partnerAccountIdsSet = new set<Id>();
	private list<PartnerSharing> PartnerSharingList = new list<PartnerSharing>();
	private list<ClonedSharing> ClonedSharingList = new list<ClonedSharing>();
	private String ShareObjectName;
	private set<Id> deleteList = new set<Id>();

	public SharingHelper(String ShareObjectName) {
		this.ShareObjectName = ShareObjectName;
	}


	/**
	 * Add to the delete list
	 * @return N/A
	 */
	public void addToDeleteList(Id parentId){
		deleteList.add(parentId);
	}

	/**
	 * Deletes the Sharing records where the parent record has been added to the delete list and the row cause is the apex sharing reason
	 * NEEDS TO BE RUN BEFORE THE INSERT JOB
	 * @return N/A
	 */
	public void deleteSharingRules(set<String> RowCause){
		try{
			delete queryRowCauseSharing(deleteList, RowCause);
		}
		catch(DmlException e){
			String emailSubject = 'ERROR: Apex Sharing Deletion Exception';
			String emailBody = 'The Apex sharing deletion threw the following exception: ' + e.getMessage();
			Utility.sendEmail(emailSubject, emailBody, 'Error');
		}
		
	}

	static boolean isAccountShare(String ShareObjectName) {
		return ShareObjectName == AccountShare.sObjectType.getDescribe().getName();
	}

	static String getParentIdKey(String ShareObjectName) {
		return isAccountShare(ShareObjectName)? 'AccountId':'ParentId';
	}

	/**
	 * query the sharing records for deletion
	 * @return N/A
	 */
	public list<Sobject> queryRowCauseSharing(set<Id> parentIds, set<String> RowCause){

		String query = 'SELECT Id, UserOrGroupId FROM ' + ShareObjectName + ' WHERE RowCause IN :RowCause AND '+ getParentIdKey(ShareObjectName) +' IN :parentIds';
		
		system.debug(query);
		return database.query(query);
	}

	/**
	 * query all the sharing records against the parent
	 * @return N/A
	 */
	public list<Sobject> querySharing(set<Id> parentIds){
		String query = 'SELECT Id, UserOrGroupId FROM ' + ShareObjectName + ' WHERE ParentId IN :parentIds';
		
		system.debug(query);
		return database.query(query);
	}

	/**
	 * Add the portalAccountId and Parent Id set to the list in the class
	 * @param portalAccountId = The portal Account
	 * @param ParentId = The record that is being shared
	 * @param RowCause = The Sharing reason
	 * @param AccessLevel = The level of access either read or edit
	 * @return  N/A
	 */
	public void addToSharing(Id portalAccountId, Id ParentId, String RowCause, String AccessLevel){
		addToSharing(portalAccountId, ParentId, RowCause, AccessLevel, Constants.SHARING_TYPE_ALL);
	}

	/**
	 * Add the portalAccountId and Parent Id set to the list in the class
	 * @param portalAccountId = The portal Account
	 * @param ParentId = The record that is being shared
	 * @param RowCause = The Sharing reason
	 * @param AccessLevel = The level of access either read or edit
	 * @param contactIds = For which contact the sharing will be applied to
	 * @return  N/A
	 */
	public void addToSharing(Id portalAccountId, Id ParentId, String RowCause, String AccessLevel, String ShareType){
		//if ParentId is not null then add to Set
		if(portalAccountId != null){
			partnerAccountIdsSet.add(portalAccountId);

			//if parent Id is not null as well then add to wrapper class
			if(ParentId != null){
				PartnerSharingList.add(new PartnerSharing(portalAccountId, ParentId, RowCause, AccessLevel, ShareType));
			}
		}
		
	}

	/**
	 * Add the portalAccountId and Parent Id set to the list in the class
	 * @param portalAccountId = The portal Account
	 * @param ParentId = The record that is being shared
	 * @param RowCause = The Sharing reason
	 * @param AccessLevel = The level of access either read or edit
	 * @return  N/A
	 */
	public void addToSharingUsingACR(Id portalAccountId, Id ParentId, String RowCause, String AccessLevel){
		addToSharing(portalAccountId, ParentId, RowCause, AccessLevel, Constants.SHARING_TYPE_ACR);
	}

	/**
	 * Class to hold the Partner and Parent Id pairs
	 * @return N/A
	 */
	private class PartnerSharing{
		private Id portalAccountId;
		private Id ParentId;
		private String AccessLevel;
		private String RowCause;
		private String ShareType;

		private PartnerSharing(Id portalAccountId, Id ParentId, String RowCause, String AccessLevel, String ShareType){
			this.portalAccountId = portalAccountId;
			this.ParentId = ParentId;
			this.AccessLevel = AccessLevel;
			this.RowCause = RowCause;
			this.ShareType = ShareType;
		}
	}

	/**
	 * Add the portalAccountId and Parent Id set to the list in the class
	 * @param ParentId = The record that is being shared
	 * @param RowCause = The Sharing reason
	 * @param AccessLevel = The level of access either read or edit
	 * @return  N/A
	 */
	public void addToClonedSharing(Id ParentId, String RowCause, Id UserOrGroupId, String AccessLevel){
		//if parent Id is not null as well then add to wrapper class
		if(ParentId != null){
			ClonedSharingList.add(new ClonedSharing( ParentId, RowCause, UserOrGroupId, AccessLevel));
		}
	}

	/**
	 * Class to hold the Partner and Parent Id pairs
	 * @return N/A
	 */
	private class ClonedSharing{
		private Id ParentId;
		private String RowCause;
		private Id UserOrGroupId;
		private String AccessLevel;

		private ClonedSharing(Id ParentId, String RowCause, Id UserOrGroupId, String AccessLevel){
			this.ParentId = ParentId;
			this.RowCause = RowCause;
			this.UserOrGroupId = UserOrGroupId;
			this.AccessLevel = AccessLevel;
		}
	}

	/**
	 * Get the Portal Role group that can be added to a record to share with the whole Partner
	 * @return Map of the Account Id to the sharing groups to share with
	 */
	private map<Id, list<Group>> getPartnerSharingGroups(){
		//Retrieved all partner account user roles into a map
		map<Id,UserRole> userRoleMap = new map<Id,UserRole>([SELECT Id, PortalAccountId
											FROM UserRole 
											WHERE PortalType =: Constants.PORTAL_TYPE_CUSTOMER 
											AND PortalAccountId IN :partnerAccountIdsSet]);
		System.debug('***userRoleMap '+userRoleMap);
	
   		//Collate into a map of Partner Id to a list of groups to add as sharing rules
		map<Id, list<Group>> PartnerAccountToGroupsMap = new map<Id, list<Group>>();
		for(Group g: [SELECT Id,DeveloperName,RelatedId,Type
							FROM Group 
							WHERE RelatedId IN :userRoleMap.KeySet() 
							AND type =: Constants.USER_ROLE_TYPE_ROLE_SUB]){
		
			UserRole ur = userRoleMap.get(g.RelatedId);
			
			//If the Partner Account already exists in the map then add to the group list
			if(PartnerAccountToGroupsMap.ContainsKey(ur.PortalAccountId)){
				list<Group> groupList = PartnerAccountToGroupsMap.get(ur.PortalAccountId);
				groupList.add(g);
			}
			//else create new group list
			else{
				PartnerAccountToGroupsMap.put(ur.PortalAccountId, new list<Group>{g});
			}
		}
		return PartnerAccountToGroupsMap;
	}

	/**
	 * Get the Portal USers that can be added to a record to share with the Users with Access in ACR
	 * @return Map of the Account Id to the sharing users to share with
	 */
	private map<Id, list<User>> getPartnerSharingUsers(){

		map<Id, list<User>> partnerSharingUsers = new map<Id, list<User>>();

		Map<Id, List<Id>> accountContactIdMap = new Map<Id, List<Id>>();

		Map<Id, User> allContacts = new Map<Id, User>();

		String query = 'SELECT Id, AccountId, ContactId, Full_Access__c '+
					    'FROM AccountContactRelation '+
						'WHERE AccountId IN:partnerAccountIdsSet '+
						'AND Full_Access__c = true ';
										
		System.debug(query);
		// get all relations that have full access
		for(AccountContactRelation relation: database.query(query)) {
			// get the contact list for the account
			List<Id> contacts = accountContactIdMap.get(relation.AccountId);
			if(contacts == null) {
				// create a new contact List
				contacts = new List<Id>();
				accountContactIdMap.put(relation.AccountId, contacts);
			}
			// Add ContactId to the list of contacts for this account
			contacts.add(relation.ContactId);

			//add to map with placeholder user 
			allContacts.put(relation.ContactId, null);
		}

		System.debug('accountContactIdMap::' + accountContactIdMap);

		if(allContacts.keySet().size() == 0)
			return partnerSharingUsers;

		// get the user records for the contacts
		for(User u: [SELECT Id, ContactId, IsPortalEnabled
						FROM User
						Where IsPortalEnabled = true 
						AND ContactId IN :allContacts.KeySet()]) {
			//place the user object in the map 
			allContacts.put(u.ContactId, u);
		}

		System.debug('allContacts::' + allContacts);

		// create
		for(Id accountId: accountContactIdMap.keySet()) {

			// get the ids contacts with full access from each account
			List<Id> contactIds = accountContactIdMap.get(accountId);

			// check if there are any contacts with full access in this account
			if(contactIds.size() > 0) {

				List<User> users = new List<User>();

				// get the user records for the contacts if it exist
				for(Id contactId: contactIds) {
					User u = allContacts.get(contactId);
					if(u != null)
						users.add(u);
				}

				// there are any users with full access for the account 
				if(users.size() > 0)
					partnerSharingUsers.put(accountId, users);
			}

		}

		return partnerSharingUsers;

	}

	/**
	 * Creates dynamic sharing row sobject to share the record (parentId) with the sharingGroup
	 * @return Sobject of the sharing row
	 */
	public static sObject addSharingRowsSobject(String ShareObjectName, String RowCause, Id parentId, Id sharingGroup, String AccessLevel){
		//get sobject token for object name
		Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ShareObjectName);
		System.debug('***targetType= '+targetType);

		//create new Sobject from object token
		sObject SharingSObject = targetType.newSObject();

		SharingSObject.put(getParentIdKey(ShareObjectName), parentId);

		if(isAccountShare(ShareObjectName)) {
			SharingSObject.put('AccountAccessLevel', AccessLevel);
			SharingSObject.put('OpportunityAccessLevel', AccessLevel);
		} else {
			SharingSObject.put('AccessLevel', AccessLevel);

		}

		//set all of the fields passed in by method
		SharingSObject.put('UserOrGroupId', sharingGroup);
		
		SharingSObject.put('RowCause', RowCause);
		
		System.debug('***SharingSObject= '+SharingSObject);

		//return the Sharing Sobject
		return SharingSObject;
	}

	/**
	 * Collates the Sharing rows
	 * @return list<Sobject> sharing rows to be inserted
	 */
	private list<Sobject> collateApexSharing(){
		list<Sobject> sharingList = new list<Sobject>();

		//if the list is not empty
		if(PartnerSharingList != null && PartnerSharingList.size() > 0){
			
			//get the partner sharing groups for group based sharing
			// key is PortalAccountId
			map<Id, list<Group>> partnerSharingGroups = getPartnerSharingGroups();

			//get the partner users for ACR based sharing 
			// key is PortalAccountId
			map<Id, list<User>> partnerSharingUsers = getPartnerSharingUsers();			
			map<Id, list<User>> filteredPartnerSharingUsers = new Map<id, list<User>>();

			System.debug('partnerSharingUsers::' + partnerSharingUsers);

			//loop round the PartnerSharing and create Apex Sharing for each
			for(PartnerSharing sharingItem : PartnerSharingList){
				System.debug('sharingItem.ShareType::' + sharingItem.ShareType);
				if(sharingItem.ShareType == Constants.SHARING_TYPE_ALL) {	
					sharingList.addAll(getSharingRowsSobjectForPartner(sharingItem, partnerSharingGroups));	
				}
				else if(sharingItem.ShareType == Constants.SHARING_TYPE_ACR) {
					System.debug('Sharing for users');					
					sharingList.addAll(getSharingRowsSobjectForPartner(sharingItem, partnerSharingUsers));
				}
			}	
		}
		return sharingList;
	}


	List<sObject> getSharingRowsSobjectForPartner(PartnerSharing sharingItem, map<Id, list<sObject>> partnerSharingList) {		
		List<sObject> sharingList = new List<sObject>();
		if(partnerSharingList != null && partnerSharingList.size() > 0){
			list<sObject> sharingGroups;

			//if the sharing list contains a match for the current portalAccountId
			if(partnerSharingList.containsKey(sharingItem.portalAccountId)){
				sharingGroups = partnerSharingList.get(sharingItem.portalAccountId);
			}
			//else skip as no sharing group means no apex sharing rule
			else{
				System.debug('***Sharing Group Not found for Account Id = '+ sharingItem.portalAccountId);
				return sharingList;	//skip past the Apex creation
			}

			//loop round the sharing list available for groups / users
			for(sObject G : sharingGroups){
				//create apex sharing
				System.debug('Creating Share for::' + sharingItem.parentId + ' for reason ' + sharingItem.RowCause + ' with entity ' + G.Id);
				sObject shareSobj = addSharingRowsSobject(ShareObjectName, sharingItem.RowCause, sharingItem.parentId, G.Id, sharingItem.AccessLevel);
				sharingList.add(shareSobj);
			}	
		}

		return sharingList;
	}

	/**
	 * Clone the Sharing rows
	 * @return list<Sobject> sharing rows to be inserted
	 */
	private list<Sobject> cloneApexSharing(){
		list<Sobject> sharingList = new list<Sobject>();
		
		//loop round the ClonedSharing and create Apex Sharing for each
		for(ClonedSharing sharingItem : ClonedSharingList){
			System.debug('Creating Share for::' + sharingItem.parentId + ' for reason ' + sharingItem.RowCause + ' with entity ' + sharingItem.UserOrGroupId);
			sObject shareSobj = addSharingRowsSobject(ShareObjectName, sharingItem.RowCause, sharingItem.parentId, sharingItem.UserOrGroupId, sharingItem.AccessLevel);
			sharingList.add(shareSobj);	
		}	

		return sharingList;
	}


	/**
	 * Inserts the sharing rules Syncronously
	 * @return list<Sobject> sharing rows inserted
	 */
	public list<Sobject> insertApexSharing_Syncronously(){
		list<Sobject> theSharingList = collateApexSharing();
		insertSharingRulesWithErrorHandling(theSharingList);
		return theSharingList;
	}

	/**
	 * Inserts the sharing rules Asyncronously using a queueable job
	 * @return list<Sobject> sharing rows inserted
	 */
	public list<Sobject> insertApexSharing_Asyncronously(){
		list<Sobject> theSharingList = collateApexSharing();

		
		System.debug('theSharingList::'+theSharingList);
		//insert sharingList;
		if(Limits.getQueueableJobs() == 0)
			System.enqueueJob(new SharingQueueable(theSharingList));

		system.debug('Enquing job for: ' + theSharingList);
		return theSharingList;
	}

	/**
	 * Clone the sharing rules Asyncronously using a queueable job
	 * @return list<Sobject> sharing rows inserted
	 */
	public list<Sobject> cloneApexSharing_Syncronously(){
		list<Sobject> theSharingList = cloneApexSharing();
		insertSharingRulesWithErrorHandling(theSharingList);
		return theSharingList;
	}

	/**
	 * Clone the sharing rules Asyncronously using a queueable job
	 * @return list<Sobject> sharing rows inserted
	 */
	public list<Sobject> cloneApexSharing_Asyncronously(){
		list<Sobject> theSharingList = cloneApexSharing();

		
		System.debug('theSharingList::'+theSharingList);
		//insert sharingList;
		if(Limits.getQueueableJobs() == 0)
			System.enqueueJob(new SharingQueueable(theSharingList));

		system.debug('Enquing job for: ' + theSharingList);
		return theSharingList;
	}

	/**
	 * Clone the sharing rules Asyncronously using a queueable job
	 * @return list<Sobject> sharing rows inserted
	 */
	public list<Sobject> insertAndCloneApexSharing_Syncronously(){
		list<Sobject> theSharingList = collateApexSharing();
		theSharingList.addAll(cloneApexSharing());
		insertSharingRulesWithErrorHandling(theSharingList);
		return theSharingList;
	}

	/**
	 * Clone the sharing rules Asyncronously using a queueable job
	 * @return list<Sobject> sharing rows inserted
	 */
	public list<Sobject> insertAndCloneApexSharing_Asyncronously(){
		list<Sobject> theSharingList = collateApexSharing();
		theSharingList.addAll(cloneApexSharing());
		
		System.debug('theSharingList::'+theSharingList);
		//insert sharingList;
		if(Limits.getQueueableJobs() == 0)
			System.enqueueJob(new SharingQueueable(theSharingList));

		system.debug('Enquing job for: ' + theSharingList);
		return theSharingList;
	}


	/**
	 * Inserts the sharing rules 
	 * @return list<Sobject> sharing rows that were inserted
	 */
	public static list<Sobject> insertSharingRulesWithErrorHandling(list<Sobject> sharingList){
		//insert the sharing rules
		Database.SaveResult[] lsr = Database.insert(sharingList,false);

		// Process the save results for insert.
       	for(Database.SaveResult sr : lsr){
           	if(!sr.isSuccess()){
               	// Get the first save result error.
               	Database.Error err = sr.getErrors()[0];
               
				//If error is not Access level due to OWD making Apex sharing unneccessary 
				if(!(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  &&  err.getMessage().contains('AccessLevel'))){
					//Send email
					String emailSubject = 'ERROR: Apex Sharing Creation Exception';
					String emailBody = 'Apex sharing creation threw the following exception: ' + err.getMessage();
					Utility.sendEmail(emailSubject, emailBody, 'Error');
               	}
           	}
       	}
       	return sharingList;
	}

}