@isTest
private class Mod7ChecksumGeneratorTest
{
	@isTest
	static void testMod7Checksum()
	{
		Mod7ChecksumGenerator generator = new Mod7ChecksumGenerator();

		System.assertEquals('3', generator.getChecksum(050852427));
		System.assertEquals('4', generator.getChecksum(029994444));
		System.assertEquals('3', generator.getChecksum(020011778));
		System.assertEquals('2', generator.getChecksum(023451234));
		System.assertEquals('3', generator.getChecksum(26780022));
		System.assertEquals('5', generator.getChecksum(027781234));
		System.assertEquals('7', generator.getChecksum(027781236));

	}
}