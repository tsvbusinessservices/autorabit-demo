@isTest
private class ContentHandlerTest {
	
	@isTest static void test_method_one() {
		// Implement test code
	}
	
	//P - Create handler with manager
	@isTest static void PositiveTest_CreateNotificationWithFacility() {
		//Get the Partner Account Id
		Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;

		Location__c loc1 = TestDataFactory.createLocation(portalAccountId, Constants.LOCATION_RT_ID_FACILITY);
		insert loc1;
        
        Allocation__c alocGroup = TestDataFactory.createGroup();
        alocGroup.Group_Type__c = 'Facility';
        insert alocGroup;

        Allocation__c alocFacilityGroup = TestDataFactory.createLocationGroup(Constants.ALLOCATION_RT_ID_FACILITY_GROUP);
        alocFacilityGroup.Facility__c = loc1.id;
        alocFacilityGroup.Group__c = alocGroup.id;
        insert alocFacilityGroup;
        
		alocGroup.Status__c = Constants.ALLOCATION_STATUS_FINALIZED;
		update alocGroup;

		test.startTest();

        Content__c cont1 = TestDataFactory.createContent();
        cont1.Group_of_Facility__c = alocGroup.Id;
        insert cont1;

		test.stopTest();
		
		//check that there has been a sharing record created
		List<Content__Share> conSharing = ContentHelper.getConSharing(new set<Id>{cont1.Id}, new set<String>{Schema.Content__Share.RowCause.Facility_Manager__c});
        system.assertEquals(1, conSharing.size());
	}

	@isTest static void PositiveTest_CreateNotificationWithWaterway() {
		//Get the Partner Account Id
		Id portalAccountId1 = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;
		Id portalAccountId2 = [SELECT Id FROM Account WHERE Name = 'portalAccount2'].Id;

		Location__c waterway = TestDataFactory.createLocation(portalAccountId2, Constants.LOCATION_RT_ID_WATERWAY);
		insert waterway;

		Location__c facility  = TestDataFactory.createLocation(portalAccountId1, Constants.LOCATION_RT_ID_FACILITY);
		facility.Waterway_Name__c = waterway.id;
		insert facility;
		
        Waterway_Role__c wwr1 = TestDataFactory.createWaterwayRole(portalAccountId2, waterway.id);
        insert wwr1;
        
        Allocation__c alocGroup = TestDataFactory.createGroup();
        alocGroup.Group_Type__c = 'Waterway';
        insert alocGroup;

        Allocation__c alocWaterwayGroup = TestDataFactory.createLocationGroup(Constants.ALLOCATION_RT_ID_WATERWAY_GROUP);
        alocWaterwayGroup.Waterway_Name__c = waterway.id;
        alocWaterwayGroup.Group__c = alocGroup.id;
        insert alocWaterwayGroup;

		alocGroup.Status__c = Constants.ALLOCATION_STATUS_FINALIZED;
		update alocGroup;

		test.startTest();

        Content__c cont1 = TestDataFactory.createContent();
        cont1.Group_of_Waterway__c = alocGroup.Id;
        insert cont1;

		test.stopTest();

		//check that there has been a sharing record created
		List<Content__Share> conSharing = ContentHelper.getConSharing(new set<Id>{cont1.Id}, new set<String>{Schema.Content__Share.RowCause.Secondary_Waterway_Manager__c, Schema.Content__Share.RowCause.Parent_Waterway_Manager__c});
        
        system.assertEquals(1, conSharing.size());
	}

    @isTest static void PositiveTest_UpdatingNotificationWithFacility() {
		//Get the Partner Account Id
		Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;

		Location__c loc1 = TestDataFactory.createLocation(portalAccountId, Constants.LOCATION_RT_ID_FACILITY);
		insert loc1;

        Allocation__c alocGroup = TestDataFactory.createGroup();
        alocGroup.Group_Type__c = 'Facility';
        insert alocGroup;

        Allocation__c alocFacilityGroup = TestDataFactory.createLocationGroup(Constants.ALLOCATION_RT_ID_FACILITY_GROUP);
        alocFacilityGroup.Facility__c = loc1.id;
        alocFacilityGroup.Group__c = alocGroup.id;
        insert alocFacilityGroup;
        
		alocGroup.Status__c = Constants.ALLOCATION_STATUS_FINALIZED;
		update alocGroup;

        Content__c cont1 = TestDataFactory.createContent();
        cont1.Group_of_Facility__c = alocGroup.Id;
        insert cont1;

		test.startTest();
		
        cont1.Group_of_Facility__c = null;
        update cont1;

		test.stopTest();

		//check that there has been a sharing record created
		List<Content__Share> conSharing = ContentHelper.getConSharing(new set<Id>{cont1.Id}, new set<String>{Schema.Content__Share.RowCause.Facility_Manager__c});
		system.assertEquals(0, conSharing.size());
	}

    @isTest
	private static void NegativeTest_EducationalContentNoFileInsert(){
		//Get the Partner Account Id
		Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;

		Location__c loc1 = TestDataFactory.createLocation(portalAccountId, Constants.LOCATION_RT_ID_FACILITY);
		insert loc1;

        Allocation__c alocGroup = TestDataFactory.createGroup();
        alocGroup.Group_Type__c = 'Facility';
        insert alocGroup;

        Allocation__c alocFacilityGroup = TestDataFactory.createLocationGroup(Constants.ALLOCATION_RT_ID_FACILITY_GROUP);
        alocFacilityGroup.Facility__c = loc1.id;
        alocFacilityGroup.Group__c = alocGroup.id;
        insert alocFacilityGroup;
        
		alocGroup.Status__c = Constants.ALLOCATION_STATUS_FINALIZED;
		update alocGroup;

        Content__c cont1 = TestDataFactory.createContent(Constants.CONTENT_RT_ID_EDUCATIONAL_CONTENT);
        cont1.Group_of_Facility__c = alocGroup.Id;
        

		try{			
			test.startTest();

			cont1.Status__c = 'Active';
			insert cont1;

			test.stopTest();

		}catch(Exception e){
			Boolean expectedExceptionThrown =  e.getMessage().contains('You cannot create an active educational content.') ? true : false;
			System.AssertEquals(expectedExceptionThrown, true);
		}	
	}

	@isTest
	private static void NegativeTest_EducationalContentNoFileUpdate(){
		//Get the Partner Account Id
		Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;

		Location__c loc1 = TestDataFactory.createLocation(portalAccountId, Constants.LOCATION_RT_ID_FACILITY);
		insert loc1;

        Allocation__c alocGroup = TestDataFactory.createGroup();
        alocGroup.Group_Type__c = 'Facility';
        insert alocGroup;

        Allocation__c alocFacilityGroup = TestDataFactory.createLocationGroup(Constants.ALLOCATION_RT_ID_FACILITY_GROUP);
        alocFacilityGroup.Facility__c = loc1.id;
        alocFacilityGroup.Group__c = alocGroup.id;
        insert alocFacilityGroup;
        
		alocGroup.Status__c = Constants.ALLOCATION_STATUS_FINALIZED;
		update alocGroup;

        Content__c cont1 = TestDataFactory.createContent(Constants.CONTENT_RT_ID_EDUCATIONAL_CONTENT);
        cont1.Group_of_Facility__c = alocGroup.Id;
		cont1.Status__c = 'Draft';
        insert cont1;

		try{			
			test.startTest();

			cont1.Status__c = 'Active';
			update cont1;

			test.stopTest();

		}catch(Exception e){
			Boolean expectedExceptionThrown =  e.getMessage().contains('Please add 1 and only 1 image file.') ? true : false;
			System.AssertEquals(expectedExceptionThrown, true);
		}	
	}

	@isTest
	private static void NegativeTest_EducationalContentMoreThanOneFile(){
		//Get the Partner Account Id
		Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;

		Location__c loc1 = TestDataFactory.createLocation(portalAccountId, Constants.LOCATION_RT_ID_FACILITY);
		insert loc1;

        Allocation__c alocGroup = TestDataFactory.createGroup();
        alocGroup.Group_Type__c = 'Facility';
        insert alocGroup;

        Allocation__c alocFacilityGroup = TestDataFactory.createLocationGroup(Constants.ALLOCATION_RT_ID_FACILITY_GROUP);
        alocFacilityGroup.Facility__c = loc1.id;
        alocFacilityGroup.Group__c = alocGroup.id;
        insert alocFacilityGroup;
        
		alocGroup.Status__c = Constants.ALLOCATION_STATUS_FINALIZED;
		update alocGroup;

        Content__c cont1 = TestDataFactory.createContent(Constants.CONTENT_RT_ID_EDUCATIONAL_CONTENT);
        cont1.Group_of_Facility__c = alocGroup.Id;
		cont1.Status__c = 'Draft';
        insert cont1;

		//1st Document
		ContentVersion contentVersion = new ContentVersion();
		contentVersion.Title = 'Penguins';
		contentVersion.PathOnClient = 'Penguins.jpg';
		contentVersion.VersionData = Blob.valueOf('Test Content');
		contentVersion.IsMajorVersion = true;
		insert contentVersion;

		List <ContentVersion> contentVersions = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id];

		System.assertEquals(1, contentVersions.size());

		contentVersion = contentVersions.get(0);

		List <ContentDocument> contentDocuments = [SELECT Id FROM ContentDocument WHERE Id = :contentVersion.ContentDocumentId];

		System.assertEquals(1, contentDocuments.size());

		ContentDocument contentDocument = contentDocuments.get(0);

		ContentDocumentLink contentDocumentLink = New ContentDocumentLink();
		contentDocumentLink.LinkedEntityId = cont1.Id;
		contentDocumentLink.ContentDocumentId = contentDocument.Id;
		contentDocumentLink.shareType = 'V';

		insert contentDocumentLink;

		//2nd Document
		ContentVersion contentVersion2 = new ContentVersion();
		contentVersion2.Title = 'Penguins';
		contentVersion2.PathOnClient = 'Penguins.jpg';
		contentVersion2.VersionData = Blob.valueOf('Test Content');
		contentVersion2.IsMajorVersion = true;
		insert contentVersion2;

		List <ContentVersion> contentVersions2 = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion2.Id];

		System.assertEquals(1, contentVersions2.size());

		contentVersion2 = contentVersions2.get(0);

		List <ContentDocument> contentDocuments2 = [SELECT Id FROM ContentDocument WHERE Id = :contentVersion2.ContentDocumentId];

		System.assertEquals(1, contentDocuments2.size());

		ContentDocument contentDocument2 = contentDocuments2.get(0);

		ContentDocumentLink contentDocumentLink2 = New ContentDocumentLink();
		contentDocumentLink2.LinkedEntityId = cont1.Id;
		contentDocumentLink2.ContentDocumentId = contentDocument2.Id;
		contentDocumentLink2.shareType = 'V';

		insert contentDocumentLink2;


		try{			
			test.startTest();

			cont1.Status__c = 'Active';
			update cont1;

			test.stopTest();

		}catch(Exception e){
			Boolean expectedExceptionThrown =  e.getMessage().contains('Please add 1 and only 1 image file.') ? true : false;
			System.AssertEquals(expectedExceptionThrown, true);
		}	
	}

	@testSetup static void testSetup(){
		TestDataFactory.createPartnerAccount(new List<Id>{Constants.ACCOUNT_RT_ID_MARITIME,Constants.ACCOUNT_RT_ID_MARITIME},new List<String>{Constants.ACCOUNT_TYPE_FACILITY_MANAGER,Constants.ACCOUNT_TYPE_WATERWAY_MANAGER});
	}
	
}