public class ChecksumGeneratorFactoryImpl implements ChecksumGeneratorFactory {

	private static ChecksumGenerator mod97Generator = new Mod97ChecksumGenerator(12);
	
	private static ChecksumGenerator mod7Generator = new Mod7ChecksumGenerator();


	public ChecksumGenerator getChecksumGenerator(String algorithm) {
		
		if(algorithm == Constants.ORG_VIC_POLICE)
			return mod7Generator;
		else if(algorithm == Constants.ORG_TSV)
			return mod97Generator;
		else
			return null;
	}
}