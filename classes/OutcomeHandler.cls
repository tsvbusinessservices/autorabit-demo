public class OutcomeHandler extends TriggerBase implements ITriggerBase {

	@TestVisible
	private static ChecksumGeneratorFactory checksumfactoryInstance = null;

	@TestVisible
	private static InfringementNumberGenerator infNumberGeneratorInstance = null;

	/**
	 * afterInsert Trigger Event
	 * @return null
	 */
	public override void beforeInsert() {

		updateLicenseConditions();

		List<Outcome__c> infringements = new List<Outcome__c>();
		List<Outcome__c> scope = (List<Outcome__c>) Trigger.NEW;
		for(Outcome__c rec: scope) {
			if(OutcomeHelper.isInfringementRecordType(rec.RecordTypeId))
				infringements.add(rec);
		}

		if(infringements.size() > 0) {

			updateInfringementNumber(infringements);

			updateSuffix(infringements);

		}
	}

	/**
	 * afterUpdate Trigger Event	
	 * @return null
	 */
	public override void beforeUpdate() {
		List<Outcome__c> infringements = new List<Outcome__c>();
		List<Outcome__c> scope = (List<Outcome__c>) Trigger.NEW;
		for(Outcome__c rec: scope) {
			Outcome__c oldRec = (Outcome__c) oldMap.get(rec.Id);
			if(OutcomeHelper.isInfringementRecordType(rec.RecordTypeId) 
					&& (OutcomeHelper.isPoliceInfrigementChanged(rec, oldRec)
					|| OutcomeHelper.isInfringementNumberChanged(rec, oldRec)))
				infringements.add(rec);
		}
		System.debug('updating # and checksum for ::' + infringements);

		if(infringements.size() > 0) {
			updateInfringementNumber(infringements);

			updateSuffix(infringements);

		}
	}

	public override void afterUpdate()
	{
		// Define collections
		List <Outcome__c> targetOutcomes = new List <Outcome__c>();
		List <Outcome__c> triggerNew = (List<Outcome__c>) Trigger.new;
		Map <Id, Outcome__c> triggerOldMap = (Map <Id, Outcome__c>) Trigger.oldMap;
		
		// Retreive Bus Safety Alert Record Type Id
		Id busSafetyAlertRecordTypeId = Schema.SObjectType.Outcome__c.getRecordTypeInfosByDeveloperName().get('Bus_Safety_Alert').getRecordTypeId();

		// Determine Outcomes to target
		for (Outcome__c outcome : triggerNew)
		{
			if (outcome.RecordTypeId == busSafetyAlertRecordTypeId &&
					outcome.Status__c == 'Published' &&
					triggerOldMap.get(outcome.Id).Status__c != 'Published'
				)
			{
				targetOutcomes.add(outcome);
			}
		}

		if (targetOutcomes.size() > 0)
		{
			// Retrieve userIds from Public Group
			List <GroupMember> groupMembers = [SELECT Id, UserOrGroupId FROM GroupMember WHERE Group.DeveloperName = 'Bus_Operator_Responsible_Persons'];
			List <Id> userIds = new List <Id>();

			for (GroupMember groupMember : groupMembers)
			{
				userIds.add(groupMember.UserOrGroupId);
			}


			// Retrieve Emails for users
			List <User> users = [SELECT Id, Email FROM User WHERE Id IN :userIds];

			// Batch emails
			Integer batchNumber = 0;
			Integer maxBatchSize = 50;
			List <List <String>> emailBatches = new List <List <String>>();
			
			for (User user : users)
			{
				if (emailBatches.size() == batchNumber)
				{
					emailBatches.add(new List <String>());
				}

				if (user.Email != 'dev@systempartners.com')
				{
					emailBatches.get(batchNumber).add(user.Email);
				}

				if (emailBatches.get(batchNumber).size() >= maxBatchSize) { batchNumber = batchNumber + 1; }
			}

			// Create list of email messages to be sent
			List <Messaging.SingleEmailMessage> singleEmailMessages = new List <Messaging.SingleEmailMessage>();

			EmailTemplate emailTemplate = [SELECT Id, Body, Subject FROM EmailTemplate WHERE DeveloperName = 'Bus_Safety_Alert_Notification'];

			for (Outcome__c outcome : targetOutcomes)
			{
				for (batchNumber = 0; batchNumber < emailBatches.size(); batchNumber++)
				{
					Messaging.SingleEmailMessage singleEmailMessage = new Messaging.SingleEmailMessage();
					singleEmailMessage.setSubject(populateFields(emailTemplate.Subject, 'Outcome__c', outcome));
					singleEmailMessage.setBccAddresses(emailBatches.get(batchNumber));
					singleEmailMessage.setPlainTextBody(populateFields(emailTemplate.Body, 'Outcome__c', outcome));
					singleEmailMessage.setSaveAsActivity(false);
					singleEmailMessages.add(singleEmailMessage);
				}
			}

			Messaging.sendEmail(singleEmailMessages);
		}
	}

	private String populateFields(String content, String objectType, SObject record)
	{
		String regularExpression = '\\{\\!' + objectType + '\\.(.*)}';
		String returnString = content;
		Matcher matcher = Pattern.compile(regularExpression).matcher(content);

		while (matcher.find())
		{
			if (record.get(matcher.group(1)) != null)
			{
				try
				{
					returnString = returnString.replace('{!' + objectType + '.' + matcher.group(1) + '}', (String)(record.get(matcher.group(1))));
				}
				catch (TypeException e)
				{
					if (e.getMessage() == 'Invalid conversion from runtime type Date to String')
					{
						returnString = returnString.replace('{!' + objectType + '.' + matcher.group(1) + '}', ((Date)record.get(matcher.group(1))).format());
					}
					// else { throw new CustomException('Unhandled data population type: ' + e.getMessage()); }
				}
			}
			else { returnString = returnString.replace('{!' + objectType + '.' + matcher.group(1) + '}', ''); }
		}

		return returnString;
	}

	private void updateLicenseConditions()
	{
		// Define Sets to store Record Ids
		Set <Id> outcomeRecordTypeIds = new Set <Id>();
		Set <Id> attributeRecordTypeIds = new Set <Id>();
		
		// Retrieve Record Ids for Outcome and Attribute objects
		outcomeRecordTypeIds.add(Schema.SObjectType.Outcome__c.getRecordTypeInfosByDeveloperName().get('Infringement_Notice').getRecordTypeId());
		outcomeRecordTypeIds.add(Schema.SObjectType.Outcome__c.getRecordTypeInfosByDeveloperName().get('VicPol_Infringement_Notice').getRecordTypeId());
		attributeRecordTypeIds.add(Schema.SObjectType.Attribute__c.getRecordTypeInfosByDeveloperName().get('License_Condition').getRecordTypeId());
	
		// Define Lists to store Targeted Outcome records and License Ids
		List <Outcome__c> targetedOutcomes = new List <Outcome__c>();
		List <Id> licenseIds = new List <Id>();

		// Loop through trigger records
		for (Outcome__c outcome : (List <Outcome__c>) Trigger.new)
		{
			if (outcomeRecordTypeIds.contains(outcome.RecordTypeId) == true)
			{
				if (outcome.Licence__c != null)
				{
					// Add Outcome Record to list
					targetedOutcomes.add(outcome);
					
					// Add License Id to list
					licenseIds.add(outcome.Licence__c);
				}
			}
		}

		// Retrieve License records based on License Id List
		Map <Id, Accreditation__c> accreditationMap = new Map <Id, Accreditation__c> ([SELECT Id, (SELECT Id, RecordTypeId, VR_Reason_Code__c FROM Attributes__r WHERE (VR_Effective_To__c = NULL OR VR_Effective_To__c >= TODAY) AND (RecordTypeId IN :attributeRecordTypeIds) ORDER BY VR_Reason_Code__c ASC) FROM Accreditation__c WHERE Id IN :licenseIds]);

		// Iterate through Targeted Outcome Records
		for (Outcome__c outcome : targetedOutcomes)
		{
			// Define String for License Conditions
			String licenseConditions = '';

			// Determine if Accredation Map contains key
			if (accreditationMap.containsKey(outcome.Licence__c) == true)
			{
				// Get License from Accrediation Map
				Accreditation__c accreditation = accreditationMap.get(outcome.Licence__c);
				
				// Loop through license attributes
				for (Attribute__c attribute : accreditation.Attributes__r)
				{
					// Append Reason Code to License Condition String
					licenseConditions += attribute.VR_Reason_Code__c + ', ';
				}

				// Remove additional seperator from end
				licenseConditions = licenseConditions.removeEnd(', ');
			}

			// Update License Conditions on Outcome record
			outcome.Licence_Conditions__c = licenseConditions;
		}
	}

	@TestVisible
	private void updateInfringementNumber(List<Outcome__c> infringementList) {
		for(Outcome__c infringement: infringementList) {
			if(infringement.Infringement_Auto_Number__c == null ||  infringement.Infringement_Auto_Number__c == 0) {
				infringement.Infringement_Auto_Number__c = getInfringementNumberGenerator().getNewInfringementNumber();
			}
		}
	}

	@TestVisible
	private void updateSuffix(List<Outcome__c> infringementList) {
		ChecksumGeneratorFactory factory = getChecksumGeneratorFactory();

		for(Outcome__c infringement: infringementList) {
			System.debug('Setting suffix for ::' + infringement);
			ChecksumGenerator cg;
			Long checksumSource;
			if(infringement.Police_Infringement__c) {
				cg = factory.getChecksumGenerator(Constants.ORG_VIC_POLICE);
				checksumSource = (Long) infringement.Infringement_Auto_Number__c;
				System.debug('Setting police checksum::' + checksumSource);
			}
			else {
				cg = factory.getChecksumGenerator(Constants.ORG_TSV);
				checksumSource = (Long) (infringement.Infringement_Auto_Number__c + 250000000000L);
				System.debug('Setting TSV checksum::' + checksumSource);
			}

			infringement.Infringement_Suffix__c = cg.getChecksum(checksumSource);
			System.debug('Setting Infringement_Suffix__c to ::' + infringement.Infringement_Suffix__c);
		}

	}

	public class CustomException extends Exception {}

	@TestVisible
	private ChecksumGeneratorFactory getChecksumGeneratorFactory() {
		if(checksumfactoryInstance == null)
			checksumfactoryInstance = new ChecksumGeneratorFactoryImpl();
		return checksumfactoryInstance;
	}

	@TestVisible
	private InfringementNumberGenerator getInfringementNumberGenerator() {
		if(infNumberGeneratorInstance == null)
			infNumberGeneratorInstance = new InfringementNumberGeneratorImpl();
		return infNumberGeneratorInstance;
	}
}