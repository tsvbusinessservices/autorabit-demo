@isTest
public class TestDataFactory
{


    /**
     * Creates a User
     **/ 
    public static user createUser(Id ProfileId, Id roleId, Id ContactId){ 
        String randomString = Utility.returnRandomString(5);
        User U = new user(
            ProfileID = ProfileId, 
            ContactId = ContactId,
            UserRoleId = roleId,
            Isactive = true, 
            Firstname='First' + randomString, 
            LastName = 'last' + randomString, 
            Username = 'Testuser' + randomString + '@Test.com', 
            Email = 'Testuser' + randomString + '@Test.com', 
            Alias = 'U'+randomString, 
            CommunityNickname = 'U'+randomString, 
            EmailEncodingKey = 'ISO-8859-1', 
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey = 'en_AU', 
            TimeZoneSidKey = 'Australia/Sydney');
        return U;
    }

    /**
     * Gets a non portal user role
     **/ 
    public static UserRole getNonPortalRole(){
        return [Select Id From UserRole Where PortalType = 'None' Limit 1];
    }


    public static List<Account> createAccounts(Integer n) {
        return createAccounts(n, null);
    }

    public static List<Account> createAccounts(Integer n, Id OwnerId) {
        List<Account> accounts = new List<Account>();
        for(Integer i=0; i<n; i++) {
            accounts.add(createAccount('Test ' + (i+1), OwnerId));
        }
        return accounts;
    }

    public static Account createAccount(String acName) {
        return createAccount(acName, null, null, null);
    }

    public static Account createAccount(String acName, Id OwnerId) {        
        return createAccount(acName, OwnerId, null, null);
    }

    public static Account createAccount(String acName, Id OwnerId, Id RecordTypeId, String Type) {
        Account newAcc = new Account(Name = acName);
        if(OwnerId != null){
            newAcc.OwnerId = OwnerId;
        }
        if(RecordTypeId != null){
            newAcc.RecordTypeId = RecordTypeId;
        }
        if(Type != null && Type != ''){
            newAcc.Type = Type;
        }
        return newAcc;
    }



    public static List<Outcome__c> createOutcome(String recordTypeName, Integer count) {
        List<Outcome__c> outcomes = new List<Outcome__c>();
        for(Integer i =0; i < count; i++) {
            outcomes.add(createOutcome(recordTypeName));
        }
        return outcomes;
    }

    public static Outcome__c createOutcome(String recordTypeName) {
       Id recTypeId = Schema.SObjectType.Outcome__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
       return new Outcome__c(
            RecordTypeId = recTypeId);

    }

    /**
     * Creates a Contact
     * @return the Contacts
     **/    
    public static Contact createContact(Id AccountId){
        String randomString = Utility.returnRandomString(5);
        Contact Con = new Contact(lastName = randomString, 
                                firstName = randomString, 
                                MailingStreet = randomString,
                                MailingCity = randomString,
                                MailingPostalCode = randomString,
                                MailingState = randomString,
                                MailingCountry = 'Australia',                                                                                                                                
                                email = randomString + '@test.com',
                                AccountId = AccountId
                                );
        return Con;
        
    }
    
    public static AccountContactRelation createACR(Id AccountId, Id ContactId){
        AccountContactRelation ACR = new AccountContactRelation(Roles='Accounts Payable',
                                                                IsActive=TRUE,
                                                                AccountId=AccountId,
                                                                ContactId=ContactId
                                                                );
        return ACR;
    }

    public static Invoice__c createBusAnnualInvoice(Account ac) {
        Id InvrTypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('Invoice').getRecordTypeId();
        return createInvoice(ac, InvrTypeId, Constants.INVOICE_TYPE_BUS);
    }

    public static Invoice__c createInvoice(Account ac, Id InvrTypeId, String type) {
        return new Invoice__c(
             Account__c = ac.Id,
             RecordTypeId = InvrTypeId,
             Type__c = type
        );
    }

    public static Accreditation__c createBusAccreditation(Account ac) {
        Id rTypeId = Schema.SObjectType.Accreditation__c.getRecordTypeInfosByName().get('Bus').getRecordTypeId();
        return createAccreditation(ac, rTypeId, 'Accredited', Constants.ACCREDITATION_STATUS_ACCREDITED_BSA);
    }

    public static Accreditation__c createAccreditation(Account ac, Id rTypeId, String type, String status) {
        return new Accreditation__c(
            Account__c = ac.Id,
            RecordTypeId = rTypeId,
            Type__c = type,
            Status__c = status
        );
    }

    
    public static User createPartnerAccount(){
        return createPartnerAccount(null,null);
    }

    /**
     * @description create partner accounts
     * @param none
     * @return none
     **/
    public static User createPartnerAccount(List<Id> AccountRecordTypeIds, List<String> AccountTypes){ 
        //create Admin User
        Id AdminProfileId = Utility.getProfileID('System Administrator');
        Id roleId = getNonPortalRole().Id;
        User AccountOwner = createUser(AdminProfileId, roleId, null);
        insert AccountOwner;
        system.debug('Account owner = ' + AccountOwner);

        system.runAs(AccountOwner){
            
            List<Id> recordTypeIds = new List<Id>{null, null, null, null};
            List<String> types = new List<String>{null, null, null, null};

            if(AccountRecordTypeIds != null && !AccountRecordTypeIds.isEmpty() && AccountRecordTypeIds.size() <= 4){
                for(Integer i = 0; i < AccountRecordTypeIds.size(); i++){
                    recordTypeIds.set(i, AccountRecordTypeIds.get(i));
                }
            }

            if(AccountTypes != null && !AccountTypes.isEmpty() && AccountTypes.size() <= 4){
                for(Integer i = 0; i < AccountTypes.size(); i++){
                    types.set(i, AccountTypes.get(i));
                }
            }


            //Create an Account with above as owner
            Account portalAccount1 = createAccount('portalAccount1', AccountOwner.Id, recordTypeIds.get(0), types.get(0));
            Insert portalAccount1;

            system.debug('Account = ' + portalAccount1);

            //Create a contact against the PoratalAccount
            Contact portalContact = createContact(portalAccount1.Id);
            insert portalContact;

            system.debug('portalContact = ' + portalContact);

            //create a portal user for the portalContact
            Id portalProfileId = Utility.getProfileID(Constants.CommunityProfileForTesting);
            User portalUser = createUser(portalProfileId, null, portalContact.Id);
            insert portalUser;

            system.debug('portalUser = ' + portalUser);            

            //Create an Account with above as owner
            Account portalAccount2 = createAccount('portalAccount2', AccountOwner.Id, recordTypeIds.get(1), types.get(1));
            Insert portalAccount2;

            system.debug('Account = ' + portalAccount2);

            //Create a contact against the PoratalAccount
            Contact portalContact2 = createContact(portalAccount2.Id);
            insert portalContact2;

            system.debug('portalContact = ' + portalContact2);

            //create a portal user for the portalContact2
            User portalUser2 = createUser(portalProfileId, null, portalContact2.Id);
            insert portalUser2;
            system.debug('portalUser = ' + portalUser2);

            
            //Create an Account with above as owner
            Account portalAccount3 = createAccount('portalAccount3', AccountOwner.Id, recordTypeIds.get(2), types.get(2));
            Insert portalAccount3;

            system.debug('Account = ' + portalAccount3);

            //Create a contact against the PoratalAccount
            Contact portalContact3 = createContact(portalAccount3.Id);
            insert portalContact3;

            system.debug('portalContact = ' + portalContact3);

            //create a portal user for the portalContact
            User portalUser3 = createUser(portalProfileId, null, portalContact3.Id);
            insert portalUser3;
            
            //Create an Account with above as owner
            Account portalAccount4 = createAccount('portalAccount4', AccountOwner.Id, recordTypeIds.get(3), types.get(3));
            Insert portalAccount4;

            system.debug('Account = ' + portalAccount4);

            //Create a contact against the PoratalAccount
            Contact portalContact4 = createContact(portalAccount4.Id);
            insert portalContact4;

            //create a portal user for the portalContact
            User portalUser4 = createUser(portalProfileId, null, portalContact4.Id);
            insert portalUser4;

            system.debug('portalContact = ' + portalContact4);
        }
        return AccountOwner;
    } 

     /**
     * create multiple assets with Recordtypes
     **/
    public static list<Asset__c> createAssets(Integer count, Id RecordTypeId){ 
        list<Asset__c> newAssets = new list<Asset__c>();
        for(Integer i = 0; i<count; i++){
            newAssets.add(createAsset(RecordTypeId));
        }
        return newAssets;
    } 

    /**
     * create multiple assets
     **/
    public static list<Asset__c> createAssets(Integer count){ 
        list<Asset__c> newAssets = new list<Asset__c>();
        for(Integer i = 0; i<count; i++){
            newAssets.add(createAsset());
        }
        return newAssets;
    } 

     /**
     * create assets
     **/
    public static Asset__c createAsset(){ 
        return new Asset__c(name = 'Test Asset');
    }   

    /**
     * create assets with RecordType
     **/
    public static Asset__c createAsset(Id RecordTypeId){ 
        return new Asset__c(name = 'Test Asset', RecordTypeId = RecordTypeId);
    } 


    /**
     * create SAR assets
     **/
    public static Asset__c createSARAsset(){ 
        return createAsset(AssetHelper.AssetRTMap.get('SAR_Asset'));
    } 

    /**
     * create SAR assets
     **/
    public static Asset__c createBus(){ 
        return createAsset(AssetHelper.AssetRTMap.get('Bus'));
    } 

    /**
     * create ATON assets
     **/
    public static Asset__c createATONAsset(){ 
        return createAsset(AssetHelper.AssetRTMap.get('ATON'));
    }

    /**
     * create multiple assets with Recordtypes
     **/
    public static list<Asset__c> createATONAssets(Integer count, Id WaterwayId){ 
        list<Asset__c> newAssets = new list<Asset__c>();
        for(Integer i = 0; i<count; i++){
            Asset__c a = createATONAsset();
            a.Location__c = waterwayId;
            newAssets.add(a);
        }
        return newAssets;
    }  

    /**
     * create Location
     **/
    public static Location__c createLocation(Id portalAccountId){
        return createLocation(portalAccountId, null);
    }
    
    /**
     * create Location with recordType
     **/
    public static Location__c createLocation(Id portalAccountId, Id recordTypeId){
        Location__c loc = new location__c(name = 'test', Manager__c = portalAccountId);
        if(recordTypeId != null){
            loc.RecordTypeId = recordTypeId;
        }
        return loc;
    }

    public static List<Location__c> createMultipleLocation(Integer count, Id portalAccountId){
        return createMultipleLocation(count, portalAccountId, null);
    }

    public static List<Location__c> createMultipleLocation(Integer count, Id portalAccountId, Id recordTypeId){
        list<Location__c> newLoc = new list<Location__c>();
        for(Integer i = 0; i<count; i++){
            newLoc.add(createLocation(portalAccountId, recordTypeId));
        }
        return newLoc;
    }

    public static User getAdminUserForTests(){
        return [Select Id FROM user WHERE ProfileId = :Utility.getProfileID('System Administrator') AND isActive = true LIMIT 1 ];
    }
    
    public static Case createIncidentCase(Id recTypeId, Decimal testLat, Decimal testLongs) {
        return new Case(RecordTypeId = recTypeId, 
                        MALatitude__c = testLat,
                        MALongitude__c = testLongs);
    }    

    public static Case createCase(Id recTypeId, Id actId) {
        return new Case(RecordTypeId = recTypeId, accountId = actId);
    }
    
    public static Compliance_Activity__c createComplianceActivity(id recTypeId) {
        return new Compliance_Activity__c(RecordTypeId = recTypeId);
    }

    public static Asset__c createAssetLocation(id recTypeId, Decimal testLat, Decimal testLongs) {
        return new Asset__c(RecordTypeId = recTypeId,
                                          MALatitude__c = testLat,
                                          MALongitude__c = testLongs);
    }
        
    public static Compliance_Activity__c createComplianceActivityBus(id recTypeId, Decimal testLat, Decimal testLongs) {
        return new Compliance_Activity__c(RecordTypeId = recTypeId,
                                          MALatitude__c = testLat,
                                          MALongitude__c = testLongs);
    }

    public static Location__c createWaterway(String wName) {
        return new Location__c(Name = wName,
                                RecordTypeId = Constants.LOCATION_RT_ID_WATERWAY );
    }

    public static Waterway_Role__c createWaterwayRole(Id portalAccountId, Id waterwayId) {
        return new Waterway_Role__c(Type__c = 'Regional Waterway Manager',
                                Account__c = portalAccountId,
                                Waterway__c = waterwayId,
                                RecordTypeId = Constants.WATERWAY_ROLE_RT_ID_WATERWAY_MANAGER);
    }

    public static Allocation__c createGroup(){ 
        return new Allocation__c(Group_Name__c = 'group1',
                                RecordTypeId = Constants.ALLOCATION_RT_ID_GROUP);
    } 

    public static Allocation__c createLocationGroup(Id RecordTypeId){ 
        return new Allocation__c(RecordTypeId = RecordTypeId);
    } 
    
    public static Content__c createContent(){ 
        return createContent(null);
    } 

    public static Content__c createContent(Id recordTypeId){ 
        Content__c c = new Content__c(Heading__c = 'Notif heading',
                                Description__c = 'Notif descriptions',
                                Start_Date_Time__c = System.now().addDays(1),
                                End_Date_Time__c = System.now().addDays(2) ,
                                Status__c = 'Active',
                                Priority__c = 'Low');
        if(recordTypeId != null){
            c.RecordTypeId = recordTypeId;
        }
        return c;
    } 
    
    public static maps__ShapeLayer__c createMATerritory(String wwName, Id waterwayId) {
        return new maps__ShapeLayer__c(Name = wwName, Location__c = waterwayId);

    }

    public static maps__ShapeLayerGeometry__c createMAGeometry(String geoName, Id territoryId) {
        return new maps__ShapeLayerGeometry__c(Name = geoName, maps__ShapeLayer__c = territoryId);
    }

    public static List<Task> createTasks(Integer count) {
        List<Task> tasks = new List<Task>();
        for(Integer i=0; i<count; i++) {
            tasks.add(new Task(subject = 'Test ' + i));
        }
        return tasks;
    }

    public static Involved_Party__c createInvolvedPersonForVesselInspection() {
        return createInvolvedPerson(Schema.SObjectType.Involved_Party__c.getRecordTypeInfosByDeveloperName().get('Vessel_Participant').getRecordTypeId());
    }
    public static Involved_Party__c createInvolvedPersonForMarineIncident() {
        return createInvolvedPerson(Schema.SObjectType.Involved_Party__c.getRecordTypeInfosByDeveloperName().get('Incident_Participant').getRecordTypeId());        
    }
    public static Involved_Party__c createInvolvedPersonForBusIncident() {
        return createInvolvedPerson(Schema.SObjectType.Involved_Party__c.getRecordTypeInfosByDeveloperName().get('Bus_Casualty').getRecordTypeId());        
    }

    public static Involved_Party__c createInvolvedPerson(Id RTypeId) {
        Involved_Party__c ip = new Involved_Party__c(
            recordtypeid = RTypeId
        );
        return ip;
    }

    public static Account createPersonAccount() {
        Account ac = new Account();
        ac.FirstName = 'TestFirst';
        ac.LastName = 'TestLast';
        ac.recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();
        return ac;
    }

    public static List<Account> createPersonAccounts(Integer count) {
        List<Account> accs = new List<Account>();

        for(Integer i=0; i<count; i++) {
            Account ac = new Account();
            ac.FirstName = 'TestFirst_'+ (i + 1);
            ac.LastName = 'TestLast_'+ (i + 1);
            ac.recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();
            accs.add(ac);
        }

        return accs;
    }
    public static String arcGISAddress() {
        String address = '{"address":{"Match_addr":"1 Spring St, Melbourne, Victoria, 3000","LongLabel":"1 Spring St, Melbourne, Victoria, 3000, AUS","ShortLabel":"1 Spring St", "Addr_type":"PointAddress","Type":"testType","PlaceName":"testPlace","AddNum":"1","Address":"1 Spring St","Block":"testBlock","Sector":"testSector","Neighborhood":"Melbourne","District":"testDistrict","City":"Melbourne","MetroArea":"testMetro","Subregion":"testSubregion","Region":"Victoria","Territory":"testTerritory","Postal":"3000","PostalExt":"testPostExt","CountryCode":"AUS"}, "location":{"x":144.97447389409621,"y":-37.815089406359519,"spatialReference":{"wkid":4326,"latestWkid":4326}}}' ;
        return address ;
    }
    
    public static String arcGISToken() {
        String token = '{"Access_token":"This is a test token", "expires_in": 7200}';
        return token;
    }

}