/**
 * Compliance Activity Helper
 *
 * @date 18.May.2018
 * @author P.Victoriano
 */
public class ComplianceActivityHelper {

    /**
     * Gets the record types for the Compliance Activity into a map
     * @return null
     */
    public static map<Id, String> ComplianceActivityRTMap{
        get{
            if(ComplianceActivityRTMap == null){
                ComplianceActivityRTMap = Utility.getRecordTypeMapById('Compliance_Activity__c');
            }
            return ComplianceActivityRTMap;
        }
        set;
    }

    /**
     * Creates  the S for the Assets into a map
     * @return null
     */
    public static Map<String, StatusAwareShare> SharedComplianceActivity{
        get{
            if(SharedComplianceActivity == null){
                SharedComplianceActivity = new Map<String, StatusAwareShare>();
                SharedComplianceActivity.put(Constants.BUS_AUDIT_ACCREDITED_OPERATOR, new StatusAwareShare(Constants.BUS_AUDIT_ACCREDITED_OPERATOR, new Set<String>{Constants.STATUS_PUBLISHED, Constants.STATUS_COMPLETED}, Constants.SHARING_TYPE_ACR));
                SharedComplianceActivity.put(Constants.BUS_AUDIT_REGISTERED_OPERATOR, new StatusAwareShare(Constants.BUS_AUDIT_REGISTERED_OPERATOR, new Set<String>{Constants.STATUS_PUBLISHED, Constants.STATUS_COMPLETED}, Constants.SHARING_TYPE_ACR));
                SharedComplianceActivity.put(Constants.BUS_INSPECTION_QUICK_CHECK, new StatusAwareShare(Constants.BUS_INSPECTION_QUICK_CHECK, new Set<String>{},Constants.SHARING_TYPE_ACR, true));
                SharedComplianceActivity.put(Constants.BUS_INSPECTION_STANDARD, new StatusAwareShare(Constants.BUS_INSPECTION_STANDARD, new Set<String>{}, Constants.SHARING_TYPE_ACR, true));
                SharedComplianceActivity.put(Constants.MARITIME_BAE_AUDIT ,new StatusAwareShare(Constants.MARITIME_BAE_AUDIT, new Set<String>{Constants.STATUS_COMPLETED}));
                SharedComplianceActivity.put(Constants.MARITIME_AUDIT_WWAY ,new StatusAwareShare(Constants.MARITIME_AUDIT_WWAY, new Set<String>{Constants.STATUS_COMPLETED}));
            }
            return SharedComplianceActivity;
        }
        set;
    }


    @TestVisible
    private static CheckPointInPolygonUtility pipUtil = null;


    /**
     * Asynchronous Call to set Water Way
     * @param setId Id of Compliance Activity Records
     */
    
    public static void setWaterWay(List<Compliance_Activity__c> lrecords) {
        System.debug('ComplianceActivityHelper::setWaterWay');

        if (lrecords != null) {
            // List to hold cases that has updated Waterways
            Map<Id, Id> caToWaterwayMap = new Map<Id, Id>();
            Map<Id, Decimal> latMap = new Map<Id, Decimal>();
            Map<Id, Decimal> longMap = new Map<Id, Decimal>();

            for (Compliance_Activity__c oCA : lrecords) {
                if (oCA.MALatitude__c != null && oCA.MALongitude__c != null) {                  
                    caToWaterwayMap.put(oCA.Id, oCA.Waterway__c);
                    latMap.put(oCA.Id, oCA.MALatitude__c);
                    longMap.put(oCA.Id, oCA.MALongitude__c);
                } else {
                    System.debug('ComplianceActivityHelper::setWaterWay:: Setting waterway to null');
                    oCA.Waterway__c = null;
                }
            }
            if(!caToWaterwayMap.isEmpty()){
                System.debug('ComplianceActivityHelper::setWaterWay:: Calling future method for::' + caToWaterwayMap);
                updateWaterway(caToWaterwayMap, latMap,longMap);
            }
        }
    }


    @Future(Callout=true)
    static void updateWaterway(Map<Id, Id> caToWaterwayMap, 
                                Map<Id, Decimal> latMap, 
                                Map<Id, Decimal> longMap) {

        System.debug('ComplianceActivityHelper::updateWaterway:: Start of Future method');


        if(caToWaterwayMap != null && caToWaterwayMap.size() >0) {
            List<Compliance_Activity__c> caToUpdate = new List<Compliance_Activity__c>();
            for(Id caID: caToWaterwayMap.keySet()) {
                Id recordWaterWayId = caToWaterwayMap.get(caID);
                // get the waterway Id based on the location
                Id waterWayId = getPIPUtil().checkPointInPolygon(
                                      latMap.get(caID),
                                      longMap.get(caID));
                System.debug('ComplianceActivityHelper::recordWaterWayId::' + recordWaterWayId);
                System.debug('ComplianceActivityHelper::waterWayId::' + waterWayId);

                // update waterway only if it has changed
                if(recordWaterWayId != waterWayId) {
                        caToUpdate.add(new Compliance_Activity__c(Id = caID, Waterway__c = waterWayId)); 
                }
            }

            if(caToUpdate.size() > 0) {
                System.debug('Updating the Waterways for::' + caToUpdate);
                update caToUpdate;
            }
            
        }
    }

    @TestVisible
    private static CheckPointInPolygonUtility getPIPUtil() {
        if(pipUtil == null) {
            pipUtil = new CheckPointInPolygonUtilityImpl();
        }
        return pipUtil;
    }

    public static List<Compliance_Activity__c> getEnrichedData(Set<Id> theCAIds) {
        return [Select Id, Bus_Operator__c, Status__c, RecordTypeId, RecordType.DeveloperName
                    From Compliance_Activity__c 
                    Where Id IN: theCAIds];
    }


    public static boolean isVesselInspection(Compliance_Activity__c rec) {
        return Constants.VESSEL_INSPECTION == ComplianceActivityRTMap.get(rec.recordTypeId) || Constants.VESSEL_INSPECTION_UNREGISTERED == ComplianceActivityRTMap.get(rec.recordTypeId);
    }
    
    public static boolean isBusInspection(Compliance_Activity__c rec) {
        return Constants.BUS_INSPECTION_QUICK_CHECK == ComplianceActivityRTMap.get(rec.recordTypeId) || Constants.BUS_INSPECTION_STANDARD == ComplianceActivityRTMap.get(rec.recordTypeId) || Constants.BUS_INSPECTION_HAZARDOUS == ComplianceActivityRTMap.get(rec.recordTypeId);
    }    

    public static boolean isLocationChanged(Compliance_Activity__c newRec, Compliance_Activity__c oldRec) {
        return newRec.MALatitude__c != oldRec.MALatitude__c || newRec.MALongitude__c != oldRec.MALongitude__c;
    }


    public static StatusAwareShare getShareRecord(Compliance_Activity__c rec) {
        String rtName = ComplianceActivityRTMap.get(rec.recordTypeId);
        return SharedComplianceActivity.get(rtName);
    }

}