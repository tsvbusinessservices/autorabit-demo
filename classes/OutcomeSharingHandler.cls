public class OutcomeSharingHandler extends TriggerBase implements ITriggerBase{
	/**
	 * afterInsert Trigger Event
	 * @return null
	 */
	public override void afterInsert() {
		outcomeSharing(records, null, 'Insert');
	}
	/**
	 * afterUpdate Trigger Event	
	 * @return null
	 */
	public override void afterUpdate() {
		outcomeSharing(records, oldmap, 'Update');
	}

	/**
	 * Controlls the Apex sharing against the Outcome Object	
	 * @param mode = 'Insert', 'Update', 'Recalculation'
	 * @return null
	 */
	public static void outcomeSharing(list<SObject> records, Map<Id, SObject> oldMap, String mode){
		//Get an instance of the SharingHelper class with the Object Name
		SharingHelper SharingHelp = new SharingHelper(Outcome__Share.sObjectType.getDescribe().getName());

		//get a set of outcome Ids by casting to map and getting keys
		set<Id> theOutcomeIds = new map<Id, Outcome__c>((list<Outcome__c>) records).keySet();

		//loop round and add the Id and the Portal account Id
		for(Outcome__c outcome : OutcomeHelper.getEnrichedData(theOutcomeIds) ){

			//get old Outcome - only applicable if running in Update mode
			Outcome__c oldOutcome = null;
			if(mode == 'Update' && oldMap != null){
				oldOutcome = (Outcome__c) oldMap.get(outcome.Id);
			}
			
			//if new or changed or Recalculate then add the new Account to the sharing
			if(mode == 'Insert' || mode == 'Recalculation' 
				|| outcome.Account__c != oldOutcome.Account__c 
				|| outcome.Status__c != oldOutcome.Status__c){
				// check if sharing is required
				StatusAwareShare shareRecord = OutcomeHelper.getShareRecord(outcome);
				System.debug('shareRecord::' + shareRecord);
				
				String status = OutcomeHelper.getStatus(outcome);

				if(shareRecord != null && shareRecord.canShare(status)) {
					SharingHelp.addToSharing(outcome.Account__c, outcome.Id, Schema.Outcome__Share.RowCause.Operator__c, 'Edit', shareRecord.shareType);
				}

				//if changed OR Recalculation mode than delete the old one
				if(mode == 'Recalculation' 
					|| (mode == 'Update' && (outcome.Account__c != oldOutcome.Account__c
												|| outcome.Status__c != oldOutcome.Status__c))){
					SharingHelp.addToDeleteList(outcome.Id);
				}
			}
		}

		//delete any sharing rules that need to be deleted
		SharingHelp.deleteSharingRules(new set<String> {Schema.Outcome__Share.RowCause.Operator__c});

		//Insert Sharing rules asyncronously
		SharingHelp.insertApexSharing_Asyncronously();
	}
}