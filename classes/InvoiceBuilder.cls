public class InvoiceBuilder {
    
    public static Invoice__c buildInvoice(Id accountId, Id accreditationId, String invoiceSettingType, String type) {
    
    Id invRecordTypeID= Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get(Constants.INVOICE_RECORDTYPE).getRecordTypeId();
        BillingSettingWrapper setting = BillingSettingWrapper.getInstance(invoiceSettingType);
        Invoice__c invoice = new Invoice__c();
        invoice.RecordTypeID = invRecordTypeID;
        invoice.Account__c = accountId;
        invoice.accreditation__c = accreditationId;
        invoice.Type__c = type;
        invoice.Term__c = setting.getTermName();
        invoice.Payment_Terms__c = setting.getTermName();
//        invoice.Due_Date__c = Date.today().addDays(setting.getTermDays());
        invoice.Comments__c = setting.getComments();
        invoice.Transaction_Type__c = setting.getTransactionType();
        invoice.Oracle_Contact_Name__c = setting.getOracleContactName();
        return invoice;
    }

    public static List<Invoice_Line_Item__c> buildBusAnnualFeeLineItems(Invoice__c invoice, Integer totalBuses) {
        List<Invoice_Line_Item__c> lineItems = new List<Invoice_Line_Item__c>();
        BillingSettingWrapper billingSetting = BillingSettingWrapper.getInstance(Constants.BILLING_SETTINGS_BUS);
        BusConfigWrapper busConfigBasefee = BusConfigWrapper.getConfig(Constants.BILLING_CONFIG_BUS_BASE_FEE);

        //Calculate Tax amount
        Decimal TaxPerc_base = billingSetting.getTaxPercentage();
        Decimal FeeAmt_base = busConfigBasefee.getFeeAmount();
        
        Decimal AppliedTax_base = FeeAmt_base * TaxPerc_base ;

        lineItems.add(createLI(invoice, 1, busConfigBasefee.getFeeAmount(), busConfigBasefee.getFeeDesc(), 
                                billingSetting.getTaxCode(), 
                                AppliedTax_base,                                 
                                billingSetting.getChargeCode(), 1 ));

        if(totalBuses > 1) {
        
            BusConfigWrapper busConfigAdditionalFee = BusConfigWrapper.getConfig(Constants.BILLING_CONFIG_BUS_ADDITIONAL_FEE);
            
            //Calculate Tax amount
            Decimal TaxPerc_add = billingSetting.getTaxPercentage();
            Decimal FeeAmt_add = busConfigAdditionalFee.getFeeAmount();
            
            Decimal AppliedTax_add = FeeAmt_add * TaxPerc_add ;
                
            lineItems.add(createLI(invoice, totalBuses - 1, busConfigAdditionalFee.getFeeAmount(), 
                                    busConfigAdditionalFee.getFeeDesc(), 
                                    billingSetting.getTaxCode(), 
                                    AppliedTax_add,
                                    billingSetting.getChargeCode(), 2));
        }

        return lineItems;
    } 

    static Invoice_Line_Item__c createLI(Invoice__c invoice, Integer qty, Decimal price, String description,
                                            String taxCode, Decimal taxAmt,
                                            String chargeCode, Integer index) {
        return new Invoice_Line_Item__c(
            Invoice__c = invoice.Id,
            ILI_Flow_Trigger__c = description,
            Quantity__c = qty,
            Unit_Price__c = price,
            Description__c = description,
            Tax_Code__c = taxCode,
            Gst__c = taxAmt,
            Charge_Code__c = chargeCode,
            Line_Sequence_Number__c  = index
            );
    }
}