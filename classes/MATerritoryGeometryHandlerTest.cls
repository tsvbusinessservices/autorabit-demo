@isTest
private class MATerritoryGeometryHandlerTest
{
	@testSetup
    static void init() {

        Location__c testWW = TestDataFactory.createWaterway('Test Waterway');
        insert testWW;

        maps__ShapeLayer__c testTerritory = TestDataFactory.createMATerritory('Test testTerritory', testWW.Id);
        testTerritory.Extent_Territory__c = true;
        insert testTerritory;

    }

    @isTest
    static void testInsert() {
    	TriggerSettingsClass.testOverride = true;
    	maps__ShapeLayer__c testTerritory = [Select Id from maps__ShapeLayer__c Limit 1];

    	maps__ShapeLayerGeometry__c testGeometry = TestDataFactory.createMAGeometry('testGeometry', testTerritory.Id);

        testGeometry.maps__Geometry__c = '{  ' 
                                        +'   "proximityType":"Polygon",'
                                        +'   "points":[  '
                                        +'      {  '
                                        +'         "lng":-80.8431,'
                                        +'         "lat":35.2271'
                                        +'      },'
                                        +'      {  '
                                        +'         "lng":-78.8784,'
                                        +'         "lat":35.0527'
                                        +'      },'
                                        +'      {  '
                                        +'         "lng":-79.7626,'
                                        +'         "lat":34.1954'
                                        +'      }'
                                        +'   ],'
                                        +'   "isCustom":true'
                                        +'}';
        insert testGeometry;

		maps__ShapeLayerGeometry__c result = [SELECT Id, Max_X__c, Min_X__c, Max_Y__c, Min_Y__c FROM maps__ShapeLayerGeometry__c WHERE Id =: testGeometry.Id];
        System.assertEquals(35.2271, result.Max_X__c);
        System.assertEquals(-78.8784, result.Max_Y__c);
        System.assertEquals(34.1954, result.Min_X__c);
        System.assertEquals(-80.8431, result.Min_Y__c);
    }

    @isTest
    static void testUpdate() {
    	//disable for insert trigger
    	TriggerSettingsClass.testOverride = false;
    	maps__ShapeLayer__c testTerritory = [Select Id from maps__ShapeLayer__c Limit 1];

    	maps__ShapeLayerGeometry__c testGeometry = TestDataFactory.createMAGeometry('testGeometry', testTerritory.Id);

        testGeometry.maps__Geometry__c = '{  ' 
                                        +'   "proximityType":"Polygon",'
                                        +'   "points":[  '
                                        +'      {  '
                                        +'         "lng":-80.8431,'
                                        +'         "lat":35.2271'
                                        +'      },'
                                        +'      {  '
                                        +'         "lng":-78.8784,'
                                        +'         "lat":35.0527'
                                        +'      },'
                                        +'      {  '
                                        +'         "lng":-79.7626,'
                                        +'         "lat":34.1954'
                                        +'      }'
                                        +'   ],'
                                        +'   "isCustom":true'
                                        +'}';
        insert testGeometry;

        // enable trigger
        TriggerSettingsClass.testOverride = true;
        testGeometry.maps__Geometry__c = '{  ' 
                                        +'   "proximityType":"Polygon",'
                                        +'   "points":[  '
                                        +'      {  '
                                        +'         "lng":-88.8431,'
                                        +'         "lat":39.2271'
                                        +'      },'
                                        +'      {  '
                                        +'         "lng":-78.8784,'
                                        +'         "lat":35.0527'
                                        +'      },'
                                        +'      {  '
                                        +'         "lng":-79.7626,'
                                        +'         "lat":34.1954'
                                        +'      }'
                                        +'   ],'
                                        +'   "isCustom":true'
                                        +'}';

        update testGeometry;

		maps__ShapeLayerGeometry__c result = [SELECT Id, Max_X__c, Min_X__c, Max_Y__c, Min_Y__c FROM maps__ShapeLayerGeometry__c WHERE Id =: testGeometry.Id];
        System.assertEquals(39.2271, result.Max_X__c);
        System.assertEquals(-78.8784, result.Max_Y__c);
        System.assertEquals(34.1954, result.Min_X__c);
        System.assertEquals(-88.8431, result.Min_Y__c);
    }

    @isTest 
    static void testClearGeometry() {
    	TriggerSettingsClass.testOverride = true;
    	maps__ShapeLayer__c testTerritory = [Select Id from maps__ShapeLayer__c Limit 1];

    	maps__ShapeLayerGeometry__c testGeometry = TestDataFactory.createMAGeometry('testGeometry', testTerritory.Id);

        testGeometry.maps__Geometry__c = '{  ' 
                                        +'   "proximityType":"Polygon",'
                                        +'   "points":[  '
                                        +'      {  '
                                        +'         "lng":-80.8431,'
                                        +'         "lat":35.2271'
                                        +'      },'
                                        +'      {  '
                                        +'         "lng":-78.8784,'
                                        +'         "lat":35.0527'
                                        +'      },'
                                        +'      {  '
                                        +'         "lng":-79.7626,'
                                        +'         "lat":34.1954'
                                        +'      }'
                                        +'   ],'
                                        +'   "isCustom":true'
                                        +'}';
        insert testGeometry;

   
        testGeometry.maps__Geometry__c = null;

        update testGeometry;

		maps__ShapeLayerGeometry__c result = [SELECT Id, Max_X__c, Min_X__c, Max_Y__c, Min_Y__c FROM maps__ShapeLayerGeometry__c WHERE Id =: testGeometry.Id];
        System.assertEquals(null, result.Max_X__c);
        System.assertEquals(null, result.Max_Y__c);
        System.assertEquals(null, result.Min_X__c);
        System.assertEquals(null, result.Min_Y__c);
    }

    @isTest 
    static void testInvalidJSON() {
    	TriggerSettingsClass.testOverride = true;
    	maps__ShapeLayer__c testTerritory = [Select Id from maps__ShapeLayer__c Limit 1];

    	maps__ShapeLayerGeometry__c testGeometry = TestDataFactory.createMAGeometry('testGeometry', testTerritory.Id);

        testGeometry.maps__Geometry__c = '{  ' 
                                        +'   "proximityType":"Polygon",'
                                        +'   "points":[  '
                                        +'      {  '
                                        +'         "lng":-80.8431,'
                                        +'         "lat":35.2271'
                                        +'      },'
                                        +'      {  '
                                        +'         "lng":-78.8784,'
                                        +'         "lat":35.0527'
                                        +'      },'
                                        +'      {  '
                                        +'         "lng":-79.7626,'
                                        +'         "lat":34.1954'
                                        +'      }'
                                        +'   ],'
                                        +'   "isCustom":true'
                                        +'}';
        insert testGeometry;


        testGeometry.maps__Geometry__c = '{  ' 
                                        +'   "proximityType":"Polygon",'
                                        +'   "points":[  '
                                        +'      {  '
                                        +'         "lng":-80.8431,'
                                        +'         "lat":35.2271'
                                        +'      },';

        update testGeometry;

		maps__ShapeLayerGeometry__c result = [SELECT Id, Max_X__c, Min_X__c, Max_Y__c, Min_Y__c FROM maps__ShapeLayerGeometry__c WHERE Id =: testGeometry.Id];
        System.assertEquals(null, result.Max_X__c);
        System.assertEquals(null, result.Max_Y__c);
        System.assertEquals(null, result.Min_X__c);
        System.assertEquals(null, result.Min_Y__c);
    }

}