/**
 * AllocationHandler class. creates Apex sharing rules for the Location
 *
 * @author Arnie Ug
 * @date 3.June.2019
 */
public with sharing class AllocationHandler extends TriggerBase implements ITriggerBase {
    public AllocationHandler() {

    }

    /**
     * afterInsert Trigger Event
     * @return null
     */
    public override void afterInsert() {
        allocationSharing(records, null, 'Insert');
    }

    /**
     * beforeUpdate Trigger Event    
     * @return null
     */
    public override void beforeUpdate() {
        checkduplicates(records, oldmap);
    }

    /**
     * afterUpdate Trigger Event    
     * @return null
     */
    public override void afterUpdate() {
        allocationSharing(records, oldmap, 'Recalculation');
    }
    /**
     * afterDelete Trigger Event    
     * @return null
     */
    public override void afterDelete() {
        allocationSharing(oldRecords, null, 'Recalculation');
    }


    /**
     * Controlls the Apex sharing against the Content Object   
     * @param mode = 'Insert', 'Update', 'Recalculation'
     * @return null
     */
    public static void allocationSharing(list<SObject> records, Map<Id, SObject> oldMap, String mode){
        //Get an instance of the SharingHelper class with the Object Name
        SharingHelper SharingHelp = new SharingHelper(Allocation__Share.sObjectType.getDescribe().getName());

        //Get all sharing under facility group and waterway group
        Set<Id> groupAllocationIds = new Set<Id>();
        for(Allocation__c allocation : (List<Allocation__c>) records){
            //Only run sharing calculation if the record is owned by a System Admin or has BV Admin Permissions
            if(allocation.RecordTypeId == Constants.ALLOCATION_RT_ID_FACILITY_GROUP || allocation.RecordTypeId == Constants.ALLOCATION_RT_ID_WATERWAY_GROUP){
                groupAllocationIds.add(allocation.Group__c);
            }else if(allocation.RecordTypeId == Constants.ALLOCATION_RT_ID_GROUP){
                groupAllocationIds.add(allocation.Id);
            }            
        }

        //Get all related facility and waterway groups
        Set<Id> recordTypeIds = new Set<Id>{Constants.ALLOCATION_RT_ID_FACILITY_GROUP, Constants.ALLOCATION_RT_ID_WATERWAY_GROUP};
        List<Allocation__c> facilityWaterwayGroupAllocations = [SELECT Id, Facility__c, Waterway_Name__c, Group__c, RecordTypeId FROM Allocation__c WHERE RecordTypeId IN :recordTypeIds AND Group__c IN :groupAllocationIds];
        List<Allocation__c> groupAllocations = [SELECT Id, Facility__c, Waterway_Name__c, Group__c, Private__c, RecordTypeId FROM Allocation__c WHERE Id IN :groupAllocationIds];

        //Get a map of all sharing and group to facility/waterway groups
        Map<Id,List<Location__Share>> mapOfLocationGroupIdToShares = AllocationHelper.getLocationGroupIdtoSharingMap(facilityWaterwayGroupAllocations);
        Map<Id,List<Allocation__c>> mapOfGroupIdToLocationGroups = AllocationHelper.getGroupIdToLocationGroups(groupAllocationIds, facilityWaterwayGroupAllocations);

        //loop round facility group and waterway groups to add sharing access
        for(Allocation__c allocation : groupAllocations){                   
            //For Groups
            if(allocation.RecordTypeId == Constants.ALLOCATION_RT_ID_GROUP){  
                if(mapOfGroupIdToLocationGroups != null && mapOfGroupIdToLocationGroups.get(allocation.id) != null){          
                    for(Allocation__c locgroupAlloc : mapOfGroupIdToLocationGroups.get(allocation.id)){
                        if(allocation.Private__c == false && (mode == 'Insert' || mode == 'Recalculation')){       
                            if(mapOfLocationGroupIdToShares != null && !mapOfLocationGroupIdToShares.isEmpty() && mapOfLocationGroupIdToShares.get(locgroupAlloc.Id) != null){
                                for(Location__Share share : mapOfLocationGroupIdToShares.get(locgroupAlloc.Id)){
                                    SharingHelp.addToClonedSharing(allocation.id, share.RowCause, share.UserOrGroupId, 'Read');  
                                    SharingHelp.addToClonedSharing(locgroupAlloc.id, share.RowCause, share.UserOrGroupId, 'Read');                     
                                } 
                            }                            
                        }

                        //if changed OR Recalculation mode than delete the old one
                        if(mode == 'Recalculation'){
                            SharingHelp.addToDeleteList(locgroupAlloc.id);
                        }                        
                    }
                }

                //if changed OR Recalculation mode than delete the old one
                if(mode == 'Recalculation'){
                    SharingHelp.addToDeleteList(allocation.id);
                } 
            }
        }

        //delete any sharing rules that need to be deleted
        SharingHelp.deleteSharingRules(new set<String> {Schema.Allocation__Share.RowCause.Facility_Manager__c, Schema.Allocation__Share.RowCause.Secondary_Waterway_Manager__c, Schema.Allocation__Share.RowCause.Parent_Waterway_Manager__c});

        //Insert Cloned Sharing rules
        if(Test.isRunningTest()){
            SharingHelp.cloneApexSharing_Syncronously();
        }else{
            SharingHelp.cloneApexSharing_Asyncronously();
        }
        
    }


    private void checkduplicates(list<SObject> records, Map<Id, SObject> oldMap){
        List<Allocation__c> groupsToFinalize = new List<Allocation__c>();
        //loop round facility group and waterway groups to add sharing access
        for(Allocation__c allocation : (List<Allocation__c>) records){
            //get old Location - only applicable if running in Update mode
            Allocation__c oldAllocation = null;
            if(oldMap != null){
                oldAllocation = (Allocation__c) oldMap.get(allocation.Id);
            }

            if(allocation.RecordTypeId == Constants.ALLOCATION_RT_ID_GROUP 
                && allocation.Status__c == Constants.ALLOCATION_STATUS_FINALIZED
                && allocation.Unique_Group__c == false){
                groupsToFinalize.add(allocation);
            }else if(allocation.Status__c != Constants.ALLOCATION_STATUS_FINALIZED){
                allocation.GroupMembersHashCode__c = null;
            }
        }

        AllocationHelper.checkDuplicates(groupsToFinalize);
    } 
}