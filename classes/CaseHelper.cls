public class CaseHelper {


	public static boolean isMarineIncident(Case rec) {
		return rec.recordTypeId == Constants.MARINE_INCIDENT_RECORDTYPE_ID;
	}

	public static boolean isLocationChanged(Case newRec, Case oldRec) {
		return newRec.MALatitude__c != oldRec.MALatitude__c || newRec.MALongitude__c != oldRec.MALongitude__c;
	}
}