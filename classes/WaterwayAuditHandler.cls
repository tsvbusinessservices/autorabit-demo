public class WaterwayAuditHandler extends TriggerBase implements ITriggerBase{


	/**
	 * befoerUpdate Trigger Event	
	 * @return null
	 */
	public override void beforeUpdate() {
		Map<Id, AuditData> waterwayToAuditMap  = new Map<Id, AuditData>();

		Date minStartDate = null;
		Date maxEndDate = null;

		// Check If the Finalize_Audit__c Check has changed? And Waterways is populated
		// process only those records
		for(Compliance_Activity__c obj: (List<Compliance_Activity__c>)records){
			Compliance_Activity__c oldObj = (Compliance_Activity__c) oldmap.get(obj.Id);
			System.debug('Finalise::' + obj.Finalize_Audit__c);
			System.debug('Old Finalize::' + oldObj.Finalize_Audit__c);
			if(obj.Finalize_Audit__c != oldObj.Finalize_Audit__c 
				&& obj.Start_Date__c != null && obj.End_Date__c != null && obj.Waterway__c != null) {

				// calculate and store the main and max dates for the whole batch
				if(minStartDate == null || minStartDate > obj.Start_Date__c) {
					minStartDate = obj.Start_Date__c;
				}
				if(maxEndDate == null || maxEndDate < obj.End_Date__c) {
					maxEndDate = obj.End_Date__c;
				}

				System.debug('Min Start Date::' + minStartDate);
				System.debug('Max End Date::' + maxEndDate);

				// Create the Auditdata VO and store it agains the waterway Id in the map 
				AuditData ad = new AuditData(obj);
				waterwayToAuditMap.put(obj.Waterway__c, ad);

				obj.Finalize_Audit__c = false;
			}
		}

		// If there are any audits with start and end dates populated then process it
		if(waterwayToAuditMap.size()>0) {

			// populate ruleItems for all auditData
			populateRuleItems(waterwayToAuditMap.values());

			// CleanUp all realted Tasks to this audit
			cleanupOldTaks(waterwayToAuditMap);

			// Finalise this audit based on the dates supplied
			finalizeAuditData(waterwayToAuditMap, minStartDate, maxEndDate.addDays(1));
		}
	}

	// Unlinks all the tasks for the audits provided in the Map
	@TestVisible
	void cleanupOldTaks(Map<Id, AuditData> waterwayToAuditMap) {

		// Get the Unique Id List of the Audits
		Set<Id> auditIds = new Set<Id>();

		// Get the unique Ids for the Rule Items
		Set<Id> ruleItemIds = new Set<Id>();

		for(AuditData ad: waterwayToAuditMap.values()) {
			auditIds.add(ad.audit.Id);
			ruleItemIds.addAll(ad.getRuleItemIds());
		}

		// Get the link to the documents attached to rule Items
		List<ContentDocumentLink> imageLinks = getContentDocumentLinks(ruleItemIds);


		// Get all the tasks linked to the audits in the map
		List<Task> existingTasks  = [SELECT Id, Compliance_Activity__c FROM Task WHERE Compliance_Activity__c in :auditIds];

		for(Task t: existingTasks) {
			// unlink the tasks
			t.Compliance_Activity__c = null;
		}

		update existingTasks;

		// Cleanup all the Junction objects
		delete [SELECT Id  FROM Compliance_Activity_Asset__c WHERE Compliance_Activity__c in :auditIds];

		// Cleanup all the image links to the Rule Items.
		delete imageLinks;
	}


	/**
	 * Populates the Compliance_Activity__c for the Task where the LastModifiedDate is within start and end date for the range
	 */ 
	void finalizeAuditData(Map<Id, AuditData> waterwayToAuditMap, Date minStartDate, Date maxEndDate) {

		// Get all assets and their completed tasks that has LastModified date beteen the start and end date for whole batch
		List<Asset__c> auditedAssets = [SELECT Id, Name, Location__c, Rule__c, ESRI_ID__c,
											(SELECT Id, Status, LastModifiedDate
												FROM Tasks
												WHERE Status = 'Completed'
														AND CreatedDate >= :minStartDate 
													AND CreatedDate <= :maxEndDate)
											FROM Asset__c 
											WHERE recordtype.developerName = 'ATON'
												AND Location__c in :waterwayToAuditMap.keySet()];

		// List of Compliance Activity assest to be added
		List<Compliance_Activity_Asset__c> CAassets = new List<Compliance_Activity_Asset__c>();

		//
		List<ContentDocument> imagesToUpdate = new List<ContentDocument>();

		List<ContentDocumentLink> rulesToImagesLink = new List<ContentDocumentLink>();

		// Add ALL tasks to the audit where Waterway matches with the audit
		// Filtering of the tasks based on the start and end date of the Audit is done with VO
		for(Asset__c auditedAsset: auditedAssets) {


			// If there are tasks to process
			if(auditedAsset.Tasks.size() >0) {
				// get the Audit VO from Map
				AuditData audit = waterwayToAuditMap.get(auditedAsset.Location__c);

				// Add ALL tasks to VO
				audit.addTasks(auditedAsset.Tasks);

				// add Audited asset to Audit
				audit.addAsset(auditedAsset);
			}
		}


		// Filter and Populate the Audit Ids on the Tasks
		List<Task> updatedTasks = processTasks(waterwayToAuditMap);

		// Map for task Id to Rule Id Map. Rule Id is available on the asset related to task
		Map<Id, Id> taskToRuleMap = getTaskToRuleMap(auditedAssets);
		System.debug('WaterwayAuditHandler::finalizeAuditData::taskToRuleMap.size::' + taskToRuleMap.size());


		// Get the ContentDocument Links based for all the tasks that are in the map
		// This also ensures that the images have an associated rule to it
		List<ContentDocumentLink> imageLinks = getContentDocumentLinks(taskToRuleMap.keySet());
		System.debug('WaterwayAuditHandler::finalizeAuditData::imageLinks.size::' + imageLinks.size());

		// Map of task Id to ContentDocument Ids
		Map<Id, List<ContentDocumentLink>> taskToContentDocumentMap = getTaskToContentDocumentMap(imageLinks);
		System.debug('WaterwayAuditHandler::finalizeAuditData::taskToContentDocumentMap.size::' + taskToContentDocumentMap.size());


		// Map of rule id and list of images for the rule
		Map<Id, List<ContentDocument>> ruleToImagesMap = getImagesMap(auditedAssets, taskToRuleMap, imageLinks);
		System.debug('WaterwayAuditHandler::finalizeAuditData::ruleToImagesMap.size::' + ruleToImagesMap.size());

		
		
		for(AuditData data: waterwayToAuditMap.values()) {
			// Get the Junction object data for the audit
			CAassets.addAll(data.createComplianceActivityAssetList());
			
			if(imageLinks.size()> 0) {
				rulesToImagesLink.addAll(data.createContentDocumentLinkForRuleItems(ruleToImagesMap));

				imagesToUpdate.addAll(data.getRenamedContentDocuments(taskToContentDocumentMap));

			}
		}

		// Important to update image names first, so they flow through to Conga Template
		if(imagesToUpdate.size() > 0) {
			update imagesToUpdate;
		}

		// Update tasks with Audit link if there are any 
		if(updatedTasks.size() >0) {
			update updatedTasks;
		}

		System.debug('WaterwayAuditHandler::finalizeAuditData::CAassets.size::' + CAassets.size());

		// Create junction objects if there are any assets identified for Waterway for CA
		if(CAassets.size() >0) {
			insert CAassets;
		}


		if(rulesToImagesLink.size() > 0) {
			insert rulesToImagesLink;
		}
	}

	Map<Id, Id> getTaskToRuleMap(List<Asset__c> auditedAssets) {
		// Map for task Id to Rule Id Map. Rule Id is available on the asset related to task
		Map<Id, Id> taskToRuleMap = new Map<Id, Id>();

		System.debug('WaterwayAuditHandler::getTaskToRuleMap::auditedAssets.size::' + auditedAssets.size());


		for(Asset__c auditedAsset: auditedAssets) {
			System.debug('WaterwayAuditHandler::getTaskToRuleMap::auditedAsset.Name::' + auditedAsset.Name);

			for(Task t: auditedAsset.Tasks) {

				System.debug('WaterwayAuditHandler::getTaskToRuleMap::auditedAsset.Rule__c::' + auditedAsset.Rule__c);
				// populate the map if the rule is populated on the asset.
				if(auditedAsset.Rule__c != null) {
					taskToRuleMap.put(t.Id, auditedAsset.Rule__c);
				}
			}
		}
		return taskToRuleMap;

	}

	Map<Id, List<ContentDocumentLink>> getTaskToContentDocumentMap(List<ContentDocumentLink> imageLinks) {
		Map<Id, List<ContentDocumentLink>> taskToContentDocumentMap = new Map<Id, List<ContentDocumentLink>>();

		for(ContentDocumentLink cdl: imageLinks) {
			Id taskId = cdl.LinkedEntityId;
			List<ContentDocumentLink> csdlList = taskToContentDocumentMap.get(taskId);
			if(csdlList == null) {
				csdlList = new List<ContentDocumentLink>();
				taskToContentDocumentMap.put(taskId, csdlList);
			}
			csdlList.add(cdl);
		}

		return taskToContentDocumentMap;
	}

	/**
	 * Processes all the tasks against the Audits for the waterway by populating the audit on the activity where the LastModified Date
	 * is within the range of each Audit
	 * @return 
	 **/
	List<Task> processTasks(Map<Id, AuditData> waterwayToAuditMap) {
		List<Task> updatedTasks = new List<Task>();

		// Loop through all audits in the map
		for(AuditData audit: waterwayToAuditMap.values()) {

			// Get the Filterd tasks based on Dates and Compliance_Activity__c populated
			List<Task> tasks =  audit.getFilteredAuditTasks();

			// Add the filtered tasks to returning list.
			updatedTasks.addAll(tasks);
		}

		return updatedTasks;
	}

	/**
	 * Creates a map of the Documents against the rules related to the audited assets.
	 *
	 **/
	Map<Id, List<ContentDocument>>  getImagesMap(List<Asset__c> auditedAssets, Map<Id, Id> taskToRuleMap,
												 List<ContentDocumentLink> imageLinks) {

		// Initialize the rules to image map
		// Can have more than one images for each rule
		Map<Id, List<ContentDocument>> ruleToImagesMap = new Map<Id, List<ContentDocument>>();
		

		
		// Get the underlying content document (image) record for the links
		Map<Id, ContentDocument> documentMap = getContentDocumentMap(imageLinks);

		// populate the rules to image map 
		for(ContentDocumentLink imgLink: imageLinks) {
			Id taskId = imgLink.LinkedEntityId;
			Id ruleId = taskToRuleMap.get(taskId);

			List<ContentDocument> documentList = ruleToImagesMap.get(ruleId);

			if(documentList == null) {
				documentList = new List<ContentDocument>();
				ruleToImagesMap.put(ruleId, documentList);
			}
			documentList.add(documentMap.get(imgLink.ContentDocumentId));
		}

		return ruleToImagesMap;
	}

	/**
	 * Gets the Map of underlying ContentDocuments from the list of ContentDocumentLinks
	 * 
	 **/
	Map<Id, ContentDocument> getContentDocumentMap(List<ContentDocumentLink> imageLinks) {
		Set<Id> documentIds = new Set<Id>();

		for(ContentDocumentLink docLink: imageLinks) {
			documentIds.add(docLink.ContentDocumentId);
		}

		return new Map<Id, ContentDocument>([SELECT Id, Title FROM ContentDocument
												WHERE Id IN :documentIds]);
	}


	/**
	 * Get the List of ContentDocuemtLinks for the entities.
	 * This need to be done using the Dynamic SOQL due to the limitation on the  ContentDocumentLink object
	 *
	 **/
	List<ContentDocumentLink> getContentDocumentLinks(Set<Id> linkedEntityIds) {
		if(linkedEntityIds == null || linkedEntityIds.size() ==0)
			return new List<ContentDocumentLink>();
		String sql = 'SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId IN(';

		// Concatenate Ids of the entities
		for(Id linkedEntityId: linkedEntityIds ) {
			sql += '\'' + linkedEntityId + '\',';
		}

		// remove the last ,
		sql = sql.left(sql.length() -1);

		// close the IN clause of the sql
		sql += ')';

		return (List<ContentDocumentLink>) Database.query(sql);
	}

	void populateRuleItems(List<AuditData> audits) {
		Map<Id, AuditData> auditMap = new Map<Id, AuditData>();

		for(AuditData data: audits) {
			auditMap.put(data.audit.Id, data);
		}

		List<Compliance_Activity__c> dbAudits = [SELECT Id,
													(SELECT Id, Rule__c FROM Outcomes__r)
												 FROM Compliance_Activity__c
												 WHERE Id IN :auditMap.keySet()];

		for(Compliance_Activity__c dbAudit: dbAudits) {
			AuditData data = auditMap.get(dbAudit.Id);

			data.ruleItems = dbAudit.Outcomes__r;
		}

	}

}