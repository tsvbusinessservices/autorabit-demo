global class BusAnnualFeeRenewalBatch implements Database.Batchable<sObject> {
    
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
      Integer graceDays = BillingSettingWrapper.getInstance(Constants.BILLING_SETTINGS_BUS).getBillingGraceDays();
      Date invoiceDate = Date.today().addDays(graceDays);

        return Database.getQueryLocator([SELECT Id, Account__c, Account__r.Number_of_Vehicles__c, 
                                                Type__c, Anniversary_Date__c 
                                            FROM Accreditation__c
                                            WHERE Type__c =: Constants.ACCREDITATION_TYPE_BUS_OPERATOR
                                            AND Status__c =: Constants.ACCREDITATION_STATUS_ACCREDITED_BSA
                                            AND Anniversary_Date__c < :invoiceDate]);
    }

   
   global void execute(Database.BatchableContext BC, List<sObject> scope) {

        List<Accreditation__c> accreditations = (List<Accreditation__c>) scope;

        Map<String, Invoice__c> accreditationInvoiceMap = new Map<String, Invoice__c>();

        List<Invoice__c> invoices = createDraftInvoices(accreditations, accreditationInvoiceMap);
        insert invoices;

        List<Invoice_Line_Item__c> lineItems = createLineItems(accreditations, accreditationInvoiceMap);
        insert lineItems;
        
        update invoices;
        update accreditations;

    }

    List<Invoice__c> createDraftInvoices(List<Accreditation__c> accreditations,
                                        Map<String, Invoice__c> accreditationInvoiceMap) {

        List<sObject> invoices = new List<sObject>();
        Id invRecordTypeID= Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get(Constants.INVOICE_RECORDTYPE).getRecordTypeId();
        
        for(Accreditation__c accreditation: accreditations) {

            Invoice__c invoice = InvoiceBuilder.buildInvoice(accreditation.Account__c, accreditation.Id,
                                                                Constants.BILLING_SETTINGS_BUS,
                                                                Constants.INVOICE_TYPE_BUS);
            invoice.Status__c = Constants.INVOICE_STATUS_DRAFT;
            invoice.RecordTypeID = invRecordTypeID;
            invoices.add(invoice);

            accreditationInvoiceMap.put(accreditation.Id, invoice);

        }
        return invoices;
    }

    List<Invoice_Line_Item__c> createLineItems(List<Accreditation__c> accreditations, Map<String, Invoice__c> accreditationInvoiceMap) {
        List<Invoice_Line_Item__c> lineItems = new List<Invoice_Line_Item__c>();

        for(Accreditation__c accreditation: accreditations) {
            Invoice__c invoice = accreditationInvoiceMap.get(accreditation.Id);
            lineItems.addAll(InvoiceBuilder.buildBusAnnualFeeLineItems(invoice, (Integer)accreditation.Account__r.Number_of_Vehicles__c));
            
            invoice.Status__c = Constants.INVOICE_STATUS_COMPLETE;
            accreditation.Anniversary_Date__c = accreditation.Anniversary_Date__c.addYears(1);
        }

        return lineItems;
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
}