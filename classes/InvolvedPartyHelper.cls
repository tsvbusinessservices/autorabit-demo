public class InvolvedPartyHelper {

    /**
     * Checks if the Involved Person record is a Marine type record
     */ 
    public static Boolean isMarineRecordType(Involved_Party__c rec) {
        return isVesselInspectionRecordType(rec) || isMarineIncidentRecordType(rec);
    }

    /**
     * Checks if the Involved Person record is a Vessel Inspection record
     */ 
    public static Boolean isVesselInspectionRecordType(Involved_Party__c rec) {
        Id recordId = rec.recordTypeId;
        return recordId == Schema.SObjectType.Involved_Party__c.getRecordTypeInfosByDeveloperName().get('Vessel_Participant').getRecordTypeId();
    }

    /**
     * Checks if the Involved Person record is a Marine Incident record
     */ 
    public static Boolean isMarineIncidentRecordType(Involved_Party__c rec) {
        Id recordId = rec.recordTypeId;
        return recordId == Schema.SObjectType.Involved_Party__c.getRecordTypeInfosByDeveloperName().get('Incident_Participant').getRecordTypeId();
    }

    /**
     * Creates a Person Account from the values available on the involved Person record.
     */ 
    public static Account createPersonFromInvolvedPerson(Involved_Party__c involvedPerson) {
        Account person = new Account();
        person.recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();
        person.FirstName = involvedPerson.First_Name__c;
        person.LastName = involvedPerson.Last_Name__c;
        person.PersonBirthdate = involvedPerson.Birthdate__c;
        person.Gender__c = involvedPerson.Gender__c;
        person.BillingStreet = involvedPerson.Street__c;
        person.BillingCity = involvedPerson.Suburb__c;
        person.BillingState = involvedPerson.State__c;
        person.BillingPostalCode = involvedPerson.Postcode__c;
        return person;
    }


    /**
     * Creates a Accreditation Record from the values available on the involved person record.
     */ 
    public static Accreditation__c createLicenceFromInvolvedPerson(Involved_Party__c involvedPerson) {
        Accreditation__c accr = new Accreditation__c();
        accr.recordTypeId = Schema.SObjectType.Accreditation__c.getRecordTypeInfosByDeveloperName().get('Recreational_Boat_User').getRecordTypeId();
        accr.Account__c = involvedPerson.Person__c;
        If(involvedPerson.License_Origin__c == 'VIC')
            accr.VR_Cert_Id__c = involvedPerson.Licence_Number__c;
        else 
            accr.Licence_Number__c = involvedPerson.Licence_Number__c;
        accr.Expiry_Date__c = involvedPerson.Licence_Expiry_Date__c;
        accr.Status__c = involvedPerson.Licence_Status__c;
        accr.License_Category__c = involvedPerson.License_Category__c;
        accr.Type__c = involvedPerson.Licence_Type__c;
        accr.State__c = involvedPerson.License_Origin__c;
        accr.Endorsement_Type__c = involvedPerson.Endorsement_Type__c;
        return accr;
    }
}