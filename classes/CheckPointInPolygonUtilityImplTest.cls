@isTest
private class CheckPointInPolygonUtilityImplTest
{
    @testSetup
    static void init() {

        Location__c testWW = TestDataFactory.createWaterway('Test Waterway');
        insert testWW;

        maps__ShapeLayer__c testTerritory = TestDataFactory.createMATerritory('Test testTerritory', testWW.Id);
        testTerritory.Extent_Territory__c = true;
        insert testTerritory;

        maps__ShapeLayerGeometry__c testGeometry = TestDataFactory.createMAGeometry('testGeometry', testTerritory.Id);

        testGeometry.maps__Geometry__c = '{  ' 
                                        +'   "proximityType":"Polygon",'
                                        +'   "points":[  '
                                        +'      {  '
                                        +'         "lng":-80.8431,'
                                        +'         "lat":35.2271'
                                        +'      },'
                                        +'      {  '
                                        +'         "lng":-78.8784,'
                                        +'         "lat":35.0527'
                                        +'      },'
                                        +'      {  '
                                        +'         "lng":-79.7626,'
                                        +'         "lat":34.1954'
                                        +'      }'
                                        +'   ],'
                                        +'   "isCustom":true'
                                        +'}';
        testGeometry.Min_X__c=34.1954;
        testGeometry.Max_X__c=35.2271;
        testGeometry.Min_Y__c=-80.8431;
        testGeometry.Max_Y__c=-78.8784;

        insert testGeometry;

    }

    @isTest
    static void testServiceCreation() {
        System.assert(CheckPointInPolygonUtilityImpl.getPIPService() instanceof MapAnyThingPointInPolygonService);
    }

    @isTest
    static void testPointInPolygon() {

        MockPointInPolygonService mockService = new MockPointInPolygonService();
        mockService.success = true;

        CheckPointInPolygonUtilityImpl.service = mockService;

        Location__c testWW = [Select Id from Location__c where Name = 'Test Waterway' Limit 1];

        CheckPointInPolygonUtilityImpl instance = new CheckPointInPolygonUtilityImpl();
        String wwId = instance.checkPointInPolygon(34.5127, -79.8509);
		Geolocation.getWaterway(34.5127, -79.8509);

        System.assertEquals(null, wwId);

    }

    @isTest
    static void testPointInPolygon_Negative() {

        MockPointInPolygonService mockService = new MockPointInPolygonService();
        mockService.success = false; 

        CheckPointInPolygonUtilityImpl.service = mockService;

        Location__c testWW = [Select Id from Location__c where Name = 'Test Waterway' Limit 1];

        CheckPointInPolygonUtilityImpl instance = new CheckPointInPolygonUtilityImpl();
        String wwId = instance.checkPointInPolygon(34.5127, -79.8509);


        System.assertEquals(null, wwId);

    }


}