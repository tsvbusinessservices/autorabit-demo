global  class OutcomeSharingRecalcBatch implements Database.Batchable<sObject>, Schedulable {
	
	
	global Database.QueryLocator start(Database.BatchableContext BC) {

		return Database.getQueryLocator([SELECT Id FROM Outcome__c WHERE RecordType.DeveloperName IN :OutcomeHelper.SharedOutcome.KeySet()]);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		OutcomeSharingHandler.OutcomeSharing(scope, null, 'Recalculation');
	}
	
	global void finish(Database.BatchableContext BC) {
		//Send email
		String emailSubject = 'SUCCESS: Apex Sharing Recalculation For Outcome Completed';
		String emailBody = 'The Apex sharing recalculation finished processing for the Outcome Object';
		Utility.sendEmail(emailSubject, emailBody, 'Success');
	}

	global void execute(SchedulableContext sc) {
		OutcomeSharingRecalcBatch b = new OutcomeSharingRecalcBatch();
		database.executeBatch(b, 200);
	}
}