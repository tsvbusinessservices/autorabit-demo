@isTest
private class CaseHandlerTest
{
	

	@isTest
	static void testUpdates()
	{
		TriggerSettingsClass.testOverride = false;

		Account a = TestDataFactory.createAccount('Test Account');
		insert a;

		Case newCase = TestDataFactory.createCase(Constants.MARINE_INCIDENT_RECORDTYPE_ID, a.Id);
		newCase.MALatitude__c = 40.7;
		newCase.MALongitude__c = 30.8;
		
		Id busRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Bus - Inquiry').getRecordTypeId();
		Case otherCase = TestDataFactory.createCase(busRecTypeId, a.Id);
		
		insert new List<Case>{newCase, otherCase};

		List<Case> results = [Select Id, Location__c From Case
								Where RecordTypeId =: Constants.MARINE_INCIDENT_RECORDTYPE_ID];

		// Validate if the trigger was not fired
		System.assertEquals(1, results.size());
		System.assertEquals(null, results.get(0).Location__c);
		
		TriggerSettingsClass.testOverride = null;

		newCase.MALatitude__c = 10.2;
		newCase.MALongitude__c = 20.2;
		otherCase.Subject = 'Test Case';

		Test.startTest();
		update new List<Case>{newCase, otherCase};
		Test.stopTest();

		results = [Select Id, Waterway_Verified__c From Case
								Where RecordTypeId =: Constants.MARINE_INCIDENT_RECORDTYPE_ID];

		System.assertEquals(1, results.size());
		System.assert(!results.get(0).Waterway_Verified__c);


	}
}