/**
 * Accreditation Handler Class
 *
 * @author P.Victoriano
 * @date 16.May.2018
 */
public class AccreditationHandler extends TriggerBase implements ITriggerBase {
	Map<String, Accreditation_Setting__mdt> mpAccreditaionSetting = new Map<String, Accreditation_Setting__mdt>();
	public AccreditationHandler() {
		//default the Accreditation_Setting__mdt
		for (Accreditation_Setting__mdt mdt : [SELECT Endorsement_Header__c, Footer__c, Label, MasterLabel, Preamble__c, Details__c FROM Accreditation_Setting__mdt]) {
			mpAccreditaionSetting.put(mdt.MasterLabel, mdt);
		}
	}
	/**
	 * Override beforeInsert for Custom Implementation
	 * @return null
	 */
	public override void beforeInsert() {
		//Get Record Type Id Maritime Operator
		Id moRTId = Schema.SObjectType.Accreditation__c.getRecordTypeInfosByName().get('Maritime Operator').getRecordTypeId();
		//
		for (Accreditation__c obj : (List<Accreditation__c>)records) {
			if (obj.RecordTypeId == moRTId) {
				//Apply defaulting logic
				defaultAccreditaionSetting(obj);
			}
		}
	}
	/**
	 * Maps the Accreditation values to the record
	 * @param obj Accreditaion record
	 */
	@TestVisible
	private void defaultAccreditaionSetting(Accreditation__c obj) {
		if (mpAccreditaionSetting.containsKey(obj.Type__c)) {
			Accreditation_Setting__mdt mdtRec = mpAccreditaionSetting.get(obj.Type__c);
			//map fields
			if (String.isBlank(obj.Endorsement_Header__c)) {
				obj.Endorsement_Header__c = mdtRec.Endorsement_Header__c;
			}
			if (String.isBlank(obj.Preamble__c)) {
				obj.Preamble__c = mdtRec.Preamble__c;
			}
			if (String.isBlank(obj.Footer__c)) {
				obj.Footer__c = mdtRec.Footer__c;
			}
			obj.Details__c = mdtRec.Details__c;
		}
	}
}