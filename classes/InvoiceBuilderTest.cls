@isTest
private class InvoiceBuilderTest
{
    @isTest
    static void testBuildInvoice() {
        Account ac = TestDataFactory.createAccount('Test');
        insert ac;

        String BillSetting = Constants.BILLING_SETTINGS_BUS;

        Accreditation__c accred = TestDataFactory.createBusAccreditation(ac);

        Invoice__c invoice = InvoiceBuilder.buildInvoice(ac.Id, accred.Id, Constants.BILLING_SETTINGS_BUS, Constants.INVOICE_TYPE_BUS);

        Billing_Setting__mdt setting = [SELECT DeveloperName, Charge_Code__c, Comments__c,
                                            Tax_Code__c, 
                                            Term_Days__c, Term_Name__c, Transaction_Type__c
                                         FROM Billing_Setting__mdt 
                                         WHERE DeveloperName = :BillSetting Limit 1];


        System.assertEquals(ac.Id, invoice.Account__c);
        System.assertEquals(accred.Id, invoice.Accreditation__c);
        System.assertEquals(Constants.INVOICE_TYPE_BUS, invoice.Type__c);
        System.assertEquals(setting.Term_Name__c, invoice.Term__c);
//        System.assertEquals(Date.today().addDays((Integer)setting.Term_Days__c), invoice.Due_Date__c);
        System.assertEquals(setting.Comments__c, invoice.Comments__c);
        System.assertEquals(setting.Transaction_Type__c, invoice.Transaction_Type__c);
//        System.assertEquals(setting.Oracle_Contact_Name__c, invoice.Oracle_Contact_Name__c);

    }

    @isTest
    static void testBuildBusAnnualFeeInvoiceLineItems() {
        Account ac = TestDataFactory.createAccount('Test');
        Contact acc = TestDataFactory.createContact(ac.id);
        AccountContactRelation acr = TestDataFactory.createACR(acc.AccountId, acc.Id);
        insert ac;

        Invoice__c invoice = TestDataFactory.createBusAnnualInvoice(ac);
        insert invoice;

        String BillSetting = Constants.BILLING_SETTINGS_BUS;
        String BaseFee = Constants.BILLING_CONFIG_BUS_BASE_FEE ;
        String AdditionalFee = Constants.BILLING_CONFIG_BUS_ADDITIONAL_FEE ;                

        Billing_Setting__mdt billingSetting = [SELECT DeveloperName, 
                                         Tax_Code__c, 
                                         Charge_Code__c
                                         FROM Billing_Setting__mdt 
                                         WHERE DeveloperName = :BillSetting Limit 1];

        Bus_Config__mdt busBaseConfig = [Select DeveloperName, Fee__c, Line_Description__c
                                                FROM Bus_Config__mdt
                                                WHERE DeveloperName = :BaseFee Limit 1];

        Bus_Config__mdt busAdditionalConfig = [Select DeveloperName, Fee__c, Line_Description__c
                                                FROM Bus_Config__mdt
                                                WHERE DeveloperName = :AdditionalFee Limit 1];


        List<Invoice_Line_Item__c> li = InvoiceBuilder.buildBusAnnualFeeLineItems(invoice, 1);
        System.assertEquals(1, li.size());

        insert li;

        List<Invoice_Line_Item__c> result = [ Select Id, Invoice__r.Name, Quantity__c, Unit_Price__c, Description__c, 
                                                        Tax_Code__c, 
                                                        CompositeKey__c, Line_Sequence_Number__c
                                                FROM Invoice_Line_Item__c
                                                Where Invoice__c =: invoice.Id];

        System.assertEquals(1, result.size());
        System.assertEquals(1, result.get(0).Quantity__c);
        System.assertEquals(busBaseConfig.Fee__c, result.get(0).Unit_Price__c);
        System.assertEquals(busBaseConfig.Line_Description__c, result.get(0).Description__c);
        System.assertEquals(billingSetting.Tax_Code__c, result.get(0).Tax_Code__c);
        System.assertEquals(1, result.get(0).Line_Sequence_Number__c);
        // Compositekey is populated by workflow rule : Populate Composite Key
        System.assertEquals(result.get(0).Invoice__r.name + '-1', result.get(0).CompositeKey__c);


        li = InvoiceBuilder.buildBusAnnualFeeLineItems(invoice, 20);
        System.assertEquals(2, li.size());

        System.assertEquals(19, li.get(1).Quantity__c);
        System.assertEquals(busAdditionalConfig.Fee__c, li.get(1).Unit_Price__c);
        System.assertEquals(busAdditionalConfig.Line_Description__c, li.get(1).Description__c);
        System.assertEquals(billingSetting.Tax_Code__c, li.get(1).Tax_Code__c);
        System.assertEquals(billingSetting.Charge_Code__c, li.get(1).Charge_Code__c);   
        System.assertEquals(1, li.get(0).Line_Sequence_Number__c);
        System.assertEquals(2, li.get(1).Line_Sequence_Number__c);

    }
}