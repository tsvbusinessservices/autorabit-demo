public interface ChecksumGenerator {

	String getChecksum(Long source);
	
}