@isTest

private class WaterwayRoleHandlerTest {
    @isTest static void test_method_one() {
        // Implement test code
    }
    
    //P - Create Waterway Role with Account Only
    @isTest static void PositiveTest_CreateWaterwayRoleAccount() {
        //Get the Partner Account Id
        Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount2'].Id;

        Location__c loc1 = TestDataFactory.createLocation(portalAccountId, Constants.LOCATION_RT_ID_WATERWAY);
        insert loc1;

        test.startTest();

        Waterway_Role__c wwr1 = TestDataFactory.createWaterwayRole(portalAccountId, loc1.id);
        insert wwr1;

        test.stopTest();

        //check that there has been a sharing record created
        List<Location__Share> locSharing = LocationHelper.getLocSharing(new set<Id>{loc1.Id}, new set<String>{Location__Share.RowCause.Secondary_Waterway_Manager__c});
        system.assertEquals(1, locSharing.size());
    }

    //P - Create Location with manager
    @isTest static void PositiveTest_UpdateWaterwayRole() {
        //Get the Partner Account Id
        Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount2'].Id;

        Location__c loc1 = TestDataFactory.createLocation(portalAccountId, Constants.LOCATION_RT_ID_WATERWAY);
        insert loc1;

        test.startTest();
        
        Waterway_Role__c wwr1 = TestDataFactory.createWaterwayRole(portalAccountId, loc1.id);
        insert wwr1;

        test.stopTest();

        //check that there has been a sharing record created
        List<Location__Share> locSharing = LocationHelper.getLocSharing(new set<Id>{loc1.Id}, new set<String>{Location__Share.RowCause.Secondary_Waterway_Manager__c});
        system.assertEquals(1, locSharing.size());
    }

    //P - Create Waterway Role for facilities with Account Only
    @isTest static void PositiveTest_CreateWaterwayRoleAccountForFacility() {
        //Get the Partner Account Id
        Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;
        Id portalAccountId2 = [SELECT Id FROM Account WHERE Name = 'portalAccount2'].Id;

        Location__c waterway = TestDataFactory.createLocation(portalAccountId2, Constants.LOCATION_RT_ID_WATERWAY);
        insert waterway;

        Location__c facility  = TestDataFactory.createLocation(portalAccountId);
        facility.RecordTypeId = Constants.LOCATION_RT_ID_FACILITY;
        facility.Waterway_Name__c = waterway.id;
        insert facility;

        test.startTest();

        Waterway_Role__c wwr1 = TestDataFactory.createWaterwayRole(portalAccountId2, waterway.id);
        insert wwr1;

        test.stopTest();

        //check that there has been a sharing record created
        List<Location__Share> locSharing = LocationHelper.getLocSharing(new set<Id>{facility.Id}, new set<String>{Location__Share.RowCause.Secondary_Waterway_Manager__c});
        //system.assertEquals(1, locSharing.size());
    }

    //P - Create Waterway Role for facilities with Account and Contact
    @isTest static void NegativeTest_DeleteWaterwayRoleAccountForFacility() {
        //Get the Partner Account Id
        Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount2'].Id;
        Id portalAccountId2 = [SELECT Id FROM Account WHERE Name = 'portalAccount2'].Id;

        Location__c waterway = TestDataFactory.createLocation(portalAccountId2, Constants.LOCATION_RT_ID_WATERWAY);
        insert waterway;

        Location__c facility  = TestDataFactory.createLocation(portalAccountId, Constants.LOCATION_RT_ID_FACILITY);
        facility.Waterway_Name__c = waterway.id;
        insert facility;
        
        Waterway_Role__c wwr1 = TestDataFactory.createWaterwayRole(portalAccountId2, waterway.id);
        insert wwr1;

        test.startTest();

        delete wwr1;

        test.stopTest();

        //check that there has been a sharing record created
        List<Location__Share> locSharing = LocationHelper.getLocSharing(new set<Id>{facility.Id}, new set<String>{Location__Share.RowCause.Secondary_Waterway_Manager__c});
        system.assertEquals(0, locSharing.size());
    }

    @testSetup static void testSetup(){
        TestDataFactory.createPartnerAccount(new List<Id>{Constants.ACCOUNT_RT_ID_MARITIME,Constants.ACCOUNT_RT_ID_MARITIME},new List<String>{Constants.ACCOUNT_TYPE_FACILITY_MANAGER,Constants.ACCOUNT_TYPE_WATERWAY_MANAGER});
    }
    
}