@isTest
private class AssetSharingRecalcBatchTests {
	
	
	@isTest static void test_Scedhuling() {
		AssetSharingRecalcBatch batch = new AssetSharingRecalcBatch();
		String sch = '0 0 10 * * ?';
		system.schedule('Recalc test 12345', sch, batch);
	}

	//P - Run Recalc job - testing new location for ATONsis picked up when the batch job runs
	@isTest static void PositiveTest_RunRecalcJob() {
		Id portalAccountId1 = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;
		Location__c loc1 = TestDataFactory.createLocation(portalAccountId1);
		insert loc1;

		//insert ATONs with NO location
		list<Asset__c> Assets = TestDataFactory.createAssets(200, AssetHelper.AssetRTMap.get('ATON'));
		insert Assets;

		//add in the location after - this does not trigger an update
		for(Asset__c A : Assets){
			A.Location__c = loc1.Id;
		}
		
		update Assets;

		map<Id, Asset__c> AssetMap = new map<Id, Asset__c>(Assets);

		//check that there are no sharing
		List<Asset__Share> AssetNone = AssetHelper.getAssetSharing(AssetMap.keySet(), new set<String>{Schema.Asset__Share.RowCause.Waterway_Manager__c});
		system.assertEquals(0, AssetNone.size());

		test.startTest();
		
		AssetSharingRecalcBatch b = new AssetSharingRecalcBatch();
		database.executeBatch(b, 200);

		test.stopTest();

		//check that there has been a sharing record created
		List<Asset__Share> AssetSharing = AssetHelper.getAssetSharing(AssetMap.keySet(), new set<String>{Schema.Asset__Share.RowCause.Waterway_Manager__c});
		system.assertEquals(200, AssetSharing.size());
	}

	@testSetup static void testSetup(){
		TestDataFactory.createPartnerAccount();
	}
	
}