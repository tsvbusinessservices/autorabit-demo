@isTest
private class ComplianceActivityHandlerTest
{
    @isTest
    static void testInserts()
    {
        Map<String, Id> rtMap = Utility.getRecordTypeMap('Compliance_Activity__c');

        Location__c l = TestDataFactory.createLocation(null);
        insert l;

        MockPIPUtility mpipUtil = new MockPIPUtility(l.Id);

        ComplianceActivityHelper.pipUtil = mpipUtil;

        Compliance_Activity__c newCa = TestDataFactory.createComplianceActivity(rtMap.get(Constants.VESSEL_INSPECTION));
        //newCa.Waterway_Vessel_Category__c = 'Coastal Offshore-Yacht';
        newCa.MALatitude__c = 10.2;
        newCa.MALongitude__c = 20.2;
        newCa.Waterway__c = l.Id;



        Id busRecTypeId = rtMap.get(Constants.BUS_AUDIT_ACCREDITED_OPERATOR);
        Compliance_Activity__c otherCa = TestDataFactory.createComplianceActivity(busRecTypeId);
        

        Test.startTest();
        insert new List<Compliance_Activity__c>{newCa, otherCa};
        Test.stopTest();

        List<Compliance_Activity__c> results = [Select Id, Waterway__c From Compliance_Activity__c
                                Where RecordTypeId =: rtMap.get(Constants.VESSEL_INSPECTION)];

        System.assertEquals(1, results.size());
        System.assertEquals(l.Id, results.get(0).Waterway__c);


    }

    @isTest
    static void testUpdates()
    {
        Map<String, Id> rtMap = Utility.getRecordTypeMap('Compliance_Activity__c');

        TriggerSettingsClass.testOverride = false;

        Location__c l = TestDataFactory.createLocation(null);
        insert l;

        MockPIPUtility mpipUtil = new MockPIPUtility(l.Id);

        ComplianceActivityHelper.pipUtil = mpipUtil;

        Compliance_Activity__c newCa = TestDataFactory.createComplianceActivity(rtMap.get(Constants.VESSEL_INSPECTION));
        //newCa.Waterway_Vessel_Category__c = 'Coastal Offshore-Yacht';
        
        Id busAuditRecTypeId = rtMap.get(Constants.BUS_AUDIT_ACCREDITED_OPERATOR);
        Compliance_Activity__c auditCa = TestDataFactory.createComplianceActivity(busAuditRecTypeId);
        
        Id busInspectRecTypeId = rtMap.get(Constants.BUS_INSPECTION_STANDARD);
        Compliance_Activity__c busInsCa = TestDataFactory.createComplianceActivity(busInspectRecTypeId);        
        
        insert new List<Compliance_Activity__c>{newCa, auditCa, busInsCa};

        List<Compliance_Activity__c> results = [Select Id, Waterway__c From Compliance_Activity__c
                                Where RecordTypeId =: rtMap.get(Constants.VESSEL_INSPECTION)];

        // Validate if the trigger was not fired
        System.assertEquals(1, results.size());
        System.assertEquals(null, results.get(0).Waterway__c);
        
        TriggerSettingsClass.testOverride = null;

        newCa.MALatitude__c = 10.2;
        newCa.MALongitude__c = 20.2;
        newCa.Waterway__c = l.Id;
        auditCa.Location_Description__c = 'Test CA';
        busInsCa.Location_Description__c = 'Test CA';
        busInsCa.MALatitude__c = 10.2;
        busInsCa.MALongitude__c = 20.2;
        busInsCa.Waterway__c = l.Id;

        Test.startTest();
        update new List<Compliance_Activity__c>{newCa, auditCa, busInsCa};
        Test.stopTest();

        results = [Select Id, Waterway__c From Compliance_Activity__c
                                Where RecordTypeId =: rtMap.get(Constants.VESSEL_INSPECTION)];

        System.assertEquals(1, results.size());
        System.assertEquals(l.Id, results.get(0).Waterway__c);
        
        results = [Select Id, Waterway__c From Compliance_Activity__c
                   Where RecordTypeId =: rtMap.get(Constants.BUS_AUDIT_ACCREDITED_OPERATOR)];
                   
        System.assertEquals(1, results.size());
        
         results = [Select Id, Waterway__c From Compliance_Activity__c
                   Where RecordTypeId =: rtMap.get(Constants.BUS_INSPECTION_STANDARD)];       
        
        System.assertEquals(1, results.size());
        System.assertEquals(l.Id, results.get(0).Waterway__c);                   
    }

    @isTest
    static void vesselInspections1()
    {
        // Create Test Account
        Id account_RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();
        
        Date birthDate = System.today().addYears(-30);

        Account account = new Account();
        account.RecordTypeId = account_RecordTypeId;
        account.BillingCity = 'Melbourne';
        account.BillingPostalCode = '3000';
        account.BillingState = 'VIC';
        account.BillingStreet = '1 Bourke Street';
        account.FirstName = 'Test';
        account.LastName = 'User';
        account.Gender__c = 'Male';
        account.PersonBirthDate = birthDate;
        insert account;

        // Create Test Asset
        Id asset_recordTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByDeveloperName().get('Vessel').getRecordTypeId();

        Date expiryDate = System.today();
        Date stolenDate = System.today().addDays(-5);

        Asset__c asset = new Asset__c();
        asset.RecordTypeId = asset_recordTypeId;
        asset.Owner__c = account.Id;
        asset.Colour__c = 'White';
        asset.Engine_Count__c = 2;
        asset.Engine_Make__c = 'Ferrari';
        asset.Hull_Length__c = 20;
        asset.Hull_Material__c = 'Steel';
        asset.Maximum_Engine_Power__c = 200;
        asset.Name = 'TEST-ASSET';
        asset.Registration_Expiry_Date__c = expiryDate;
        asset.Registration_Number__c = 'TEST-ASSET';
        asset.Status__c = 'Active';
        asset.Stolen__c = true;
        asset.Stolen_Date__c = stolenDate;
        asset.Vessel_Fuel_Type__c = 'Electric';
        asset.Vessel_Make__c = 'Royal Carribean';
        asset.Vessel_Model__c = 'Ovation Of The Seas';
        asset.Vessel_Propulsion__c = 'In/Outboard';
        asset.Vessel_Type__c = 'Open';
        insert asset;

        Id accreditation_recordTypeId = Schema.SObjectType.Accreditation__c.getRecordTypeInfosByDeveloperName().get('Recreational_Boat_User').getRecordTypeId();

        Date firstIssueDate = System.today().addDays(-20);
        Date expiryDate2 = firstIssueDate.addYears(2);

        Accreditation__c accreditation = new Accreditation__c();
        accreditation.RecordTypeId = accreditation_recordTypeId;
        accreditation.Account__c = account.Id;
        accreditation.State__c = 'VIC';
        accreditation.Country__c = 'Australia';
        accreditation.Endorsement_Type__c = 'PWC Endorsed';
        accreditation.Type__c = 'General';
        accreditation.License_Category__c = 'Recreational';
        accreditation.VR_Cert_ID__c = '1234567890';
        accreditation.First_Issue_Date__c = firstIssueDate;
        accreditation.Expiry_Date__c = expiryDate2;
        accreditation.Status__c = 'Current';
        insert accreditation;

        Id complianceActivity_recordTypeId = Schema.SObjectType.Compliance_Activity__c.getRecordTypeInfosByDeveloperName().get('Vessel_Inspection').getRecordTypeId();
        
        Compliance_Activity__c complianceActivity = new Compliance_Activity__c();
        complianceActivity.RecordTypeId = complianceActivity_recordTypeId;
        complianceActivity.Vehicle_Vessel__c = asset.Id;
        complianceActivity.Owner_is_Operator__c = true;
        complianceActivity.Status__c = 'Submitted';


        Test.startTest();
        
        insert complianceActivity;

        complianceActivity = new Compliance_Activity__c();
        complianceActivity.RecordTypeId = complianceActivity_recordTypeId;
        insert complianceActivity;

        Id account2_recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Organisation').getRecordTypeId();

        Account account2 = new Account();
        account2.Name = 'Test Company Pty. Ltd.';
        account2.Trading_Name__c = 'Test Company';
        insert account2;

        Accreditation__c accreditation2 = new Accreditation__c();
        accreditation2.RecordTypeId = accreditation_recordTypeId;
        accreditation2.Account__c = account2.Id;
        accreditation2.State__c = 'VIC';
        accreditation2.Country__c = 'Australia';
        accreditation2.Endorsement_Type__c = 'PWC Endorsed';
        accreditation2.Type__c = 'General';
        accreditation2.License_Category__c = 'Recreational';
        accreditation2.VR_Cert_ID__c = '1234567891';
        accreditation2.First_Issue_Date__c = firstIssueDate;
        accreditation2.Expiry_Date__c = expiryDate2;
        accreditation2.Status__c = 'Current';
        insert accreditation2;

        asset.Owner__c = account2.Id;
        update asset;

        complianceActivity.Vehicle_Vessel__c = asset.Id;
        complianceActivity.Owner_is_Operator__c = true;
        complianceActivity.Status__c = 'Submitted';
        update complianceActivity;
        
        Test.stopTest();
    }

    @isTest
    static void vesselInspections2()
    {
        Id complianceActivity_recordTypeId = Schema.SObjectType.Compliance_Activity__c.getRecordTypeInfosByDeveloperName().get('Vessel_Inspection').getRecordTypeId();
        
        Date birthDate = System.today().addDays(-30);

        Compliance_Activity__c complianceActivity = new Compliance_Activity__c();
        complianceActivity.RecordTypeId = complianceActivity_recordTypeId;

        complianceActivity.Create_Individual__c = true;
        complianceActivity.Suburb_Operator__c = 'Melbourne';
        complianceActivity.Postcode_Operator__c = '3000';
        complianceActivity.State_Operator__c = 'VIC';
        complianceActivity.Street_Operator__c = '1 Bourke Street';
        complianceActivity.First_Name_Operator__c = 'Test';
        complianceActivity.Last_Name_Operator__c = 'User';
        complianceActivity.Gender_Operator__c = 'Male';
        complianceActivity.Date_of_Birth_Operator__c = birthDate;

        Date expiryDate = System.today().addYears(1);
        Date stolenDate = System.today().addDays(-5);

        complianceActivity.Create_Vessel__c = true;
        complianceActivity.Hull_Colour__c = 'White';
        complianceActivity.Engine_Number__c = 2;
        complianceActivity.Engine_Make__c = 'Ferrari';
        complianceActivity.Hull_Length__c = 20;
        complianceActivity.Vessel_Material__c = 'Steel';
        complianceActivity.Maximum_Engine_Power__c = 200;
        complianceActivity.Rego_Number_Call_Sign__c = 'TEST-BOAT';
        complianceActivity.Registration_Expiry_Date__c = expiryDate;
        complianceActivity.Vessel_Status__c = 'Active';
        complianceActivity.Stolen__c = true;
        complianceActivity.Stolen_Date__c = stolenDate;
        complianceActivity.Vessel_Fuel_Type__c = 'Electric';
        complianceActivity.Vessel_Make__c = 'Royal Carribean';
        complianceActivity.Vessel_Model__c = 'Ovation of the Seas';
        complianceActivity.Vessel_Propulsion__c = 'In/Outboard';
        complianceActivity.Vessel_Type__c = 'Open';
        complianceActivity.Master_fail_to_carry_licence_Rec__c = true;

        Test.startTest();
        insert complianceActivity;
        
        complianceActivity.Create_Individual__c = true;
        complianceActivity.Create_Vessel__c = true;

        update complianceActivity;

        Test.stopTest();
    }
}