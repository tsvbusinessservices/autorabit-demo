///////////Retrieve Token from Custom Metadata//////////////
 public class SecuritySettingWrapper {

    Ext_Security_Setting__mdt setting;
    
    static SecuritySettingWrapper instance = null;

    SecuritySettingWrapper(Ext_Security_Setting__mdt setting) {
        this.setting = setting;
    } 

    public static SecuritySettingWrapper getConfig(String name) {
        
        {
            Ext_Security_Setting__mdt settingMeta = [SELECT DeveloperName, Platform__c, ClientID__c, ClientSecret__c, Token__c        
                                         FROM Ext_Security_Setting__mdt
                                         WHERE DeveloperName =: name Limit 1];

            instance= new SecuritySettingWrapper (settingMeta);
        }

        return instance;
        
    }
    
  
    public String getClientID() {
        return setting.ClientID__c;
    }
    public String getClientSecret() {
        return setting.ClientSecret__c;
    }  
    
    public String getToken() {
        return setting.Token__c;
    }       
    
 }