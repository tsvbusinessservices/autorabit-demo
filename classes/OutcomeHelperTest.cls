@isTest
private class OutcomeHelperTest
{
	@isTest
	static void test_isPoliceInfrigementChanged() {

		Outcome__c oldInf = TestDataFactory.createOutcome('Maritime - Infringement Notice');
		oldInf.Police_Infringement__c = true;
		Outcome__c newInf = TestDataFactory.createOutcome('Maritime - Infringement Notice');
		newInf.Police_Infringement__c = false;

		System.assertEquals(true, OutcomeHelper.isPoliceInfrigementChanged(newInf, oldInf));

		newInf.Police_Infringement__c = true;
		System.assertEquals(false, OutcomeHelper.isPoliceInfrigementChanged(newInf, oldInf));


	}

	@isTest
	static void test_isInfringementNumberChanged() {
		Outcome__c oldInf = TestDataFactory.createOutcome('Maritime - Infringement Notice');
		oldInf.Infringement_Auto_Number__c = 1234;
		Outcome__c newInf = TestDataFactory.createOutcome('Maritime - Infringement Notice');
		newInf.Infringement_Auto_Number__c = 4567;

		System.assertEquals(true, OutcomeHelper.isInfringementNumberChanged(newInf, oldInf));

		newInf.Infringement_Auto_Number__c = 1234;
		System.assertEquals(false, OutcomeHelper.isInfringementNumberChanged(newInf, oldInf));

	}

	@isTest
	static void test_isInfringementRecordType() {
		
		Outcome__c policeInf = TestDataFactory.createOutcome('VicPol - Infringement Notice');
		Outcome__c marineInf = TestDataFactory.createOutcome('Maritime - Infringement Notice');
		Outcome__c busInf = TestDataFactory.createOutcome('Bus - Infringement');
		Outcome__c otherInf = TestDataFactory.createOutcome('Bus Safety Alert');
		
		
		System.assertEquals(true, OutcomeHelper.isInfringementRecordType(policeInf.recordTypeId));
		System.assertEquals(true, OutcomeHelper.isInfringementRecordType(marineInf.recordTypeId));
		System.assertEquals(true, OutcomeHelper.isInfringementRecordType(busInf.recordTypeId));
		System.assertEquals(false, OutcomeHelper.isInfringementRecordType(otherInf.recordTypeId));


	}

	

	
}