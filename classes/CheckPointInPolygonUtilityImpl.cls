public class CheckPointInPolygonUtilityImpl implements CheckPointInPolygonUtility{
    
    @TestVisible
    private static PointInPolygonService service = null;

    /**
     * Checks Waterway Geometry and Returns Location Id
     * @param  xlat  Record Latitude
     * @param  xlong Record Longtitude
     * @return       Location Id
     */
    public String checkPointInPolygon(Decimal xlat, Decimal xlong) {
        //xlat = -38.3704029993570;
        //xlong = 144.5693329988640;
        List<maps__ShapeLayerGeometry__c> lterritoryGeom = getGeoMetries(xlat, xlong);
        String isCovered;
        System.debug('!!@@##: xlat:' + xlat+' xlong:'+xlong);

        Map<Id, Id> shapeLayerLocationMap = new Map<Id, Id>();
        list<string> MATerritoryIds = new list<string>();

        for (maps__ShapeLayerGeometry__c geo : lterritoryGeom) {
            MATerritoryIds.add(geo.maps__ShapeLayer__c);
            shapeLayerLocationMap.put(geo.maps__ShapeLayer__c, geo.maps__ShapeLayer__r.Location__r.Id);
        }
        //MATerritoryIds.add('a2iN0000000wK6fIAE');
        system.debug('MATerritoryIds==>'+MATerritoryIds);
        list<map<string,object>> coordinateList = new list<map<string,object>>();

        map<string,object> coordinateLocation = new map<string,object>{
            //'id' => string.valueOf('Test Id'),
            'lat' => string.valueOf(xlat),
            'lng' => string.valueOf(xlong)
        };
        coordinateList.add(coordinateLocation);
        map<string,object> requestMap = new map<string,object>{
            'version' => '2',
            'points' => coordinateList,
            'MATerritoryIds' => MATerritoryIds
        };
        system.debug('requestMap==>'+requestMap);
        //map<string,object> PolygonMap = maps.API.PointInPolygon(requestMap);

        map<string,object> PolygonMap;
        if(!Test.isRunningTest())
            PolygonMap = maps.API.PointInPolygon(requestMap);
        else {
            PolygonMap = new map<string,object>();
        }
        system.debug(JSON.serializePretty(PolyGonMap));
        try {
            list<object> ResultsList = (list<object>) PolygonMap.get('results');
            for (object res : ResultsList) {
                map<string, object> ResultMap = (map<string, object>) res;
                list<object> PolygonsList = (list<object>) ResultMap.get('polygons');
                if (PolygonsList.size() > 0) {
                    for (object polygon : PolygonsList) {
                        // These strings are the IDs of your MA Territory Shape Layer IDs
                        // that the point falls within
                        string strPolygonID = (string)polygon;
                        isCovered = shapeLayerLocationMap.get(strPolygonID.substring(0,18));
                        break;
                    }
                }
            }
        }
        catch(Exception e) {
            system.debug('Exception ==> '+e.getMessage() + ' StackTrace==>'+e.getStackTraceString());
        }
        return isCovered;
        /*String resultString = JSON.serializePretty(result);
        System.debug('!!@@##: result:' + result.get('polygons'));
        ResponsePolygon resp = (ResponsePolygon) JSON.deserialize(resultString, ResponsePolygon.class);
        //Check if There is a response
        system.debug('resp==>'+resp);
        //if (resp != null && resultString.trim() != '{ }') {
 //           if (resp.success && resp.polygons != null) {
                for (cls_polygons obj : resp.polygons) {
                    if (obj.success && obj.intersects) {
                        isCovered = obj.id;
                        break;
                    }
                }
           // }
        //}
        return isCovered;*/
    }

    private List<maps__ShapeLayerGeometry__c> getGeometries(Decimal xlat, Decimal xlong) {
        return [SELECT Id,
                          maps__ShapeLayer__r.Location__r.RecordType.Name,
                          maps__ShapeLayer__r.Location__r.Id,
                          maps__Geometry__c, maps__ShapeLayer__c
                          FROM maps__ShapeLayerGeometry__c
                          WHERE maps__ShapeLayer__r.Location__r.RecordType.DeveloperName in ('Waterway','Bus_Operational_Area')
                          AND maps__ShapeLayer__r.Extent_Territory__c = true
                          AND Min_X__c <= :xlat AND Max_X__c >= :xlat
                          AND Min_Y__c <= :xlong AND Max_Y__c >= :xlong];
    }

    @TestVisible
    private static PointInPolygonService getPIPService() {
        if(service == null) {
            service = new MapAnyThingPointInPolygonService();
        }
        return service;
    }
}