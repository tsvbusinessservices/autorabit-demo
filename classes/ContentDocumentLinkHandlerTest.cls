@isTest
private class ContentDocumentLinkHandlerTest
{
	@isTest
	private static void HandlerTest()
	{
		Outcome__c outcome = new Outcome__c();
		insert outcome;

		ContentVersion contentVersion = new ContentVersion();
		contentVersion.Title = 'Penguins';
		contentVersion.PathOnClient = 'Penguins.jpg';
		contentVersion.VersionData = Blob.valueOf('Test Content');
		contentVersion.IsMajorVersion = true;
		insert contentVersion;

		List <ContentVersion> contentVersions = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id];

		System.assertEquals(1, contentVersions.size());

		contentVersion = contentVersions.get(0);

		List <ContentDocument> contentDocuments = [SELECT Id FROM ContentDocument WHERE Id = :contentVersion.ContentDocumentId];

		System.assertEquals(1, contentDocuments.size());

		ContentDocument contentDocument = contentDocuments.get(0);

		ContentDocumentLink contentDocumentLink = New ContentDocumentLink();
		contentDocumentLink.LinkedEntityId = outcome.Id;
		contentDocumentLink.ContentDocumentId = contentDocument.Id;
		contentDocumentLink.shareType = 'V';

		insert contentDocumentLink;

		List <Outcome_File__c> outcomeFiles = [SELECT Id FROM Outcome_File__c WHERE Outcome__c = :outcome.Id];
		System.assertEquals(1, outcomeFiles.size());

		delete contentDocumentLink;

		outcomeFiles = [SELECT Id FROM Outcome_File__c WHERE Outcome__c = :outcome.Id];
		System.assertEquals(0, outcomeFiles.size());
	}
}