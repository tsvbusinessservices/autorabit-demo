public class  httpCallout {

@future(callout=true)
public static void getHttpResponse (Id recordId, String oname, String type, String purpose, String endpoint){

        //Initiate HTTP object 
        Http httpProtocol = new Http();
        
        // Create HTTP request to send remember to specify the method (GET) and endpoint
        HttpRequest request = new HttpRequest();
        // Set the HTTP method to GET. Set other common variables to Null
        request.setMethod('GET');
        HttpResponse response = null;
        String token = null;
        String strResponse = null;
        
     
        
        if (type == 'arcRequest') {
        //Retrieve token first
           SecuritySettingWrapper arcOnline = SecuritySettingWrapper.getConfig(Constants.SECURITY_ARCONLINE);
       
           String ClientID =  arcOnline.getClientID();
           String ClientSecret =  arcOnline.getClientSecret();
           String GetTokenURL = 'https://www.arcgis.com/sharing/oauth2/token?';    
           String storage='true';
         
           System.debug('#ClientID & Secret : '+ClientID + '#'+ ClientSecret);

                // Set the endpoint URL.        
                String tokenEndpoint = GetTokenURL+'client_id='+ClientID+'&grant_type=client_credentials&client_secret='+ClientSecret+'&f=pjson';
                    System.debug('Endpoint set to: '+tokenEndpoint);
                    
                // Send the HTTP request and get the response.
                //Specify string to contain the json response
                request.setEndPoint(tokenEndpoint);
   
               // Send the HTTP request and get the response.  
                if (Test.isRunningTest() ) {
                   strResponse = TestDataFactory.arcGISToken();
                } else {
                   response = httpProtocol.send(request);
                   strResponse = response.getBody();
                }      
 
                    
                // Parse JSON response to get returned values.
                arcGIS_TokenWrapper tok = tokParse(strResponse);        
                    System.assert(tok != null);
                    System.assert(tok.expires_in != null);                     
                    System.debug('Deserialized token:' + tok);
                    System.debug('ArcOnline Token:' + tok.access_token);  
                token = tok.access_token;
                
                //Append token values to primary endpoint       
                endpoint = endpoint +'&storage='+storage+'&token='+token;                           
        }

            request.setEndPoint(endpoint);
            // Send the HTTP request and get the response.   
              if (Test.isRunningTest() ) {
                   strResponse = TestDataFactory.arcGISAddress();
                } else {
                   response = httpProtocol.send(request); 
                   
                  // The response is in JSON format.
                     System.debug('#HTTP Response : '+response.getBody());
                  strResponse = response.getBody();                    
               } 
            
           
            //return strResponse;

            //deSerialize address if purpose is reverse geocode
            if (purpose == 'revGeocode'){
               // Parse JSON response to get returned values.
                arcGIS_AddressWrapper adr = adrParse(strResponse);            
                    System.assert(adr != null);
                    System.debug('Deserialized address:' + adr);
                    System.debug('Match address:' + adr.address.Match_addr); 
                    System.debug('Long Label:' + adr.address.LongLabel);                     
                    System.debug('ShortLabel :' + adr.address.ShortLabel );                                         
                    System.debug('Addr_type  :' + adr.address.Addr_type  );                                         
                    System.debug('Type:' + adr.address.Type);                                         
                    System.debug('PlaceName  :' + adr.address.PlaceName  );                                         
                    System.debug('AddNum :' + adr.address.AddNum );                                         
                    System.debug('Address :' + adr.address.Address);                                         
                    System.debug('Block :' + adr.address.Block );                                         
                    System.debug('Sector:' + adr.address.Sector);                                         
                    System.debug('Neighborhood:' + adr.address.Neighborhood);                                         
                    System.debug('District:' + adr.address.District);                                                                                                                                                                                                                             
                    System.debug('City:' + adr.address.City);         
                    System.debug('MetroArea:' + adr.address.MetroArea);         
                    System.debug('Subregion:' + adr.address.Subregion );         
                    System.debug('Region :' + adr.address.Region );         
                    System.debug('Territory:' + adr.address.Territory);                                                                                                             
                    System.debug('Postal:' + adr.address.Postal);      
                    System.debug('PostalExt:' + adr.address.PostalExt);                                              
                    System.debug('CountryCode :' + adr.address.CountryCode );                     
                    System.debug('x coord:' + adr.location.x);
                    System.debug('y coord:' + adr.location.y);                                        
                    System.debug('spatial ref ID:' + adr.location.spatialReference.wkid);                
                    System.debug('latest spatial ref ID:' + adr.location.spatialReference.latestWkid);                                                        
                    
                String revAddress =    adr.address.Match_addr;   
                //Write address back to record
                arcGIS_revGeocode.writeAddress(recordID, oname,revAddress); 
            }

            
}


public static arcGIS_TokenWrapper tokParse(String json){
            System.debug('String to parse:'+json);
        return (arcGIS_TokenWrapper) System.JSON.deserialize(json, arcGIS_TokenWrapper.class);
    
}

public static arcGIS_AddressWrapper  adrParse(String json){
            System.debug('String to parse:'+json);
        return (arcGIS_AddressWrapper) System.JSON.deserialize(json, arcGIS_AddressWrapper.class);
    
}
 

}