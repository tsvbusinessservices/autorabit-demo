public class InvolvedPartyTriggerHandler extends TriggerBase implements ITriggerBase {

    /**
     * Override afterInsert for Custom Implementation
     * @return null
     */
    public override void beforeInsert() {
        // Since this is a before insert trigger the ids are not available on the record.
        // To create a map for the bulkification, temporary key values are used.
        // This variable is used to create the temporary keys for the map.
        Integer keyVal = 1;

        // Map that holds the temp key and the Involved Person record where Person Account needs to be created
        Map<Integer, Involved_Party__c> createPersonForIPs = new Map<Integer, Involved_Party__c>();

        // Map that holds the temp key and the Involved Person record where Accreditation record needs to be created
        Map<Integer, Involved_Party__c> createLicenseForIPs = new Map<Integer, Involved_Party__c>();
        
        for(Involved_Party__c rec: (List<Involved_Party__c>)records) {

            // Get the records for which Person Record is to be created
            if(InvolvedPartyHelper.isMarineRecordType(rec)
                && Utility.isNullOrEmpty(rec.Person__c) 
                && ((!Utility.isNullOrEmpty(rec.First_Name__c) && !Utility.isNullOrEmpty(rec.Last_Name__c)))
              ) {
                createPersonForIPs.put(keyVal++, rec);
            }

            // Get the records for which License record is to be created. 
            if(InvolvedPartyHelper.isMarineRecordType(rec)
                && Utility.isNullOrEmpty(rec.Licence__c) 
                && (!Utility.isNullOrEmpty(rec.Licence_Number__c))
              ) {
                  createLicenseForIPs.put(keyVal++, rec);
              }
        }
        // There may be some records for which both person and license is to be creted
        // First create the person record and populate it on the Involved Person record
        // Then create the license record (linked with the Person record on the IP record)
        if(createPersonForIPs.size() > 0) {
            createPersonRecords(createPersonForIPs);
        }

        if(createLicenseForIPs.size() > 0) {
            createLicenseRecords(createPersonForIPs);
        }

    }

    /**
     * Create the Person Account records for each Involved Person in the map
     */
    void createPersonRecords(Map<Integer, Involved_Party__c> createPersonForIPs) {

        // Map to hold the key from the input map and the Account created against that involved person
        // This is used to link the person account to the involved person record.
        Map<Integer, Account> ipToPersonMap = new Map<Integer, Account>();

        for(Integer key: createPersonForIPs.keySet()) {
            // Gets the involved person record for each key
            Involved_Party__c involvedPerson = createPersonForIPs.get(key);

            // Create a person account record.
            Account person = InvolvedPartyHelper.createPersonFromInvolvedPerson(involvedPerson);

            // add the Person Account to the map with the original key.
            ipToPersonMap.put(key, person);
        }

        // Create Person Records in Salesforce
        insert ipToPersonMap.values();


        // Populate the Person lookup on the Involved Person Record
        // This is also required for the license lookup to be populated.
        linkPersonWithInvolvedPerson(ipToPersonMap, createPersonForIPs);


    }

    /**
     * Links the person account record in person account map with the Involved Person record in the
     * Involved person map where the keys match. This is done by populating the Person__c lookup on the
     * involved person record.
     */
    void linkPersonWithInvolvedPerson(Map<Integer, Account> ipToPersonMap, Map<Integer, Involved_Party__c> IPMap) {
        for(Integer involvedPersonId: ipToPersonMap.keySet()) {
            // Get the person account and involved person account for the same key
            Account person = ipToPersonMap.get(involvedPersonId);
            Involved_Party__c involvedPerson = IPMap.get(involvedPersonId);

            // Populate the Person__c lookup on the involved person record.
            if(person != null && involvedPerson != null) {
                System.debug('Adding Person record to ' + person.Id);
                involvedPerson.Person__c = person.Id;
            }
        }
    }

    /**
     * Creates Accreditation Records for each Involved Person record in the input map
     */
    void createLicenseRecords(Map<Integer, Involved_Party__c> createLicenceForIPs) {

        // Map to hold the key from the input map and the Accreditation record created against that involved person
        // This is used to link the licence record to the involved person record.
        Map<Integer, Accreditation__c> ipToLicenceMap = new Map<Integer, Accreditation__c>();

        // Get the unique license numbers from the records list.
        Set<String> licenseNumbers = getUniqueLicenceNumbers(createLicenceForIPs.values());

        // Get all licenses for these numbers. Duplucates numbers may exist in different states
        Map<String, List<Accreditation__c>> allAccreditaions = getAllLicences(licenseNumbers);

        for(Integer key: createLicenceForIPs.keySet()) {
            // Get involved person record
            Involved_Party__c involvedPerson = createLicenceForIPs.get(key);

            // Check if the Licence already exist. Dont create duplicate licence in the system.
            if(accreditationExists(involvedPerson, allAccreditaions)) {
                involvedPerson.addError('License already Exists.');
                continue;
            }

            // Create licence record
            Accreditation__c licence = InvolvedPartyHelper.createLicenceFromInvolvedPerson(involvedPerson);

            // Add to the map with the same key as the involved person in the input map.
            // This will be used to link the newly created licence record with the involved person record.
            ipToLicenceMap.put(key, licence);
        }

        // If any licence record is to be created 
        if(ipToLicenceMap.size() > 0) {
            // Create Licence Record in Salesforce
            insert ipToLicenceMap.values();


            // Populate the license lookup on the involved person record.
            linkLicenceWithInvolvedPerson(ipToLicenceMap, createLicenceForIPs);

        }

    }

    /**
     * Links the licence in licence map with the Involved Person record in the Involved person map where the keys match. 
     * This is done by populating the Licence__c lookup on the involved person record.
     */
    void linkLicenceWithInvolvedPerson(Map<Integer, Accreditation__c> ipToLicenceMap, Map<Integer, Involved_Party__c> IPMap) {
        for(Integer involvedPersonId: ipToLicenceMap.keySet()) {
            // Get the licence record for each involved person in the involved person map
            Accreditation__c licence = ipToLicenceMap.get(involvedPersonId);

            // Link the licence record to the involved person record by populating the Licence__c lookup
            if(licence != null) {
                Involved_Party__c involvedPerson = IPMap.get(involvedPersonId);
                System.debug('Adding Person record to ' + licence.Id);
                involvedPerson.Licence__c = licence.Id;
            }        
        }
    }

    /**
     * Checks if the Licence record already exists in the sytem. 
     * It matches the Licence number and the issuing state for the recreational boat user licence types only
     */
    @TestVisible
    boolean accreditationExists(Involved_Party__c involvedPerson, Map<String, List<Accreditation__c>> allLicenceMap) {

        // Map to hold all the licences with Licence # as the key.
        // The same licence number may exist for the licences issued in different state.
        List<Accreditation__c> licenceList = allLicenceMap.get(involvedPerson.Licence_Number__c);

        if(licenceList == null) {
            // No licence exist for this licence number
            return false;
        }
        
        // Loop for all licences found with the same number
        for(Accreditation__c licence: licenceList) {
            // Get the Licence number on the accreditation record from the relevant field
            String licenceNumber = Utility.getLicenceNumber(licence);

            // Check if the licence number and the state matches on the 
            // licence record and the involved person record.
            if(licenceNumber == involvedPerson.Licence_Number__c 
                && licence.State__c == involvedPerson.License_Origin__c) {
                return true;
            }
        }

        // No matches found.
        return false;
        
    }

    /**
     * Gets the set of unique licence numbers from the involved person list
     */
    @TestVisible
    Set<String> getUniqueLicenceNumbers(List<Involved_Party__c> ipList) {
        Set<String> licenceNumbers = new Set<String>();

        for(Involved_Party__c ip: ipList) {
            licenceNumbers.add(ip.Licence_Number__c);
        }
        return licenceNumbers;
    }


    /**
     * Get all the recreational Boat User licences for the given license numbers
     *
     * @returns Map to hold all the licences with Licence # as the key.
     *          The same licence number may exist for the licences issued in different state.
     */
    @TestVisible    
    Map<String, List<Accreditation__c>> getAllLicences(Set<String> licenceNumbers) {

        //Map to hold all the licences with Licence # as the key.
        Map<String, List<Accreditation__c>> allLicencesMap = new Map<String, List<Accreditation__c>>();

        // Retrieve all licences with licence number in either of the licence number fields.AppLauncher
        List<Accreditation__c> allLicences = [SELECT Id, VR_Cert_Id__c, Licence_Number__c, State__c
                                                FROM Accreditation__c
                                                WHERE RecordType.DeveloperName = 'Recreational_Boat_User'
                                                AND (VR_Cert_Id__c in :licenceNumbers OR Licence_Number__c in :licenceNumbers)];


        for(Accreditation__c licence: allLicences) {
            // Get the licence number from the record
            String licenceNumber = Utility.getLicenceNumber(licence);

            // Get the list from the map to add the licence number
            List<Accreditation__c> currentLicenceList = allLicencesMap.get(licenceNumber);

            // If the list does not exist then create it and add it to the map.
            if(currentLicenceList == null) {
                currentLicenceList = new List<Accreditation__c>();
                allLicencesMap.put(licenceNumber, currentLicenceList);
                
            }

            // Add the licence to the map.
            currentLicenceList.add(licence);
        }

        // return the map.
        return allLicencesMap;
    }

   
}