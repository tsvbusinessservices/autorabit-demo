public class ContentDocumentHandler extends TriggerBase implements ITriggerBase {
	/**
	 * constructor
	 */
	public ContentDocumentHandler() {

	}
	/**
	 * Custom afterInsert implementation
	 * @return null
	 */
	public override void afterInsert() {
		Map<Id, ContentDocumentLink> cdlList = new Map<Id, ContentDocumentLink>();
		Id intgUsrGrpId = null;
		//get Integration User Group
		for (CollaborationGroup grp : [SELECT Id FROM CollaborationGroup WHERE Name = 'Integration User']) {
			intgUsrGrpId = grp.Id;
		}
		for (ContentDocument cd : (List<ContentDocument>)records) {
			//Share File to Integration User
			if (!cdlList.containsKey(cd.Id)) {
				ContentDocumentLink share = new ContentDocumentLink();
				share.ContentDocumentId = cd.Id;
				share.LinkedEntityId = intgUsrGrpId;//Shared to Integration User
				share.ShareType = 'V';
				if (String.isNotBlank(intgUsrGrpId)) {
					cdlList.put(cd.Id, share);
				}
			}
		}
		if (!cdlList.isEmpty()) {
			Database.SaveResult[] lsr = Database.insert(cdlList.values(), false);
		}
	}
}