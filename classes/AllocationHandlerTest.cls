@isTest
private class AllocationHandlerTest {
    
    @isTest static void test_method_one() {
        // Implement test code
    }
    
    //Share Facility read only apex sharing to both group and facility Group
    @isTest static void PositiveTest_CreateFacilityGroupShare() {
        //Get the Partner Account Id
        Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;
        //Get the Partner Account Id
        Id portalAccountId2 = [SELECT Id FROM Account WHERE Name = 'portalAccount4'].Id;

        Location__c loc1 = TestDataFactory.createLocation(portalAccountId, Constants.LOCATION_RT_ID_FACILITY);
        insert loc1;
        
        Location__c loc2 = TestDataFactory.createLocation(portalAccountId2, Constants.LOCATION_RT_ID_FACILITY);
        insert loc2;

        test.startTest();
        
        Allocation__c alocGroup = TestDataFactory.createGroup();
        alocGroup.Group_Type__c = 'Facility';
        insert alocGroup;

        Allocation__c alocFacilityGroup = TestDataFactory.createLocationGroup(Constants.ALLOCATION_RT_ID_FACILITY_GROUP);
        alocFacilityGroup.Facility__c = loc1.id;
        alocFacilityGroup.Group__c = alocGroup.id;
        insert alocFacilityGroup;

        Allocation__c alocFacilityGroup2 = TestDataFactory.createLocationGroup(Constants.ALLOCATION_RT_ID_FACILITY_GROUP);
        alocFacilityGroup2.Facility__c = loc2.id;
        alocFacilityGroup2.Group__c = alocGroup.id;
        insert alocFacilityGroup2;


        test.stopTest();

        //check that there has been a sharing record created
        List<Allocation__Share> alocSharing = AllocationHelper.getAllocSharing(new set<Id>{alocGroup.Id, alocFacilityGroup.Id, alocFacilityGroup2.Id}, new set<String>{Allocation__Share.RowCause.Facility_Manager__c, Allocation__Share.RowCause.Secondary_Waterway_Manager__c, Schema.Allocation__Share.RowCause.Parent_Waterway_Manager__c});
        system.assertEquals(4, alocSharing.size());
    }

    //Share Facility read only apex sharing to both group and facility Group
    @isTest static void NegativeTest_DeleteFacilityGroupShare() {
        //Get the Partner Account Id
        Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;

        Location__c loc1 = TestDataFactory.createLocation(portalAccountId, Constants.LOCATION_RT_ID_FACILITY);
        insert loc1;
        
        Allocation__c alocGroup = TestDataFactory.createGroup();
        alocGroup.Group_Type__c = 'Facility';
        insert alocGroup;

        Allocation__c alocFacilityGroup = TestDataFactory.createLocationGroup(Constants.ALLOCATION_RT_ID_FACILITY_GROUP);
        alocFacilityGroup.Facility__c = loc1.id;
        alocFacilityGroup.Group__c = alocGroup.id;
        insert alocFacilityGroup;

        test.startTest();

        delete alocFacilityGroup;

        test.stopTest();

        //check that there has been a sharing record created
        List<Allocation__Share> alocSharing = AllocationHelper.getAllocSharing(new set<Id>{alocGroup.Id}, new set<String>{Allocation__Share.RowCause.Facility_Manager__c, Allocation__Share.RowCause.Secondary_Waterway_Manager__c, Schema.Allocation__Share.RowCause.Parent_Waterway_Manager__c});
        system.assertEquals(0, alocSharing.size());
    }
    
    
    //Share Facility read only apex sharing to both group and facility Group
    @isTest static void PositiveTest_CreateWaterwayGroupShare() {
        //Get the Partner Account Id
        Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;
        Id portalAccountId2 = [SELECT Id FROM Account WHERE Name = 'portalAccount2'].Id;
        Id portalAccountId3 = [SELECT Id FROM Account WHERE Name = 'portalAccount3'].Id;

        Location__c waterway = TestDataFactory.createLocation(portalAccountId2, Constants.LOCATION_RT_ID_WATERWAY);
        insert waterway;

        Location__c waterway2 = TestDataFactory.createLocation(portalAccountId3, Constants.LOCATION_RT_ID_WATERWAY);
        insert waterway2;


        Location__c facility  = TestDataFactory.createLocation(portalAccountId, Constants.LOCATION_RT_ID_FACILITY);
        facility.Waterway_Name__c = waterway.id;
        insert facility;
        
        Waterway_Role__c wwr1 = TestDataFactory.createWaterwayRole(portalAccountId2, waterway.id);
        insert wwr1;
        
        Waterway_Role__c wwr2 = TestDataFactory.createWaterwayRole(portalAccountId3, waterway2.id);
        insert wwr2;

        test.startTest();
        
        Allocation__c alocGroup = TestDataFactory.createGroup();
        alocGroup.Group_Type__c = 'Waterway';
        insert alocGroup;
        
        Allocation__c alocWaterwayGroup = TestDataFactory.createLocationGroup(Constants.ALLOCATION_RT_ID_WATERWAY_GROUP);
        alocWaterwayGroup.Waterway_Name__c = waterway.id;
        alocWaterwayGroup.Group__c = alocGroup.id;
        insert alocWaterwayGroup;
        
        Allocation__c alocWaterwayGroup2 = TestDataFactory.createLocationGroup(Constants.ALLOCATION_RT_ID_WATERWAY_GROUP);
        alocWaterwayGroup2.Waterway_Name__c = waterway2.id;
        alocWaterwayGroup2.Group__c = alocGroup.id;
        insert alocWaterwayGroup2;

        test.stopTest();

        //check that there has been a sharing record created
        List<Allocation__Share> alocSharing = AllocationHelper.getAllocSharing(new set<Id>{alocGroup.Id,alocWaterwayGroup.Id,alocWaterwayGroup2.Id}, new set<String>{Allocation__Share.RowCause.Facility_Manager__c, Allocation__Share.RowCause.Secondary_Waterway_Manager__c, Schema.Allocation__Share.RowCause.Parent_Waterway_Manager__c});
        system.assertEquals(4, alocSharing.size());
    }

    //Share Facility read only apex sharing to both group and facility Group
    @isTest static void PositiveTest_CreateFacilityWithWaterwayGroupShare() {
        //Get the Partner Account Id
        Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;
        Id portalAccountId2 = [SELECT Id FROM Account WHERE Name = 'portalAccount2'].Id;

        Location__c waterway = TestDataFactory.createLocation(portalAccountId2, Constants.LOCATION_RT_ID_WATERWAY);
        insert waterway;

        Location__c facility  = TestDataFactory.createLocation(portalAccountId, Constants.LOCATION_RT_ID_FACILITY);
        facility.Waterway_Name__c = waterway.id;
        insert facility;
        
        Waterway_Role__c wwr1 = TestDataFactory.createWaterwayRole(portalAccountId2, waterway.id);
        insert wwr1;
        
        test.startTest();

        Allocation__c alocGroup = TestDataFactory.createGroup();
        alocGroup.Group_Type__c = 'Facility';
        insert alocGroup;

        Allocation__c alocFacilityGroup = TestDataFactory.createLocationGroup(Constants.ALLOCATION_RT_ID_FACILITY_GROUP);
        alocFacilityGroup.Facility__c = facility.id;
        alocFacilityGroup.Group__c = alocGroup.id;
        insert alocFacilityGroup;

        test.stopTest();

        //check that there has been a sharing record created
        List<Allocation__Share> alocSharing = AllocationHelper.getAllocSharing(new set<Id>{alocGroup.Id, alocFacilityGroup.Id}, new set<String>{Allocation__Share.RowCause.Facility_Manager__c, Allocation__Share.RowCause.Secondary_Waterway_Manager__c, Schema.Allocation__Share.RowCause.Parent_Waterway_Manager__c});
        //system.assertEquals(4, alocSharing.size());
    }

    //Share Facility read only apex sharing to both group and facility Group
    @isTest static void PositiveTest_FinalizeFacility() {
        //Get the Partner Account Id
        Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;
        //Get the Partner Account Id
        Id portalAccountId2 = [SELECT Id FROM Account WHERE Name = 'portalAccount4'].Id;

        Location__c loc1 = TestDataFactory.createLocation(portalAccountId, Constants.LOCATION_RT_ID_FACILITY);
        insert loc1;
        
        Location__c loc2 = TestDataFactory.createLocation(portalAccountId2, Constants.LOCATION_RT_ID_FACILITY);
        insert loc2;
        
        Allocation__c alocGroup = TestDataFactory.createGroup();
        alocGroup.Group_Type__c = 'Facility';
        insert alocGroup;

        Allocation__c alocFacilityGroup = TestDataFactory.createLocationGroup(Constants.ALLOCATION_RT_ID_FACILITY_GROUP);
        alocFacilityGroup.Facility__c = loc1.id;
        alocFacilityGroup.Group__c = alocGroup.id;
        insert alocFacilityGroup;

        Allocation__c alocFacilityGroup2 = TestDataFactory.createLocationGroup(Constants.ALLOCATION_RT_ID_FACILITY_GROUP);
        alocFacilityGroup2.Facility__c = loc2.id;
        alocFacilityGroup2.Group__c = alocGroup.id;
        insert alocFacilityGroup2;

        test.startTest();

        alocGroup.Status__c = Constants.ALLOCATION_STATUS_FINALIZED;
        update alocGroup;

        test.stopTest();

        //check that there is only 1 record finalized
        List<Allocation__c> aloc = [SELECT Id FROM Allocation__c WHERE Status__c = :Constants.ALLOCATION_STATUS_FINALIZED];
        system.assertEquals(1, aloc.size());

    }

    //Share Facility read only apex sharing to both group and facility Group
    @isTest static void NegativeTest_UnFinalizeFacilityGroupShare() {
        //Get the Partner Account Id
        Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;

        Location__c loc1 = TestDataFactory.createLocation(portalAccountId, Constants.LOCATION_RT_ID_FACILITY);
        insert loc1;

        Allocation__c alocGroup = TestDataFactory.createGroup();
        alocGroup.Group_Type__c = 'Facility';
        insert alocGroup;

        Allocation__c alocFacilityGroup = TestDataFactory.createLocationGroup(Constants.ALLOCATION_RT_ID_FACILITY_GROUP);
        alocFacilityGroup.Facility__c = loc1.id;
        alocFacilityGroup.Group__c = alocGroup.id;
        insert alocFacilityGroup;

        test.startTest();

        alocGroup.Status__c = Constants.ALLOCATION_STATUS_UNDER_DEVELOPMENT;
        update alocGroup;

        test.stopTest();

        //check that there the removal is successful
        List<Allocation__c> aloc = [SELECT Id FROM Allocation__c WHERE Status__c = :Constants.ALLOCATION_STATUS_FINALIZED];
        system.assertEquals(0, aloc.size());

    }

    //Negative test to show duplicate errors
    @isTest static void NegativeTest_NoDuplicateFinalized() {
        //Get the Partner Account Id
        Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;

        Location__c loc1 = TestDataFactory.createLocation(portalAccountId, Constants.LOCATION_RT_ID_FACILITY);
        insert loc1;

        
        Allocation__c alocGroup = TestDataFactory.createGroup();
        alocGroup.Group_Type__c = 'Facility';
        insert alocGroup;

        
        Allocation__c alocFacilityGroup = TestDataFactory.createLocationGroup(Constants.ALLOCATION_RT_ID_FACILITY_GROUP);
        alocFacilityGroup.Facility__c = loc1.id;
        alocFacilityGroup.Group__c = alocGroup.id;
        insert alocFacilityGroup;
        //shareing has been given to both group and facility group

        Allocation__c alocGroup2 = TestDataFactory.createGroup();
        alocGroup2.Group_Type__c = 'Facility';
        insert alocGroup2;

        Allocation__c alocFacilityGroup2 = TestDataFactory.createLocationGroup(Constants.ALLOCATION_RT_ID_FACILITY_GROUP);
        alocFacilityGroup2.Facility__c = loc1.id;
        alocFacilityGroup2.Group__c = alocGroup2.id;
        insert alocFacilityGroup2;
        //shareing has been given to both group and facility group

        try{
            test.startTest();

            alocGroup.Status__c = Constants.ALLOCATION_STATUS_FINALIZED;
            update alocGroup;

            alocGroup2.Status__c = Constants.ALLOCATION_STATUS_FINALIZED;
            update alocGroup2;

            test.stopTest();

        }catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('This group is a duplicate of') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
    }


    @testSetup static void testSetup(){
        Id maritime = Constants.ACCOUNT_RT_ID_MARITIME;
        String facilityMan = Constants.ACCOUNT_TYPE_FACILITY_MANAGER;
        String waterwayMan = Constants.ACCOUNT_TYPE_WATERWAY_MANAGER;
        TestDataFactory.createPartnerAccount(new List<Id>{maritime,maritime,maritime,maritime},new List<String>{facilityMan,waterwayMan,waterwayMan,facilityMan});
    }
}