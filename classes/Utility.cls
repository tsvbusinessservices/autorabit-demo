public class Utility {

	/**
     * @description get Object Record Types
     **/
	public static map<String,ID> getRecordTypeMap(String ObjectName){
		map<String,ID> RecordTypeMap = new map<String,ID>();
		for(RecordType RT : [SELECT DeveloperName,Id 
							 FROM RecordType 
							 WHERE SobjectType = :ObjectName
							 AND IsActive = true]){
			RecordTypeMap.put(RT.DeveloperName, RT.Id);
		}
		return RecordTypeMap;
	}

	/**
     * @description get Object Record Types
     **/
	public static map<Id,String> getRecordTypeMapById(String ObjectName){
		map<ID, String> RecordTypeMap = new map<Id, String>();
		for(RecordType RT : [SELECT DeveloperName,Id 
							 FROM RecordType 
							 WHERE SobjectType = :ObjectName
							 AND IsActive = true]){
			RecordTypeMap.put(RT.Id, RT.DeveloperName);
		}
		return RecordTypeMap;
	}


	/**
	 * Sends an email to the email address defined in ...
	 * @return null
	 */
	public static void sendEmail(String Subject, String body, String emailType){
		//get email settings
		SharingEmailSettingsWrapper EmailSettings = SharingEmailSettingsWrapper.getSetting();

		//Send email
		if((emailType == 'Success' && EmailSettings.getEnableSuccess()) || (emailType == 'Error' && EmailSettings.getEnableErrorEmail())){
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			String[] toAddresses = new list<String>{EmailSettings.getEmailAddress()}; 
			mail.setToAddresses(toAddresses); 
			mail.setSubject(Subject);
			mail.setPlainTextBody(body);
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
		}
	}


	/**
     * @description map of profile name to profile ID
     **/
	public static map<string, ID> profileNameToID{
		get{
			if(profileNameToID == null){
				profileNameToID = new map<string, ID>();
				for(profile prof : [SELECT Id,Name FROM Profile]){
					profileNameToID.put(prof.name, prof.id);
				}
			}
			return profileNameToID;
		}
		set;
	}

	/**
     * @description get specific profile ID
     **/
	public static ID getProfileID(string profileName){
		return profileNameToID.get(profileName);
	}

	/**
     * @description Truncates a string to the required length. Used for shortening text to the field length
     * @return sting - the truncated sting 
     **/
    public static string ConcatanateStringLength(string stringIn, integer length){
        if(stringIn != null && stringIn.length() > length){
            return stringIn.left(length);
        }
        else{
            return stringIn;
        }
    }

    /**
     * @description Returns a random string
     * @return the random string 
     **/
    public static string returnRandomString(integer length){
        return ConcatanateStringLength(String.ValueOf(Math.random()).subString(2), length);
    }


    /**
     * @description Returns a random string
     * @return the random string 
     **/
    public static List<Group> getGroup(set<Id> groupIds){
        return[SELECT DeveloperName,Id,Name,Type FROM Group WHERE Id IN : groupIds];
    }

    /**
     * @description Returns the date value for the given Date Time
     * @return the date value
     **/
    public static Date getDateValue(DateTime dt) {
    	return date.newinstance(dT.year(), dT.month(), dT.day());
    }

	public static Boolean isNullOrEmpty(String str) {
		return (str == null || str == '');
	}

	public static String getLicenceNumber(Accreditation__c licence) {
        return isNullOrEmpty(licence.VR_Cert_Id__c)? licence.Licence_Number__c:licence.VR_Cert_Id__c;
    }

}