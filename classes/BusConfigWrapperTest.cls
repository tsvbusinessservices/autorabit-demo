@isTest
private class BusConfigWrapperTest
{
    @isTest
    
    static void testGetDefault() {
        System.assertNotEquals(null, BusConfigWrapper.getConfig(Constants.BILLING_CONFIG_BUS_APPLICATION_FEE));
        System.assertNotEquals(null, BusConfigWrapper.getConfig(Constants.BILLING_CONFIG_BUS_BASE_FEE));        
        System.assertNotEquals(null, BusConfigWrapper.getConfig(Constants.BILLING_CONFIG_BUS_ADDITIONAL_FEE));            

    }

    @isTest
    static void testGetters() {
    //Test get application fee
        BusConfigWrapper instanceApplication = BusConfigWrapper.getConfig(Constants.BILLING_CONFIG_BUS_APPLICATION_FEE);
        
        String varApplication = Constants.BILLING_CONFIG_BUS_APPLICATION_FEE;
        
        Bus_Config__mdt settingApplication = [Select DeveloperName, Fee__c, Line_Description__c
                                                FROM Bus_Config__mdt
                                                WHERE DeveloperName = :varApplication  Limit 1];

        System.assertEquals(settingApplication.Fee__c, instanceApplication.getFeeAmount());
        System.assertEquals(settingApplication.Line_Description__c, instanceApplication.getFeeDesc());

    //Test get base fee
        BusConfigWrapper instanceBase = BusConfigWrapper.getConfig(Constants.BILLING_CONFIG_BUS_BASE_FEE);
        
        String varBase = Constants.BILLING_CONFIG_BUS_BASE_FEE;
        
        Bus_Config__mdt settingBase = [Select DeveloperName, Fee__c, Line_Description__c
                                                FROM Bus_Config__mdt
                                                WHERE DeveloperName = :varBase Limit 1];

        System.assertEquals(settingBase.Fee__c, instanceBase.getFeeAmount());
        System.assertEquals(settingBase.Line_Description__c, instanceBase.getFeeDesc());
        
    //Test get Additionalfee
        BusConfigWrapper instanceAdditional = BusConfigWrapper.getConfig(Constants.BILLING_CONFIG_BUS_ADDITIONAL_FEE);
        
        String varAdditional = Constants.BILLING_CONFIG_BUS_ADDITIONAL_FEE;
        
        Bus_Config__mdt settingAdditional = [Select DeveloperName, Fee__c, Line_Description__c
                                                FROM Bus_Config__mdt
                                                WHERE DeveloperName = :varAdditional Limit 1];

        System.assertEquals(settingAdditional.Fee__c, instanceAdditional.getFeeAmount());
        System.assertEquals(settingAdditional.Line_Description__c, instanceAdditional.getFeeDesc());
    }
    
}