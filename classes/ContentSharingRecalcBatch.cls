global class ContentSharingRecalcBatch implements Database.Batchable<sObject>, Schedulable  {
    global ContentSharingRecalcBatch() {

    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(ContentHelper.getContentForSharingRecalc());
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        ContentHandler.contentSharing(scope, null, 'Recalculation');
    }
    
    global void finish(Database.BatchableContext BC) {
        //Send email
        String emailSubject = 'SUCCESS: Apex Sharing Recalculation For Content Completed';
        String emailBody = 'The Apex sharing recalculation finished processing for the Content Object';
        Utility.sendEmail(emailSubject, emailBody, 'Success');
    }

    global void execute(SchedulableContext sc) {
        ContentSharingRecalcBatch b = new ContentSharingRecalcBatch();
        database.executeBatch(b, 200);
    }
}