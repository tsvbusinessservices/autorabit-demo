@isTest
public class MockPointInPolygonService implements PointInPolygonService
{
	public Boolean success = false;

	public Map<String, Object> checkPointInPolygon(Map<String, Object> optionsParams) {
		system.debug('optionsParams==>'+optionsParams);
		Map<String, Object> result = new Map<String, Object>();
		List<Map<String, Object>> resultPolygons = new List<Map<String, Object>>();


		String resultString = JSON.serializePretty(optionsParams);
		System.debug('!!@@##: result:' + resultString);
		Map<String, Object> options = (Map<String, Object>)JSON.deserializeUntyped(resultString);
		
		List<Object> polygons = (List<Object>) options.get('polygons');

		for(Integer i=0; i<polygons.size(); i++) {
			Map<String, Object> polygon = (Map<String, Object>) polygons.get(i);
			Boolean intersects = false;
			Boolean polysuccess = false;
			if(i==0) {
				intersects = success;
				polySuccess = success;
			}

			Map<String, Object> resultPoly = new Map<String, Object>();
			resultPoly.put('id', polygon.get('id'));
			resultPoly.put('intersects', intersects);
			resultPoly.put('success', polySuccess);

			resultPolygons.add(resultPoly);
		}

		result.put('success',success);
		result.put('polygons',resultPolygons);
		return result;
	}
}