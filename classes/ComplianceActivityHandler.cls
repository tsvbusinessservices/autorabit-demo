/**
 * Compliance Activity Handler 
 *
 * @date 18.May.2018
 * @author P.Victoriano
 */
public class ComplianceActivityHandler extends TriggerBase implements ITriggerBase {

    Id registeredInspectionId = Schema.SObjectType.Compliance_Activity__c.getRecordTypeInfosByDeveloperName().get('Vessel_Inspection').getRecordTypeId();
    Id unregisteredInspectionId = Schema.SObjectType.Compliance_Activity__c.getRecordTypeInfosByDeveloperName().get('Vessel_Inspection_Unregistered').getRecordTypeId();
    Id busauditAccreditedId = Schema.SObjectType.Compliance_Activity__c.getRecordTypeInfosByDeveloperName().get('Bus_Audit_Accredited_Operator').getRecordTypeId();
    Id busauditRegisteredId = Schema.SObjectType.Compliance_Activity__c.getRecordTypeInfosByDeveloperName().get('Bus_Audit_Registered_Operator').getRecordTypeId();
    /**
     * beforeInsert Trigger Event
     * @return null
     */
    public override void beforeInsert() {
        populateWaterway();
        populateVesselDetails();
        populateVesselOwnerDetails();
        createVessel();
        setOperatorAsOwner();
        populateOperatorDetails();
        populateLicenseDetails();
        createIndividual();
        populateBusAuditContact();//TIMS-187 & TIMS-342
    }

    /**
     * beforeUpdate Trigger Event
     * @return null
     */
    public override void beforeUpdate() {
        populateWaterway();
        populateVesselDetails();
        populateVesselOwnerDetails();
        createVessel();
        setOperatorAsOwner();
        populateOperatorDetails();
        populateLicenseDetails();
        createIndividual();
    }

    /**
     * afterInsert Trigger Event
     * @return null
     */
    public override void afterInsert() {
        createInvolvedPerson();
        updateNonComplianceSummary();
        //initiateReverseGeocode();
    }

    /**
     * afterUpdate Trigger Event    
     * @return null
     */
    public override void afterUpdate() {
        createInvolvedPerson();
        updateNonComplianceSummary();
        //initiateReverseGeocode();
    }

    private void populateWaterway() {
        //prepopulate Waterway__c
        if(!System.isFuture()){
            List<Compliance_Activity__c> records = retrieveInspectionRecords();
            if(records.size() > 0)
                ComplianceActivityHelper.setWaterWay(records);
        }
    }
/* callout from triggers not currently supported.
    private static void initiateReverseGeocode() { //added by Andrew Lambert - ticket TIMS-774
    
        //reverse geocode call on location when ready
        List <Compliance_Activity__c> complianceActivities = [SELECT Id, Status__c, RecordTypeId FROM Compliance_Activity__c WHERE Id IN :Trigger.newMap.keySet()];
        
        Id BusStandardInspection_RecordTypeId = Schema.SObjectType.Compliance_Activity__c.getRecordTypeInfosByDeveloperName().get('Bus_Compliance_Inspection_Standard').getRecordTypeId();
        Id BusQuickInspection_RecordTypeId = Schema.SObjectType.Compliance_Activity__c.getRecordTypeInfosByDeveloperName().get('Bus_Compliance_Inspection_Quick_Check').getRecordTypeId();
        Id BusHazardInspection_RecordTypeId = Schema.SObjectType.Compliance_Activity__c.getRecordTypeInfosByDeveloperName().get('Bus_Compliance_Inspection_Hazardous_Area').getRecordTypeId();
        
        for (Compliance_Activity__c complianceActivity : complianceActivities)
        {            
            if (complianceActivity.get('RecordTypeId') == BusStandardInspection_RecordTypeId ||
                complianceActivity.get('RecordTypeId') == BusQuickInspection_RecordTypeId ||
                complianceActivity.get('RecordTypeId') == BusHazardInspection_RecordTypeId )
            {
              if (complianceActivity.get('Status__c') == 'In Progress' || complianceActivity.get('Status__c') == 'Completed') 
              {  
                String recordID = String.ValueOf(complianceActivity.get('Id'));
                arcGIS_revGeocode.reverseGeocode(recordID);
              } else {
                //Do Nothing
              }
            }  else {
                //Do Nothing
            }
        }
    }
*/    
    
//        @future 
        private static void updateNonComplianceSummary() 
//    private void updateNonComplianceSummary()
    {
        List <Compliance_Activity__c> complianceActivities = [SELECT Id, Non_Compliance_Summary__c, NonComplianceSummary1__c, NonComplianceSummary2__c, NonComplianceSummary3__c, NonComplianceSummary4__c FROM Compliance_Activity__c WHERE Id IN :Trigger.newMap.keySet()];

        List <Compliance_Activity__c> recordsToUpdate = new List <Compliance_Activity__c>();

        for (Compliance_Activity__c complianceActivity : complianceActivities)
        {
            String summaryField = '';

            for (Integer index = 1; index <= 4; index++)
            {
                if (complianceActivity.get('NonComplianceSummary' + index + '__c') != null)
                {
                    summaryField += complianceActivity.get('NonComplianceSummary' + index + '__c');
                    if (summaryField.endsWith(',')) { summaryField += ' '; }
                }
            }
            summaryField = summaryField.removeEnd(', ');
            
            if (complianceActivity.Non_Compliance_Summary__c != summaryField)
            {
                complianceActivity.Non_Compliance_Summary__c = summaryField;
              //Determine initial inspection result based on NCS in code as NCS is long text area and can't be referenced by formulae --added by Andrew Lambert  
                If (String.isBlank(complianceActivity.Non_Compliance_Summary__c))
                {
                    complianceActivity.Result__c = 'Pass';
                } else {
                    complianceActivity.Result__c = 'Fail';
                }
              //Update compliance activity 
                recordsToUpdate.add(complianceActivity);
            }
        }

        TriggerDispatcher.bypass('ComplianceActivityHandler');
      //  update recordsToUpdate;
        TriggerDispatcher.clearBypass('ComplianceActivityHandler');
    }

    /**
     * Retrieve Vessel Inspection Record Ids
     * @return Set Id of Compliance Activity
     */
    @TestVisible
    private List<Compliance_Activity__c> retrieveInspectionRecords(){
        List<Compliance_Activity__c> listComplianceAct = new List<Compliance_Activity__c>();
        System.debug('ComplianceActivityHandler::retrieveInspectionRecords');
        for(Compliance_Activity__c obj: (List<Compliance_Activity__c>)records){
            //Select Vessel Inpection Record Types
            if(!ComplianceActivityHelper.isVesselInspection(obj) && !ComplianceActivityHelper.isBusInspection(obj))
                continue;
            if(getContext() == 'BEFORE_INSERT') {
                System.debug('BEFORE_INSERT Checking PIP for ca::' + obj.Id);
                listComplianceAct.add(obj);
            } else if(getContext() == 'BEFORE_UPDATE' 
                        && ComplianceActivityHelper.isLocationChanged(obj, (Compliance_Activity__c) oldmap.get(obj.Id))) {
                System.debug('BEFORE_UPDATE Checking PIP for ca::' + obj.Id);
                System.debug('Waterway__c::' + obj.Waterway__c);
                listComplianceAct.add(obj);

            }
        }
        return listComplianceAct;
    }
    
    // Populate Vessel Details
    private void populateVesselDetails()
    {   

        // Define Collections
        List <Compliance_Activity__c> targetRecords = new List <Compliance_Activity__c>();
        Set <Id> assetIds = new Set <Id>();

        // Loop through trigger records
        for (Compliance_Activity__c complianceActivity : (List <Compliance_Activity__c>)Trigger.new)
        {
            // Identify target records
            if (isVesselInspectionRecord(complianceActivity) && complianceActivity.Vehicle_Vessel__c != null)
            {
                // Handle Before Insert
                if (Trigger.isBefore == true && Trigger.isInsert == true)
                {
                    // Add record to target list
                    targetRecords.add(complianceActivity);
                    
                    // Add Asset Id  to list
                    assetIds.add(complianceActivity.Vehicle_Vessel__c);
                }
                
                // Handle Before Update
                else if (Trigger.isBefore == true && Trigger.isUpdate == true)
                {
                    // Retrieve oldRecord from oldMap
                    Compliance_Activity__c oldRecord = (Compliance_Activity__c)Trigger.oldMap.get(complianceActivity.Id);

                    // Check if field has changed
                    if (complianceActivity.Vehicle_Vessel__c != oldRecord.Vehicle_Vessel__c)
                    {
                        // Add record to target list
                        targetRecords.add(complianceActivity);
                        
                        // Add Asset Id  to list
                        assetIds.add(complianceActivity.Vehicle_Vessel__c);
                    }
                }
            }
        }

        // Retrieve Asset records in a map
        Map <Id, Asset__c> assetMap = new Map <Id, Asset__c>([SELECT Id, VIN__c, Registration_State__c, Vessel_Make__c, Colour__c, Hull_Length__c, Maximum_Engine_Power__c, Registration_Expiry_Date__c, Stolen_Date__c, Stolen__c, Vessel_Fuel_Type__c, Hull_Material__c, Vessel_Model__c, Vessel_Propulsion__c, Status__c, Vessel_Type__c, Engine_Count__c, Registration_Number__c, Rego_Status__c FROM Asset__c WHERE Id IN :assetIds]);
        System.debug('assetMap' + assetMap);
        // Loop through Target Records
        for (Compliance_Activity__c complianceActivity : targetRecords)
        {
            // Check if assetMap contains Asset
            if (assetMap.containsKey(complianceActivity.Vehicle_Vessel__c))
            {
                // Create asset reference record
                Asset__c asset = assetMap.get(complianceActivity.Vehicle_Vessel__c);
                
                // Populate Compliance Activity Fields
                complianceActivity.Engine_Number__c = asset.Engine_Count__c;
                complianceActivity.Engine_Make__c = asset.Vessel_Make__c;
                complianceActivity.Hull_Colour__c = asset.Colour__c;
                complianceActivity.Hull_Length__c = asset.Hull_Length__c;
                complianceActivity.Rego_Number_Call_Sign__c = asset.Registration_Number__c;
                complianceActivity.Rego_Number__c = asset.Registration_Number__c;
                complianceActivity.Maximum_Engine_Power__c = asset.Maximum_Engine_Power__c;
                complianceActivity.Registration_State__c = asset.Registration_State__c;
                complianceActivity.Stolen__c = asset.Stolen__c;
                complianceActivity.Stolen_Date__c = asset.Stolen_Date__c;
                complianceActivity.Vessel_Fuel_Type__c = asset.Vessel_Fuel_Type__c;
                complianceActivity.Vessel_Make__c = asset.Vessel_Make__c;
                complianceActivity.Vessel_Material__c = asset.Hull_Material__c;
                complianceActivity.Vessel_Model__c = asset.Vessel_Model__c;
                complianceActivity.Vessel_Propulsion__c = asset.Vessel_Propulsion__c;
                complianceActivity.Vessel_Status__c = asset.Status__c;
                complianceActivity.Vessel_Type__c = asset.Vessel_Type__c;
                complianceActivity.VIN__c = asset.VIN__c;
                complianceActivity.Registration_Expiry_Date__c = asset.Registration_Expiry_Date__c;
                complianceActivity.Rego_Status__c = asset.Rego_Status__c;
            }
        }
    }
    
    // Populate contact for bus audit
    private void populateBusAuditContact()
    {
        List <Compliance_Activity__c> targetRecords = new List <Compliance_Activity__c>();
        Set <Id> accountIds = new Set <Id>();
        // Loop through trigger records
        for (Compliance_Activity__c complianceActivity : (List <Compliance_Activity__c>)Trigger.new)
        {
            // Identify target records
            if (isBusAuditRecord(complianceActivity) && complianceActivity.Bus_Operator__c != null)
            {
                // Handle Before Insert
                if (Trigger.isBefore == true && Trigger.isInsert == true)
                {
                    // Add record to target list
                    targetRecords.add(complianceActivity);
                    System.debug('targetReCords: ' + targetRecords);
                    
                    // Add Account Id  to list
                    accountIds.add(complianceActivity.Bus_Operator__c);
                    System.debug('accountIds: ' + accountIds);
                }
            }
        }
        
        //Retrieve account contact roles in a list
        AccountContactRelation[] accountContactList = new List<AccountContactRelation>();
        accountContactList = [SELECT AccountId,ContactId FROM AccountContactRelation WHERE AccountId IN :accountIds AND Roles includes ('Bus Responsible Person') AND IsActive = True];
        // Loop through Target Records
        System.debug('accountContactList' + accountContactList);
        for (Compliance_Activity__c complianceActivity : targetRecords)
        {
            System.debug('Inside for loop');
            // Check if accountContactMap has results
            if (!accountContactList.isEmpty())
            {
                // Create account contact reference record
                AccountContactRelation acr = accountContactList[0];
                System.debug('Inside if loop');
                System.debug('acr: ' + acr);
                // Populate Compliance Activity Fields
                complianceActivity.Contact__c = acr.ContactId;
                System.debug('Contact: ' + complianceActivity.Contact__c);
            }
        }
    }    
    // Update Vessel Owner Details
    private void populateVesselOwnerDetails()
    {

        // Define Collections
        List <Compliance_Activity__c> targetRecords = new List <Compliance_Activity__c>();
        Set <Id> assetIds = new Set <Id>();

        // Loop through trigger records
        for (Compliance_Activity__c complianceActivity : (List <Compliance_Activity__c>)Trigger.new)
        {
            // Identify target records
            if (isVesselInspectionRecord(complianceActivity) && complianceActivity.Vehicle_Vessel__c != null)
            {
                // Handle Before Insert
                if (Trigger.isBefore == true && Trigger.isInsert == true)
                {
                    // Add record to target list
                    targetRecords.add(complianceActivity);

                    // Add Asset Id  to list
                    assetIds.add(complianceActivity.Vehicle_Vessel__c);
                }

                // Handle Before Update
                else if (Trigger.isBefore == true && Trigger.isUpdate == true)
                {
                    // Retrieve oldRecord from oldMap
                    Compliance_Activity__c oldRecord = (Compliance_Activity__c)oldMap.get(complianceActivity.Id);

                    // Check if field has changed
                    if (complianceActivity.Vehicle_Vessel__c != oldRecord.Vehicle_Vessel__c)
                    {
                        // Add record to target list
                        targetRecords.add(complianceActivity);

                        // Add Asset Id  to list
                        assetIds.add(complianceActivity.Vehicle_Vessel__c);
                    }
                }
            }
        }

        // Retrieve Asset records in a map
        Map <Id, Asset__c> assetMap = new Map <Id, Asset__c>([SELECT Owner__r.PersonBirthDate, Owner__r.Gender__c, Owner__r.Marine_License_Summary__c, Owner__r.BillingPostalCode, Owner__r.BillingState, Owner__r.BillingStreet, Owner__r.BillingCity, Owner__c, Owner__r.RecordType.DeveloperName, Owner__r.FirstName, Owner__r.LastName, Owner__r.Trading_Name__c FROM Asset__c WHERE Id IN :assetIds]);

        // Loop through Target Records
        for (Compliance_Activity__c complianceActivity : targetRecords)
        {
            // Check if assetMap contains Asset
            if (assetMap.containsKey(complianceActivity.Vehicle_Vessel__c))
            {
                // Create asset reference record
                Asset__c asset = assetMap.get(complianceActivity.Vehicle_Vessel__c);

                // Populate Compliance Activity Fields
                complianceActivity.Date_of_Birth_Owner__c = asset.Owner__r.PersonBirthDate;
                complianceActivity.Gender_Owner__c = asset.Owner__r.Gender__c;
                complianceActivity.Marine_License_Summary_Owner__c = asset.Owner__r.Marine_License_Summary__c;
                complianceActivity.Postcode_Owner__c = asset.Owner__r.BillingPostalCode;
                complianceActivity.State_Owner__c = asset.Owner__r.BillingState;
                complianceActivity.Street_Owner__c = asset.Owner__r.BillingStreet;
                complianceActivity.Suburb_Owner__c = asset.Owner__r.BillingCity;
                complianceActivity.Vessel_Owner__c = asset.Owner__c;

                // Fix <BR> Issue
                if (complianceActivity.Marine_License_Summary_Owner__c != null)
                {
                    complianceActivity.Marine_License_Summary_Owner__c = complianceActivity.Marine_License_Summary_Owner__c.replace('<br>', ' / ');
                }

                if (asset.Owner__r.RecordType.DeveloperName == 'Individual')
                {
                    complianceActivity.Vessel_Owner_Name__c = asset.Owner__r.FirstName + ' ' + asset.Owner__r.LastName;
                }
                else
                {
                    complianceActivity.Vessel_Owner_Name__c = asset.Owner__r.Trading_Name__c;
                }
            }
        }
    }

    // Create Vessel
    private void createVessel()
    {

        // Define Collections
        List <Compliance_Activity__c> targetRecords = new List <Compliance_Activity__c>();
        Set <Id> assetIds = new Set <Id>();

        // Loop through trigger records
        for (Compliance_Activity__c complianceActivity : (List <Compliance_Activity__c>)Trigger.new)
        {
            // Identify target records
            if (isVesselInspectionRecord(complianceActivity) && complianceActivity.Create_Vessel__c == true)
            {
                // Handle Before Insert
                if (Trigger.isBefore == true && Trigger.isInsert == true)
                {
                    // Add record to target list
                    targetRecords.add(complianceActivity);
                }
                // Handle Before Update
                else if (Trigger.isBefore == true && Trigger.isUpdate == true)
                {
                    // Retrieve oldRecord from oldMap
                    Compliance_Activity__c oldRecord = (Compliance_Activity__c)Trigger.oldMap.get(complianceActivity.Id);

                    // Check if field has changed
                    if (complianceActivity.Create_Vessel__c != oldRecord.Create_Vessel__c)
                    {
                        // Add record to target list
                        targetRecords.add(complianceActivity);
                    }
                }
            }
        }

        // Retrieve Asset Record Type
        Id vessel_recordTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByDeveloperName().get('Vessel').getRecordTypeId();

        // Define collection for insert of asset records
        List <Asset__c> assets = new List <Asset__c>();

        // Loop through target records
        for (Compliance_Activity__c complianceActivity : targetRecords)
        {
            // Setup Create_Vessel__c field to false
            complianceActivity.Create_Vessel__c = false;
            
            // Create new asset record
            Asset__c asset = new Asset__c();
            
            // Populate asset fields
            asset.RecordTypeId = vessel_recordTypeId;
            asset.Colour__c = complianceActivity.Hull_Colour__c;
            asset.Engine_Count__c = complianceActivity.Engine_Number__c;
            asset.Engine_Make__c = complianceActivity.Engine_Make__c;
            asset.Hull_Length__c = complianceActivity.Hull_Length__c;
            asset.Hull_Material__c = complianceActivity.Vessel_Material__c; 
            asset.Maximum_Engine_Power__c = complianceActivity.Maximum_Engine_Power__c;
            asset.Name = complianceActivity.Rego_Number_Call_Sign__c;
            asset.Registration_Expiry_Date__c = complianceActivity.Registration_Expiry_Date__c;
            asset.Registration_Number__c = complianceActivity.Rego_Number_Call_Sign__c;
            asset.Status__c = complianceActivity.Vessel_Status__c;
            asset.Stolen__c = complianceActivity.Stolen__c;
            asset.Stolen_Date__c = complianceActivity.Stolen_Date__c;
            asset.Vessel_Fuel_Type__c = complianceActivity.Vessel_Fuel_Type__c;
            asset.Vessel_Make__c = complianceActivity.Vessel_Make__c;
            asset.Vessel_Model__c = complianceActivity.Vessel_Model__c;
            asset.Vessel_Propulsion__c = complianceActivity.Vessel_Propulsion__c;
            asset.Vessel_Type__c = complianceActivity.Vessel_Type__c;
            
            // Add asset to insert list
            assets.add(asset);
        }

        // Insert assets
        insert assets;

        // Define index and set to zero
        Integer assetIndex = 0;

        // Iterate through target records
        for (Compliance_Activity__c complianceActivity : targetRecords)
        {
            // Assign newly created asset to Compliance Activity
            complianceActivity.Vehicle_Vessel__c = assets.get(assetIndex).Id;
            
            // Increment Asset Index
            assetIndex++;
        }
    }

    // Set Operator as Owner
    private void setOperatorAsOwner()
    {

        // Loop through Trigger records
        for (Compliance_Activity__c complianceActivity : (List <Compliance_Activity__c>)Trigger.new)
        {
            // Identify target records
            if (isVesselInspectionRecord(complianceActivity) && complianceActivity.Owner_is_Operator__c == true)
            {
                // Handle Before Insert
                if (Trigger.isBefore == true && Trigger.isInsert == true)
                {
                    // Set Operator as Vessel Owner
                    complianceActivity.Operator__c = complianceActivity.Vessel_Owner__c;
                }
                // Handle Before Update
                else if (Trigger.isBefore == true && Trigger.isUpdate == true)
                {
                    // Retrieve oldRecord from oldMap
                    Compliance_Activity__c oldRecord = (Compliance_Activity__c)Trigger.oldMap.get(complianceActivity.Id);

                    // Check if field has changed
                    if (complianceActivity.Owner_is_Operator__c != oldRecord.Owner_is_Operator__c)
                    {
                        // Set Operator as Vessel Owner
                        complianceActivity.Operator__c = complianceActivity.Vessel_Owner__c;
                    }
                }
                
            }
        }
    }

    // Populate Operator Details
    private void populateOperatorDetails()
    {

        // Update Operator
        List <Compliance_Activity__c> targetRecords = new List <Compliance_Activity__c>();
        Set <Id> operatorIds = new Set <Id>();

        // Loop through Trigger records
        for (Compliance_Activity__c complianceActivity : (List <Compliance_Activity__c>)Trigger.new)
        {
            // Identify target records
            if (isVesselInspectionRecord(complianceActivity) && complianceActivity.Operator__c != null)
            {
                // Handle Before Insert
                if (Trigger.isBefore == true && Trigger.isInsert == true)
                {
                    // Add record to target list 
                    targetRecords.add(complianceActivity);

                    // Add Operator Id to set
                    operatorIds.add(complianceActivity.Operator__c);
                }
                // Handle Before Update
                else if (Trigger.isBefore == true && Trigger.isUpdate == true)
                {
                    // Retrieve oldRecord from oldMap
                    Compliance_Activity__c oldRecord = (Compliance_Activity__c)Trigger.oldMap.get(complianceActivity.Id);

                    // Check if field has changed
                    if (complianceActivity.Operator__c != oldRecord.Operator__c)
                    {
                        // Add record to target list
                        targetRecords.add(complianceActivity);

                        // Add Operator Id to set
                        operatorIds.add(complianceActivity.Operator__c);
                    }
                }
            }
        }

        // Retrieve Acoount records in a map
        Map <Id, Account> operatorMap = new Map <Id, Account>([SELECT PersonBirthDate, FirstName, MiddleName, Gender__c, LastName, License_Id_Text__c, Marine_License_Summary__c, BillingPostalCode, BillingState, BillingStreet, BillingCity, License_Expiry_Date_c__c FROM Account WHERE Id IN :operatorIds]);

        // Loop through Target records
        for (Compliance_Activity__c complianceActivity : targetRecords)
        {
            // Check if operatorMap contains Operator
            if (operatorMap.containsKey(complianceActivity.Operator__c))
            {
                // Create operator reference record
                Account operator = operatorMap.get(complianceActivity.Operator__c);
                
                // Populate Compliance Activity fields
                complianceActivity.First_Name_Operator__c = operator.FirstName;
                complianceActivity.Middle_Name_Operator__c = operator.MiddleName;
                complianceActivity.Last_Name_Operator__c = operator.LastName;
                complianceActivity.Gender_Operator__c = operator.Gender__c;
                complianceActivity.Date_of_Birth_Operator__c = operator.PersonBirthDate;
                complianceActivity.License__c = operator.License_Id_Text__c;
                complianceActivity.License_Expiry_Date_Operator__c = operator.License_Expiry_Date_c__c;
                complianceActivity.Marine_License_Summary_Operator__c = operator.Marine_License_Summary__c;
                complianceActivity.Postcode_Operator__c = operator.BillingPostalCode;
                complianceActivity.State_Operator__c = operator.BillingState;
                complianceActivity.Street_Operator__c = operator.BillingStreet;
                complianceActivity.Suburb_Operator__c = operator.BillingCity;

                // Fix <BR> Issue
                if (complianceActivity.Marine_License_Summary_Owner__c != null)
                {
                    complianceActivity.Marine_License_Summary_Owner__c = complianceActivity.Marine_License_Summary_Owner__c.replace('<br>', ' / ');
                }
            }

        }
    }

    // Update License
    private void populateLicenseDetails()
    {

        // Define Collections
        List <Compliance_Activity__c> targetRecords = new List <Compliance_Activity__c>();
        Set <Id> licenseIds = new Set <Id>();

        // Loop through Trigger records
        for (Compliance_Activity__c complianceActivity : (List <Compliance_Activity__c>)Trigger.new)
        {
            // Identify Target records
            if (isVesselInspectionRecord(complianceActivity) && complianceActivity.License__c != null)
            {
                // Handle Before Insert
                if (Trigger.isBefore == true && Trigger.isInsert == true)
                {
                    // Add record to target list 
                    targetRecords.add(complianceActivity);
                    
                    // Add License Id to set
                    licenseIds.add(complianceActivity.License__c);
                }
                // Handle Before Update
                else if (Trigger.isBefore == true && Trigger.isUpdate == true)
                {
                    // Retrieve oldRecord from oldMap
                    Compliance_Activity__c oldRecord = (Compliance_Activity__c)Trigger.oldMap.get(complianceActivity.Id);

                    // Check if field has changed
                    if (complianceActivity.License__c != oldRecord.License__c)
                    {
                        // Add record to target list 
                        targetRecords.add(complianceActivity);

                        // Add License Id to set
                        licenseIds.add(complianceActivity.License__c);
                    }
                }
            }
        }

        // Retrieve Accreditation records in a map
        Map <Id, Accreditation__c> licenseMap = new Map <Id, Accreditation__c>([SELECT VR_Cert_Id__c, Expiry_Date__c, State__c, License_Category__c, Type__c, Endorsement_Type__c FROM Accreditation__c WHERE Id IN :licenseIds]);

        // Iterate through Target records
        for (Compliance_Activity__c complianceActivity : targetRecords)
        {
            // Check if licenseMap contains License
            if (licenseMap.containsKey(complianceActivity.License__c))
            {
                // Create license reference record
                Accreditation__c license = licenseMap.get(complianceActivity.License__c);
                
                // Populate Compliance Activity Fields
                complianceActivity.License_Category_Operator__c = license.License_Category__c;
                complianceActivity.Endorsement_Type_Operator__c = license.Endorsement_Type__c;
                complianceActivity.License_Expiry_Date_Operator__c = license.Expiry_Date__c;
                complianceActivity.License_Number_Operator__c = license.VR_Cert_Id__c;
                complianceActivity.License_Origin_Operator__c = license.State__c;
                complianceActivity.License_Type_Operator__c = license.Type__c;
            }
        }
    }

    // Create Individual
    private void createIndividual()
    {
        Id account_RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();

        // Define Collections
        List <Compliance_Activity__c> targetRecords = new List <Compliance_Activity__c>();
        List <Account> individuals = new List <Account>();

        // Loop through Trigger records
        for (Compliance_Activity__c complianceActivity : (List <Compliance_Activity__c>)Trigger.new)
        {
            // Identify target records
            if (isVesselInspectionRecord(complianceActivity) && complianceActivity.Create_Individual__c == true)
            {
                // Add record to target list 
                targetRecords.add(complianceActivity);
                
                // Set Create Invididual to false
                complianceActivity.Create_Individual__c = false;
                
                // Create new Account record
                Account account = new Account();
                
                // Populate Account fields
                account.RecordTypeId = account_RecordTypeId;
                account.BillingCity = complianceActivity.Suburb_Operator__c;
                account.BillingPostalCode = complianceActivity.Postcode_Operator__c;
                account.BillingState = complianceActivity.State_Operator__c;
                account.BillingStreet = complianceActivity.Street_Operator__c;
                account.FirstName = complianceActivity.First_Name_Operator__c;
                account.MiddleName = complianceActivity.Middle_Name_Operator__c;
                account.LastName = complianceActivity.Last_Name_Operator__c;
                account.Gender__c = complianceActivity.Gender_Operator__c;
                account.PersonBirthDate = complianceActivity.Date_of_Birth_Operator__c;

                // Add account record to insert list
                individuals.add(account);
            }
        }

        // Insert individuals
        insert individuals;

        // Create and set Individual index to 0
        Integer individualIndex = 0;

        // Loop through target records
        for (Compliance_Activity__c complianceActivity : targetRecords)
        {
            // Set operator on Compliance Activity
            complianceActivity.Operator__c = individuals.get(individualIndex).Id;
            
            // Increment individual Index
            individualIndex++;
        }
    }

    // Create Involved Person
    private void createInvolvedPerson()
    {

        // Define Collections
        List <Compliance_Activity__c> targetRecords = new List <Compliance_Activity__c>();

        // Loop through Trigger records
        for (Compliance_Activity__c complianceActivity : (List <Compliance_Activity__c>)Trigger.new)
        {
            // Identify target records
            if (isVesselInspectionRecord(complianceActivity) && complianceActivity.Operator__c != null && complianceActivity.Status__c == 'Submitted')
            {
                // Handle Before Insert
                if (Trigger.isAfter == true && Trigger.isInsert == true)
                {
                    // Add record to target list 
                    targetRecords.add(complianceActivity);
                }
                // Handle Before Update
                else if (Trigger.isAfter == true && Trigger.isUpdate == true)
                {
                    // Retrieve oldRecord from oldMap
                    Compliance_Activity__c oldRecord = (Compliance_Activity__c)Trigger.oldMap.get(complianceActivity.Id);

                    // Check if field has changed
                    if (complianceActivity.Status__c != oldRecord.Status__c)
                    {
                        // Add record to target list 
                        targetRecords.add(complianceActivity);
                    }
                }
            }
        }

        // Retrieve Involved Party RecordType Id
        Id involvedParty_recordTypeId = Schema.SObjectType.Involved_Party__c.getRecordTypeInfosByDeveloperName().get('Vessel_Participant').getRecordTypeId();

        // Define collection for insert of Involved Party records
        List <Involved_Party__c> InvolvedParties = new List <Involved_Party__c>();

        // Loop through Target records
        for (Compliance_Activity__c complianceActivity : targetRecords)
        {
            // Create new Involved Party record
            Involved_Party__c involvedParty = new Involved_Party__c();
            
            // Populate Involved Party record
            involvedParty.RecordTypeId = involvedParty_recordTypeId;
            involvedParty.Compliance_Activity__c = complianceActivity.Id;
            involvedParty.BAC_Level__c = complianceActivity.BAC_Level__c;
            involvedParty.BAC_Range__c = complianceActivity.BAC_Range__c;
            involvedParty.Licence__c = complianceActivity.License__c;
            involvedParty.Person_Type__c = 'Operator';
            involvedParty.Person__c = complianceActivity.Operator__c;
            if(complianceActivity.ID_other_than_Licence__c == true) {
                involvedParty.ID_other_than_Licence__c = complianceActivity.ID_other_than_Licence__c;
                involvedParty.Method_of_identification__c = complianceActivity.Method_of_identification__c;
                involvedParty.Related_details__c = complianceActivity.Related_details__c;
            }
            

            // Add Involved Party to record to insert list
            InvolvedParties.add(involvedParty);
        }

        // Insert Involved Party records
        insert involvedParties;
    }

    boolean isVesselInspectionRecord(Compliance_Activity__c rec) {
        return rec.RecordTypeId == registeredInspectionId || rec.RecordTypeId == unregisteredInspectionId;
    }
    boolean isBusAuditRecord(Compliance_Activity__c rec) {
        return rec.RecordTypeId == busauditAccreditedId || rec.RecordTypeId == busauditRegisteredId;
    }
}