public class SharingQueueable implements Queueable {
	private list<Sobject> sharingList;
	

	public SharingQueueable(list<Sobject> sharingList){
		this.sharingList = sharingList;
	}

	//insert the Sharing list asyncronously
	public void execute(QueueableContext context) {
		SharingHelper.insertSharingRulesWithErrorHandling(sharingList);  
	}
}