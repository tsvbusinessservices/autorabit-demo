public class BillingSettingWrapper {

    static Map<String, BillingSettingWrapper> settingMap = new Map<String, BillingSettingWrapper>();

    Billing_Setting__mdt setting;

    BillingSettingWrapper(Billing_Setting__mdt setting) {
        this.setting = setting;
    } 

    public static BillingSettingWrapper getInstance(String name) {
        BillingSettingWrapper setting = settingMap.get(name);
        if( setting == null) {
            List<Billing_Setting__mdt> settingMeta = [SELECT DeveloperName, Charge_Code__c, Comments__c,
                                            Tax_Code__c, Tax_Code_Percentage__c, Oracle_contact_name__c,
                                            Term_Days__c, Term_Name__c, Billing_grace_days__c,
                                            Transaction_Type__c                                           
                                         FROM Billing_Setting__mdt 
                                         WHERE DeveloperName =: name ];
            if(settingMeta.size() == 0)
                return null;

            setting = new BillingSettingWrapper(settingMeta.get(0));
            settingMap.put(name, setting);
        }

        return setting;
        
    }

    public String getChargeCode() {
        return setting.Charge_Code__c;
    }

    public String getComments() {
        return setting.Comments__c;
    }

    public String getTaxCode() {
        return setting.Tax_Code__c;
    }

    public Integer getTermDays() {
        return (Integer) setting.Term_Days__c;
    }

    public String getTermName() {
        return setting.Term_Name__c;
    }

    public Integer getBillingGraceDays() {
        return (Integer) setting.Billing_grace_days__c;
    }

    public Decimal getTaxPercentage() {
        return (Decimal) setting.Tax_Code_Percentage__c/100;
    }    

    public String getTransactionType() {
        return setting.Transaction_Type__c;
    }

    public String getOracleContactName() {
        return setting.Oracle_Contact_Name__c;
    }


}