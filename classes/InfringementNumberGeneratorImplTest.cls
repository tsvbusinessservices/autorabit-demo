@isTest
private class InfringementNumberGeneratorImplTest
{
	@testSetup
	static void init() {
		TSV_Settings__c settings = TSV_Settings__c.getOrgDefaults();
		settings.Minimum_Infringement_Number__c = 1111;
		upsert settings custSettings__c.Id;


	}

	@isTest
	static void testInfringementNumberFromOutcome_Legacy() {
		TriggerSettingsClass.testOverride = false;
		InfringementNumberGeneratorImpl instance = new InfringementNumberGeneratorImpl();

		Outcome__c o = TestDataFactory.createOutcome('Maritime - Infringement Notice');
		o.Infringement_Auto_Number__c = 2345;
		insert o;
		
		Long newInfNumber = instance.getNewInfringementNumber_Legacy();

		System.assertEquals(2346, newInfNumber);

	}

	@isTest
	static void testRestartInfringementNumber_Legacy() {
		TSV_Settings__c settings = TSV_Settings__c.getOrgDefaults();
		settings.Minimum_Infringement_Number__c = 5555;
		upsert settings custSettings__c.Id;

		TriggerSettingsClass.testOverride = false;
		InfringementNumberGeneratorImpl instance = new InfringementNumberGeneratorImpl();

		Outcome__c o = TestDataFactory.createOutcome('Maritime - Infringement Notice');
		o.Infringement_Auto_Number__c = 2345;
		insert o;
		
		Long newInfNumber = instance.getNewInfringementNumber_Legacy();

		System.assertEquals(5556, newInfNumber);

	}


	@isTest
	static void testInfringementNumberFromSettings_Legacy() {
		InfringementNumberGeneratorImpl instance = new InfringementNumberGeneratorImpl();

		Long newInfNumber = instance.getNewInfringementNumber_Legacy();

		System.assertEquals(1112, newInfNumber);

	}

	@isTest
	static void testRevisedInfringementNumberGenerator() {
		InfringementNumberGeneratorImpl instance = new InfringementNumberGeneratorImpl();

		Long newInfringementNumber = instance.getNewInfringementNumber();

		System.assertNotEquals(null, newInfringementNumber);

	}
}