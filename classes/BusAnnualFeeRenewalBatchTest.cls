@isTest
public class BusAnnualFeeRenewalBatchTest
{
    static List<Accreditation__c> accreds;

    @isTest
    static void testBatch() {
        prepareTestData();
        

        Test.startTest();
        Database.executeBatch(new BusAnnualFeeRenewalBatch());
        Test.stopTest();

        
        assertData();
    }

    public static void assertData() {
        List<Invoice__c> invoices = [Select Id, Status__c 
                                        FROM Invoice__c];

        List<Invoice_Line_Item__c> invoiceLIs = [Select Id 
                                        FROM Invoice_Line_Item__c];

//        System.assertEquals(3, invoices.size());
//        System.assertEquals(6, invoiceLIs.size());

        System.assertEquals(Constants.INVOICE_STATUS_COMPLETE, invoices.get(0).Status__c);

        Accreditation__c result = [Select Id, Anniversary_Date__c
                                    From Accreditation__c
                                    WHERE Id =: accreds.get(0).Id];
        Date dt = getAnniversaryDate();
        System.assertEquals( Date.today().addDays(1).addYears(1), result.Anniversary_Date__c);
    }

    public static void prepareTestData() {
        List<Account> accounts = TestDataFactory.createAccounts(5);
        for(Account ac: accounts) {
            ac.Number_of_Vehicles__c = 5;
        }
        insert accounts;

        accreds = new List<Accreditation__c>();
        Date dt = getAnniversaryDate();

        for(Account ac: accounts) {
            Accreditation__c accred = TestDataFactory.createBusAccreditation(ac);
            accred.Anniversary_Date__c = dt;
            accreds.add(accred);
        }

        accreds.get(0).Anniversary_Date__c = Date.today().addDays(1);
        accreds.get(1).Anniversary_Date__c = Date.today().addDays(1);
        accreds.get(3).Anniversary_Date__c = Date.today().addDays(1);
        insert accreds;
    }

    private static Date getAnniversaryDate() {
        Billing_Setting__mdt setting = [SELECT DeveloperName, Billing_grace_days__c
                                         FROM Billing_Setting__mdt 
                                         WHERE DeveloperName = 'Bus' Limit 1];
        return Date.today().addDays(((Integer)setting.Billing_grace_days__c + 1));

    }
}