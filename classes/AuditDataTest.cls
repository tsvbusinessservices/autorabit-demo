@isTest
private class AuditDataTest
{

	@isTest
	static void testAuditDate()
	{
		Id rType = Schema.SObjectType.Compliance_Activity__c.getRecordTypeInfosByDeveloperName().get('Maritime_Audit_Wway').getRecordTypeId();
		Compliance_Activity__c rec = TestDataFactory.createComplianceActivity(rType);
		rec.Start_Date__c = Date.newInstance(2018, 4, 15);
		rec.End_Date__c = Date.newInstance(2018, 5, 15);
		AuditData audit = new AuditData(rec);

		System.assertEquals(true, audit.isAuditedDate( Date.newInstance(2018, 4, 28)));
		System.assertEquals(true, audit.isAuditedDate( Date.newInstance(2018, 5, 7)));
		System.assertEquals(true, audit.isAuditedDate( Date.newInstance(2018, 4, 15)));
		System.assertEquals(true, audit.isAuditedDate( Date.newInstance(2018, 5, 15)));
		System.assertEquals(false, audit.isAuditedDate( Date.newInstance(2018, 5, 17)));
		System.assertEquals(false, audit.isAuditedDate( Date.newInstance(2018, 4, 14)));


		rec.Start_Date__c = Date.newInstance(2018, 4, 15);
		rec.End_Date__c = Date.newInstance(2018, 4, 15);

		System.assertEquals(true, audit.isAuditedDate( Date.newInstance(2018, 4, 15)));
	}

	@isTest
	static void testAddTasks() {
		Id rType = Schema.SObjectType.Compliance_Activity__c.getRecordTypeInfosByDeveloperName().get('Maritime_Audit_Wway').getRecordTypeId();
		Compliance_Activity__c rec = TestDataFactory.createComplianceActivity(rType);
		rec.Start_Date__c = Date.newInstance(2018, 4, 15);
		rec.End_Date__c = Date.newInstance(2018, 5, 15);
		AuditData audit = new AuditData(rec);

		audit.addTasks(TestDataFactory.createTasks(5));

		System.assertEquals(5, audit.allTasks.size());

		audit.addTasks(null);
		System.assertEquals(5, audit.allTasks.size());
		
		audit.addTasks(new List<Task>());
		System.assertEquals(5, audit.allTasks.size());

		audit.addTasks(TestDataFactory.createTasks(3));
		System.assertEquals(8, audit.allTasks.size());

	}


	@isTest
	static void testFilteredTasks() {
		Id rType = Schema.SObjectType.Compliance_Activity__c.getRecordTypeInfosByDeveloperName().get('Maritime_Audit_Wway').getRecordTypeId();
		Compliance_Activity__c rec = TestDataFactory.createComplianceActivity(rType);
		rec.Start_Date__c = Date.newInstance(2018, 4, 15);
		rec.End_Date__c = Date.newInstance(2018, 5, 15);
		String recId = Schema.SObjectType.Compliance_Activity__c.getKeyPrefix() + '8E000000N9dr';
		rec.Id = recId;
		AuditData audit = new AuditData(rec);

		List<Task> tasks = TestDataFactory.createTasks(5);
		tasks.get(2).LastModifiedDate = DateTime.newInstance(2018, 4, 20);
		tasks.get(3).LastModifiedDate = DateTime.newInstance(2018, 4, 24);
		tasks.get(4).LastModifiedDate = DateTime.newInstance(2018, 4, 25);
		tasks.get(0).LastModifiedDate = DateTime.newInstance(2018, 4, 1);
		tasks.get(1).LastModifiedDate = DateTime.newInstance(2018, 5, 25);

		audit.addTasks(tasks);
		System.assertEquals(5, audit.allTasks.size());

		List<Task> filteredTasks = audit.getFilteredAuditTasks();

		System.assertEquals(3, filteredTasks.size());
		System.assertEquals(recId, filteredTasks.get(0).Compliance_Activity__c);
		System.assertEquals(recId, filteredTasks.get(1).Compliance_Activity__c);
		System.assertEquals(recId, filteredTasks.get(2).Compliance_Activity__c);




	}
}