public class Mod7ChecksumGenerator implements ChecksumGenerator{
	
	public String getChecksum(Long source) {
		System.debug('Checking mod 7 for::' + source);
		Long remainder = math.mod((long)source, (long)7);

		return (remainder == 0)? '7': ''+ remainder;

	}
}