/**
 * LocationHandler class. creates Apex sharing rules for the Location
 *
 * @author Ricky Lowe
 * @date 29.May.2018
 */

 
public with sharing class LocationHandler extends TriggerBase implements ITriggerBase {
    public LocationHandler() {
        
    }

    /**
     * afterInsert Trigger Event
     * @return null
     */
    public override void afterInsert() {
        locationSharing(records, null, 'Insert');
    }
    /**
     * afterUpdate Trigger Event    
     * @return null
     */
    public override void afterUpdate() {
        locationSharing(records, oldmap, 'Update');
    }

    /**
     * Controlls the Apex sharing against the Location Object   
     * @param mode = 'Insert', 'Update', 'Recalculation'
     * @return null
     */
    public static void locationSharing(list<SObject> records, Map<Id, SObject> oldMap, String mode){
        //Get an instance of the SharingHelper class with the Object Name
        SharingHelper SharingHelp = new SharingHelper(Location__Share.sObjectType.getDescribe().getName());

        Set<Id> parentLoactionIds  = new Set<Id>();
        for(Location__c Location : (list<Location__c>) records){
            if(Location.Location__c != null){
                parentLoactionIds.add(Location.Location__c);
            }
        }

        Map<Id, List<Location__Share>> mapOfParentLoactionToSharing = LocationHelper.getMapOfParentLocationToSharing(parentLoactionIds);
       
        //loop round and add the Id and the Portal account Id
        for(Location__c Location : (list<Location__c>) records){
            
            //get old Location - only applicable if running in Update mode
            Location__c oldLocation = null;
            if(mode == 'Update' && oldMap != null){
                oldLocation = (Location__c) oldMap.get(Location.Id);
            }

            //if new or changed or Recalculate then add the new Account to the sharing
            if(mode == 'Insert' || mode == 'Recalculation' || Location.Manager__c != oldLocation.Manager__c || Location.Location__c != oldLocation.Location__c){
                String rowCause = Schema.Location__Share.RowCause.Waterway_Manager__c; //In line with existing functionalities
                //if a Facility Location 
                if(Location.RecordTypeId == Constants.LOCATION_RT_ID_FACILITY){
                    rowCause = Schema.Location__Share.RowCause.Facility_Manager__c;
                }

                SharingHelp.addToSharing(Location.Manager__c, Location.Id, rowCause, 'Edit');

                if(mapOfParentLoactionToSharing !=null && mapOfParentLoactionToSharing.get(Location.Location__c) != null){
                    for(Location__Share share : mapOfParentLoactionToSharing.get(Location.Location__c)){
                        SharingHelp.addToClonedSharing(Location.Id, Schema.Location__Share.RowCause.Parent_Waterway_Manager__c, share.UserOrGroupId, 'Edit');
                    }
                }
                
                //if changed OR Recalculation mode than delete the old one
                if(mode == 'Recalculation' || (mode == 'Update' && (Location.Manager__c != oldLocation.Manager__c || Location.Location__c != oldLocation.Location__c))){
                    SharingHelp.addToDeleteList(Location.Id);
                }
            }
        }

        //delete any sharing rules that need to be deleted
        SharingHelp.deleteSharingRules(new set<String> {Schema.Location__Share.RowCause.Waterway_Manager__c, Schema.Location__Share.RowCause.Facility_Manager__c, Schema.Location__Share.RowCause.Parent_Waterway_Manager__c});

        //Insert Sharing rules
        if(Test.isRunningTest()){
            SharingHelp.insertAndCloneApexSharing_Syncronously();
        }else{
            SharingHelp.insertAndCloneApexSharing_Asyncronously();
        }
    }
}