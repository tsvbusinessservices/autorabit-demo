@isTest
private class WaterwayAuditHandlerTest
{

	@testSetup
	static void init() {
		List<Location__c> waterways = new List<Location__c>();
		waterways.add(TestDataFactory.createWaterway('Test Waterway 1'));

		waterways.add(TestDataFactory.createWaterway('Test Waterway 2'));

		waterways.add(TestDataFactory.createWaterway('Test Waterway 3'));
		insert waterways;

		Rule__c rule = new Rule__c();
		rule.Name = 'Test Rule';
		insert rule;


		
		List<Asset__c> assets = TestDataFactory.createATONAssets(5, waterways.get(0).Id);
		assets.get(0).Rule__c = rule.Id;
		assets.get(1).Rule__c = rule.Id;
		assets.get(2).Rule__c = rule.Id;
		assets.get(3).Rule__c = rule.Id;
		assets.get(4).Rule__c = rule.Id;
		assets.addAll(TestDataFactory.createATONAssets(3, waterways.get(1).Id));
		assets.addAll(TestDataFactory.createATONAssets(7, waterways.get(2).Id));

		insert assets;


	}

	@isTest 
	static void TestCleanupOldTaks() {

		Location__c testWaterway = [SELECT Id FROM Location__c WHERE Name = 'Test Waterway 1' LIMIT 1];

		Id rType = Schema.SObjectType.Compliance_Activity__c.getRecordTypeInfosByDeveloperName().get('Maritime_Audit_Wway').getRecordTypeId();
		Compliance_Activity__c auditRec = TestDataFactory.createComplianceActivity(rType);
		auditRec.Waterway__c = testWaterway.Id;
		insert auditRec;

		List<Asset__c> atons = [SELECT Id FROM Asset__c WHERE Location__c =: testWaterway.Id];
		System.assertEquals(5, atons.size());

		Integer i=0;

		List<Task> checkOuts = TestDataFactory.createTasks(3);
		for(Task t: checkOuts) {
			t.Status = 'Completed';
			t.Compliance_Activity__c = auditRec.Id;
			t.WhatId = atons.get(i++).Id;
		}
		insert checkOuts;

		List<Task> caTasks = [SELECT Id FROM Task WHERE Compliance_Activity__c =: auditRec.Id];
		System.assertEquals(3, caTasks.size());

		Map<Id, AuditData> auditMap = new Map<Id, AuditData>();
		auditMap.put(testWaterway.Id, new AuditData(auditRec));

		WaterwayAuditHandler handler = new WaterwayAuditHandler();
		handler.cleanupOldTaks(auditMap);

		caTasks = [SELECT Id FROM Task WHERE Compliance_Activity__c =: auditRec.Id];
		System.assertEquals(0, caTasks.size());
	}

	/**
	 * Test that when start and End dates are updated the completed tasks related to the ATONS of the waterway between those dates are 
	 * linked to the Audit record as well
	 **/
	@isTest
	static void TestBeforeUpdatePositive()
	{
		Location__c testWaterway = [SELECT Id FROM Location__c WHERE Name = 'Test Waterway 1' LIMIT 1];

		Id rType = Schema.SObjectType.Compliance_Activity__c.getRecordTypeInfosByDeveloperName().get('Maritime_Audit_Wway').getRecordTypeId();
		Compliance_Activity__c auditRec = TestDataFactory.createComplianceActivity(rType);
		auditRec.Waterway__c = testWaterway.Id;
		auditRec.Start_Date__c = Date.today().addDays(-1);
		auditRec.End_Date__c = Date.today().addDays(1);
		insert auditRec;

		Rule__c rule = [SELECT Id FROM Rule__c LIMIT 1];

		rType = Schema.SObjectType.Outcome__c.getRecordTypeInfosByDeveloperName().get('Maritime_Audit_Item_Waterway').getRecordTypeId();

		Outcome__c ruleItem = new Outcome__c();
		ruleItem.Compliance_Activity__c = auditRec.Id;
		ruleItem.Rule__c = rule.Id;
		insert ruleItem;

		

		List<Asset__c> atons = [SELECT Id FROM Asset__c WHERE Location__c =: testWaterway.Id];
		System.assertEquals(5, atons.size());


		List<Task> checkOuts = TestDataFactory.createTasks(3);
		checkOuts.get(0).Status = 'Completed';
		checkOuts.get(0).WhatId = atons.get(0).Id;

		checkOuts.get(1).Status = 'Completed';
		checkOuts.get(1).WhatId = atons.get(0).Id;
		
		checkOuts.get(2).Status = 'Completed';
		checkOuts.get(2).WhatId = atons.get(0).Id;

		insert checkOuts;

		Blob beforeblob = Blob.valueOf('Unit Test Attachment Body');

		ContentVersion cv = new ContentVersion();
		cv.title = 'test content trigger';
		cv.PathOnClient = 'test';
		cv.VersionData = beforeblob;
		insert cv;

		ContentVersion testContent = [SELECT Id, ContentDocumentId FROM ContentVersion where Id = :cv.Id Limit 1].get(0);

		ContentDocumentLink contentlink=new ContentDocumentLink();
        contentlink.LinkedEntityId=checkOuts.get(0).Id;
        contentlink.ShareType= 'V';
        contentlink.ContentDocumentId=testContent.ContentDocumentId;
        contentlink.Visibility = 'AllUsers'; 
        insert contentlink;

		auditRec.Finalize_Audit__c = true;
		update auditRec;

		List<Task> auditTasks = [SELECT Id FROM Task WHERE Compliance_Activity__c =: auditRec.Id];
		System.assertEquals(3, auditTasks.size());

		Task testTask = [SELECT Id, Compliance_Activity__c FROM Task WHERE Id =: checkOuts.get(0).Id];
		System.assertEquals(auditRec.Id, testTask.Compliance_Activity__c);

		Compliance_Activity__c audit = [SELECT Id, Finalize_Audit__c FROM Compliance_Activity__c WHERE Id =:auditRec.Id];
		System.assertEquals(false, audit.Finalize_Audit__c);

	}

	/**
	 * Test that when start and End dates are updated the completed tasks related to the ATONS of the waterway that are outside 
	 * those dates are not linked to the Audit record
	 **/
	
	@isTest
	static void TestBeforeUpdateNegative()
	{
		Location__c testWaterway = [SELECT Id FROM Location__c WHERE Name = 'Test Waterway 1' LIMIT 1];

		Id rType = Schema.SObjectType.Compliance_Activity__c.getRecordTypeInfosByDeveloperName().get('Maritime_Audit_Wway').getRecordTypeId();
		Compliance_Activity__c auditRec = TestDataFactory.createComplianceActivity(rType);
		auditRec.Waterway__c = testWaterway.Id;
		auditRec.Start_Date__c = Date.today().addDays(-5);
		auditRec.End_Date__c = Date.today().addDays(-4);
		insert auditRec;

		

		List<Asset__c> atons = [SELECT Id FROM Asset__c WHERE Location__c =: testWaterway.Id];
		System.assertEquals(5, atons.size());

		List<Task> checkOuts = TestDataFactory.createTasks(3);
		checkOuts.get(0).Status = 'Completed';
		checkOuts.get(0).WhatId = atons.get(0).Id;

		checkOuts.get(1).Status = 'Completed';
		checkOuts.get(1).WhatId = atons.get(0).Id;
		
		checkOuts.get(2).Status = 'Completed';
		checkOuts.get(2).WhatId = atons.get(0).Id;

		insert checkOuts;

		auditRec.Finalize_Audit__c = true;
		update auditRec;

		List<Task> auditTasks = [SELECT Id FROM Task WHERE Compliance_Activity__c =: auditRec.Id];
		System.assertEquals(0, auditTasks.size());
		
	}

	/**
	 * Test that when start and End dates are updated the completed tasks related to the ATONS of the waterway between those dates are 
	 * linked to the Audit record as well
	 **/
	@isTest
	static void TestBeforeUpdateMultipleAudits()
	{
		Location__c testWaterway = [SELECT Id FROM Location__c WHERE Name = 'Test Waterway 1' LIMIT 1];

		Id rType = Schema.SObjectType.Compliance_Activity__c.getRecordTypeInfosByDeveloperName().get('Maritime_Audit_Wway').getRecordTypeId();
		Compliance_Activity__c auditRec = TestDataFactory.createComplianceActivity(rType);
		auditRec.Waterway__c = testWaterway.Id;
		auditRec.Start_Date__c = Date.today().addDays(-1);
		auditRec.End_Date__c = Date.today().addDays(1);
		insert auditRec;

		Location__c testWaterway2 = [SELECT Id FROM Location__c WHERE Name = 'Test Waterway 2' LIMIT 1];

		Compliance_Activity__c auditRec2 = TestDataFactory.createComplianceActivity(rType);
		auditRec2.Waterway__c = testWaterway2.Id;
		auditRec2.Start_Date__c = Date.today().addDays(-1);
		auditRec2.End_Date__c = Date.today().addDays(1);
		insert auditRec2;

		List<Asset__c> atons = [SELECT Id FROM Asset__c WHERE Location__c =: testWaterway.Id];
		System.assertEquals(5, atons.size());

		List<Task> checkOuts = TestDataFactory.createTasks(3);
		checkOuts.get(0).Status = 'Completed';
		checkOuts.get(0).WhatId = atons.get(0).Id;

		checkOuts.get(1).Status = 'Completed';
		checkOuts.get(1).WhatId = atons.get(1).Id;
		
		checkOuts.get(2).Status = 'Completed';
		checkOuts.get(2).WhatId = atons.get(2).Id;

		insert checkOuts;
		

		atons = [SELECT Id FROM Asset__c WHERE Location__c =: testWaterway2.Id];
		System.assertEquals(3, atons.size());
		
		checkOuts = TestDataFactory.createTasks(2);
		checkOuts.get(0).Status = 'Completed';
		checkOuts.get(0).WhatId = atons.get(0).Id;

		checkOuts.get(1).Status = 'Completed';
		checkOuts.get(1).WhatId = atons.get(1).Id;

		insert checkOuts;

		auditRec.Finalize_Audit__c = true;
		auditRec2.Finalize_Audit__c = true;
		update new List<Compliance_Activity__c>{auditRec, auditRec2};

		List<Task> auditTasks = [SELECT Id FROM Task WHERE Compliance_Activity__c =: auditRec.Id];
		System.assertEquals(3, auditTasks.size());

		auditTasks = [SELECT Id FROM Task WHERE Compliance_Activity__c =: auditRec2.Id];
		System.assertEquals(2, auditTasks.size());
		
	}


}