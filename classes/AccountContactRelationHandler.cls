public class AccountContactRelationHandler extends TriggerBase implements ITriggerBase{
	/**
	 * afterInsert Trigger Event
	 * @return null
	 */
	public override void afterInsert() {
		AccountSharing(records, null, 'Insert');
	}
	/**
	 * afterUpdate Trigger Event	
	 * @return null
	 */
	public override void afterUpdate() {
		AccountSharing(records, oldmap, 'Update');
	}

	/**
	 * Controlls the Apex sharing against the AccountContactRelation Object	
	 * @param mode = 'Insert', 'Update', 'Recalculation'
	 * @return null
	 */
	public static void AccountSharing(list<SObject> records, Map<Id, SObject> oldMap, String mode){
		//Get an instance of the SharingHelper class with the Object Name
		SharingHelper SharingHelp = new SharingHelper(AccountShare.sObjectType.getDescribe().getName());

		//get a set of AccountContactRelation Ids by casting to map and getting keys
		List<AccountContactRelation> relations = (list<AccountContactRelation>) records;

		//loop round and add the Id and the Portal account Id
		for(AccountContactRelation relation : relations ){

			//get old AccountContactRelation - only applicable if running in Update mode
			AccountContactRelation oldRelation = null;
			if(mode == 'Update' && oldMap != null){
				oldRelation = (AccountContactRelation) oldMap.get(relation.Id);
			}
			
			//if new or changed or Recalculate then add the new Account to the sharing
			if(mode == 'Insert' || mode == 'Recalculation' 
				|| relation.Full_Access__c != oldRelation.Full_Access__c ){

				// check if sharing is required				
				if(relation.Full_Access__c)
					SharingHelp.addToSharingUsingACR(relation.AccountId, relation.AccountId, 'Manual', 'Edit');

				//if changed OR Recalculation mode than delete the old one
				if(mode == 'Recalculation' 
					|| (mode == 'Update' && relation.Full_Access__c != oldRelation.Full_Access__c)){
					SharingHelp.addToDeleteList(relation.AccountId);
				}
			}
		}

		//delete any sharing rules that need to be deleted
		SharingHelp.deleteSharingRules(new set<String> {'Manual'});

		//Insert Sharing rules asyncronously
		SharingHelp.insertApexSharing_Asyncronously();
	}
}