public class arcGIS_AddressWrapper{
        public cls_address address;
        public cls_location location;

   public class cls_address {
        public String Match_addr {get; set;}    //1 Spring St, Melbourne, Victoria, 3000
        public String LongLabel  {get; set;}     //1 Spring St, Melbourne, Victoria, 3000, AUS
        public String ShortLabel {get; set;}    //1 Spring St
        public String Addr_type  {get; set;}     //PointAddress
        public String Type       {get; set;}          //
        public String PlaceName  {get; set;}     //
        public String AddNum     {get; set;}        //1
        public String Address    {get; set;}       //1 Spring St     
        public String Block      {get; set;}         //
        public String Sector     {get; set;}        //
        public String Neighborhood {get; set;}  //Melbourne     
        public String District   {get; set;}      // 
        public String City       {get; set;}          //Melbourne
        public String MetroArea  {get; set;}     //
        public String Subregion  {get; set;}     //
        public String Region     {get; set;}        //Victoria
        public String Territory  {get; set;}     //
        public String Postal     {get; set;}        //3000
        public String PostalExt  {get; set;}     //
        public String CountryCode {get; set;}   //AUS
    }
    
   public  class cls_location{
    public Double x {get; set;} //144.9744738940962
    public Double y {get; set;} //-37.81508940635952
    public cls_spatialReference spatialReference;
   }    
   
       public class cls_spatialReference{
        public Integer wkid {get; set;}    //4326
        public Integer latestWkid {get; set;}  //4326
    }

}