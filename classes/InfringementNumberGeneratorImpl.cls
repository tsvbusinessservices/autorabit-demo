public class InfringementNumberGeneratorImpl implements InfringementNumberGenerator{

	public Long getNewInfringementNumber_Legacy() {
		List<Outcome__c>  maxInfringementOutcome = [Select Id, Infringement_Auto_Number__c 
														From Outcome__c 
														Where Infringement_Auto_Number__c > 0
														Order by Infringement_Auto_Number__c desc Limit 1];

		// get the min infringement # from Setting
		Long maxInfringementNumber = getMinInfringementNumber_Legacy();

		// update the maxInfringement # if the data has a bigger number
		if(maxInfringementOutcome.size() > 0 && maxInfringementOutcome.get(0).Infringement_Auto_Number__c > maxInfringementNumber)
			maxInfringementNumber = (Long) maxInfringementOutcome.get(0).Infringement_Auto_Number__c;

		// increment the number with 1
		return maxInfringementNumber + 1;
	}

	private Long getMinInfringementNumber_Legacy() {
		return (Long) TSV_Settings__c.getInstance().Minimum_Infringement_Number__c;
	}

	public Long getNewInfringementNumber() {

		Long generatedInfringmentNumber;

		// Generate Infringement Number Record
		Infringement_Number__c infringmentNumberRecord = new Infringement_Number__c();
		insert infringmentNumberRecord;

		// Retrieve Infringment Number Record
		List <Infringement_Number__c> infringementNumbers = [SELECT Id, Infringement_Auto_Number__c FROM Infringement_Number__c WHERE Id = :infringmentNumberRecord.Id];

		// Confirm record was retrieved
		if (infringementNumbers.size() == 1)
		{
			// Return Infringement Auto Number
			generatedInfringmentNumber = Long.valueOf(infringementNumbers.get(0).Infringement_Auto_Number__c);
		}
		
		return generatedInfringmentNumber;
	}
}