@isTest
private class ContentDocumentHandlerTest {

	static testMethod void onAfterInsertTest() {
		
		ContentVersion testContent;
		
		CollaborationGroup[] lgroups = [SELECT Id FROM CollaborationGroup];
		System.assertNotEquals(null,lgroups);

		Test.startTest();

		Blob beforeblob = Blob.valueOf('Unit Test Attachment Body');

		ContentVersion cv = new ContentVersion();
		cv.title = 'test content trigger';
		cv.PathOnClient = 'test';
		cv.VersionData = beforeblob;
		insert cv;
		System.assertNotEquals(null,cv.Id);

		testContent = [SELECT Id, ContentDocumentId FROM ContentVersion where Id = :cv.Id Limit 1].get(0);

		Test.stopTest();
		System.assertEquals(1,[SELECT count() FROM ContentDocumentLink WHERE ContentDocumentId =: testContent.ContentDocumentId]);
	}

}