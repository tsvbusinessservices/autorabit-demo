public class Mod97ChecksumGenerator implements ChecksumGenerator {

	private Integer sourceLength;

	public Mod97ChecksumGenerator(Integer length) {
		this.sourceLength = length;
	}
	
	public String getChecksum(Long source) {
		String sourceText = '' + source;
		System.debug('Generating Mod97 Checksum for ' + sourceText);

		if(sourceText.length() < sourceLength) 
			throw new InvalidSourceException('Invalid characters in source');

		// split the digits. first element will be empty
		List<String> digits = sourceText.split('');

		Integer total = 0;
		for(Integer i=0; i<digits.size(); i++) {
			total += Integer.valueOf(digits.get(i)) * (9 + i);
		}

		System.debug('total::' + total);

		Integer remainder = Math.mod(total, 97);

		return (remainder == 0? '97': '' + remainder);
	}

}