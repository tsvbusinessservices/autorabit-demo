public without sharing class ContentHelper {
    
    /**
    * Gets Information to supplement the Content using a query for Batch
    * @return null
    */
    public static string getContentForSharingRecalc(){
        return 'SELECT Id, Group_of_Waterway__c, Group_of_Facility__c, RecordTypeId FROM Content__c WHERE RecordTypeId IN (\''+ Constants.CONTENT_RT_ID_NOTIFICATION + '\',\''+Constants.CONTENT_RT_ID_EDUCATIONAL_CONTENT+'\',\''+Constants.CONTENT_RT_ID_FEATURED_STATISTICS+'\')';
    }

    public static List<Content__Share> getConSharing(set<Id> conIds, Set<String> rowCause){
        return [SELECT Id, UserOrGroupId FROM Content__Share WHERE ParentId IN :conIds AND RowCause IN :rowCause];
    }

    public static Map<Id,List<Allocation__Share>> getContentIdtoFacilityGroupSharingMap(List<Content__c> records, Map<Id,List<Allocation__Share>> mapOfGroupIdToSharings){
        Map<Id,List<Allocation__Share>> mapOfContentIdsToFacilityGroupSharings = new Map<Id,List<Allocation__Share>>();

        for(Content__c cont : records){
            if(cont.Group_of_Facility__c != null){
                mapOfContentIdsToFacilityGroupSharings.put(cont.Id, mapOfGroupIdToSharings.get(cont.Group_of_Facility__c));
            }
        }

        return mapOfContentIdsToFacilityGroupSharings;   
	} 

	public static Map<Id,List<Allocation__Share>> getContentIdtoWaterwayGroupSharingMap(List<Content__c> records, Map<Id,List<Allocation__Share>> mapOfGroupIdToSharings){
        Map<Id,List<Allocation__Share>> mapOfContentIdsToWaterwayGroupSharings = new Map<Id,List<Allocation__Share>>();

        for(Content__c cont : records){
            if(cont.Group_of_Waterway__c != null){
                mapOfContentIdsToWaterwayGroupSharings.put(cont.Id, mapOfGroupIdToSharings.get(cont.Group_of_Waterway__c));
            }
        }

        return mapOfContentIdsToWaterwayGroupSharings;
	} 

    public static Map<Id,List<Allocation__Share>> getGroupIdstoSharingMap(List<Content__c> records){
        Map<Id,List<Allocation__Share>> mapOfGroupIdToSharings = new Map<Id,List<Allocation__Share>>();        
        Set<Id> groupIds = new Set<Id>();

        //Get all groups
        for(Content__c cont : records){
            if(cont.Group_of_Facility__c != null){
                groupIds.add(cont.Group_of_Facility__c);
            }else if(cont.Group_of_Waterway__c != null){
                groupIds.add(cont.Group_of_Waterway__c);
            }
        }

        //Get all locationShares
        Set<String> rowCauses = new Set<String>{Schema.Allocation__Share.RowCause.Facility_Manager__c,Schema.Allocation__Share.RowCause.Secondary_Waterway_Manager__c, Schema.Allocation__Share.RowCause.Parent_Waterway_Manager__c};
        List<Allocation__Share> allocationShares = [SELECT Id, UserOrGroupId, RowCause, ParentId FROM Allocation__Share WHERE RowCause IN :rowCauses AND ParentId IN :groupIds];
        
        //create a map of location to the sharing.   
        for(Allocation__Share alocShare : allocationShares){
            //If the location already exists in the map then add to the account list
            if(mapOfGroupIdToSharings.ContainsKey(alocShare.ParentId)){
                List<Allocation__Share> sharings = mapOfGroupIdToSharings.get(alocShare.ParentId);
                if(sharings != null){                    
                    sharings.add(alocShare);
                } 
                
            }
            //else create new location set name list
            else{     
                mapOfGroupIdToSharings.put(alocShare.ParentId, new List<Allocation__Share>{alocShare});
            }            
        }

        return mapOfGroupIdToSharings;
	} 


	public static Map<Id,Integer> getContentIdtoCountofDocumentsMap(Set<Id> educContentIds){
		Map<Id,Integer> mapOfContentIdToCountofDocuments = new Map<Id,Integer>();
         
        //add all counts for the content
        for(AggregateResult agrContent : [SELECT LinkedEntityId contentId, Count(Id) recordCount FROM ContentDocumentLink WHERE LinkedEntityId IN :educContentIds GROUP BY LinkedEntityId ]){
            mapOfContentIdToCountofDocuments.put((Id) agrContent.get('contentId'), (Integer) agrContent.get('recordCount'));
        }

        //if not content found put 0
        for(Id educContent : educContentIds){
            if(!mapOfContentIdToCountofDocuments.keySet().contains(educContent)){
                mapOfContentIdToCountofDocuments.put(educContent,0);
            }
        }

        return mapOfContentIdToCountofDocuments;
	} 
}