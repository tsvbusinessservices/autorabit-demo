@isTest
private class ComplianceActivityCCTest {
	
	static testMethod void retrieveUserComplianceActivityDefault() {
		ComplianceActivityCS__c csObj = new ComplianceActivityCS__c();
		csObj.SetupOwnerId = UserInfo.getUserId();
		csObj.Temperature__c = '';
		csObj.Water_Conditions__c = ''; 
		csObj.Weather__c = '';
		csObj.Wind_Direction__c = '';
		csObj.Wind_Speed__c = '';
		insert csObj;
		Test.startTest();
			//Validate if Object is retrieved
			System.assertNotEquals(null,ComplianceActivityCC.retrieveUserComplianceActivityDefault());
		Test.stopTest();
	}
	static testMethod void retrieveUserComplianceActivityDefaultNew() {
		Test.startTest();
			ComplianceActivityCS__c csObj = new ComplianceActivityCS__c();
			csObj.Temperature__c = 'Unknown';
			csObj.Water_Conditions__c = ''; 
			csObj.Weather__c = '';
			csObj.Wind_Direction__c = '';
			csObj.Wind_Speed__c = '';
			ComplianceActivityCC.setUserComplianceActivityDefault(csObj);
		Test.stopTest();
		System.assertEquals(1,
			[SELECT count() 
			FROM ComplianceActivityCS__c 
			WHERE SetupOwnerId = :UserInfo.getUserId() 
			AND Temperature__c = 'Unknown']
		);
	}
	static testMethod void retrieveUserComplianceActivityDefaultExisting() {
		ComplianceActivityCS__c csObj = new ComplianceActivityCS__c();
		csObj.SetupOwnerId = UserInfo.getUserId();
		csObj.Temperature__c = '';
		csObj.Water_Conditions__c = ''; 
		csObj.Weather__c = '';
		csObj.Wind_Direction__c = '';
		csObj.Wind_Speed__c = '';
		insert csObj;
		Test.startTest();
			ComplianceActivityCS__c csObjEx = new ComplianceActivityCS__c();
			csObjEx.Temperature__c = 'Unknown';
			ComplianceActivityCC.setUserComplianceActivityDefault(csObjEx);
		Test.stopTest();
		System.assertEquals(1,
			[SELECT count() 
			FROM ComplianceActivityCS__c 
			WHERE SetupOwnerId = :UserInfo.getUserId() 
			AND Temperature__c = 'Unknown']
		);
	}
	
}