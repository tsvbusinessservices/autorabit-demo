public class CaseHandler extends TriggerBase implements ITriggerBase {

	/**
	 * afterUpdate Trigger Event	
	 * @return null
	 */
	public override void beforeUpdate() {
		System.debug('CaseHandler::beforeUpdate');
		resetWaterwayVerified();
	}

	
	/**
	 * Retrieve Vessel Inspection Record Ids
	 * @return Set Id of Compliance Activity
	 */
	@TestVisible
	private void resetWaterwayVerified(){
		Set<Id> listCases = new Set<Id>();
		for(Case obj: (List<Case>)records){
			System.debug('CaseHandler::resetWaterwayVerified::' + obj.Location__c);
			//Select Vessel Inpection Record Types
			if(CaseHelper.isMarineIncident(obj)
				&& CaseHelper.isLocationChanged(obj, (Case) oldmap.get(obj.Id))) {
				System.debug('Resetting Waterway_Verified__c::' + obj.Id);
				obj.Waterway_Verified__c = false;
			}
		}
	}
}