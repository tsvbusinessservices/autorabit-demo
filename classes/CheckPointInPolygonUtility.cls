public interface CheckPointInPolygonUtility {

	String checkPointInPolygon(Decimal xlat, Decimal xlong);

}