public class AuditData {
	
	public Compliance_Activity__c audit;

	List<Asset__c> allAssets = new List<Asset__c>();

	@TestVisible
	List<Task> allTasks = new List<Task>();

	Map<Id, Outcome__c> ruleItemsMap = new Map<Id, Outcome__c>();	

	public List<Outcome__c> ruleItems{
		get;
		set {
			ruleItems = value;
			ruleItemsMap.clear();
			if(value != null && value.size() >0) {
				for(Outcome__c ruleItem: value) {
					ruleItemsMap.put(ruleItem.Rule__c, ruleItem);
				}
			}
		}
	}

	public Set<Id> getRuleItemIds() {
		Set<Id> ruleItemIds = new Set<Id>();

		if(ruleItems != null && ruleItems.size() > 0) {
			for(Outcome__c ruleItem: ruleItems) {
				ruleItemIds.add(ruleItem.Id);
			}
		}

		return ruleItemIds;
	}

	public AuditData(Compliance_Activity__c audit) {
		this.audit = audit;
	}

	public boolean isAuditedDate(Date dt) {
		return dt >=audit.Start_Date__c && dt <= audit.End_Date__c; 
	}

	public void addTasks(List<Task> tasks) {
		if(tasks != null) {
			System.debug('AuditData::addTasks:: tasks::' + tasks);

			allTasks.addAll(tasks);
		}
	}

	Map<Id, Asset__c> getTaskToAssetMap() {
		Map<Id, Asset__c> taskToAssetMap = new Map<Id, Asset__c>();
		for(Asset__c ast: allAssets) {
			for(Task t: ast.Tasks) {
				taskToAssetMap.put(t.Id, ast);
			}
		}
		return taskToAssetMap;
	}

	public List<Task> getFilteredAuditTasks() {
		List<Task> tasks = new List<Task>();

		for(Task t: allTasks) {

			if(isAuditedDate(Utility.getDateValue(t.LastModifiedDate))) {
				t.Compliance_Activity__c = audit.Id;
				tasks.add(t);
			}
		}
		return tasks;
	}

	public void addAsset(Asset__c a) {
		allAssets.add(a);
	}

	public List<Compliance_Activity_Asset__c> createComplianceActivityAssetList() {
		List<Compliance_Activity_Asset__c> CAAssetList = new List<Compliance_Activity_Asset__c>();
		for(Asset__c asset: allAssets) {
			CAAssetList.add(new Compliance_Activity_Asset__c(
									Asset__c = asset.Id,
									Compliance_Activity__c = audit.Id));
		}
		System.debug('AuditData::createComplianceActivityAssetList:: CAAssetList.size::' + CAAssetList.size());
		return CAAssetList;
	}

	public List<ContentDocumentLink> createContentDocumentLinkForRuleItems(Map<Id, List<ContentDocument>> ruleToImagesMap) {
		List<ContentDocumentLink> imageLinks = new List<ContentDocumentLink>();

		System.debug('AuditData::createContentDocumentLinkForRuleItems:: ruleItemsMap.size::' + ruleItemsMap.size());
		System.debug('AuditData::createContentDocumentLinkForRuleItems:: ruleToImagesMap.size::' + ruleToImagesMap.size());

		for(Id ruleId: ruleItemsMap.keySet()) {
			Outcome__c ruleItem = ruleItemsMap.get(ruleId);
			System.debug('AuditData::createContentDocumentLinkForRuleItems:: ruleItem::' + ruleItem);
			imageLinks.addAll(createContentDocumentLinkForRuleItem(ruleId, ruleItem.Id, ruleToImagesMap));
		}
		System.debug('AuditData::createContentDocumentLinkForRuleItems:: imageLinks.size::' + imageLinks.size());
		return imageLinks;
	}

	public List<ContentDocumentLink> createContentDocumentLinkForRuleItem(Id ruleId, Id ruleItemId, Map<Id, List<ContentDocument>> ruleToImagesMap) {
		List<ContentDocumentLink> imageLinks = new List<ContentDocumentLink>();
		System.debug('AuditData::createContentDocumentLinkForRuleItem:: ruleToImagesMap::' + ruleToImagesMap);

		List<ContentDocument> documents = ruleToImagesMap.get(ruleId);

		if(documents != null && documents.size() > 0) {
			System.debug('AuditData::createContentDocumentLinkForRuleItem:: documents.size::' + documents.size());
			for(ContentDocument doc: documents) {
				ContentDocumentLink share = new ContentDocumentLink();
				share.ContentDocumentId = doc.Id;
				share.LinkedEntityId = ruleItemId;//Shared to RuleItem
				share.ShareType = 'V';
				imageLinks.add(share);
			}
		}

		return imageLinks;
	}

	public List<ContentDocument> getRenamedContentDocuments(Map<Id, List<ContentDocumentLink>> taskToContentDocumentMap) {
		List<ContentDocument> docs = new List<ContentDocument>();

		Map<Id, Asset__c> taskToAssetMap = getTaskToAssetMap();

		for(Id taskId: taskToAssetMap.keySet()) {
			List<ContentDocumentLink> linkedDocs = taskToContentDocumentMap.get(taskId);
			if(linkedDocs != null && linkedDocs.size() > 0) {
				Asset__c ast = taskToAssetMap.get(taskId);

				for(ContentDocumentLink cdl: linkedDocs) {
					ContentDocument doc = new ContentDocument(Id = cdl.ContentDocumentId, 
															 Title = audit.Name + ' - ' + ast.Name + ' - ' + ast.ESRI_ID__c);
					docs.add(doc);
				}
			}
		}

		return docs;
	}


}