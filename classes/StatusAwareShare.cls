public class StatusAwareShare {

	// name of the recordType
	public String rtName;

	// Set of Statuses that triggers Sharing
	public Set<String> positiveStatuses;

	// default share 
	public boolean defaultShare = false;

	// defines the type of sharing
	public String shareType;

	// Default Constructor
	public StatusAwareShare(){
	}

	public StatusAwareShare(String name, Set<String> statuses) {
		this(name, statuses, Constants.SHARING_TYPE_ALL,  false);
	}

	public StatusAwareShare(String name, Set<String> statuses, String shareType) {
		this(name, statuses, shareType,  false);
	}

	public StatusAwareShare(String name, Set<String> statuses, String shareType, boolean defaultVal) {
		this.rtName = name;
		this.positiveStatuses = statuses;
		this.shareType = shareType;
		this.defaultShare = defaultVal;
	}


	public boolean canShare(String status) {
		return (positiveStatuses!=null && positiveStatuses.contains(status)) ? true: defaultShare;

	}


}