@isTest
private class ComplianceActivityHelperTest {
    @isTest
    static void test_isInspection() {

        Map<String, Id> rtMap = Utility.getRecordTypeMap('Compliance_Activity__c');

        Compliance_Activity__c viCa = TestDataFactory.createComplianceActivity(rtMap.get(Constants.VESSEL_INSPECTION));
        Compliance_Activity__c buCa = TestDataFactory.createComplianceActivity(rtMap.get(Constants.BUS_AUDIT_ACCREDITED_OPERATOR));
        Compliance_Activity__c biCa = TestDataFactory.createComplianceActivity(rtMap.get(Constants.BUS_INSPECTION_STANDARD));

        System.assert(ComplianceActivityHelper.isVesselInspection(viCa));
        System.assert(!ComplianceActivityHelper.isVesselInspection(buCa));
        System.assert(ComplianceActivityHelper.isBusInspection(biCa));
        System.assert(!ComplianceActivityHelper.isBusInspection(viCa));

    }

    @isTest
    static void test_isLocationChanged() {

        Compliance_Activity__c newCa = new Compliance_Activity__c(MALatitude__c = 10.2,
                                    MALongitude__c = 20.2);
        Compliance_Activity__c oldCa = new Compliance_Activity__c(MALatitude__c = 4.2,
                                    MALongitude__c = 20.2);
        

        System.assert(ComplianceActivityHelper.isLocationChanged(newCa, oldCa));

        oldCa.MALatitude__c = 10.2;

        System.assert(!ComplianceActivityHelper.isLocationChanged(newCa, oldCa));

    }

    @isTest
    static void testPIPUtilCreation() {
        System.assert(ComplianceActivityHelper.getPIPUtil() instanceof CheckPointInPolygonUtilityImpl);
    }

    @isTest
    static void test_setWaterWay() {

        TriggerSettingsClass.testOverride = false;

        Location__c l = TestDataFactory.createLocation(null);
        insert l;

        Compliance_Activity__c newCa = new Compliance_Activity__c(MALatitude__c = 10.2,
                                    MALongitude__c = 20.2);
        Compliance_Activity__c newCa2 = new Compliance_Activity__c(MALatitude__c = 15.2,
                                    MALongitude__c = 20.2);
        insert new List<Compliance_Activity__c>{newCa, newCa2};

        MockPIPUtility mpipUtil = new MockPIPUtility(l.Id);

        ComplianceActivityHelper.pipUtil = mpipUtil;

        Test.startTest();
        ComplianceActivityHelper.setWaterWay(new List<Compliance_Activity__c>{newCa, newCa2});
        Test.stopTest();

        List<Compliance_Activity__c> results = [Select Id, Waterway__c From Compliance_Activity__c];

        System.assertEquals(l.Id, results.get(0).Waterway__c);
        System.assertEquals(l.Id, results.get(1).Waterway__c);


    }


    @isTest
    static void test_setWaterWayNegative() {

        TriggerSettingsClass.testOverride = false;

        Location__c l = TestDataFactory.createLocation(null);
        insert l;

        Compliance_Activity__c newCa = new Compliance_Activity__c(MALatitude__c = 10.2,
                                    MALongitude__c = 20.2);
        Compliance_Activity__c newCa2 = new Compliance_Activity__c(MALatitude__c = 15.2,
                                    MALongitude__c = 20.2);
        insert new List<Compliance_Activity__c>{newCa, newCa2};

        MockPIPUtility mpipUtil = new MockPIPUtility(null);

        ComplianceActivityHelper.pipUtil = mpipUtil;

        Test.startTest();
        ComplianceActivityHelper.setWaterWay(new List<Compliance_Activity__c>{newCa, newCa2});
        Test.stopTest();

        List<Compliance_Activity__c> results = [Select Id, Waterway__c From Compliance_Activity__c];

        System.assertEquals(null, results.get(0).Waterway__c);
        System.assertEquals(null, results.get(1).Waterway__c);


    }
}