public with sharing class GeoLocation
{
    @AuraEnabled
    public static Location__c getWaterway(Decimal latitude, Decimal longitude)
    {
    	CheckPointInPolygonUtilityImpl pipUtil = new CheckPointInPolygonUtilityImpl();
    	Id waterwayId = pipUtil.checkPointInPolygon(latitude, longitude);
    	if(waterwayId != null)
    		return [Select Id, Name from Location__c Where Id =:waterwayId];

    	return null;
    }
}