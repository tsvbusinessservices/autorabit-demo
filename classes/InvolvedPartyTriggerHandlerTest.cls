@isTest
public class InvolvedPartyTriggerHandlerTest {

    @isTest
    static void test_GetAllAccreditations() {
        
        createTestAccreditationData();

        Set<String> licences = new Set<String>();
        licences.add('VIC1234');
        licences.add('NSW1234');
        licences.add('XXXX');

        InvolvedPartyTriggerHandler handler = new InvolvedPartyTriggerHandler();

        Map<String, List<Accreditation__c>> result = handler.getAllLicences(licences);

        System.assertEquals(3, result.size());
        System.assertEquals(1, result.get('VIC1234').size());
        System.assertEquals(1, result.get('NSW1234').size());
        System.assertEquals(2, result.get('XXXX').size());
    }

    @isTest
    static void test_getUniqueLicenceNumbers() {
        Involved_Party__c ip1 = TestDataFactory.createInvolvedPersonForVesselInspection();
        ip1.Licence_Number__c = 'ABC123';

        Involved_Party__c ip2 = TestDataFactory.createInvolvedPersonForVesselInspection();
        ip2.Licence_Number__c = 'XYZ123';

        Involved_Party__c ip3 = TestDataFactory.createInvolvedPersonForVesselInspection();
        ip3.Licence_Number__c = 'ABC123';

        Involved_Party__c ip4 = TestDataFactory.createInvolvedPersonForVesselInspection();
        ip4.Licence_Number__c = 'XXXX';

        InvolvedPartyTriggerHandler handler = new InvolvedPartyTriggerHandler();

        Set<String> result = handler.getUniqueLicenceNumbers(new List<Involved_Party__c>{ip1, ip2, ip3, ip4});

        System.assertEquals(3, result.size());
        System.assertEquals(true, result.contains('ABC123'));
        System.assertEquals(true, result.contains('XYZ123'));
        System.assertEquals(true, result.contains('XXXX'));
        System.assertEquals(false, result.contains('VIC1234'));

    }

    @isTest
    static void test_AccreditationExists() {

        createTestAccreditationData();

        Set<String> licences = new Set<String>();
        licences.add('VIC1234');
        licences.add('NSW1234');
        licences.add('XXXX');

        InvolvedPartyTriggerHandler handler = new InvolvedPartyTriggerHandler();

        Map<String, List<Accreditation__c>> allLicencesMap = handler.getAllLicences(licences);

        System.debug('All Licences::' + allLicencesMap);

        Involved_Party__c ip = TestDataFactory.createInvolvedPersonForVesselInspection();
        ip.Licence_Number__c = 'VIC1234';
        ip.License_Origin__c = 'VIC';

        System.assertEquals(true, handler.accreditationExists(ip, allLicencesMap), 'VIC123 (VIC) Failed');

        ip.Licence_Number__c = 'NSW1234';
        ip.License_Origin__c = 'NSW';
        System.assertEquals(true, handler.accreditationExists(ip, allLicencesMap), 'NSW1234 (NSW) Failed');

        ip.Licence_Number__c = 'XXXX';
        ip.License_Origin__c = 'NSW';
        System.assertEquals(true, handler.accreditationExists(ip, allLicencesMap), 'XXXX (NSW) Failed');

        ip.Licence_Number__c = 'XXXX';
        ip.License_Origin__c = 'VIC';
        System.assertEquals(true, handler.accreditationExists(ip, allLicencesMap), 'XXXX (VIC) Failed');

        ip.Licence_Number__c = 'XXXX';
        ip.License_Origin__c = 'QLD';
        System.assertEquals(false, handler.accreditationExists(ip, allLicencesMap), 'XXXX (QLD) Failed');

        ip.Licence_Number__c = 'YYYY';
        ip.License_Origin__c = 'VIC';
        System.assertEquals(false, handler.accreditationExists(ip, allLicencesMap), 'YYYY (QLD) Failed');

    }

    static void createTestAccreditationData() {
        Id recordTypeId;

        List<Account> accs = TestDataFactory.createPersonAccounts(5);
        insert accs;
        
        // Create Accreditation
        recordTypeId = Schema.SObjectType.Accreditation__c.getRecordTypeInfosByDeveloperName().get('Recreational_Boat_User').getRecordTypeId();
        
        Accreditation__c accr1 = TestDataFactory.createAccreditation(accs.get(0), recordTypeId, 'General', 'Current');
        accr1.State__c = 'VIC';
        accr1.VR_Cert_Id__c = 'VIC1234';
        
        Accreditation__c accr2= TestDataFactory.createAccreditation(accs.get(1), recordTypeId, 'General', 'Current');
        accr2.State__c = 'NSW';
        accr2.Licence_Number__c = 'NSW1234';
        
        Accreditation__c accr3= TestDataFactory.createAccreditation(accs.get(2), recordTypeId, 'General', 'Current');
        accr3.State__c = 'VIC';
        accr3.VR_Cert_Id__c = 'XXXX';
        
        Accreditation__c accr4= TestDataFactory.createAccreditation(accs.get(3), recordTypeId, 'General', 'Current');
        accr4.State__c = 'NSW';
        accr4.Licence_Number__c = 'XXXX';

        insert new List<Accreditation__c>{accr1, accr2, accr3, accr4};

    }

    @isTest
    static void test_CreatePersonAndLicence_VIC() {

        Involved_Party__c involvedPerson = TestDataFactory.createInvolvedPersonForVesselInspection();

        involvedPerson.First_Name__c = 'TestFirstName';
        involvedPerson.Last_Name__c = 'TestLastName';
       
        involvedPerson.Licence_Number__c = 'TEST12345';
        involvedPerson.Licence_Expiry_Date__c = Date.newInstance(2019, 10, 23);
        involvedPerson.License_Origin__c = 'VIC';
       
        insert involvedPerson;

        List<Account> accounts = [SELECT Id, FirstName, LastName FROM Account];

        System.assertEquals(1, accounts.size());
        System.assertEquals('TestFirstName', accounts.get(0).FirstName);
        System.assertEquals('TestLastName', accounts.get(0).LastName);

        List<Accreditation__c> licences = [SELECT Id, VR_Cert_Id__c, Account__c, State__c FROM Accreditation__c WHERE Account__c =: accounts.get(0).Id];

        System.assertEquals(1, licences.size());
        System.assertEquals('TEST12345', licences.get(0).VR_Cert_Id__c);
        System.assertEquals(accounts.get(0).Id, licences.get(0).Account__c);
        System.assertEquals('VIC', licences.get(0).State__c);
        

        Involved_Party__c result = [SELECT Id, Person__c, Licence__c FROM Involved_Party__c WHERE ID =: involvedPerson.Id];

        System.assertEquals(accounts.get(0).Id, result.Person__c);
        System.assertEquals(licences.get(0).Id, result.Licence__c);

    }

    @isTest
    static void test_CreatePersonAndLicence_Other() {

        Involved_Party__c involvedPerson = TestDataFactory.createInvolvedPersonForVesselInspection();

        involvedPerson.First_Name__c = 'TestFirstName';
        involvedPerson.Last_Name__c = 'TestLastName';
       
        involvedPerson.Licence_Number__c = 'TEST12345';
        involvedPerson.Licence_Expiry_Date__c = Date.newInstance(2019, 10, 23);
        involvedPerson.License_Origin__c = 'QLD';
       
        insert involvedPerson;

        List<Account> accounts = [SELECT Id, FirstName, LastName FROM Account];

        System.assertEquals(1, accounts.size());
        System.assertEquals('TestFirstName', accounts.get(0).FirstName);
        System.assertEquals('TestLastName', accounts.get(0).LastName);

        List<Accreditation__c> licences = [SELECT Id, Licence_Number__c, Account__c, State__c FROM Accreditation__c WHERE Account__c =: accounts.get(0).Id];

        System.assertEquals(1, licences.size());
        System.assertEquals('TEST12345', licences.get(0).Licence_Number__c);
        System.assertEquals(accounts.get(0).Id, licences.get(0).Account__c);
        System.assertEquals('QLD', licences.get(0).State__c);
        

        Involved_Party__c result = [SELECT Id, Person__c, Licence__c FROM Involved_Party__c WHERE ID =: involvedPerson.Id];

        System.assertEquals(accounts.get(0).Id, result.Person__c);
        System.assertEquals(licences.get(0).Id, result.Licence__c);

    }

    @isTest
    static void test_CreatePersonAndLicence_LicenceAlreadyExist() {

        createTestAccreditationData();

        System.assertEquals(4, [SELECT Id FROM Accreditation__c].size()); 
        System.assertEquals(5, [SELECT Id FROM Account].size());

        Involved_Party__c involvedPerson = TestDataFactory.createInvolvedPersonForVesselInspection();

        involvedPerson.First_Name__c = 'TestFirstName';
        involvedPerson.Last_Name__c = 'TestLastName';
       
        involvedPerson.Licence_Number__c = 'VIC1234';
        involvedPerson.Licence_Expiry_Date__c = Date.newInstance(2019, 10, 23);
        involvedPerson.License_Origin__c = 'VIC';

        Test.startTest();
       
        try {
            insert involvedPerson;
            System.assert(false, 'Should never execute this');
        } catch (Exception e) {
           System.assertEquals(true, e.getMessage().contains('License already Exists.'));
        }

        Test.stopTest();

        System.assertEquals(0, [SELECT Id FROM Involved_Party__c].size());
        System.assertEquals(5, [SELECT Id FROM Account].size());
        System.assertEquals(4, [SELECT Id FROM Accreditation__c].size()); // Already created Accreitations. No new is created
    }

    @isTest
    static void test_CreatePersonOnly() {

        Involved_Party__c involvedPerson = TestDataFactory.createInvolvedPersonForVesselInspection();

        involvedPerson.First_Name__c = 'TestFirstName';
        involvedPerson.Last_Name__c = 'TestLastName';
              
        insert involvedPerson;

        List<Account> accounts = [SELECT Id, FirstName, LastName FROM Account];

        System.assertEquals(1, accounts.size());
        System.assertEquals('TestFirstName', accounts.get(0).FirstName);
        System.assertEquals('TestLastName', accounts.get(0).LastName);

        List<Accreditation__c> licences = [SELECT Id, Licence_Number__c, Account__c, State__c FROM Accreditation__c WHERE Account__c =: accounts.get(0).Id];

        System.assertEquals(0, licences.size());
    }
}