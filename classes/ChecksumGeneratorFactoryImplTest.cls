@isTest
private class ChecksumGeneratorFactoryImplTest
{
	@isTest
	static void testGenerators() {

		ChecksumGeneratorFactoryImpl instance = new ChecksumGeneratorFactoryImpl();

		ChecksumGenerator vicPolGenerator = instance.getChecksumGenerator(Constants.ORG_VIC_POLICE);

		System.assert(true, vicPolGenerator instanceof Mod97ChecksumGenerator);

		ChecksumGenerator tsvGenerator = instance.getChecksumGenerator(Constants.ORG_TSV);

		System.assert(true, vicPolGenerator instanceof Mod7ChecksumGenerator);

	}
}