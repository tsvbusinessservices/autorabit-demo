@isTest
private class Mod97ChecksumGeneratorTest {

	@isTest
	static void testMod97Checksum()
	{
		Mod97ChecksumGenerator generator = new Mod97ChecksumGenerator(12);

		System.assertEquals('84', generator.getChecksum(Long.valueOf('250667646334')));
		System.assertEquals('77', generator.getChecksum(Long.valueOf('250000076730')));

	}

}