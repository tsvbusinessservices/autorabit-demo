@isTest
public class MockInfNumberGenerator implements InfringementNumberGenerator {

	static Long num = 2000;

	public Long getNewInfringementNumber() {
		return num++;
	}

}