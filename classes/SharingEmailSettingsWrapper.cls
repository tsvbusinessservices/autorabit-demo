public class SharingEmailSettingsWrapper {

	private Sharing_Email_Settings__mdt setting;

	private static SharingEmailSettingsWrapper instance = null;

	public SharingEmailSettingsWrapper (Sharing_Email_Settings__mdt setting) {
		this.setting = setting;
	}

	public static SharingEmailSettingsWrapper getSetting() {
		if(instance == null) {
			Sharing_Email_Settings__mdt configSetting = [Select DeveloperName, 	Email_Address__c, Enable_Error_Emails__c, Send_Recalculation_Success_Emails__c
														FROM Sharing_Email_Settings__mdt
														WHERE DeveloperName = 'Default' 
														LIMIT 1];
			instance = new SharingEmailSettingsWrapper(configSetting);
		}
		return instance;
	}

	public String getEmailAddress() {
		return setting.Email_Address__c;
	}

	public Boolean getEnableErrorEmail() {
		return setting.Enable_Error_Emails__c;
	}

	public Boolean getEnableSuccess() {
		return setting.Send_Recalculation_Success_Emails__c;
	}
}