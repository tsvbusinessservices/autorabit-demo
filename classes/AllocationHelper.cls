public without sharing class AllocationHelper {

    /**
    * Gets Information to supplement the Assets using a query for Batch
    * @return null
    */
    public static string getAllocationForSharingRecalc(){
        String recordTypeIds = '(\''+Constants.ALLOCATION_RT_ID_WATERWAY_GROUP+'\',\''+Constants.ALLOCATION_RT_ID_FACILITY_GROUP+'\')';
        return 'SELECT Id, Facility__c, Waterway_Name__c, Group__c, RecordTypeId FROM Allocation__c WHERE RecordTypeId IN '+recordTypeIds+' AND Group__c != NULL ORDER BY Group__c ASC';
    }
    
    public static List<Allocation__Share> getAllocSharing(set<Id> allocIds, Set<String> rowCause){
        return [SELECT Id, UserOrGroupId FROM Allocation__Share WHERE ParentId IN :allocIds AND RowCause IN :rowCause];
    }

    public static Map<Id,List<Location__Share>> getLocationGroupIdtoSharingMap(List<Allocation__c> facilityWaterwayGroupAllocations){
        Map<Id,List<Location__Share>> mapOfLocationGroupIdToSharings = new Map<Id,List<Location__Share>>();        

        //Get all locations
        Set<Id> locationIds = new Set<Id>();
        for(Allocation__c facilityWaterwayGroup : facilityWaterwayGroupAllocations){
            if(facilityWaterwayGroup.RecordTypeId == Constants.ALLOCATION_RT_ID_FACILITY_GROUP){
                locationIds.add(facilityWaterwayGroup.Facility__c);
            }else{
                locationIds.add(facilityWaterwayGroup.Waterway_Name__c);
            }
        }
        
        //Get all locationShares
        Set<String> rowCauses = new Set<String>{Schema.Location__Share.RowCause.Facility_Manager__c,Schema.Location__Share.RowCause.Secondary_Waterway_Manager__c, Schema.Location__Share.RowCause.Parent_Waterway_Manager__c};
        List<Location__Share> locationShares = [SELECT Id, UserOrGroupId, RowCause, ParentId FROM Location__Share WHERE RowCause IN :rowCauses AND ParentId IN :locationIds];
        
    
        //create a map of location to the sharing.   
        Map<Id, List<Location__Share>> mapOflocationIdToSharings = new Map<Id, List<Location__Share>>();
        for(Location__Share locShare : locationShares){
            //If the location already exists in the map then add to the account list
            if(mapOflocationIdToSharings.ContainsKey(locShare.ParentId)){
                List<Location__Share> sharings = mapOflocationIdToSharings.get(locShare.ParentId);
                if(sharings != null){                    
                    sharings.add(locShare);
                } 
                
            }
            //else create new location set name list
            else{     
                mapOflocationIdToSharings.put(locShare.ParentId, new List<Location__Share>{locShare});
            }            
        }

        for(Allocation__c facilityWaterwayGroup : facilityWaterwayGroupAllocations){
            //If the group already exists in the map then add to the waterway or facility
            /* Will never loop here
            if(mapOfLocationGroupIdToSharings.ContainsKey(facilityWaterwayGroup.Id)){
                List<Location__Share> sharings = mapOfLocationGroupIdToSharings.get(facilityWaterwayGroup.Id);
                if(facilityWaterwayGroup.RecordTypeId == Constants.ALLOCATION_RT_ID_FACILITY_GROUP){
                    List<Location__Share> sharingToAdd = mapOflocationIdToSharings.get(facilityWaterwayGroup.Facility__c);
                    if(sharingToAdd != null && !sharingToAdd.isEmpty()){
                        sharings.addAll(sharingToAdd);
                    }
                }else{
                    List<Location__Share> sharingToAdd  = mapOflocationIdToSharings.get(facilityWaterwayGroup.Waterway_Name__c);
                    if(sharingToAdd != null && !sharingToAdd.isEmpty()){
                        sharings.addAll(sharingToAdd);
                    }
                }
            }
            else{ */    
            //else create new group set name list
                if(facilityWaterwayGroup.RecordTypeId == Constants.ALLOCATION_RT_ID_FACILITY_GROUP){                   
                    List<Location__Share> sharingToAdd = mapOflocationIdToSharings.get(facilityWaterwayGroup.Facility__c);
                    if(sharingToAdd != null && !sharingToAdd.isEmpty()){
                        mapOfLocationGroupIdToSharings.put(facilityWaterwayGroup.Id, sharingToAdd);
                    }
                }else{               
                    List<Location__Share> sharingToAdd = mapOflocationIdToSharings.get(facilityWaterwayGroup.Waterway_Name__c);
                    if(sharingToAdd != null && !sharingToAdd.isEmpty()){
                        mapOfLocationGroupIdToSharings.put(facilityWaterwayGroup.Id, sharingToAdd);
                    }
                }
            //}            
        }

        return mapOfLocationGroupIdToSharings;
	} 

	public static Map<Id,List<Allocation__c>> getGroupIdToLocationGroups(Set<Id> groupAllocationIds, List<Allocation__c> facilityWaterwayGroups){
        Map<Id,List<Allocation__c>> mapOfGroupIdToLocationGroups = new Map<Id,List<Allocation__c>>();

        for(Id groupAllocationId : groupAllocationIds){
            for(Allocation__c facilityWaterwayGroup : facilityWaterwayGroups){
                if(groupAllocationId == facilityWaterwayGroup.Group__c){
                    if(mapOfGroupIdToLocationGroups.ContainsKey(groupAllocationId)){
                        List<Allocation__c> locationgroups = mapOfGroupIdToLocationGroups.get(groupAllocationId);
                        locationgroups.add(facilityWaterwayGroup);
                    }else{
                        mapOfGroupIdToLocationGroups.put(groupAllocationId, new List<Allocation__c>{facilityWaterwayGroup});
                    }
                }
            }            
        }
        

        return mapOfGroupIdToLocationGroups;

	} 

    public static void checkDuplicates(List<Allocation__c> groupsToFinalize){
        Map<Id,Set<Id>> mapOfGroupToLocationGroupIds = new Map<Id, Set<Id>>();

        Set<Id> groupIds = new Set<Id>();
        for(Allocation__c groupToFinalize : groupsToFinalize){
            groupIds.add(groupToFinalize.Id);
        }

        //Get all facility group and waterway group
        Set<Id> recordTypeIds = new Set<Id>{Constants.ALLOCATION_RT_ID_FACILITY_GROUP, Constants.ALLOCATION_RT_ID_WATERWAY_GROUP};
        List<Allocation__c> facilityWaterwayGroups = [SELECT Id, Facility__c, Waterway_Name__c, Group__c, RecordTypeId FROM Allocation__c WHERE RecordTypeId IN :recordTypeIds AND Group__c IN :groupIds ORDER BY Facility__c ASC, Waterway_Name__c ASC];
        
        for(Allocation__c facilityWaterwayGroup : facilityWaterwayGroups){            
            if(mapOfGroupToLocationGroupIds.ContainsKey(facilityWaterwayGroup.Group__c)){
                Set<Id> allocs = mapOfGroupToLocationGroupIds.get(facilityWaterwayGroup.Group__c);
                if(facilityWaterwayGroup.RecordTypeId == Constants.ALLOCATION_RT_ID_FACILITY_GROUP){                
                    allocs.add(facilityWaterwayGroup.Facility__c);
                }else{
                    allocs.add(facilityWaterwayGroup.Waterway_Name__c);                
                }
            }else{
                if(facilityWaterwayGroup.RecordTypeId == Constants.ALLOCATION_RT_ID_FACILITY_GROUP){           
                    mapOfGroupToLocationGroupIds.put(facilityWaterwayGroup.Group__c, new Set<Id>{facilityWaterwayGroup.Facility__c});
                }else{
                    mapOfGroupToLocationGroupIds.put(facilityWaterwayGroup.Group__c, new Set<Id>{facilityWaterwayGroup.Waterway_Name__c});                
                }
            }
        }

        Set<Decimal> newHashCodes = new Set<Decimal>();
        for(Allocation__c groupToFinalize : groupsToFinalize){
            String idsForHash = '';
            if(mapOfGroupToLocationGroupIds.get(groupToFinalize.Id) == null){
                groupToFinalize.addError('Please add Facility/Waterway Groups');
            }else{
                for(Id locationGroups : mapOfGroupToLocationGroupIds.get(groupToFinalize.Id)){
                    idsForHash += locationGroups;
                }
                System.debug('************* hashstring : ' + idsForHash);

                Decimal hashcode = idsForHash.hashCode();  
                groupToFinalize.GroupMembersHashCode__c = hashcode; 
                newHashCodes.add(hashcode);  
            }
            
                     
        }
        
        List<Allocation__c> dupeallocs = new List<Allocation__c>();
        for(Allocation__c alloc : [SELECT Id, Name, GroupMembersHashCode__c 
                                    FROM Allocation__c 
                                    WHERE RecordTypeId = :Constants.ALLOCATION_RT_ID_GROUP 
                                    AND GroupMembersHashCode__c IN :newHashCodes 
                                    AND Unique_Group__c = false]){
            dupeallocs.add(alloc);
        }

        for(Allocation__c groupToFinalize : groupsToFinalize){
            for(Allocation__c dupealloc :dupeallocs){
                if(groupToFinalize.id != dupealloc.id && dupealloc.GroupMembersHashCode__c == groupToFinalize.GroupMembersHashCode__c){
                    groupToFinalize.addError('This group is a duplicate of '+dupealloc.Name);
                    break;
                }
            }                       
        }
    } 
}