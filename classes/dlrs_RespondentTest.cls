/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_RespondentTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_RespondentTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Respondent__c());
    }
}