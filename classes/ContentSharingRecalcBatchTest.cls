@isTest
private class ContentSharingRecalcBatchTest {     
	@isTest static void test_Scedhuling() {
		ContentSharingRecalcBatch batch = new ContentSharingRecalcBatch();
		String sch = '0 0 10 * * ?';
		system.schedule('Recalc test 12345', sch, batch);
	}

	//P - Run Recalc job - locations
	@isTest static void PositiveTest_RunRecalcJob() {
		Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount2'].Id;

		list<Location__c> locations =  TestDataFactory.createMultipleLocation(100, portalAccountId, Constants.LOCATION_RT_ID_WATERWAY);
		insert locations;

        List<Waterway_Role__c> waterwayroles =  new List<Waterway_Role__c>();
        for(Location__c location : locations){
            Waterway_Role__c wwr1 = TestDataFactory.createWaterwayRole(portalAccountId, location.id);
            waterwayroles.add(wwr1);
        }
        insert waterwayroles;
        
        List<Allocation__c> parentgroups = new List<Allocation__c>();
        for(Integer i = 0; i < 100; i++){            
            Allocation__c alocGroup = TestDataFactory.createGroup();
            alocGroup.Group_Type__c = 'Waterway';
            parentgroups.add(alocGroup);
        }
        insert parentgroups;

        List<Allocation__c> waterwayGroups = new List<Allocation__c>();
        for(Integer i = 0; i < 100; i++){  
            Allocation__c alocWaterwayGroup = TestDataFactory.createLocationGroup(Constants.ALLOCATION_RT_ID_WATERWAY_GROUP);
            alocWaterwayGroup.Waterway_Name__c = locations.get(i).id;
            alocWaterwayGroup.Group__c = parentgroups.get(i).id;
            waterwayGroups.add(alocWaterwayGroup);
        }
        insert waterwayGroups;

        for(Allocation__c parentgroup : parentgroups){
            parentgroup.Status__c = Constants.ALLOCATION_STATUS_FINALIZED;
        }
        update parentgroups;

        List<Content__c> contents =  new List<Content__c>();
        for(Allocation__c parentgroup : parentgroups){
            Content__c cont1 = TestDataFactory.createContent();            
            cont1.Group_of_Waterway__c = parentgroup.Id;
            contents.add(cont1);
        }
        insert contents;
		map<Id, Content__c> contentsMap = new map<Id, Content__c>(contents);

		test.startTest();
		
		ContentSharingRecalcBatch b = new ContentSharingRecalcBatch();
		database.executeBatch(b, 200);

		test.stopTest();

		//check that there has been a sharing record created
		List<Content__Share> conSharing = ContentHelper.getConSharing(contentsMap.keySet(), new set<String>{Schema.Content__Share.RowCause.Secondary_Waterway_Manager__c, Schema.Content__Share.RowCause.Parent_Waterway_Manager__c});
		system.assertEquals(100, conSharing.size());
	}

	@testSetup static void testSetup(){
		TestDataFactory.createPartnerAccount(new List<Id>{null,Constants.ACCOUNT_RT_ID_MARITIME},new List<String>{null,Constants.ACCOUNT_TYPE_WATERWAY_MANAGER});
	}


}