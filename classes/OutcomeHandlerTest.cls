@isTest
public class OutcomeHandlerTest
{

	@testSetup
	static void init() {
		TSV_Settings__c settings = TSV_Settings__c.getOrgDefaults();
		settings.Minimum_Infringement_Number__c = 1111;
		settings.Infringement_grace_days__c = 30;
		upsert settings custSettings__c.Id;

	}

	@isTest
	static void testServiceInstances() {

		OutcomeHandler handler = new OutcomeHandler();
		
		System.assertEquals(true, handler.getChecksumGeneratorFactory() != null);
		System.assertEquals(true, handler.getInfringementNumberGenerator() != null);

	}

	@isTest
	static void testInsertOutcome() {

		OutcomeHandler.checksumfactoryInstance = new MockChecksumGeneratorFactory();
		OutcomeHandler.infNumberGeneratorInstance = new MockInfNumberGenerator();

		List<Outcome__c> outcomes = TestDataFactory.createOutcome('Bus - Infringement', 2);
		outcomes.add(TestDataFactory.createOutcome('Bus Safety Alert'));
		outcomes.addAll(TestDataFactory.createOutcome('Maritime - Infringement Notice',3));
		outcomes.get(3).Police_Infringement__c = true;
		outcomes.get(4).Police_Infringement__c = true;
		outcomes.get(5).Police_Infringement__c = true;

		insert outcomes;

		List<Outcome__c> results = [Select Id, Infringement_Auto_Number__c, Infringement_Suffix__c, RecordType.Name
										From Outcome__c
										Order by Infringement_Auto_Number__c];

		System.assertEquals(6, results.size());

		System.assertEquals(null, results.get(0).Infringement_Auto_Number__c);
		System.assertEquals('Bus Safety Alert', results.get(0).RecordType.Name);

		System.assertEquals(2000, results.get(1).Infringement_Auto_Number__c);
		System.assertEquals('TS00', results.get(1).Infringement_Suffix__c);
		System.assertEquals('Bus - Infringement', results.get(1).RecordType.Name);

		System.assertEquals(2002, results.get(3).Infringement_Auto_Number__c);
		System.assertEquals('VI02', results.get(3).Infringement_Suffix__c);
		System.assertEquals('Maritime - Infringement Notice', results.get(3).RecordType.Name);
	}



	@isTest
	static void testUpdateOutcome() {

		OutcomeHandler.checksumfactoryInstance = new MockChecksumGeneratorFactory();
		OutcomeHandler.infNumberGeneratorInstance = new MockInfNumberGenerator();


		List<Outcome__c> outcomes = TestDataFactory.createOutcome('Bus - Infringement', 2);
		outcomes.add(TestDataFactory.createOutcome('Bus Safety Alert'));
		outcomes.addAll(TestDataFactory.createOutcome('Maritime - Infringement Notice',3));
		outcomes.get(3).Police_Infringement__c = true;
		outcomes.get(4).Police_Infringement__c = true;
		outcomes.get(5).Police_Infringement__c = true;

		insert outcomes;

		TriggerSettingsClass.testOverride = null;

		outcomes.get(0).Infringement_Auto_Number__c = 3456;
		outcomes.get(3).recordTypeId = Constants.BUS_INFRINGEMENT_RECORDTYPE_ID;
		outcomes.get(3).Police_Infringement__c = false;
		outcomes.get(4).Police_Infringement__c = false;

		update new List<Outcome__c>{outcomes.get(0), outcomes.get(3), outcomes.get(4)};

		List<Outcome__c> results = [Select Id, Infringement_Auto_Number__c, Infringement_Suffix__c, RecordType.Name
										From Outcome__c
										Order by Infringement_Auto_Number__c];

		System.assertEquals(6, results.size());

		System.assertEquals(null, results.get(0).Infringement_Auto_Number__c);
		System.assertEquals('Bus Safety Alert', results.get(0).RecordType.Name);

		System.assertEquals(3456, results.get(5).Infringement_Auto_Number__c);
		System.assertEquals('TS56', results.get(5).Infringement_Suffix__c);
		System.assertEquals('Bus - Infringement', results.get(5).RecordType.Name);

		System.assertEquals(2002, results.get(2).Infringement_Auto_Number__c);
		System.assertEquals('TS02', results.get(2).Infringement_Suffix__c);
		System.assertEquals('Bus - Infringement', results.get(2).RecordType.Name);

		System.assertEquals(2003, results.get(3).Infringement_Auto_Number__c);
		System.assertEquals('TS03', results.get(3).Infringement_Suffix__c);
		System.assertEquals('Maritime - Infringement Notice', results.get(3).RecordType.Name);

	}


	@isTest
	static void testAfterUpdateOutcome()
	{
		Date dateOfSignature = System.today();

		// Create and populate Bus Safety Alert Outcome Record
		Outcome__c outcome = TestDataFactory.createOutcome('Bus Safety Alert');
		outcome.Status__c = 'Unpublished';
		outcome.Title__c = 'Title';
		outcome.Content__c = null;
		outcome.Date_of_Signature__c = dateOfSignature;
		
		// Insert Outcome record
		insert outcome;

		// Update status on Outcome Record to published
		outcome.Status__c = 'Published';
		
		// Start Test
		Test.startTest();

		// Update Outcome Record
		update outcome;

		// End Test
		Test.stopTest();

		List <Outcome__c> results = [SELECT Id, RecordType.Name, Status__c, Title__c, Content__c, Date_of_Signature__c FROM Outcome__c WHERE Id = :outcome.Id];
		
		System.assertEquals('Published', results.get(0).Status__c);
		System.assertEquals('Title', results.get(0).Title__c);
		System.assertEquals(null, results.get(0).Content__c);
		System.assertEquals(dateOfSignature, results.get(0).Date_of_Signature__c);
	}

	@isTest
	static void testUpdateLicenseConditions()
	{
		Id recordTypeId;

		// Create Account
		recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();
		Account account = new Account();
		account.RecordTypeId = recordTypeId;
		account.FirstName = 'Test';
		account.LastName = 'User';
		insert account;
		
		// Create Accreditation
		recordTypeId = Schema.SObjectType.Accreditation__c.getRecordTypeInfosByDeveloperName().get('Recreational_Boat_User').getRecordTypeId();
		Accreditation__c accreditation = TestDataFactory.createAccreditation(account, recordTypeId, 'General', 'Current');
		insert accreditation;
		
		// Create Attributes
		Attribute__c attribute;
		recordTypeId = Schema.SObjectType.Attribute__c.getRecordTypeInfosByDeveloperName().get('License_Condition').getRecordTypeId();
		
		attribute = new Attribute__c();
		attribute.RecordTypeId = recordTypeId;
		attribute.Credential__c = accreditation.Id;
		attribute.VR_Reason_Code__c = 'CL';
		insert attribute;

		attribute = new Attribute__c();
		attribute.RecordTypeId = recordTypeId;
		attribute.Credential__c = accreditation.Id;
		attribute.VR_Reason_Code__c = 'OA';
		insert attribute;

		attribute = new Attribute__c();
		attribute.RecordTypeId = recordTypeId;
		attribute.Credential__c = accreditation.Id;
		attribute.VR_Reason_Code__c = 'XX';
		attribute.VR_Effective_To__c = Date.newInstance(2000, 1, 1);
		insert attribute;

		recordTypeId = Schema.SObjectType.Involved_Party__c.getRecordTypeInfosByDeveloperName().get('Vessel_Participant').getRecordTypeId();
		Involved_Party__c ip = new Involved_Party__c();
		ip.RecordTypeId = recordTypeId;
		ip.Person__c = account.Id;
		insert ip;


		recordTypeId = Schema.SObjectType.Outcome__c.getRecordTypeInfosByDeveloperName().get('Infringement_Notice').getRecordTypeId();
		Outcome__c outcome = new Outcome__c();
		outcome.RecordTypeId = recordTypeId;
		outcome.Account__c = account.Id;
		outcome.Licence__c = accreditation.Id;
		outcome.Involved_Person__c = ip.Id;
		insert outcome;

		List <Outcome__c> outcomes = [SELECT Id, Licence_Conditions__c FROM Outcome__c WHERE Id = :outcome.Id];
		
		System.assertEquals(outcomes.size(), 1);
		
		outcome = outcomes.get(0);

		System.assertEquals('CL, OA', outcome.Licence_Conditions__c);
	}
}