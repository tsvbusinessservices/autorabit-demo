public interface PointInPolygonService {

	Map<String, Object> checkPointInPolygon(Map<String, Object> options);
}