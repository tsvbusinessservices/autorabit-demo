@isTest
private class LocationSharingRecalcBatchTests {
	
	
	@isTest static void test_Scedhuling() {
		LocationSharingRecalcBatch batch = new LocationSharingRecalcBatch();
		String sch = '0 0 10 * * ?';
		system.schedule('Recalc test 12345', sch, batch);
	}

	//P - Run Recalc job - locations
	@isTest static void PositiveTest_RunRecalcJob() {
		Id portalAccountId1 = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;
		list<Location__c> locations =  TestDataFactory.createMultipleLocation(200, portalAccountId1);
		insert locations;
		map<Id, Location__c> locationsMap = new map<Id, Location__c>(locations);

		test.startTest();
		
		LocationSharingRecalcBatch b = new LocationSharingRecalcBatch();
		database.executeBatch(b, 200);

		test.stopTest();

		//check that there has been a sharing record created
		List<Location__Share> locSharing = LocationHelper.getLocSharing(locationsMap.keySet(), new set<String>{Location__Share.RowCause.Waterway_Manager__c});
		system.assertEquals(200, locSharing.size());
	}

	@testSetup static void testSetup(){
		TestDataFactory.createPartnerAccount();
	}
}