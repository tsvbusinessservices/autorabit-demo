public interface ChecksumGeneratorFactory {
	
	ChecksumGenerator getChecksumGenerator(String algorithm);
}