/**
 * Compliance Activity Default User Mapping
 *
 * @author P.Victoriano
 * @date 16.May.2018
 */
public class ComplianceActivityCC {
	/**
	 * Updates or Creates default weather and Temperature settings
	 * @param compAct [description]
	 */
	@AuraEnabled
	public static void setUserComplianceActivityDefault(ComplianceActivityCS__c compAct) {
		Id userId = UserInfo.getUserId();
		ComplianceActivityCS__c userComplActivity = compAct;
		ComplianceActivityCS__c existingComplActivity = retrieveUserComplianceActivityDefault();
		userComplActivity.SetupOwnerId = userId;
		if (String.isNotBlank(existingComplActivity.Id)) {
			userComplActivity.Id = existingComplActivity.Id;
		}
		upsert userComplActivity;
	}
	/**
	 * Retrieves default weather and Temperature settings
	 * @return [Compliance Activity CS]
	 */
	@AuraEnabled
	public static ComplianceActivityCS__c retrieveUserComplianceActivityDefault() {
		Id userId = UserInfo.getUserId();
		ComplianceActivityCS__c userComplActivity = new ComplianceActivityCS__c();
		ComplianceActivityCS__c[] complianceActivities = new List<ComplianceActivityCS__c>();
		complianceActivities = [SELECT Id, SetupOwnerId,
		                        Temperature__c, Water_Conditions__c, Weather__c, Wind_Direction__c, Wind_Speed__c
		                        FROM ComplianceActivityCS__c WHERE SetupOwnerId = :userId LIMIT 1];

		if (!complianceActivities.isEmpty()) {
			userComplActivity = complianceActivities.get(0);
		}

		return userComplActivity;
	}
}