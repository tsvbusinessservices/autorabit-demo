public with sharing class WaterwayRoleHelper {

    /**
    * Gets Information to supplement the Waterway Roles using a query for Batch
    * @return null
    */
    public static string getWaterwayRoleForSharingRecalc(){
        return 'SELECT Id, Waterway__c, RecordTypeId FROM Waterway_Role__c WHERE RecordTypeId = \''+ Constants.WATERWAY_ROLE_RT_ID_WATERWAY_MANAGER + '\' AND Waterway__c != NULL ORDER BY Waterway__c ASC';
    }

    //get a map of all facilities under a waterway
    public static Map<Id, List<Location__c>> mapWaterwayIdsWithFacilities(List<Location__c> facilityList){
        Map<Id, List<Location__c>> mapOfWaterwayIdsToFacilities = new Map<Id, List<Location__c>>();

        for(Location__c facility : facilityList){

			//If the contactId already exists in the map then add to the permission set name list
			if(mapOfWaterwayIdsToFacilities.ContainsKey(facility.Waterway_Name__c)){
				List<Location__c> facilities = mapOfWaterwayIdsToFacilities.get(facility.Waterway_Name__c);
				facilities.add(facility);
			}
			//else create new permission set name list
			else{
				mapOfWaterwayIdsToFacilities.put(facility.Waterway_Name__c, new List<Location__c>{facility});
			}
		
        }

        return mapOfWaterwayIdsToFacilities;
    }

    
    //get a map of all facilities under a waterway
    public static Map<Id, List<Waterway_Role__c>> mapWaterwayIdsWithWaterwayRoles(Set<Id> waterwayIds, List<Waterway_Role__c> waterwayRoles){
        Map<Id, List<Waterway_Role__c>> mapOfWaterwayIdsToWaterwayRoles = new Map<Id, List<Waterway_Role__c>>();

        for(Id waterwayId : waterwayIds){
            List<Waterway_Role__c> waterRoleList = new List<Waterway_Role__c>();
            for(Waterway_Role__c waterwayRole : waterwayRoles){
                if(waterwayRole.Waterway__c == waterwayId){
                    waterRoleList.add(waterwayRole);
                }
            }
            mapOfWaterwayIdsToWaterwayRoles.put(waterwayId,waterRoleList);
        }

        return mapOfWaterwayIdsToWaterwayRoles;
    }
}