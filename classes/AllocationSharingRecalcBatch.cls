global class AllocationSharingRecalcBatch implements Database.Batchable<sObject>, Schedulable {
    global AllocationSharingRecalcBatch() {

    }
     
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(AllocationHelper.getAllocationForSharingRecalc());
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        AllocationHandler.allocationSharing(scope, null, 'Recalculation');
    }
    
    global void finish(Database.BatchableContext BC) {
        //Send email
        String emailSubject = 'SUCCESS: Apex Sharing Recalculation For Allocation Completed';
        String emailBody = 'The Apex sharing recalculation finished processing for the Allocation Object';
        Utility.sendEmail(emailSubject, emailBody, 'Success');
    }

    global void execute(SchedulableContext sc) {
        AllocationSharingRecalcBatch b = new AllocationSharingRecalcBatch();
        database.executeBatch(b, 200);
    }

}