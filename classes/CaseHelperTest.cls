@isTest
private class CaseHelperTest
{
	@isTest
	static void test_isMarineIncident() {

		Case miCase = TestDataFactory.createCase(Constants.MARINE_INCIDENT_RECORDTYPE_ID, null);
		Case biCase = TestDataFactory.createCase(Schema.SObjectType.Case.getRecordTypeInfosByName().get('Bus - Incident').getRecordTypeId(), null);

		System.assert(CaseHelper.isMarineIncident(miCase));
		System.assert(!CaseHelper.isMarineIncident(biCase));


	}

	@isTest
	static void test_isLocationChanged() {

		Case newCase = new Case(MALatitude__c = 10.2,
									MALongitude__c = 20.2);
		Case oldCase = new Case(MALatitude__c = 4.2,
									MALongitude__c = 20.2);
		

		System.assert(CaseHelper.isLocationChanged(newCase, oldCase));

		oldCase.MALatitude__c = 10.2;

		System.assert(!CaseHelper.isLocationChanged(newCase, oldCase));

	}

}