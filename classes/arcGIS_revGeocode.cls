public class arcGIS_revGeocode  {


        @InvocableMethod(label='Obtain address' description='Performs a reverse geocode on lat and long to retrieve the address')
        public static List<String> reverseGeocode(List<ID> ids) { 
 
       
    //Set variables for future use    
    List<String> addresses = new List<String>();   
        String oName = null;
        String locField = null;
        String locDesc = null;
        String locationId = null; 
        String arcLat = null;
        String arcLong = null;
        String endpoint = null;

        
     //   SObject geoObject = null;
     List<SObject> geoObject = new List<SObject>();
                
     //Prepare config for reverseGeocode HTTPCallout

        String format='json';
        String featureTypes='PointAddress';
        String RevGeocodeURL ='https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/reverseGeocode?';        
           System.debug('###Address Retrieval###');

        ///Loop through records passed and retrieve the address                     
        for (String recordIds :ids) {
        
           Id recordId = recordIds;
             oName = String.valueOf(recordId.getsobjecttype());            
         //determine location fields
            IF (oName == 'compliance_activity__c') {
                locField = 'Waterway__c';                    
                locDesc = 'Location_Description__c';
                geoObject = [SELECT Id, MALatitude__c, MALongitude__c, Waterway__c, Location_Description__c FROM compliance_activity__c WHERE Id IN :ids];    
                
            } ELSE IF (oName == 'case') {
                locField = 'Location__c';                    
                locDesc = 'Location_Description__c';  
                geoObject = [SELECT Id, MALatitude__c, MALongitude__c, Location__c, Location_Description__c FROM case WHERE Id IN :ids];  
            } else if (oName == 'asset__c') {
                locField = 'Location__c';                    
                locDesc = 'Location_Description__c';  
                geoObject = [SELECT Id, MALatitude__c, MALongitude__c, Location__c, Location_Description__c FROM asset__c WHERE Id IN :ids]; 

            } else {
                //Do nothing
                    System.debug('####Object not configured for reverse geoCode###');
                return Null;
            }        
    
        for (SObject geoRecord :geoObject) {
             //Message to log that a record was retrieved
                    System.debug(geoRecord);
                
             //determine if the location ID has been set
                //locationId = String.valueOf(geoObject.get(locField));
                locationId = String.valueOf(geoRecord.get(locField));
                   System.debug('#Location specified:'+ locationId);
               
            //If location not set continue to retrieve revGeocode, otherwise do nothing
                IF (locationId == null) {
                //Retrieve lat/long from Sobject
                arcLat = String.valueOf(geoRecord.get('MALatitude__c')); //arcLat='-37.81508849838504';
                arcLong = String.valueOf(geoRecord.get('MALongitude__c')); //arcLong='144.97439002710632';
  
                //Now retrieve the address
                // To return correct coordinates with ArcGIS World, X = Long and Y = Lat
                endpoint = RevGeocodeURL+'f='+format+'&featureTypes='+featureTypes+'&location='+arcLong+','+arcLat;
                    
                    System.debug('Endpoint set to: '+endpoint );
                // Send the HTTP request.
                httpCallout.getHttpResponse(String.valueOf(geoRecord.get('Id')), oname, 'arcRequest','revGeocode', endpoint);
                       
                    System.debug('###record passed to update in future action###');
                    return null;
                 } ELSE {           
                        System.debug('###No action taken###');
                        return null;
                 }   
          }
          }  
             System.debug('###Finish');       
             return null;
   
      }   

public static void writeAddress(Id recordIds, String oname, String address){
    
     string oldDesc = null;

     SObject locRecord = Null;
            IF (oName == 'compliance_activity__c') {
                locRecord = [SELECT Id, Location_Description__c FROM compliance_activity__c WHERE Id = :recordIds];                 
            } ELSE IF (oName == 'case') {
                locRecord = [SELECT Id, Location_Description__c FROM case WHERE Id =:recordIds];  
            } else if (oName == 'asset__c') {
                locRecord = [SELECT Id, Location_Description__c FROM asset__c WHERE Id =:recordIds]; 
            } else {
                //Do nothing
                    System.debug('####Object not configured for reverse geoCode###');
            }
            
         //assign address to field
            oldDesc =  (String)locRecord.put('Location_Description__c',address);
            //locRecord.Location_Description__c = address;
            update locRecord;

}
 
 }