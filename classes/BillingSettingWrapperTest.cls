@isTest
private class BillingSettingWrapperTest
{
    @isTest
    static void testGetInstance() {

        System.assertNotEquals(null, BillingSettingWrapper.getInstance(Constants.BILLING_SETTINGS_BUS));
        System.assertNotEquals(null, BillingSettingWrapper.getInstance(Constants.BILLING_SETTINGS_BUS_APPLICATION));
        System.assertEquals(null, BillingSettingWrapper.getInstance('xxx'));
       
    }

    @isTest
    static void testGetters() {
        String AnnualSetting = Constants.BILLING_SETTINGS_BUS;
        
        Billing_Setting__mdt setting = [SELECT DeveloperName, Charge_Code__c, Comments__c,
                                            Tax_Code__c, Tax_Code_Percentage__c,
                                            Term_Days__c, Term_Name__c
                                         FROM Billing_Setting__mdt 
                                         WHERE DeveloperName = :AnnualSetting  Limit 1];

                                         
        BillingSettingWrapper instance = BillingSettingWrapper.getInstance(Constants.BILLING_SETTINGS_BUS);
        System.assertEquals(setting.Charge_Code__c, instance.getChargeCode());
        System.assertEquals(setting.Comments__c, instance.getComments());
        System.assertEquals(setting.Tax_Code__c, instance.getTaxCode());
        System.assertEquals(setting.Tax_Code_Percentage__c, instance.getTaxPercentage());        
        System.assertEquals(setting.Term_Days__c, instance.getTermDays());
        System.assertEquals(setting.Term_Name__c, instance.getTermName());


        String ApplicationSetting = Constants.BILLING_SETTINGS_BUS_APPLICATION;  
        
        Billing_Setting__mdt Appsetting = [SELECT DeveloperName, Charge_Code__c, Comments__c,
                                            Tax_Code__c, Tax_Code_Percentage__c,
                                            Term_Days__c, Term_Name__c
                                         FROM Billing_Setting__mdt 
                                         WHERE DeveloperName = :ApplicationSetting Limit 1];

                                         
        BillingSettingWrapper Appinstance = BillingSettingWrapper.getInstance(Constants.BILLING_SETTINGS_BUS_APPLICATION);
        System.assertEquals(Appsetting.Charge_Code__c, Appinstance.getChargeCode());
        System.assertEquals(Appsetting.Comments__c, Appinstance.getComments());
        System.assertEquals(Appsetting.Tax_Code__c, Appinstance.getTaxCode());
        System.assertEquals(Appsetting.Tax_Code_Percentage__c, Appinstance.getTaxPercentage());        
        System.assertEquals(Appsetting.Term_Days__c, Appinstance.getTermDays());
        System.assertEquals(Appsetting.Term_Name__c, Appinstance.getTermName());           
    }
}