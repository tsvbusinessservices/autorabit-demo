public interface InfringementNumberGenerator {

	Long getNewInfringementNumber();
}