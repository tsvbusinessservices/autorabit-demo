global class BusAnnualFeeScheduler implements Schedulable {
	global void execute(SchedulableContext sc) {
		BusAnnualFeeRenewalBatch b = new BusAnnualFeeRenewalBatch();
		database.executebatch(b);
	}
}