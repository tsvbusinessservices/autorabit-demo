global class LocationSharingRecalcBatch implements Database.Batchable<sObject>, Schedulable {
	global LocationSharingRecalcBatch() {
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(LocationHelper.getLocationForSharingRecalc());
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		LocationHandler.locationSharing(scope, null, 'Recalculation');
	}
	
	global void finish(Database.BatchableContext BC) {
		//Send email
		String emailSubject = 'SUCCESS: Apex Sharing Recalculation For Location Completed';
		String emailBody = 'The Apex sharing recalculation finished processing for the Location Object';
		Utility.sendEmail(emailSubject, emailBody, 'Success');
	}

	global void execute(SchedulableContext sc) {
		LocationSharingRecalcBatch b = new LocationSharingRecalcBatch();
		database.executeBatch(b, 200);
	}
	
}