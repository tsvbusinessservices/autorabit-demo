public class ComplianceActivitySharingHandler extends TriggerBase implements ITriggerBase{
	/**
	 * afterInsert Trigger Event
	 * @return null
	 */
	public override void afterInsert() {
		complianceActivitySharing(records, null, 'Insert');
	}
	/**
	 * afterUpdate Trigger Event	
	 * @return null
	 */
	public override void afterUpdate() {
		complianceActivitySharing(records, oldmap, 'Update');
	}

	/**
	 * Controlls the Apex sharing against the Compliance Activity Object	
	 * @param mode = 'Insert', 'Update', 'Recalculation'
	 * @return null
	 */
	public static void complianceActivitySharing(list<SObject> records, Map<Id, SObject> oldMap, String mode){
		//Get an instance of the SharingHelper class with the Object Name
		SharingHelper SharingHelp = new SharingHelper(Compliance_Activity__Share.sObjectType.getDescribe().getName());

		//get a set of compliance activity Ids by casting to map and getting keys
		set<Id> theCAIds = new map<Id, Compliance_Activity__c>((list<Compliance_Activity__c>) records).keySet();

		//loop round and add the Id and the Portal account Id
		for(Compliance_Activity__c complianceActivity : ComplianceActivityHelper.getEnrichedData(theCAIds)){
			
			//get old Compliance Activity - only applicable if running in Update mode
			Compliance_Activity__c oldComplianceActivity = null;
			if(mode == 'Update' && oldMap != null){
				oldComplianceActivity = (Compliance_Activity__c) oldMap.get(complianceActivity.Id);
			}
			
			//if new or changed or Recalculate then add the new Account to the sharing
			if(mode == 'Insert' || mode == 'Recalculation' || complianceActivity.Bus_Operator__c != oldComplianceActivity.Bus_Operator__c ){
				// check if sharing is required
				StatusAwareShare shareRecord = ComplianceActivityHelper.getShareRecord(complianceActivity);
				
				if(shareRecord != null && shareRecord.canShare(complianceActivity.Status__c)) {
					SharingHelp.addToSharing(complianceActivity.Bus_Operator__c, complianceActivity.Id, Schema.Compliance_Activity__Share.RowCause.Operator__c, 'Edit', shareRecord.shareType);
				}

				//if changed OR Recalculation mode than delete the old one
				if(mode == 'Recalculation' 
					|| (mode == 'Update' && complianceActivity.Bus_Operator__c != oldComplianceActivity.Bus_Operator__c)){
					SharingHelp.addToDeleteList(complianceActivity.Id);
				}
			}
		}

		//delete any sharing rules that need to be deleted
		SharingHelp.deleteSharingRules(new set<String> {Schema.Compliance_Activity__Share.RowCause.Operator__c});

		//Insert Sharing rules asyncronously
		SharingHelp.insertApexSharing_Asyncronously();
	}
}