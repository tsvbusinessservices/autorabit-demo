@isTest (seeAllData = false)
private class SharingHelperTest {
    
    //Single Asset - positive with create and delete in correct order
    @isTest 
    static void PositiveTest_AddingAndDeletingSharingRule() {
        //insert Asset - no Account so no apex sharing created
        Asset__c testAsset = TestDataFactory.createAsset();
        insert testAsset;

        //Get the Partner Account Id
        Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;

        test.startTest();
        //Create instance of SharingHelper Class
        SharingHelper SharingHelp = new SharingHelper(Asset__Share.sObjectType.getDescribe().getName());

        //Add a sharing rule
        SharingHelp.addToSharing(portalAccountId, testAsset.Id, Schema.Asset__Share.RowCause.SAR_Operator__c, 'Edit');

        //Insert Sharing rules
        SharingHelp.insertApexSharing_Asyncronously();

        test.stopTest();

        //Check inserted
        List<sobject> sobjSharing = SharingHelp.queryRowCauseSharing(new set<Id>{testAsset.Id}, new set<String>{Schema.Asset__Share.RowCause.SAR_Operator__c});
        List<Asset__Share> AssetSharing = (List<Asset__Share>) sobjSharing;
        system.assertEquals(1, AssetSharing.size());
        
        //add to deleted 
        SharingHelp.addToDeleteList(testAsset.Id);
        
        //delete
        SharingHelp.deleteSharingRules(new set<String> {Schema.Asset__Share.RowCause.SAR_Operator__c});
        
        //Check deleted
        List<sobject> sobjSharingAfter = SharingHelp.queryRowCauseSharing(new set<Id>{testAsset.Id}, new set<String>{Schema.Asset__Share.RowCause.SAR_Operator__c});
        List<Asset__Share> AssetSharingAfter = (List<Asset__Share>) sobjSharingAfter;
        system.assertEquals(0, AssetSharingAfter.size());
    }

    /* Depricating as addToSharingUsingACR is not used from Assets Triggers
    //Single Asset - positive with create and delete in correct order
    @isTest
    static void PositiveTest_AddingAndDeletingSharingRuleWithoutACR() {
        //insert Asset - no Account so no apex sharing created
        Asset__c testAsset = TestDataFactory.createAsset();
        insert testAsset;

        //Get the Partner Account Id
        Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;

        test.startTest();
        //Create instance of SharingHelper Class
        SharingHelper SharingHelp = new SharingHelper(Asset__Share.sObjectType.getDescribe().getName());

        //Add a sharing rule
        SharingHelp.addToSharingUsingACR(portalAccountId, testAsset.Id, Schema.Asset__Share.RowCause.SAR_Operator__c, 'Edit');

        //Insert Sharing rules
        SharingHelp.insertApexSharing_Asyncronously();

        test.stopTest();

        //Check inserted
        List<sobject> sobjSharing = SharingHelp.queryRowCauseSharing(new set<Id>{testAsset.Id}, new set<String>{Schema.Asset__Share.RowCause.SAR_Operator__c});
        List<Asset__Share> AssetSharing = (List<Asset__Share>) sobjSharing;
        system.assertEquals(0, AssetSharing.size(), AssetSharing);
        
        //add to deleted 
        SharingHelp.addToDeleteList(testAsset.Id);
        
        //delete
        SharingHelp.deleteSharingRules(new set<String> {Schema.Asset__Share.RowCause.SAR_Operator__c});
        
        //Check deleted
        List<sobject> sobjSharingAfter = SharingHelp.queryRowCauseSharing(new set<Id>{testAsset.Id}, new set<String>{Schema.Asset__Share.RowCause.SAR_Operator__c});
        List<Asset__Share> AssetSharingAfter = (List<Asset__Share>) sobjSharingAfter;
        system.assertEquals(0, AssetSharingAfter.size());
    }*/

    /* Depricating as addToSharingUsingACR is not used from Assets Triggers
    //Single Asset - positive with create and delete in correct order
    @isTest 
    static void PositiveTest_AddingAndDeletingSharingRuleWithACR() {
        //insert Asset - no Account so no apex sharing created
        Asset__c testAsset = TestDataFactory.createAsset();
        insert testAsset;

        //Get the Partner Account Id
        Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;

        ID portalContactId = [Select Id From Contact Where AccountId =: portalAccountId].Id;

        AccountContactRelation relation = [Select Id From AccountContactRelation Where AccountId =: portalAccountId AND ContactId =: portalContactId Limit 1];
        relation.Full_Access__c = true;
        update relation;



        test.startTest();
        //Create instance of SharingHelper Class
        SharingHelper SharingHelp = new SharingHelper(Asset__Share.sObjectType.getDescribe().getName());

        //Add a sharing rule
        SharingHelp.addToSharingUsingACR(portalAccountId, testAsset.Id, Schema.Asset__Share.RowCause.SAR_Operator__c, 'Edit');

        //Insert Sharing rules
        SharingHelp.insertApexSharing_Asyncronously();

        test.stopTest();

        //Check inserted
        List<sobject> sobjSharing = SharingHelp.queryRowCauseSharing(new set<Id>{testAsset.Id}, new set<String>{Schema.Asset__Share.RowCause.SAR_Operator__c});
        List<Asset__Share> AssetSharing = (List<Asset__Share>) sobjSharing;
        system.assertEquals(1, AssetSharing.size());
        
        //add to deleted 
        SharingHelp.addToDeleteList(testAsset.Id);
        
        //delete
        SharingHelp.deleteSharingRules(new set<String> {Schema.Asset__Share.RowCause.SAR_Operator__c});
        
        //Check deleted
        List<sobject> sobjSharingAfter = SharingHelp.queryRowCauseSharing(new set<Id>{testAsset.Id}, new set<String>{Schema.Asset__Share.RowCause.SAR_Operator__c});
        List<Asset__Share> AssetSharingAfter = (List<Asset__Share>) sobjSharingAfter;
        system.assertEquals(0, AssetSharingAfter.size());
    }*/

    //AccountContactRelationship - positive with full access
    @isTest 
    static void PositiveTest_UpdatingACRWithFullAccess() {

        //Get the Partner Account Id
        Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;

        ID portalContactId = [Select Id From Contact Where AccountId =: portalAccountId].Id;

        AccountContactRelation relation = [Select Id From AccountContactRelation Where AccountId =: portalAccountId AND ContactId =: portalContactId Limit 1];
        relation.Full_Access__c = true;
        update relation;

        test.startTest();
        //Create instance of SharingHelper Class
        SharingHelper SharingHelp = new SharingHelper(AccountShare.sObjectType.getDescribe().getName());

        //Add a sharing rule
        SharingHelp.addToSharingUsingACR(portalAccountId, portalAccountId, 'Manual', 'Edit');

        //Insert Sharing rules
        SharingHelp.insertApexSharing_Asyncronously();

        test.stopTest();

        //Check inserted
        List<sobject> sobjSharing = SharingHelp.queryRowCauseSharing(new set<Id>{portalAccountId}, new set<String>{'Manual'});
        List<AccountShare> AccountSharing = (List<AccountShare>) sobjSharing;
        system.assertEquals(1, AccountSharing.size());
        
        //add to deleted 
        SharingHelp.addToDeleteList(portalAccountId);
        
        //delete
        SharingHelp.deleteSharingRules(new set<String> {'Manual'});
        
        //Check deleted
        List<sobject> sobjSharingAfter = SharingHelp.queryRowCauseSharing(new set<Id>{portalAccountId}, new set<String>{'Manual'});
        List<AccountShare> AccountSharingAfter = (List<AccountShare>) sobjSharingAfter;
        system.assertEquals(0, AccountSharingAfter.size());
    }

    //AccountContactRelationship - positive with full access
    @isTest 
    static void NegativeTest_UpdatingACRWithoutFullAccess() {

        //Get the Partner Account Id
        Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;

        ID portalContactId = [Select Id From Contact Where AccountId =: portalAccountId].Id;

        AccountContactRelation relation = [Select Id From AccountContactRelation Where AccountId =: portalAccountId AND ContactId =: portalContactId Limit 1];
        relation.Full_Access__c = false;
        update relation;

        test.startTest();
        //Create instance of SharingHelper Class
        SharingHelper SharingHelp = new SharingHelper(AccountShare.sObjectType.getDescribe().getName());

        //Add a sharing rule
        SharingHelp.addToSharingUsingACR(portalAccountId, portalAccountId, 'Manual', 'Edit');

        //Insert Sharing rules
        SharingHelp.insertApexSharing_Asyncronously();

        test.stopTest();

        //Check inserted
        List<sobject> sobjSharing = SharingHelp.queryRowCauseSharing(new set<Id>{portalAccountId}, new set<String>{'Manual'});
        List<AccountShare> AccountSharing = (List<AccountShare>) sobjSharing;
        system.assertEquals(0, AccountSharing.size());
        
        //add to deleted 
        SharingHelp.addToDeleteList(portalAccountId);
        
        //delete
        SharingHelp.deleteSharingRules(new set<String> {'Manual'});
        
        //Check deleted
        List<sobject> sobjSharingAfter = SharingHelp.queryRowCauseSharing(new set<Id>{portalAccountId}, new set<String>{'Manual'});
        List<AccountShare> AccountSharingAfter = (List<AccountShare>) sobjSharingAfter;
        system.assertEquals(0, AccountSharingAfter.size());
    }


    //Test sharing on Non Portal account
    @isTest
    static void NegativeTest_CreatingAgainstNonPortalAccount() {
        //insert Asset - no Account so no apex sharing created
        Asset__c testAsset = TestDataFactory.createAsset();
        insert testAsset;

        //insert non portal account
        Account nonPortalAccount = TestDataFactory.createAccount('Non Portal');
        insert nonPortalAccount;

        test.startTest();
        //Create instance of SharingHelper Class
        SharingHelper SharingHelp = new SharingHelper(Asset__Share.sObjectType.getDescribe().getName());

        //Add a sharing rule
        SharingHelp.addToSharing(nonPortalAccount.Id, testAsset.Id, Schema.Asset__Share.RowCause.SAR_Operator__c, 'Edit');

        //Insert Sharing rules
        SharingHelp.insertApexSharing_Asyncronously();

        test.stopTest();    

        //Check NOTHING inserted
        List<sobject> sobjSharing = SharingHelp.queryRowCauseSharing(new set<Id>{testAsset.Id}, new set<String>{Schema.Asset__Share.RowCause.SAR_Operator__c});
        List<Asset__Share> AssetSharing = (List<Asset__Share>) sobjSharing;
        system.assertEquals(0, AssetSharing.size());
    }

    //test inserting 200 assetSharing rules - All portal Accounts
    @isTest
    static void PositiveTest_AddMultipleSharingRule() {
        //insert Asset - no Account so no apex sharing created
        list<Asset__c> testAssets = TestDataFactory.createAssets(200);
        insert testAssets;

        //cast into map for use later
        map<Id, Asset__c> testAssetMap = new map<Id, Asset__c>(testAssets); 

        //Get the Partner Account Id
        Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;

        test.startTest();
        //Create instance of SharingHelper Class
        SharingHelper SharingHelp = new SharingHelper(Asset__Share.sObjectType.getDescribe().getName());

        //Add a sharing rule to each of the assets
        for(Asset__c A : testAssets){
            SharingHelp.addToSharing(portalAccountId, A.Id, Schema.Asset__Share.RowCause.SAR_Operator__c, 'Edit');
        }

        //Insert Sharing rules
        SharingHelp.insertApexSharing_Syncronously();   //Use Syncronously 

        test.stopTest();

        //Check inserted 200
        List<sobject> sobjSharing = SharingHelp.queryRowCauseSharing(testAssetMap.keySet(), new set<String>{Schema.Asset__Share.RowCause.SAR_Operator__c});
        List<Asset__Share> AssetSharing = (List<Asset__Share>) sobjSharing;
        system.assertEquals(200, AssetSharing.size());
    }

    //test inserting 200 assetSharing rules - 100 portal 100 regular
    @isTest
    static void NegativeTest_AddMultipleSharingRuleMixed() {
        //insert Asset - no Account so no apex sharing created
        list<Asset__c> testAssets = TestDataFactory.createAssets(200);
        insert testAssets;

        //cast into map for use later
        map<Id, Asset__c> testAssetMap = new map<Id, Asset__c>(testAssets); 

        //Get the Partner Account Id
        Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;

        //insert non portal account
        Account nonPortalAccount = TestDataFactory.createAccount('Non Portal');
        insert nonPortalAccount;

        test.startTest();
        //Create instance of SharingHelper Class
        SharingHelper SharingHelp = new SharingHelper(Asset__Share.sObjectType.getDescribe().getName());

        //Add a sharing rule to each of the assets
        Integer i = 0;
        for(Asset__c A : testAssets){
            Id accountId;
            if(i >= 100){
                accountId = portalAccountId;
            }
            else{
                accountId = nonPortalAccount.Id;
            }
            SharingHelp.addToSharing(accountId, A.Id, Schema.Asset__Share.RowCause.SAR_Operator__c, 'Edit');
            i++;
        }

        //Insert Sharing rules
        SharingHelp.insertApexSharing_Asyncronously();

        test.stopTest();

        //Check inserted 100 - half were normal accounts with no sharing group
        List<sobject> sobjSharing = SharingHelp.queryRowCauseSharing(testAssetMap.keySet(), new set<String>{Schema.Asset__Share.RowCause.SAR_Operator__c});
        List<Asset__Share> AssetSharing = (List<Asset__Share>) sobjSharing;
        system.assertEquals(100, AssetSharing.size());
    }


    //Single Asset - delete without creating any sharing rules - check does not cause error
    @isTest
    static void NegativeTest_DeleteBeforeAddingAnySharing() {
        //insert Asset - no Account so no apex sharing created
        Asset__c testAsset = TestDataFactory.createAsset();
        insert testAsset;

        //Get the Partner Account Id
        Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;

        test.startTest();
        //Create instance of SharingHelper Class
        SharingHelper SharingHelp = new SharingHelper(Asset__Share.sObjectType.getDescribe().getName());

        //add to deleted 
        SharingHelp.addToDeleteList(testAsset.Id);
        
        //delete
        SharingHelp.deleteSharingRules(new set<String> {Schema.Asset__Share.RowCause.SAR_Operator__c});

        test.stopTest();

        //Check NOTHING inserted
        List<sobject> sobjSharing = SharingHelp.queryRowCauseSharing(new set<Id>{testAsset.Id}, new set<String>{Schema.Asset__Share.RowCause.SAR_Operator__c});
        List<Asset__Share> AssetSharing = (List<Asset__Share>) sobjSharing;
        system.assertEquals(0, AssetSharing.size());
    }


    //Single Asset - add a manual sharing rule and check it does not get removed when the delete job runs
    @isTest
    static void PositiveTest_CheckManualSharingRemains() {
        //insert Asset - no Account so no apex sharing created
        Asset__c testAsset = TestDataFactory.createAsset();
        insert testAsset;

        //Get the Portal UserId
        /*Id portalUserId = [SELECT Id FROM User WHERE ContactId != null AND isActive = true LIMIT 1].Id;*/
        
        List<Profile> portalProfiles = [Select Id from Profile where UserType = 'PowerCustomerSuccess' and Name like 'TSV%'];
        
        Id portalUserId = [SELECT Id FROM User WHERE ContactId != null AND isActive = true AND ProfileID IN :portalProfiles LIMIT 1].Id;
        


        system.runAs(TestDataFactory.getAdminUserForTests()){

            insert SharingHelper.addSharingRowsSobject(Asset__Share.sObjectType.getDescribe().getName(), 'Manual', testAsset.Id, portalUserId, 'Edit');

            test.startTest();
            //Create instance of SharingHelper Class
            SharingHelper SharingHelp = new SharingHelper(Asset__Share.sObjectType.getDescribe().getName());

            //add to deleted 
            SharingHelp.addToDeleteList(testAsset.Id);
            
            //delete
            SharingHelp.deleteSharingRules(new set<String> {Schema.Asset__Share.RowCause.SAR_Operator__c});

            test.stopTest();

            //Check no Sharing with row cause
            List<sobject> sobjSharing = SharingHelp.queryRowCauseSharing(new set<Id>{testAsset.Id}, new set<String>{Schema.Asset__Share.RowCause.SAR_Operator__c});
            List<Asset__Share> AssetSharing = (List<Asset__Share>) sobjSharing;
            system.assertEquals(0, AssetSharing.size());

            //Check there is still the manual sharing 
            List<sobject> sobjSharingManual = SharingHelp.querySharing(new set<Id>{testAsset.Id});
            List<Asset__Share> AssetSharingManual = (List<Asset__Share>) sobjSharingManual;
            system.assertEquals(2, AssetSharingManual.size());
        }
    }

    //Single Asset - create same apex sharing twice
    @isTest
    static void NegativeTest_AddingDuplicateSharingRule() {
        //insert Asset - no Account so no apex sharing created
        Asset__c testAsset = TestDataFactory.createAsset();
        insert testAsset;

        //Get the Partner Account Id
        Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;

        test.startTest();
        //Create instance of SharingHelper Class
        SharingHelper SharingHelp = new SharingHelper(Asset__Share.sObjectType.getDescribe().getName());

        //Add a sharing rule
        SharingHelp.addToSharing(portalAccountId, testAsset.Id, Schema.Asset__Share.RowCause.SAR_Operator__c, 'Edit');

        //Add a DUPLICATE sharing rule
        SharingHelp.addToSharing(portalAccountId, testAsset.Id, Schema.Asset__Share.RowCause.SAR_Operator__c, 'Edit');

        //Insert Sharing rules
        SharingHelp.insertApexSharing_Asyncronously();

        test.stopTest();

        //Check only one has been retained
        List<sobject> sobjSharing = SharingHelp.queryRowCauseSharing(new set<Id>{testAsset.Id}, new set<String>{Schema.Asset__Share.RowCause.SAR_Operator__c});
        List<Asset__Share> AssetSharing = (List<Asset__Share>) sobjSharing;
        system.assertEquals(1, AssetSharing.size());
    }


    @testSetup static void testSetup(){
        TestDataFactory.createPartnerAccount();
    }
    
}