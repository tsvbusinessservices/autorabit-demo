public class BusConfigWrapper {

    Bus_Config__mdt setting;


    static BusConfigWrapper instance = null;

    BusConfigWrapper (Bus_Config__mdt setting) {
        this.setting = setting;
    }

    public static BusConfigWrapper getConfig(String ConfigType) {

         {
            Bus_Config__mdt configSetting = [Select DeveloperName, Fee__c, Line_Description__c
                                                FROM Bus_Config__mdt
                                                WHERE DeveloperName =: ConfigType Limit 1];
            instance = new BusConfigWrapper(configSetting);
        }
        return instance;
    }
     
 
    public Decimal getFeeAmount() {
        return setting.Fee__c;
    }

    public String getFeeDesc() {
        return setting.Line_Description__c;
    }
    
}