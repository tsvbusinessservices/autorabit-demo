@isTest
public class MockChecksumGenerator implements ChecksumGenerator {

	String algo;
	
	public MockChecksumGenerator(String algo) {
		this.algo = algo;
	}

	public String getChecksum(Long source) {

		return  algo.left(2) + ('-' + source).right(2);

	}

}