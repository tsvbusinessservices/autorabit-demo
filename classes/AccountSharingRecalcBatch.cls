global class AccountSharingRecalcBatch implements Database.Batchable<sObject>, Schedulable {
	
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator([SELECT Id, AccountId, Full_Access__c FROM AccountContactRelation]);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		AccountContactRelationHandler.AccountSharing(scope, null, 'Recalculation');
	}
	
	global void finish(Database.BatchableContext BC) {
		//Send email
		String emailSubject = 'SUCCESS: Apex Sharing Recalculation For Accounts Completed';
		String emailBody = 'The Apex sharing recalculation finished processing for the Account Object';
		Utility.sendEmail(emailSubject, emailBody, 'Success');
	}

	global void execute(SchedulableContext sc) {
		AccountSharingRecalcBatch b = new AccountSharingRecalcBatch();
		database.executeBatch(b, 200);
	}
}