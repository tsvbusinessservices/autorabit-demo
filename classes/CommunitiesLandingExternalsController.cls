/**
 * An apex page controller that takes the user to the right start page based on credentials or lack thereof
 * Made for external user communities page
 */
public with sharing class CommunitiesLandingExternalsController {
    
    // Code we will invoke on page load.
    public PageReference forwardToStartPage() {    
        //return Network.communitiesLanding();
        PageReference pageRef = new PageReference(URL_Endpoints__c.getOrgDefaults().Site_Login_Callback__c);
        return pageRef;
    }
    
    public CommunitiesLandingExternalsController() {}
}