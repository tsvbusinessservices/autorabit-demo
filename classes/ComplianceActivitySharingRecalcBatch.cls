global class ComplianceActivitySharingRecalcBatch implements Database.Batchable<sObject>, Schedulable {
	
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator([SELECT Id FROM Compliance_Activity__c 
			WHERE RecordType.DeveloperName IN :ComplianceActivityHelper.SharedComplianceActivity.keySet()]);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		ComplianceActivitySharingHandler.complianceActivitySharing(scope, null, 'Recalculation');
	}
	
	global void finish(Database.BatchableContext BC) {
		//Send email
		String emailSubject = 'SUCCESS: Apex Sharing Recalculation For Compliance Activity Completed';
		String emailBody = 'The Apex sharing recalculation finished processing for the Compliance Activity Object';
		Utility.sendEmail(emailSubject, emailBody, 'Success');
	}

	global void execute(SchedulableContext sc) {
		ComplianceActivitySharingRecalcBatch b = new ComplianceActivitySharingRecalcBatch();
		database.executeBatch(b, 200);
	}
}