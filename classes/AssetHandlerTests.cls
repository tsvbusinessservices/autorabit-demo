@isTest
private class AssetHandlerTests {
/*
Test Ideas:

P - Create mix of record types and insert in all together
P - Change Record Type from SAR to Bus


*/
	//P - Create SAR Asset with Owner
	@isTest static void PositiveTest_CreateSARWithOwner() {
		//Get the Partner Account Id
		Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;

		test.startTest();

		//insert Asset - with owner
		Asset__c testAsset = TestDataFactory.createSARAsset();
		testAsset.Owner__c = portalAccountId;
		insert testAsset;

		test.stopTest();

		//check that there has been a sharing record created
		List<Asset__Share> AssetSharing = AssetHelper.getAssetSharing(new set<Id>{testAsset.Id}, new set<String>{Schema.Asset__Share.RowCause.SAR_Operator__c});
		system.assertEquals(1, AssetSharing.size());
	}

	//N - Create SAR Asset with no owner
	@isTest static void NegativeTest_CreateSARWithNoOwner() {
		test.startTest();

		//insert Asset - with owner
		Asset__c testAsset = TestDataFactory.createSARAsset();
		insert testAsset;

		test.stopTest();

		//check that there has been no sharing created
		List<Asset__Share> AssetSharing = AssetHelper.getAssetSharing(new set<Id>{testAsset.Id}, new set<String>{Schema.Asset__Share.RowCause.SAR_Operator__c});
		system.assertEquals(0, AssetSharing.size());
	}


	//P - Create SAR Asset with Owner
	@isTest static void PositiveTest_CreateBUSWithOwner() {
		//Get the Partner Account Id
		Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;

		test.startTest();

		//insert Asset - with owner
		Asset__c testAsset = TestDataFactory.createBus();
		testAsset.Owner__c = portalAccountId;
		insert testAsset;

		test.stopTest();

		//check that there has been a sharing record created
		List<Asset__Share> AssetSharing = AssetHelper.getAssetSharing(new set<Id>{testAsset.Id}, new set<String>{Schema.Asset__Share.RowCause.LBT__c});
		system.assertEquals(1, AssetSharing.size());
	}

	//N - Create SAR Asset with no owner
	@isTest static void NegativeTest_CreateBUSWithNoOwner() {
		test.startTest();

		//insert Asset - with no owner
		Asset__c testAsset = TestDataFactory.createBus();
		insert testAsset;

		test.stopTest();

		//check that there has been no sharing created
		List<Asset__Share> AssetSharing = AssetHelper.getAssetSharing(new set<Id>{testAsset.Id}, new set<String>{Schema.Asset__Share.RowCause.LBT__c});
		system.assertEquals(0, AssetSharing.size());
	}

	//N - Create ATON with Owner - no sharing created because ATONs don't get their sharing from the Owner field
	@isTest static void NegativeTest_ATONWithOwner() {
		//Get the Partner Account Id
		Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;

		test.startTest();

		//insert Asset - with owner
		Asset__c testAsset = TestDataFactory.createATONAsset();	//note ATON asset being created
		testAsset.Owner__c = portalAccountId;
		insert testAsset;

		test.stopTest();

		//check that there has NOT been a sharing record created
		List<Asset__Share> AssetSharing = AssetHelper.getAssetSharing(new set<Id>{testAsset.Id}, new set<String>{Schema.Asset__Share.RowCause.SAR_Operator__c});
		system.assertEquals(0, AssetSharing.size());
	}

	//P - Change Owner
	@isTest static void PositiveTest_ChangeSAROwner() {
		//Get the Partner Account Ids 
		Id portalAccountId1 = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;
		Id portalAccountId2 = [SELECT Id FROM Account WHERE Name = 'portalAccount2'].Id;

		//insert Asset - no owner
		Asset__c testAsset = TestDataFactory.createSARAsset();
		insert testAsset;

		test.startTest();

		//Create instance of SharingHelper Class
		SharingHelper SharingHelp = new SharingHelper(Asset__Share.sObjectType.getDescribe().getName());

		//Add a sharing rule
		SharingHelp.addToSharing(portalAccountId1, testAsset.Id, Schema.Asset__Share.RowCause.SAR_Operator__c, 'Edit');

		//Insert Sharing rules
		SharingHelp.insertApexSharing_Syncronously();

		//check that there has been a sharing record created
		List<Asset__Share> AssetSharing = AssetHelper.getAssetSharing(new set<Id>{testAsset.Id}, new set<String>{Schema.Asset__Share.RowCause.SAR_Operator__c});
		system.assertEquals(1, AssetSharing.size());

		list<Group> Groups = Utility.getGroup(new set<Id>{AssetSharing[0].UserOrGroupId});
		system.assertEquals(1, Groups.size());
		system.assertEquals('portalAccount1CustomerUser', Groups[0].DeveloperName);
		//system.assertEquals(true, Groups[0].DeveloperName.startsWith('portalAccount1'));

		//change owner
		testAsset.Owner__c = portalAccountId2;
		update testAsset;

		test.stopTest();

		//check that the sharing record has been changed
		List<Asset__Share> AssetSharing2 = AssetHelper.getAssetSharing(new set<Id>{testAsset.Id}, new set<String>{Schema.Asset__Share.RowCause.SAR_Operator__c});
		system.assertEquals(1, AssetSharing2.size());

		list<Group> Groups2 = Utility.getGroup(new set<Id>{AssetSharing2[0].UserOrGroupId});
		system.assertEquals(1, Groups2.size());
		system.assertEquals('portalAccount2CustomerUser', Groups2[0].DeveloperName);


	}

	//P - Create ATON with Location
	@isTest static void PositiveTest_CreateATON() {
		Id portalAccountId1 = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;

		Location__c loc = TestDataFactory.createLocation(portalAccountId1);
		insert loc;

		test.startTest();

		//insert ATON with location
		Asset__c testATON = TestDataFactory.createATONAsset();
		testATON.Location__c = loc.Id;
		insert testATON;

		test.stopTest();

		//check that there has been a sharing record created
		List<Asset__Share> AssetSharing = AssetHelper.getAssetSharing(new set<Id>{testATON.Id}, new set<String>{Schema.Asset__Share.RowCause.Waterway_Manager__c});
		system.assertEquals(1, AssetSharing.size());

	}

	


	//P - Insert Multiple
	@isTest static void PositiveTest_InsertMultiple() {
		Id portalAccountId1 = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;
		Location__c loc1 = TestDataFactory.createLocation(portalAccountId1);
		insert loc1;


		test.startTest();
		
		//insert ATONs with location
		list<Asset__c> Assets = TestDataFactory.createAssets(200, AssetHelper.AssetRTMap.get('ATON'));
		for(Asset__c A : Assets){
			A.Location__c = loc1.Id;
		}
		
		insert Assets;

		map<Id, Asset__c> AssetMap = new map<Id, Asset__c>(Assets);

		test.stopTest();

		//check that there has been a sharing record created
		List<Asset__Share> AssetSharing = AssetHelper.getAssetSharing(AssetMap.keySet(), new set<String>{Schema.Asset__Share.RowCause.Waterway_Manager__c});
		system.assertEquals(200, AssetSharing.size());
	}


	@testSetup static void testSetup(){
		TestDataFactory.createPartnerAccount();
	}


	
}