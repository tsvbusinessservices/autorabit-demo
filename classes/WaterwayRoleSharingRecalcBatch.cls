global class WaterwayRoleSharingRecalcBatch implements Database.Batchable<sObject>, Schedulable {
    global WaterwayRoleSharingRecalcBatch() {
         
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(WaterwayRoleHelper.getWaterwayRoleForSharingRecalc());
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        WaterwayRoleHandler.waterwayRoleSharing(scope, null, 'Recalculation');
    }
    
    global void finish(Database.BatchableContext BC) {
        //Send email
        String emailSubject = 'SUCCESS: Apex Sharing Recalculation For Waterway Role Completed';
        String emailBody = 'The Apex sharing recalculation finished processing for the Waterway Role Object';
        Utility.sendEmail(emailSubject, emailBody, 'Success');
    }

    global void execute(SchedulableContext sc) {
        WaterwayRoleSharingRecalcBatch b = new WaterwayRoleSharingRecalcBatch();
        database.executeBatch(b, 200);
    }
    
}