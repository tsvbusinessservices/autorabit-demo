/**
 * AssetHandler class. creates Apex sharing rules for the Assets
 *
 * @author Ricky Lowe
 * @date 29.May.2018
 */


public with sharing class AssetHandler extends TriggerBase implements ITriggerBase {
	public AssetHandler() {
		
	}
	/**
	 * afterInsert Trigger Event
	 * @return null
	 */
	public override void afterInsert() {
		assetSharing(records, null, 'Insert');
	}
	/**
	 * afterUpdate Trigger Event	
	 * @return null
	 */
	public override void afterUpdate() {
		assetSharing(records, oldmap, 'Update');
	}

	/**
	 * Controlls the Apex sharing against the Asset Object	
	 * @param mode = 'Insert', 'Update', 'Recalculation'
	 * @return null
	 */
	public static void assetSharing(list<SObject> records, Map<Id, SObject> oldMap, String mode){
		//Get an instance of the SharingHelper class with the Object Name
		SharingHelper SharingHelp = new SharingHelper(Asset__Share.sObjectType.getDescribe().getName());

		//get a set of asset Ids by casting to map and getting keys
		set<Id> theAssetIds = new map<Id, Asset__c>((list<Asset__c>) records).keySet();

		//loop round and add the Id and the Portal account Id
		for(Asset__c Asset : AssetHelper.getEnrichedAssetData(theAssetIds).values()){
			
			//get old Asset - only applicable if running in Update mode
			Asset__c oldAsset = null;
			if(mode == 'Update' && oldMap != null){
				oldAsset = (Asset__c) oldMap.get(Asset.Id);
			}

			//if SAR Asset 
			if(Asset.RecordTypeId == AssetHelper.AssetRTMap.get(Constants.SAR_ASSET)){
				//if new or changed or Recalculate then add the new Account to the sharing
				if(mode == 'Insert' || mode == 'Recalculation' || Asset.owner__c != oldAsset.owner__c){
					SharingHelp.addToSharing(Asset.owner__c, Asset.Id, Schema.Asset__Share.RowCause.SAR_Operator__c, 'Edit');

					//if changed OR Recalculation mode than delete the old one
					if(mode == 'Recalculation' || (mode == 'Update' && Asset.owner__c != oldAsset.owner__c)){
						SharingHelp.addToDeleteList(Asset.Id);
					}
				}
			}
			//if ATON 
			else if(Asset.RecordTypeId == AssetHelper.AssetRTMap.get(Constants.SAR_ATON)){
				//because the Account lookup is on a related object we can't check for a change - therefore we will calc sharing on Insert and Recalculation
				if(mode == 'Insert' || mode == 'Recalculation' ){
					SharingHelp.addToSharing(Asset.Location__r.Manager__c, Asset.Id, Schema.Asset__Share.RowCause.Waterway_Manager__c, 'Edit');

					//if Recalculation mode than delete the sharing
					if(mode == 'Recalculation'){
						SharingHelp.addToDeleteList(Asset.Id);
					}
				}
			}
			//If Bus
			else if(Asset.RecordTypeId == AssetHelper.AssetRTMap.get(Constants.SAR_BUS)){
				//if new or changed or Recalculate then add the new Account to the sharing
				if(mode == 'Insert' || mode == 'Recalculation' || Asset.owner__c != oldAsset.owner__c){
					SharingHelp.addToSharing(Asset.owner__c, Asset.Id, Schema.Asset__Share.RowCause.LBT__c, 'Edit');

					//if changed OR Recalculation mode than delete the old one
					if(mode == 'Recalculation' || (mode == 'Update' && Asset.owner__c != oldAsset.owner__c)){
						SharingHelp.addToDeleteList(Asset.Id);
					}
				}
			}
		}

		//delete any sharing rules that need to be deleted
		SharingHelp.deleteSharingRules(new set<String> {Schema.Asset__Share.RowCause.SAR_Operator__c, Schema.Asset__Share.RowCause.Waterway_Manager__c, Schema.Asset__Share.RowCause.LBT__c});

		//Insert Sharing rules asyncronously
		SharingHelp.insertApexSharing_Asyncronously();
	}

}