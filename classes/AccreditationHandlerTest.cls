@isTest
private class AccreditationHandlerTest {
	
	static testMethod void beforeInsertHandlerTest() {
		Account acc = new Account();
		acc.Name = 'Test Account';
		insert acc;

		Id moRTId = Schema.SObjectType.Accreditation__c.getRecordTypeInfosByName().get('Maritime Operator').getRecordTypeId();
		Test.startTest();
			Accreditation__c obj = new Accreditation__c();
			obj.RecordTypeId = moRTId;
			obj.Account__c = acc.Id;
			obj.Type__c = 'Local Knowledge Certificate';
			insert obj;
			System.assertNotEquals(null,obj.Id);
		Test.stopTest();
		//GEt MDT
		Accreditation_Setting__mdt mdt = [SELECT Endorsement_Header__c, Footer__c, Label, 
			MasterLabel, Preamble__c, Details__c 
			FROM Accreditation_Setting__mdt 
			WHERE MasterLabel = 'Local Knowledge Certificate' LIMIT 1];
		//Match	
		for(Accreditation__c tobj :[SELECT Type__c,Endorsement_Header__c,Preamble__c,Footer__c FROM Accreditation__c WHERE Type__c = 'Local Knowledge Certificate']){
			System.assertEquals(mdt.Endorsement_Header__c,tobj.Endorsement_Header__c);
			System.assertEquals(mdt.Preamble__c,tobj.Preamble__c);
			System.assertEquals(mdt.Footer__c,tobj.Footer__c);
		}
	}
	
}