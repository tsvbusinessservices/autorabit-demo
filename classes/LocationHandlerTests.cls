@isTest
private class LocationHandlerTests {
	
	@isTest static void test_method_one() {
		// Implement test code
	}
	
	//P - Create Location with manager
	@isTest static void PositiveTest_CreateSARWithOwner() {
		//Get the Partner Account Id
		Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount1'].Id;

		test.startTest();

		Location__c loc1 = TestDataFactory.createLocation(portalAccountId);
		insert loc1;

		test.stopTest();

		//check that there has been a sharing record created
		List<Location__Share> locSharing = LocationHelper.getLocSharing(new set<Id>{loc1.Id}, new set<String>{Location__Share.RowCause.Waterway_Manager__c});
		system.assertEquals(1, locSharing.size());
	}

	//P - Create Location with manager
	@isTest static void PositiveTest_CreateFacilityManagerSharing() {
		//Get the Partner Account Id
		Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount2'].Id;

		test.startTest();

		Location__c loc1 = TestDataFactory.createLocation(portalAccountId, Constants.LOCATION_RT_ID_FACILITY);
		insert loc1;

		test.stopTest();

		//check that there has been a sharing record created
		List<Location__Share> locSharing = LocationHelper.getLocSharing(new set<Id>{loc1.Id}, new set<String>{Location__Share.RowCause.Facility_Manager__c});
		system.assertEquals(1, locSharing.size());
	}

	//P - Create Location with manager
	@isTest static void PositiveTest_UpdatingFacilityManagerSharing() {
		//Get the Partner Account Id
		Id portalAccountId = [SELECT Id FROM Account WHERE Name = 'portalAccount2'].Id;
		
		Location__c loc1 = TestDataFactory.createLocation(portalAccountId, Constants.LOCATION_RT_ID_FACILITY);
		insert loc1;

		test.startTest();

		loc1.Manager__c = null;
		update loc1;

		test.stopTest();

		//check that there has been a sharing record created
		List<Location__Share> locSharing = LocationHelper.getLocSharing(new set<Id>{loc1.Id}, new set<String>{Location__Share.RowCause.Facility_Manager__c});
		system.assertEquals(0, locSharing.size());
	}

	//P - Create Location with manager
	@isTest static void PositiveTest_CreateParentWaterwayManagerSharing() {
		//Get the Partner Account Id
		Id portalAccountId1 = [SELECT Id FROM Account WHERE Name = 'portalAccount3'].Id;
		Id portalAccountId2 = [SELECT Id FROM Account WHERE Name = 'portalAccount4'].Id;

		Location__c waterway = TestDataFactory.createLocation(portalAccountId1, Constants.LOCATION_RT_ID_WATERWAY);
		insert waterway;
		
        Waterway_Role__c wwr1 = TestDataFactory.createWaterwayRole(portalAccountId1, waterway.id);
        insert wwr1;

        Waterway_Role__c wwr2 = TestDataFactory.createWaterwayRole(portalAccountId2, waterway.id);
        insert wwr2;

		test.startTest();

		Location__c waterway2  = TestDataFactory.createLocation(portalAccountId2, Constants.LOCATION_RT_ID_WATERWAY);
		waterway2.Location__c = waterway.id;
		insert waterway2;

		test.stopTest();

		//check that there has been a sharing record created
		List<Location__Share> locSharing = LocationHelper.getLocSharing(new set<Id>{waterway2.Id}, new set<String>{Location__Share.RowCause.Parent_Waterway_Manager__c});
		system.assertEquals(2, locSharing.size());
	}

	@testSetup static void testSetup(){
		TestDataFactory.createPartnerAccount(new List<Id>{null,Constants.ACCOUNT_RT_ID_MARITIME,Constants.ACCOUNT_RT_ID_MARITIME,Constants.ACCOUNT_RT_ID_MARITIME},new List<String>{null,Constants.ACCOUNT_TYPE_FACILITY_MANAGER,Constants.ACCOUNT_TYPE_WATERWAY_MANAGER,Constants.ACCOUNT_TYPE_WATERWAY_MANAGER});
	}
	
}