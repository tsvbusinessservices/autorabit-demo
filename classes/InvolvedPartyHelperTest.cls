@isTest
public class InvolvedPartyHelperTest {

    @isTest
    static void testRecordTypes() {
        Involved_Party__c vesselInspectionIP = TestDataFactory.createInvolvedPersonForVesselInspection();
        Involved_Party__c marineIncidentIP = TestDataFactory.createInvolvedPersonForMarineIncident();
        Involved_Party__c busIncidentIP = TestDataFactory.createInvolvedPersonForBusIncident();

        System.assertEquals(true, InvolvedPartyHelper.isVesselInspectionRecordType(vesselInspectionIP), 'Not a Vessel Inspection IP Record');
        System.assertEquals(true, InvolvedPartyHelper.isMarineIncidentRecordType(marineIncidentIP), 'Not a Marine Incident IP Record');
        System.assertEquals(true, InvolvedPartyHelper.isMarineRecordType(vesselInspectionIP), 'Not a Marine IP Record (Vessel Inspection)');
        System.assertEquals(true, InvolvedPartyHelper.isMarineRecordType(marineIncidentIP), 'Not a Marine IP Record (Marine Incident)');
        System.assertEquals(false, InvolvedPartyHelper.isMarineRecordType(busIncidentIP), 'Marine Incident Record is not expected');
    }

    @isTest
    static void testCreatePersonAccount() {
        Involved_Party__c involvedPerson = TestDataFactory.createInvolvedPersonForVesselInspection();

        involvedPerson.First_Name__c = 'TestFirstName';
        involvedPerson.Last_Name__c = 'TestLastName';
        involvedPerson.Birthdate__c = Date.newInstance(1980, 10, 23);
        involvedPerson.Gender__c = 'Male';
        involvedPerson.Street__c = 'Test Street';
        involvedPerson.Suburb__c = 'Test Suburb';
        involvedPerson.State__c = 'VIC';
        involvedPerson.Postcode__c = '2089';

        Account personAc = InvolvedPartyHelper.createPersonFromInvolvedPerson(involvedPerson);

        Id personAccountRTId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();

        System.assertEquals(personAccountRTId, personAc.recordTypeId);
        System.assertEquals('TestFirstName', personAc.FirstName);
        System.assertEquals('TestLastName', personAc.LastName);
        System.assertEquals('Male', personAc.Gender__c);
        System.assertEquals(Date.newInstance(1980, 10, 23), personAc.PersonBirthdate);
        System.assertEquals('Test Street', personAc.BillingStreet);
        System.assertEquals('Test Suburb', personAc.BillingCity);
        System.assertEquals('VIC', personAc.BillingState);
        System.assertEquals('2089', personAc.BillingPostalCode);

    }

    @isTest
    static void testCreateLicence() {
        Involved_Party__c involvedPerson = TestDataFactory.createInvolvedPersonForVesselInspection();

        Account personAc = TestDataFactory.createPersonAccount();
        insert personAc;

        involvedPerson.Person__c = personAc.Id;
        involvedPerson.Licence_Number__c = 'TEST12345';
        involvedPerson.Licence_Expiry_Date__c = Date.newInstance(2019, 10, 23);
        involvedPerson.Licence_Status__c = 'Current';
        involvedPerson.Licence_Type__c = 'General';
        involvedPerson.License_Origin__c = 'VIC';
        involvedPerson.License_Category__c = 'Recreational';
        involvedPerson.Endorsement_Type__c = 'PWC Endorsed';

        Accreditation__c accr = InvolvedPartyHelper.createLicenceFromInvolvedPerson(involvedPerson);
        
        System.assertEquals('TEST12345', accr.VR_Cert_Id__c);
        System.assertEquals(Date.newInstance(2019, 10, 23), accr.Expiry_Date__c);
        System.assertEquals('Current', accr.Status__c);
        System.assertEquals('General', accr.Type__c);
        System.assertEquals('VIC', accr.State__c);
        System.assertEquals('PWC Endorsed', accr.Endorsement_Type__c);

        involvedPerson.License_Origin__c = 'NSW';
        accr = InvolvedPartyHelper.createLicenceFromInvolvedPerson(involvedPerson);
        System.assertEquals('TEST12345', accr.Licence_Number__c);
        System.assertEquals(Date.newInstance(2019, 10, 23), accr.Expiry_Date__c);
        System.assertEquals('Current', accr.Status__c);
        System.assertEquals('General', accr.Type__c);
        System.assertEquals('Recreational', accr.License_Category__c);
        System.assertEquals('NSW', accr.State__c);
        System.assertEquals('PWC Endorsed', accr.Endorsement_Type__c);

    }
}