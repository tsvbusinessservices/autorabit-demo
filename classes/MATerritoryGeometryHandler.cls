public with sharing class MATerritoryGeometryHandler extends TriggerBase implements ITriggerBase {
	
	public MATerritoryGeometryHandler() {

	}

	/**
	 * beforeInsert Trigger Event
	 * @return null
	 */
	public override void beforeInsert() {
		List<maps__ShapeLayerGeometry__c> newRecords = getRecords();
		calculateBounds(newRecords);
	}

	/**
	 * beforeUpdate Trigger Event
	 * @return null
	 */
	public override void beforeUpdate() {
		List<maps__ShapeLayerGeometry__c> newRecords = getRecords();
		System.debug('Calculating bounds on ::' + newRecords);
		if(newRecords.size() > 0)
			calculateBounds(newRecords);
	}

	private List<maps__ShapeLayerGeometry__c> getRecords() {
		// initialized the results list
		List<maps__ShapeLayerGeometry__c> results = new List<maps__ShapeLayerGeometry__c>();

		//iterate on the Trigegr records 
		for(maps__ShapeLayerGeometry__c record: (List<maps__ShapeLayerGeometry__c>)records) {

			// add to the list if Geometry is populated on new record
			if(getContext() == 'BEFORE_INSERT' 
				&& record.maps__Geometry__c != null && record.maps__Geometry__c <> '') {
				results.add(record);
			} else if(getContext() == 'BEFORE_UPDATE'
				&& record.maps__Geometry__c != ((maps__ShapeLayerGeometry__c)oldmap.get(record.Id)).maps__Geometry__c) {
				//add to list if the Geometry is updated
				results.add(record);
			}
		}
		return results;
	}

	public List<maps__ShapeLayerGeometry__c> calculateBounds(List<maps__ShapeLayerGeometry__c> newRecords) {

		// Calculate bounds for all records
		for(maps__ShapeLayerGeometry__c record: newRecords) {
			calculateBounds(record);
		}

		return newRecords;
	}


	public maps__ShapeLayerGeometry__c calculateBounds(maps__ShapeLayerGeometry__c record) {

		// reset Min Max fields if geometry is reset
		if(record.maps__Geometry__c == null) {
			setValues(record, null, null, null, null);
			return record;
		} 

		// convert JSON to 
		PolygonPoints polypoints;
		try {
			polypoints = (PolygonPoints)JSON.deserialize(record.maps__Geometry__c, PolygonPoints.class);
		} catch(Exception e) {
			// Reset the bounds if JSON is non parsable
			System.debug('Error converting the JSON. reseting bounds');
			setValues(record, null, null, null, null);
			return record;
		}
		
		Decimal minX = null;
		Decimal minY = null;
		Decimal maxX = null;
		Decimal maxY = null;

		for(Point pt: polypoints.points) {
			// Set minX
			if(minX == null || minX > pt.lat) {
				minX = pt.lat;
			}

			//Set maxX
			if(maxX == null || maxX < pt.lat) {
				maxX = pt.lat;
			}

			//Set minY
			if(minY == null || minY > pt.lng) {
				minY = pt.lng;
			}

			//Set maxY
			if(maxY == null || maxY < pt.lng) {
				maxY = pt.lng;
			}
		}

		setValues(record,minX, maxX, minY, maxY);


		return record;
	}

	private void setValues(maps__ShapeLayerGeometry__c record, Decimal minX, Decimal maxX, Decimal minY, Decimal maxY) {
		system.debug('record==>'+record);
		record.Max_X__c = maxX;
		record.Max_Y__c = maxY;
		record.Min_X__c = minX;
		record.Min_Y__c = minY;
		system.debug('record==>'+record);
	}
}